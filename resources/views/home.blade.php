@extends('backend.layouts.app')


@section('title')
Home
@endsection

@section('css')

    <!-- chartist CSS -->
    <link href="{{ asset('backend/assets/plugins/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/assets/plugins/chartist-js/dist/chartist-init.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="{{ asset('backend/assets/plugins/c3-master/c3.min.css') }}" rel="stylesheet">

@endsection
@section('js')

    <!-- chartist chart -->
    <script src="{{ asset('backend/assets/plugins/chartist-js/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') }}"></script>
    <!--c3 JavaScript -->
    <script src="{{ asset('backend/assets/plugins/d3/d3.min.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/c3-master/c3.min.js') }}"></script>
    <!-- Chart JS -->
    <script src="{{ asset('backend/js/dashboard1.js') }}"></script>
@stop

@section('content-header')

                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>
@endsection

@section('content')
            @if(Auth::user()->hasRole(['admin','superadmin','cs']))
                <!-- Row -->
                    <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>{{$total_order_new}}</h3>
                                    <h6 class="card-subtitle">Order Baru</h6></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>{{$total_order_process}}</h3>
                                    <h6 class="card-subtitle">Order diproses</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>{{$total_order}}</h3>
                                    <h6 class="card-subtitle">Total Order</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>{{$total_rent}}</h3>
                                    <h6 class="card-subtitle">Sertifikat Disewa</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                <div class="row">

                    <div class="col-lg-7 col-xlg-7 col-md-7">

                        <div class="card">
                            <div class="card-body">
                                            <div class="table-responsive m-t-40">
                                                <table class="table stylish-table">
                                                    <thead>
                                                        <tr>
                                                            <th width="25%">User</th>
                                                            <th width="25%">Nomor Order</th>
                                                            <th width="15%" class="text-center">Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if($order_process->count()==0)
                                                        <tr>
                                                            <td colspan="4">
                                                                Tidak ada data Order
                                                            </td>
                                                        </tr>
                                                        @endif
                                                        @foreach ($order_process as $order)
                                                        <tr>
                                                            <td>
                                                                <h6>{{$order->username}}</h6></td>
                                                            <td><a href="/order-invoice/{{ $order->invoice}}">{{ $order->invoice }}</a></td>
                                                            <td class="text-center">

                                                                @if($order->status==0)
                                                                <span class="label label-light-warning">Menunggu konfirmasi admin</span>
                                                                @elseif($order->status==1)
                                                                <span class="label label-light-warning">Menunggu pembayaran soft copy</span>
                                                                @elseif($order->status==2)
                                                                <span class="label label-light-success">Soft copy sedang diproses</span>
                                                                @elseif($order->status==3)
                                                                <span class="label label-light-info">Soft copy sudah dikirim</span>
                                                                @elseif($order->status==4)
                                                                <span class="label label-light-warning">Menunggu konfirmasi hard copy</span>
                                                                @elseif($order->status==5)
                                                                <span class="label label-light-info">Menunggu pembayaran hard copy</span>
                                                                @elseif($order->status==6)
                                                                <span class="label label-light-success">Hard copy diproses</span>
                                                                @elseif($order->status==7)
                                                                <span class="label label-light-info">Hard copy sudah diterima</span>
                                                                @elseif($order->status==8)
                                                                <span class="label label-light-danger">Sewa dibatalkan</span>
                                                                @else
                                                                <span class="label label-light-danger">Masa sewa habis</span>
                                                                @endif
                                                                </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-lg-5 col-xlg-5 col-md-5">
                        <!-- Card -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Aktifitas Terakhir</h4>
                            </div>
                            <!-- ============================================================== -->
                            <!-- Comment widgets -->
                            <!-- ============================================================== -->
                            <div class="comment-widgets">
                                @if($logs->count()==0)
                                <!-- Comment Row -->
                                <div class="d-flex flex-row comment-row">
                                    <div class="comment-text w-100">
                                        <h4 class="text-center">Tidak ada aktifitas</h4>
                                        <p class="m-b-5">Tidak ada aktifitas terakhir.</p>
                                    </div>
                                </div>
                                @endif
                                @foreach ($logs as $log)
                                    <!-- Comment Row -->
                                    <div class="d-flex flex-row comment-row">
                                        <div class="comment-text">
                                            <h6>{{$log->subject}}</h6>
                                            <div class="comment-footer">
                                                <span class="text-muted pull-right">{{date_format($log->created_at,"j F Y G:H") }}</span>
                                            </div>
                                        </div>
                                    </div>                        
                                @endforeach
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            @else
            <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>{{$total_order_new}}</h3>
                                    <h6 class="card-subtitle">Order Baru</h6></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>{{$total_order}}</h3>
                                    <h6 class="card-subtitle">Semua Order</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>{{$total_rent_process}}</h3>
                                    <h6 class="card-subtitle">Sertifikat diproses</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>{{$total_rent_available}}</h3>
                                    <h6 class="card-subtitle">Sertifikat berlaku</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="row">
            <h1>Sertifikat Yang Anda Miliki </h1>
            @if(count($rent_have)==0)
            <div class="col-sm-12">
                        <div class="card text-center">
                            <div class="card-body">
                                <h4 class="card-title">Tidak ada sertifikat aktif </h4>
                                <p class="card-text">Anda tidak mempunyai sertifikat yang aktif, harap melakukan permohonan sewa untuk menyewa sertifikat sesuai keahlian/keterampilan anda </p>
                                <a href="/certificate/request" class="btn btn-info">Sewa</a>
                            </div>
                        </div>
                
            </div>
            @else
            <div class="card-columns">
                @foreach($rent_have as $rent)
                <div class="">
                        <div class="card blog-widget">
                            <div class="card-body">
                                <h3>{{$rent->certificate_classification_code." - ". $rent->certificate_classification}} </h3>
                                @if($rent->certificate_type=="SKA")
                                                    <label class="label label-rounded label-info">
                                                @else
                                                    <label class="label label-rounded label-warning">
                                                @endif
                                {{ $rent->certificate_type." - ".$rent->request_qualification }}
                                </label>
                                <p class="m-t-20 m-b-20">
                                Status: <b>
                                                                @if($rent->request_status==0)
                                                                <span class="text-warning">Menunggu konfirmasi admin</span>
                                                                @elseif($rent->request_status==1)
                                                                <span class="text-warning">Menunggu pembayaran soft copy</span>
                                                                @elseif($rent->request_status==2)
                                                                <span class="text-success">Soft copy sedang diproses</span>
                                                                @elseif($rent->request_status==3)
                                                                <span class="text-info">Soft copy sudah dikirim</span>
                                                                @elseif($rent->request_status==4)
                                                                <span class="text-warning">Menunggu konfirmasi hard copy</span>
                                                                @elseif($rent->request_status==5)
                                                                <span class="text-warning">Menunggu pembayaran hard copy</span>
                                                                @elseif($rent->request_status==6)
                                                                <span class="text-success">Hard copy sedang diproses</span>
                                                                @elseif($rent->request_status==7)
                                                                <span class="text-info">Hard copy sudah diterima</span>
                                                                @elseif($rent->request_status==8)
                                                                <span class="text-danger">Sewa dibatalkan</span>
                                                                @else
                                                                <span class="text-danger">Masa sewa habis</span>
                                                                @endif
                                                            </b> 
                                </p>
                                <div class="float-right">
                                   <a href="/certificate/code/{{$rent->certificate_classification_code}}" class="btn btn-info">Selengkapnya</a>
                                <hr style="border-top: 0px;">
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
                

            @endif
            </div>
            <div class="row">
            <h1>Sertifikat Sedang Diproses Admin </h1>

            @if(count($rent_process)==0)
            <div class="col-sm-12">
                        <div class="card text-center">
                            <div class="card-body">
                                <h4 class="card-title">Tidak ada sertifikat yang diproses</h4>
                                <p class="card-text">Anda tidak mempunyai sertifikat yang sedang diproses, harap melakukan permohonan sewa untuk menyewa sertifikat sesuai keahlian/keterampilan anda </p>
                                <a href="/certificate/request" class="btn btn-info">Sewa</a>
                            </div>
                        </div>
                
            </div>
            @else
            <div class="card-columns">
                @foreach($rent_process as $rent)
               <div class="">
                        <div class="card blog-widget">
                            <div class="card-body">
                                <h3>{{$rent->certificate_classification_code." - ". $rent->certificate_classification}} </h3>
                                @if($rent->certificate_type=="SKA")
                                                    <label class="label label-rounded label-info">
                                                @else
                                                    <label class="label label-rounded label-warning">
                                                @endif
                                {{ $rent->certificate_type." - ".$rent->request_qualification }}
                                </label>
                                <p class="m-t-20 m-b-20">
                                Status: <b>
                                                                @if($rent->request_status==0)
                                                                <span class="text-warning">Menunggu konfirmasi admin</span>
                                                                @elseif($rent->request_status==1)
                                                                <span class="text-warning">Menunggu pembayaran soft copy</span>
                                                                @elseif($rent->request_status==2)
                                                                <span class="text-success">Soft copy sedang diproses</span>
                                                                @elseif($rent->request_status==3)
                                                                <span class="text-info">Soft copy sudah dikirim</span>
                                                                @elseif($rent->request_status==4)
                                                                <span class="text-warning">Menunggu konfirmasi hard copy</span>
                                                                @elseif($rent->request_status==5)
                                                                <span class="text-warning">Menunggu pembayaran hard copy</span>
                                                                @elseif($rent->request_status==6)
                                                                <span class="text-success">Hard copy sedang diproses</span>
                                                                @elseif($rent->request_status==7)
                                                                <span class="text-info">Hard copy sudah diterima</span>
                                                                @elseif($rent->request_status==8)
                                                                <span class="text-danger">Sewa dibatalkan</span>
                                                                @else
                                                                <span class="text-danger">Masa sewa habis</span>
                                                                @endif
                                                            </b> 
                                </p>
                                <div class="float-right">
                                   <a href="/certificate/code/{{$rent->certificate_classification_code}}" class="btn btn-info">Selengkapnya</a>
                                <hr style="border-top: 0px;">
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @endif
            </div>
            <div class="row">
            <h1>Sertifikat Yang Pernah Anda Sewa </h1>

            @if(count($rent_history)==0)
            <div class="col-sm-12">
                        <div class="card text-center">
                            <div class="card-body">
                                <h4 class="card-title">Tidak Riwayat Penyewaan </h4>
                                <p class="card-text">Anda tidak mempunyai riwayat penyewaan sertifikat, harap melakukan permohonan sewa untuk menyewa sertifikat sesuai keahlian/keterampilan anda </p>
                                <a href="/certificate/request" class="btn btn-info">Sewa</a>
                            </div>
                        </div>
                
            </div>
            @else
            <div class="card-columns">
                @foreach($rent_history as $rent)
                <div class="">
                        <div class="card blog-widget">
                            <div class="card-body">
                                <h3>{{$rent->certificate_classification_code." - ". $rent->certificate_classification}} </h3>
                                @if($rent->certificate_type=="SKA")
                                                    <label class="label label-rounded label-info">
                                                @else
                                                    <label class="label label-rounded label-warning">
                                                @endif
                                {{ $rent->certificate_type." - ".$rent->request_qualification }}
                                </label>
                                <p class="m-t-20 m-b-20">
                                Status: <b>
                                                                @if($rent->request_status==0)
                                                                <span class="text-warning">Menunggu konfirmasi admin</span>
                                                                @elseif($rent->request_status==1)
                                                                <span class="text-warning">Menunggu pembayaran soft copy</span>
                                                                @elseif($rent->request_status==2)
                                                                <span class="text-success">Soft copy sedang diproses</span>
                                                                @elseif($rent->request_status==3)
                                                                <span class="text-info">Soft copy sudah dikirim</span>
                                                                @elseif($rent->request_status==4)
                                                                <span class="text-warning">Menunggu konfirmasi hard copy</span>
                                                                @elseif($rent->request_status==5)
                                                                <span class="text-warning">Menunggu pembayaran hard copy</span>
                                                                @elseif($rent->request_status==6)
                                                                <span class="text-success">Hard copy sedang diproses</span>
                                                                @elseif($rent->request_status==7)
                                                                <span class="text-info">Hard copy sudah diterima</span>
                                                                @elseif($rent->request_status==8)
                                                                <span class="text-danger">Sewa dibatalkan</span>
                                                                @else
                                                                <span class="text-danger">Masa sewa habis</span>
                                                                @endif
                                                            </b> 
                                </p>
                                <div class="float-right">
                                   <a href="/certificate/code/{{$rent->certificate_classification_code}}" class="btn btn-info">Selengkapnya</a>
                                <hr style="border-top: 0px;">
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @endif
            </div>

            @endif
@endsection
