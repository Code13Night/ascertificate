@extends('backend.layouts.app')


@section('title')
Riwayat Sewa
@endsection

@section('css')


@endsection
@section('js')

    <!-- This is data table -->
    <script src="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="{{ asset('backend/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        function formatDate(date) {
          var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "June", "July",
            "Aug", "Sep", "Oct",
            "Nov", "Dec"
          ];

          var day = date.getDate();
          var monthIndex = date.getMonth();
          var year = date.getFullYear();

          return day + ' ' + monthNames[monthIndex] + ' ' + year;
        }
        var table=$('#myTable').DataTable({iDisplayLength: 10,
            responsive: true,
            lengthMenu: [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "order": [
                [0, 'asc']
            ],
            "ajax": '/webApi/allrequestbystatus-all',
            "columns": [
                    { "data": "invoice" },
                    { "data": "certificate_classification" },
                    { "data": "certificate_type" },
                    { "data": "request_qualification" },
                    { "data": "request_status" },
            ], "columnDefs": [{
                "targets": 4,
                className: "text-center",
                "render": function ( data, type, row ) {
                    datax=data.split(",");
                    stringinside='';
                    $(datax).each(function (indexxx,itemxx) {
                        switch(parseInt(itemxx)){
                            case 0:
                                stringinside=stringinside+ '<span class="label label-warning">Menunggu konfirmasi admin</span>';
                                break;
                            case 1:
                                stringinside=stringinside+ '<span class="label label-warning">Menunggu Pembayaran</span>';
                                break;
                            case 2:
                                stringinside=stringinside+ '<span class="label label-success">Soft copy sedang diproses</span>';
                                break;
                            case 3:
                                stringinside=stringinside+ '<span class="label label-info">Soft copy sudah dikirim</span>';
                                break;
                            case 4:
                                stringinside=stringinside+ '<span class="label label-warning">Menunggu konfirmasi hard copy</span>';
                                break;
                            case 5:
                                stringinside=stringinside+ '<span class="label label-warning">Menunggu pembayaran soft copy</span>';
                                break;
                            case 6:
                                stringinside=stringinside+ '<span class="label label-success">Hard copy sedang diproses</span>';
                                break;
                            case 7:
                                stringinside=stringinside+ '<span class="label label-info">Hard copy sudah diteriman</span>';
                                break;
                            case 8:
                                stringinside=stringinside+ '<span class="label label-danger">Sewa dibatalkan</span>';
                                break;
                            default:
                                stringinside=stringinside+ '<span class="label label-danger">Masa sewa habis</span>';
                                break;
                        }
                        if(indexxx!=(datax.length-1)){
                            stringinside=stringinside+"<br\>";
                        }
                    });
                    return stringinside;
                }
            },{
                "targets": 0,
                className: "text-center",
                "render": function ( data, type, row ) {
                    
                    return '<a href="/order-invoice/'+data+'"  id=' + data + ' class="btnshowSerialNumber" >'+data+'</a>';
                }
            },{
                "targets": 1,
                "render": function ( data, type, row ) {
                    datax=data.split(",");
                    stringinside='';
                    $(datax).each(function (indexxx,itemxx) {
                        stringinside=stringinside+itemxx;
                        if(indexxx!=(datax.length-1)){
                            stringinside=stringinside+"<br\>";
                        }
                    });
                    return stringinside;
                }
            },{
                "targets": 2,
                "data": "active",
                className: "text-center",
                "render": function (data, type, full, meta) {

                    datax=data.split(",");
                    stringinside='';
                    $(datax).each(function (indexxx,itemxx) {
                        if(itemxx=="SKA"){
                             stringinside=stringinside+'<span class="label label-info">'+itemxx+'</span>';
                        }else{
                             stringinside=stringinside+'<span class="label label-warning">'+itemxx+'</span>';
                        }
                        if(indexxx!=(datax.length-1)){
                            stringinside=stringinside+"<br\>";
                        }
                    });
                    return stringinside;
                }
            },{
                "targets": 3,
                className: "text-center",
                "render": function ( data, type, row ) {
                    datax=data.split(",");
                    stringinside='';
                    $(datax).each(function (indexxx,itemxx) {
                        stringinside=stringinside+itemxx;
                        if(indexxx!=(datax.length-1)){
                            stringinside=stringinside+"<br\>";
                        }
                    });
                    return stringinside;
                }
            }
            ],
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf', 'print'
            ]
            
        });
        $('#select_rent').on('change', function() {
          switch(this.value)
          {
            case '1':
                $reloadurl='request';
                break;
            case '2':
                $reloadurl='waiting_for_payment';
                break;
            case '3':
                $reloadurl='process_soft_copy';
                break;
            case '4':
                $reloadurl='soft_copy_finish';
                break;
            case '5':
                $reloadurl='process_hard_copy';
                break;
            case '6':
                $reloadurl='hard_copy_finish';
                break;
            case '7':
                $reloadurl='cancel';
                break;
            case '8':
                $reloadurl='expired';
                break;
            default:
                $reloadurl='all';
                break;
          }
          table.ajax.url( '/webApi/allrequestbystatus-'+$reloadurl ).load();
        });
    </script>
@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Riwayat Permohonan</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Riwayat</a></li>
                            <li class="breadcrumb-item active">Riwayat Permohonan</li>
                        </ol>
                    </div>
                </div>
@endsection

@section('content')
                <!-- Row -->
                <div class="row" id="tableSection">

                    
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-5 col-lg-5">
                                        <h4 class="card-title">Riwayat Permohonan</h4>
                                        <h6 class="card-subtitle">Menampilkan semua riwayat permohonan sertifikat</h6>
                                    </div>
                                    <div class="col-sm-12 col-md-7 col-lg-7">
                                        <label class="col-lg-2 col-md-3 col-sm-3 col-xs-3">Filter: </label>
                                        <select class="col-lg-10 col-sm-9 col-xs-9 custom-select pull-right" name="select_rent" id="select_rent">
                                            <option value="0" selected>Semua</option>
                                            <option value="1">Permohonan sewa</option>
                                            <option value="2">Menunggu pembayaran</option>
                                            <option value="3">Soft copy sedang diproses</option>
                                            <option value="4">Soft copy sudah dikirim</option>
                                            <option value="5">Hard copy sedang diproses</option>
                                            <option value="6">Hard copy sudah dikirim</option>
                                            <option value="7">Sewa dibatalkan</option>
                                            <option value="8">Masa sewa habis</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="20%">Invoice</th>
                                                <th width="20%">Jenis Keahlian/Keterampilan</th>
                                                <th width="7%">Tipe Sertifikat</th>
                                                <th width="12%">Kualifikasi</th>
                                                <th width="10%">Status </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>         
@endsection
