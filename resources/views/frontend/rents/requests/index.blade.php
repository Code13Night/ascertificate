@extends('backend.layouts.app')


@section('title')
Permintaan Penyewaan
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.css') }}" />

    <link href="{{ asset('backend/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/dropify/dist/css/dropify.min.css') }}">

@endsection
@section('js')

    <!-- This is data table -->
    <script src="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="{{ asset('backend/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <!-- end - This is for export functionality only -->
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{ asset('backend/assets/plugins/html5-editor/wysihtml5-0.3.0.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <script>
         if(window.location.href.indexOf("cartOpen") > -1) {
            $('#createSection').hide('fast');
            $('#tableSection').removeClass('hide');
            $('#tableSection').show('slow');
        }else{
            if($('#budget_type').val()=="APBD I"){
                $('.city-section').hide();
                $('.province-section').show();
            }else if($('#budget_type').val()=="APBD II"){
                $('.city-section').show();
                $('.province-section').show();
            }else{
                $('.city-section').hide();
                $('.province-section').hide();
            }
        }
        $('.skt_section').hide();
        $('.textarea_editor').wysihtml5();
        $(".select2").select2({
                width: "100%"
            });
        var idcollection=[];
        function loadAJAX() {
           
            $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/allcarts/',
                success: function (data) {
                    $(data).each(function (index, item) {
                        if(item.success){
                            idcollection.splice(0,idcollection.length);
                            if(item.data.length>0){
                                var inside="";
                                $(item.data).each(function (indexx, itemx) {
                                    code_list=itemx.classification_code.split(",");
                                    clasification_list=itemx.certificate_classification.split(",");
                                    qualification_list=itemx.qualification.split(",");
                                    type_name_list=itemx.type_name.split(",");
                                    showclassification="";
                                    showtype_name="";
                                    showqualification="";
                                     $(code_list).each(function (indexxx,itemxx) {
                                        showclassification=showclassification+'<b>'+itemxx+"</b> - "+clasification_list[indexxx];
                                        showqualification=showqualification+qualification_list[indexxx];
                                        showtype_name=showtype_name+type_name_list[indexxx];
                                        if(indexxx!=(code_list.length-1)){
                                            showclassification=showclassification+"<br\>";
                                            showqualification=showqualification+"<br\>";
                                            showtype_name=showtype_name+"<br\>";
                                        }
                                    })
                                   inside=inside+'<tr>'+
                                                    '<td ><input type="checkbox" id="basic_checkbox_'+itemx.id+'" class="checkboxcheckout" value="'+itemx.id+'" ><label for="basic_checkbox_'+itemx.id+'" ></label></td>'+
                                                    '<td> '+showclassification+'</td>'+
                                                    '<td>'+showtype_name+'</td>'+
                                                    '<td>'+showqualification+'</td>'+
                                                    '<td>'+'  <a  href="javascript:void(0)" id=' + itemx.id + ' data-toggle="modal" data-target="#myModal" title="Delete" class="btndelete icon text-danger"><i class="mdi mdi-delete"></i></a></td>'+
                                                '</tr>';
                                    idcollection.push({id:itemx.id});
                                });
                                $('#cartBody').html(inside);
                                $('.cartnotify').html('<span class="heartbit"></span> <span class="point"></span>');
                                $('.checkoutbtn').show();                                   
                            }else{
                                $('#cartBody').html('<td colspan=4>Keranjang Permohonan Sewa Kosong</td>');
                                $('.cartnotify').html('');
                                $('.checkoutbtn').hide();
                            }        
                        }
                    });
                }
            });
        }
        loadAJAX();
        var type_idfilter="all",profession_idfilter=$('#profession_id').val();
        $('#type_id1').on('click', function() {
            $('.subtitlerequest').html('Sub Keahlian');
            type_idfilter=$(this).val();
            $('.skt_section').hide();
            $('.ska_section').show();
            $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/allcertificates/type-'+type_idfilter+'/profession-'+profession_idfilter,
                success: function (data) {
                    $(data).each(function (index, item) {
                        if(item.success){
                            $('#qualification').val('');    
                            $('#certificate_id').empty();
                            $('#certificate_id').select2({
                                placeholder: "Select a pill",
                                data: item.data
                            });
                        }
                    });
                }
            });
        });
        $('#type_id2').on('click', function() {
            $('.subtitlerequest').html('Sub Keterampilan');
            type_idfilter=$(this).val();
            $('.skt_section').show();
            $('.ska_section').hide();
            $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/allcertificates/type-'+type_idfilter+'/profession-'+profession_idfilter,
                success: function (data) {
                    $(data).each(function (index, item) {
                        if(item.success){
                            $('#qualification').val('');       
                            $('#certificate_id').empty();
                            $('#certificate_id').select2({
                                placeholder: "Pilih Klasifikasi Sertifikat",
                                data: item.data
                            });
                        }
                    });
                }
            });
        });

        $('.hideadd').on('click', function (e) {
            $('#createSection').hide('fast');
            $('#tableSection').removeClass('hide');
            $('#tableSection').show('slow');
        });
        data = [];
        //Delete Confirmation
        $('#myTable tbody').on('click', '.checkboxcheckout', function (e) {
            if ($(this).is(':checked')) {
                data.push({id:$(this).val()});
            }else{
                x=data.map(function(e) { return e.id; }).indexOf($(this).val());
                data.splice(x,1);
            }
        });

        //Delete Confirmation
        $('#myTable thead').on('click', '.checkboxcheckall', function (e) {
            if ($(this).is(':checked')) {
                data.splice(0,data.length);
                data= idcollection.slice();
                $('.checkboxcheckout').prop('checked', true);
            }else{
                $('.checkboxcheckout').prop('checked', false);
                data.splice(0,data.length);
            }
        });
        $("#createuser").attr("action", "/webApi/cart/add");
        $('.btncreate').on('click', function (e) {
            $('#createSection').show('slow');
            if($('#budget_type').val()=="APBD I"){
                $('.city-section').hide();
                $('.province-section').show();
            }else if($('#budget_type').val()=="APBD II"){
                $('.city-section').show();
                $('.province-section').show();
            }else{
                $('.city-section').hide();
                $('.province-section').hide();
            }
            $('#tableSection').hide('fast');
            $("#createuser").attr("action", "/webApi/cart/add");
        });
        $('#budget_type').on('change', function() {
            if($(this).val()=="APBD I"){
                $('.city-section').hide();
                $('.province-section').show();
            }else if($(this).val()=="APBD II"){
                $('.city-section').show();
                $('.province-section').show();
            }else{
                $('.city-section').hide();
                $('.province-section').hide();
            }
        });
        $('#province').on('change', function() {
            city_filter=$(this).val();
            $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/allcity/filter_by-'+city_filter,
                success: function (data) {
                    $(data).each(function (index, item) {
                        if(item.success){       
                            $('#city').empty();
                            $('#city').select2({
                                placeholder: "Select a pill",
                                data: item.data
                            });
                        }
                    });
                }
            });
        });
        $('#profession_id').on('change', function() {
            profession_idfilter=$(this).val();
            $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/allcertificates/type-'+type_idfilter+'/profession-'+profession_idfilter,
                success: function (data) {
                    $(data).each(function (index, item) {
                        if(item.success){       
                            $('#certificate_id').empty();
                            $('#certificate_id').select2({
                                placeholder: "Select a pill",
                                data: item.data
                            });
                        }
                    });
                }
            });
        });
        //post new request
        $('.addrequest').on('click', function (e) {
            //prevent from action submit default
            e.preventDefault();
            if($('#qualification').val()==""){
                $('#myModalerror2').modal('toggle');
                return;
            }
            //Inserting data from Form to JSON format
            var datastring = new FormData($('#createuser')[0]);
            //get action URL
            var $form = $('#createuser'),
              url = '/webApi/cart/add';
            //ajax POST
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                data: datastring,
                success: function (data) { 
                    $(data).each(function (index, item) {
                        if(item.success){
                            $.toast({
                                heading: 'Permohonan ditambahkan',
                                text: $('#certificate_id').select2('data')[0].text+' ditambahkan ke cart',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'info',
                                hideAfter: 3000, 
                                stack: 6
                              });
                            loadAJAX();
                            type_display="";
                            if(type_idfilter==2){
                                type_display='<span class="label label-warning">SKT</span> - ';  
                            }else{
                                type_display='<span class="label label-info">SKA</span> - ';
                            }
                            $('.request_list').append('<div class="form-group row request'+item.request_id+'">'+
                                                        '<div class="col-md-8">'+
                                                            type_display+$('#certificate_id').select2('data')[0].text+
                                                        '</div>'+
                                                        '<div class="col-md-4">'+
                                                            '<button type="button" class="btn btn-danger removeRequest" id="'+item.request_id+'"><i class="fa fa-trash"></i> Hapus</button>'+
                                                        '</div>'+
                                                    '</div>');
                            $('#cart_id').val(item.data_id);
                        }else{
                            $('.insidemessage').html(item.message);
                            $('#myModalerror').modal('toggle');
                        }
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    $('#myModalerror').modal('toggle');
                }                   
            });  
        });
        //Post new User
        $('#createuser').submit(function (e) {
            //prevent from action submit default
            e.preventDefault();
            if($('#qualification').val()==""){
                $('#myModalerror2').modal('toggle');
                return;
            }
            //Inserting data from Form to JSON format
            var datastring = new FormData($(this)[0]);
            //get action URL
            var $form = $(this),
              url = $form.attr("action");
            //ajax POST
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                data: datastring,
                success: function (data) { 
                    $(data).each(function (index, item) {
                        if(item.success){

                            $.toast({
                                heading: 'Permohonan cart baru',
                                text: 'Permohonan berhasil ditambahkan kedalam cart',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'info',
                                hideAfter: 3000, 
                                stack: 6
                              });
                            loadAJAX();               
                            $('#createSection').hide('fast');
                            $('#tableSection').show('slow');
                            $('#cart_id').val('');
                        }else{
                            if(item.message=="Sertifikat sudah ada dicart "&&$('#cart_id').val()!=''){
                                loadAJAX();               
                                $('#createSection').hide('fast');
                                $('#tableSection').show('slow');
                                $('#cart_id').val('');
                            }else{
                                $('.insidemessage').html(item.message);
                                $('#myModalerror').modal('toggle');
                            }
                        }
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    $('#myModalerror').modal('toggle');
                }                   
            });  
        });
        //Delete Confirmation
        $('#myTable tbody').on('click', '.btndelete', function (e) {
            e.preventDefault();
            $iddel=this.id;
        });
        //Delete Confirmation
        $('.request_list').on('click', '.removeRequest', function (e) {
            e.preventDefault();
            val_selected=this.id;
            $.ajax({
                cache: false,
                type: 'DELETE',
                url: '/webApi/cart-request/'+ this.id,
                data: { _method: 'DELETE', _token: $('input[name=_token]').val() },
                success: function (data) {
                    $(data).each(function (index, item) {
                        if(item.count_request==1){
                            $('#cart_id').val('');
                        }
                            $.toast({
                                heading: 'Permohonan dibatalkan',
                                text: 'Permohonan dibatalkan',
                                position: 'top-right',
                                loaderBg:'#fc4b6c',
                                icon: 'error',
                                hideAfter: 3000, 
                                stack: 6
                              });
                        $('.request'+val_selected).remove(); 
                        loadAJAX();
                    }); 
                }
            });
        });
        //Delete Confirmation
        $('#btnconfirmdelete').on('click', function (e) {
            $('#myModal').modal('toggle');
            
            $.ajax({
                cache: false,
                type: 'DELETE',
                url: '/webApi/cart/'+$iddel,
                data: { _method: 'DELETE', _token: $('input[name=_token]').val() },
                success: function (data) {
                            $.toast({
                                heading: 'Cart dihapus',
                                text: 'Cart yang terpilih telah dihapus',
                                position: 'top-right',
                                loaderBg:'#fc4b6c',
                                icon: 'error',
                                hideAfter: 3000, 
                                stack: 6
                              });
                            loadAJAX();      
                }
            });
        });
        //Delete Confirmation
        $('#btnconfirmcheckout').on('click', function (e) {
            $('#myModalconfirmcheckout').modal('toggle');
            
            $.ajax({
                cache: false,
                type: 'POST',
                url: '/webApi/checkout',
                contentType: 'application/json',
                data: JSON.stringify({ _token: $('input[name=_token]').val(), cart:data, note:$('#note').val()}),
                dataType: 'json',
                success: function (data) {
                    loadAJAX();

                            $.toast({
                                heading: 'Order dibuat',
                                text: 'Berhasil checkout, Order berhasil dibuat',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'info',
                                hideAfter: 3000, 
                                stack: 6
                              });
                    data.splice(0,data.length);
                    $('#budget_type').val('');
                    $('#city').val('').trigger('checked');
                    $('#province').val('').trigger('checked');
                    $('#company').val('');
                    $('#position').val('');
                    $('.checkboxcheckall').prop('checked', false);      
                }
            });
        });
    </script>
@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Permintaan Sewa</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Sertifikat</a></li>
                            <li class="breadcrumb-item active">Permintaan Sewa</li>
                        </ol>
                    </div>
                </div>
@endsection

@section('content')
                
             <!-- Row -->
                <div class="row" id="createSection">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <button class="pull-right btn btn-sm btn-rounded btn-info hideadd"  data-toggle="tooltip" title="Tutup permintaan Sewa">X</button>
                                <h4 class="m-b-0 text-white">Form Permintaan Sewa</h4>
                            </div>
                            <div class="card-body">
                                {!! Form::open(array('route' => 'certificate.store','method'=>'POST','id'=>'createuser','class'=>'form-horizontal ')) !!}
                                    <div class="form-body">

                                        <div class="row" id="editSection">

                                        </div>
                                        <h3 class="box-title">Informasi Anggaran</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Anggaran</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control  custom-select"  name="budget_type" id="budget_type">
                                                            
                                                            @if(Session::get('budget_type')==''||Session::get('budget_type')=='null')
                                                            <option value="" selected>
                                                            @else
                                                            <option value="">
                                                            @endif
                                                            Pilih Jenis Anggaran</option>
                                                            @if(Session::get('budget_type')=='APBN')
                                                            <option value="APBN" selected>
                                                            @else
                                                            <option value="APBN">
                                                            @endif
                                                            APBN</option>
                                                            @if(Session::get('budget_type')=='APBD I')
                                                            <option value="APBD I" selected>
                                                            @else
                                                            <option value="APBD I">
                                                            @endif
                                                            APBD I</option>
                                                            @if(Session::get('budget_type')=='APBD II')
                                                            <option value="APBD II" selected>
                                                            @else
                                                            <option value="APBD II">
                                                            @endif
                                                            APBD II</option>
                                                            @if(Session::get('budget_type')=='Lainnya')
                                                            <option value="Lainnya" selected>
                                                            @else
                                                            <option value="Lainnya">
                                                            @endif
                                                            Lainnya</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="cart_id" id="cart_id" class="cart_id">
                                        <!--/row-->
                                        <div class="row province-section">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Provinsi</label>
                                                    <div class="col-md-9">
                                                        {!! Form::select('province', $provinces,Session::get('province'), array('class' => 'form-control select2 m-b-10','style'=>'width: 100%' ,'id'=>'province')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row city-section">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Kota/Kabupaten</label>
                                                    <div class="col-md-9">
                                                        {!! Form::select('city', $cities, Session::get('city'), array('class' => 'form-control select2 m-b-10','style'=>'width: 100%' ,'id'=>'city')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Instansi</label>
                                                    <div class="col-md-9">
                                                        {!! Form::text('company',Session::get('company'), array('id'=>'company','class' => 'form-control')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Satuan Kerja</label>
                                                    <div class="col-md-9">
                                                        {!! Form::text('position',Session::get('position'), array('id'=>'position','class' => 'form-control')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <h3 class="box-title">Informasi Sertifikat</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-2">
                                            </div>
                                            <div class="col-md-10">
                                                <div class="request_list">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <!--/span-->
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Tipe Sertifikat</label>
                                                    <div class="col-md-2">
                                                        <div class="m-b-10">
                                                            <label class="custom-control custom-radio">
                                                                <input id="type_id1" name="type_id" value=1 type="radio" class="custom-control-input type_id" checked>
                                                                <span class="custom-control-label">SKA</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="m-b-10">
                                                            <label class="custom-control custom-radio">
                                                                <input id="type_id2" name="type_id" type="radio" value=2 class="custom-control-input type_id">
                                                                <span class="custom-control-label">SKT</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <button type="button" class="btn btn-info addrequest"><i class="fa fa-plus"></i> Tambah</button>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Profesi</label>
                                                    <div class="col-md-9">
                                                        {!! Form::select('profession_id', $professions, Session::get('profession_id'), array('class' => 'form-control select2 m-b-10','style'=>'width: 100%' ,'id'=>'profession_id')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3 subtitlerequest">Sub Keahlian</label>
                                                    <div class="col-md-9">
                                                        {!! Form::select('certificate_id', $certificates,null, array('class' => 'form-control select2 m-b-10','style'=>'width: 100%' ,'id'=>'certificate_id')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Kualifikasi</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control  custom-select"  name="qualification" id="qualification">
                                                            <option value="" >Pilih Kualifikasi </option>
                                                            <option value="Tingkat I" class="skt_section">Tingkat I</option>
                                                            <option value="Tingkat II" class="skt_section">Tingkat II</option>
                                                            <option value="Tingkat III" class="skt_section">Tingkat III</option>
                                                            <option value="Muda" class="ska_section">Muda</option>
                                                            <option value="Madya" class="ska_section">Madya</option>
                                                            <option value="Utama" class="ska_section">Utama</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Jenjang Pendidikan</label>
                                                    <div class="col-md-9">
                                                        {!! Form::select('education_level_id', $education_levels,Session::get('education_level_id'), array('class' => 'm-b-10 form-control select2','style'=>'width: 100%','id'=>'education_level_id')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Program Studi</label>
                                                    <div class="col-md-9">
                                                        {!! Form::select('study_program_id', $study_programs,Session::get('study_program_id'), array('class' => 'm-b-10 form-control select2','style'=>'width: 100%','id'=>'study_program_id')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Tahun Lulus</label>
                                                    <div class="col-md-9">
                                                        {!! Form::number('graduate_year', Session::get('graduate_year'), array('id'=>'graduate_year','class' => 'form-control')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-2">
                                            </div>
                                            <div class="col-md-10 ">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Masukkan ke Cart</button>
                                                        <button type="button" class="btn btn-inverse">Batal</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div> 

                <!-- Row -->
                <div class="row hide" id="tableSection">

                    
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <button class="pull-right btn btn-sm btn-rounded btn-success btncreate" >Tambah Permohonan Sewa</button>
                                <h4 class="card-title">Permohonan Sewa</h4>
                                <h6 class="card-subtitle">Keranjang Permohonan Sewa</h6>
                                <div class="table-responsive">
                                    <table id="myTable"  class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th width="5%"><input type="checkbox" id="checkall" class="checkboxcheckall"><label for="checkall" ></label></th>
                                                <th>Keahlian/Keterampilan</th>
                                                <th>Jenis Sertifikat</th>
                                                <th>Kualifikasi</th>
                                                <th width="5%"></th>
                                            </tr>
                                        </thead>
                                        <tbody id="cartBody">

                                        </tbody>
                                    </table>
                                </div>

                                <a href="#" class="btn btn-primary float-right checkoutbtn" data-toggle="modal" data-target="#myModalconfirmcheckout" title="checkout">Checkout</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- sample modal content -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Hapus Cart</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Anda yakin membatalkan permintaan sewa sertifikat ini?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Tutup</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmdelete">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal --> 

                <!-- sample modal content -->
                <div id="myModalconfirmcheckout" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Konfirmasi Checkout</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Anda yakin untuk melakukan permohonan sewa ?</p>
                                <form class="form-horizontal">
                                    
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <textarea class="textarea_editor form-control" name="note" id="note" rows="15" placeholder="Masukkan catatan tambahan ke admin"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Tutup</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmcheckout">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal --> 
                <!-- sample modal content -->
                <div id="myModalerror" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Terjadi Kesalahan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p class="insidemessage"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">OK</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <!-- sample modal content -->
                <div id="myModalerror2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Terjadi Kesalahan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Mohon Memilih Kualifikasi dengan benar</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">OK</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
@endsection
