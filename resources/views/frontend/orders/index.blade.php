@extends('backend.layouts.app')


@section('title')
Permintaan Penyewaan
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.css') }}" />

    <link href="{{ asset('backend/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/dropify/dist/css/dropify.min.css') }}">

@endsection
@section('js')

    <!-- This is data table -->
    <script src="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="{{ asset('backend/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <!-- end - This is for export functionality only -->
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{ asset('backend/assets/plugins/html5-editor/wysihtml5-0.3.0.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <script>
        $('.dropify').dropify();

        $('.textarea_editor').wysihtml5();
        $(".select2").select2({
                width: "100%"
            });
        tablestate='request';
        var table=$('#myTable').DataTable({iDisplayLength: 10,
            responsive: true,
            lengthMenu: [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "order": [
                [0, 'asc']
            ],
            "ajax": '/webApi/allorderbystatus-all',
            "columns": [
                    { "data": "invoice" },
                    { "data": "total_request" },
                    { "data": "note" },
                    { "data": "status" },
                    { "data": "id" },
            ], "columnDefs": [{
                "targets": 4,
                "data": "Action_Link",
                orderable: false,
                className: "text-center",
                "render": function (data, type, full, meta) {
                    return '<div class="btn-group">'+
                                    '<button type="button" class="btn btn-sm btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                        '<i class="ti-settings"></i>'+
                                    '</button>'+
                                    '<div class="dropdown-menu dropdown-menu-right">'+
                                        '<a class="dropdown-item btncancel" href="javascript:void(0)" id=' + data + ' data-toggle="modal" data-target="#myModalCancel"> Batalkan penyewaan</a>'+
                                        '<div class="dropdown-divider"></div>'+
                                        '<a class="dropdown-item btndelete" href="javascript:void(0)" id=' + data + ' data-toggle="modal" data-target="#myModal">Hapus</a>'+
                                    '</div>'+
                                '</div>';

                }
            },{
                "targets": 3,
                className: "text-center",
                "render": function ( data, type, row ) {
                    switch(data){
                        case 0:
                            return '<span class="label label-warning">Menunggu konfirmasi admin</span>';
                            break;
                        case 1:
                            return '<span class="label label-warning">Menunggu Pembayaran</span>';
                            break;
                        case 2:
                            return '<span class="label label-success">Soft copy sedang diproses</span>';
                            break;
                        case 3:
                            return '<span class="label label-info">Soft copy sudah dikirim</span>';
                            break;
                        case 4:
                            return '<span class="label label-warning">Menunggu konfirmasi hard copy</span>';
                            break;
                        case 5:
                            return '<span class="label label-warning">Menunggu pembayaran soft copy</span>';
                            break;
                        case 6:
                            return '<span class="label label-success">Hard copy sedang diproses</span>';
                            break;
                        case 7:
                            return '<span class="label label-info">Hard copy sudah diteriman</span>';
                            break;
                        case 8:
                            return '<span class="label label-danger">Sewa dibatalkan</span>';
                            break;
                        default:
                            return '<span class="label label-danger">Masa sewa habis</span>';
                            break;
                    }
                }
            },{
                "targets": 0,
                className: "text-center",
                "render": function ( data, type, row ) {
                    
                    return '<a href="/order-invoice/'+data+'"  id=' + data + ' class="btnshowSerialNumber" >'+data+'</a>';
                }
            },{
                "targets": 1,
                className: "text-center"
            }
            ]
            
        });
        $('#select_rent').on('change', function() {
          switch(this.value)
          {
            case '0':
                $reloadurl='request';
                break;
            case '1':
                $reloadurl='waiting_for_payment';
                break;
            case '2':
                $reloadurl='process_soft_copy';
                break;
            case '3':
                $reloadurl='process_hard_copy';
                break;
            case '4':
                $reloadurl='cancel';
                break;
            case '5':
                $reloadurl='all';
                break;
            default:
                $reloadurl='request';
                break;
          }

            tablestate=$reloadurl;
            table.ajax.url( '/webApi/allorderbystatus-'+$reloadurl ).load();
        });
        var $iddel;
        //Delete Confirmation
        $('#myTable tbody').on('click', '.btndelete', function (e) {
            e.preventDefault();
            $iddel=this.id;
        });
        //Delete Confirmation
        $('#btnconfirmdelete').on('click', function (e) {
            $('#myModal').modal('toggle');
            
            $.ajax({
                cache: false,
                type: 'DELETE',
                url: '/webApi/order/'+$iddel,
                data: { _method: 'DELETE', _token: $('input[name=_token]').val() },
                success: function (data) {

                    $(data).each(function (index, item) {
                        if(item.success){                         
                            table.ajax.reload();
                            $.toast({
                                heading: 'Order dihapus',
                                text: 'Order berhasil dihapus',
                                position: 'top-right',
                                loaderBg:'#fc4b6c',
                                icon: 'error',
                                hideAfter: 3000, 
                                stack: 6
                              });              
                        }else{
                            $('.bodymsg').html(item.message);
                            $('#myModalerror').modal('toggle'); 
                        }
                    });
                }
            });
        });
        //Delete Confirmation
        $('#myTable tbody').on('click', '.btncancel', function (e) {
            e.preventDefault();
            $iddel=this.id;
        });
        //Delete Confirmation
        $('#btnconfirmcancel').on('click', function (e) {
            $('#myModalCancel').modal('toggle');
            
            $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/ordercancel/'+$iddel,
                success: function (data) {

                    $(data).each(function (index, item) {
                        if(item.success){
                            $.toast({
                                heading: 'Order dibatalkan',
                                text: 'Semua permohonan yang ada di order dibatalkan',
                                position: 'top-right',
                                loaderBg:'#fc4b6c',
                                icon: 'error',
                                hideAfter: 3000, 
                                stack: 6
                              });                         
                            table.ajax.reload();  
                        }else{
                            $('.bodymsg').html(item.message);
                            $('#myModalerror').modal('toggle');
                        }
                    });
                }
            });
        });
    </script>
@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Riwayat Order</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Riwayat</a></li>
                            <li class="breadcrumb-item active">Order</li>
                        </ol>
                    </div>
                </div>
@endsection

@section('content')
                
                <!-- Row -->
                <div class="row" id="tableSection">

                    
                                {!! Form::open(array('route' => 'certificate.store','method'=>'POST','id'=>'createuser','class'=>'form-horizontal ')) !!}

                                {!! Form::close() !!}
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-5 col-lg-5">
                                        <h4 class="card-title">Riwayat Order</h4>
                                        <h6 class="card-subtitle">Riwayat order sertifikat</h6>
                                    </div>
                                    <!-- <div class="col-sm-12 col-md-7 col-lg-7">
                                        <select class="custom-select pull-right" name="select_rent" id="select_rent">
                                            <option value="0" selected>Permohonan sewa</option>
                                            <option value="1">Proses pembayaran soft copy</option>
                                            <option value="2">Proses pembuatan soft copy</option>
                                            <option value="3">Proses pembuatan hard copy</option>
                                            <option value="4">Permohonan sewa ditolak</option>
                                            <option value="5">Semua permohonan sewa</option>
                                        </select>
                                    </div> -->
                                </div>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="20%">Invoice</th>
                                                <th width="10%" class="text-center">Jumlah Permohonan</th>
                                                <th width="30%">Catatan</th>
                                                <th width="20%">Status</th>
                                                <th width="10%" class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- sample modal content -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Hapus Sertifikat</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Anda yakin menghapus order ini?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Tutup</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmdelete">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <!-- sample modal content -->
                <div id="myModalCancel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Batalkan Order Penyewaan Sertifikat</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Anda yakin membatalkan order ini?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Tutup</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmcancel">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                
@endsection
