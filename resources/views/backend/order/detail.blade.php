@extends('backend.layouts.app')


@section('title')
Permintaan Penyewaan
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.css') }}" />

    <link href="{{ asset('backend/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/dropify/dist/css/dropify.min.css') }}">
    <link href="{{ asset('backend/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('js')

    <script src="{{ asset('backend/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <!-- end - This is for export functionality only -->
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{ asset('backend/assets/plugins/html5-editor/wysihtml5-0.3.0.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
        <!-- Date Picker Plugin JavaScript -->
        <script src="{{ asset('backend/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        $('.dropify').dropify();

        $('.datepicker-autocloserent').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        $('.textarea_editor').wysihtml5();
        if($('#order_status_view').val()==0||$('#order_status_view').val()==1){

        $('.btneditProcessHard').hide();
        $('.btneditSendHard').hide();
        $('.btneditSendSoft').hide();
        $('.btneditProcessPayHard').hide();
        }else if($('#order_status_view').val()==2){
        $('.btneditSendSoft').show();
        $('.btneditProcessHard').hide();
        $('.btneditSendHard').hide();
        $('.btneditProcessPayHard').hide()
        }else if($('#order_status_view').val()==3){
            
        $('.btneditProcessHard').show();
        $('.btneditSendHard').hide();
        $('.btneditSendSoft').hide();
        $('.btneditProcessPayHard').hide();
        }else if($('#order_status_view').val()==4){
            
        $('.btneditProcessHard').show();
        $('.btneditSendHard').hide();
        $('.btneditSendSoft').hide();
        $('.btneditProcessPayHard').show();
        }else if($('#order_status_view').val()==5){
        $('.btneditProcessHard').hide();
        $('.btneditSendHard').hide();
        $('.btneditSendSoft').hide();
        $('.btneditProcessPayHard').show();
        }else if($('#order_status_view').val()==6){
        $('.btneditProcessHard').hide();
        $('.btneditSendHard').show();
        $('.btneditSendSoft').hide();
        $('.btneditProcessPayHard').show();
        }else{
        $('.btneditProcessHard').hide();
        $('.btneditSendHard').hide();
        $('.btneditSendSoft').hide();
        $('.btneditProcessPayHard').hide();
        }
        data = [];
        //Delete Confirmation
        $('#myTable tbody').on('click', '.checkboxcheckout', function (e) {
            if ($(this).is(':checked')) {
                data.push({id:$(this).val()});
            }else{
                x=data.map(function(e) { return e.id; }).indexOf($(this).val());
                data.splice(x,1);
            }
            if($('#order_status_view').val()==3||$('#order_status_view').val()==4||$('#order_status_view').val()==5){
                if(data.length>0){
                    $('.btneditProcessHard').show();
                    $('.btneditSendHard').hide();
                }else{
                    $('.btneditProcessHard').hide();
                    $('.btneditSendHard').hide();
                }  
            }else if($('#order_status_view').val()==6){
                    $('.btneditSendHard').hide();
                    $('.btneditSendHard').show();
            }else{
                $('.btneditProcessHard').hide();
                $('.btneditSendHard').hide();
            }
        });
        //PRocess Payment
        $('.btneditProcessPaySoft').on('click', function (e) {
            e.preventDefault();
            $('#order_id').val(this.id);
             e.preventDefault();
             $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/paymentbyOrderID/'+this.id,
                success: function (data) {

                    $(data).each(function (index, item) {
                        if(item.success){
                            $(item.data).each(function (indexx, itemx) {
                                $('.section_payment').html('<b> soft copy</b>');
                                $('.soft_copy_payment').show();
                                $('.hard_copy_payment').hide();
                                $('#total_soft').val(itemx.total_soft);
                                $('#payment_method_soft').val(itemx.payment_method_soft);
                                $('#payment_detail_soft').val(itemx.payment_detail_soft);
                                $('#total_hard').val('');
                                $('#payment_method_hard').val('');
                                $('#payment_detail_hard').val('');
                                $('#editSectionProcessPayment').append('<input name="_method" type="hidden" value="PATCH">');
                                $("#createpayment").attr("action", "/webApi/payment/" + $('#order_id').val());
                            });
                        }else{
                            $('.section_payment').html('<b> soft copy</b>');
                            $('.soft_copy_payment').show();
                            $('.hard_copy_payment').hide();
                            $('#total_hard').val('');
                            $('#payment_method_hard').val('');
                            $('#payment_detail_hard').val('');
                            $('#total_soft').val('');
                            $('#payment_method_soft').val('');
                            $('#payment_detail_soft').val('');
                            $("#createpayment").attr("action", "/webApi/payment/create");
                            $('#editSectionProcessPayment').html('');
                        }
                         $('#modalProcessPayment').modal('toggle');
                    });
                }
            });
        });
        //PRocess Payment
        $('.btneditProcessPayHard').on('click', function (e) {
            e.preventDefault();
            $('#order_id').val(this.id);
            $('#total_hard').val($('#total_must_pay').val());
             e.preventDefault();
             $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/paymentbyOrderID/'+this.id,
                success: function (data) {

                    $(data).each(function (index, item) {
                        if(item.success){
                            $(item.data).each(function (indexx, itemx) {
                                $('.section_payment').html('<b> hard copy</b>');
                                $('.soft_copy_payment').hide();
                                $('.hard_copy_payment').show();
                                $('#total_soft').val(itemx.total_soft);
                                $('#payment_method_soft').val(itemx.payment_method_soft);
                                $('#payment_detail_soft').val(itemx.payment_detail_soft);
                                $('#total_hard').val(itemx.total_hard);
                                $('#payment_method_hard').val(itemx.payment_method_hard);
                                $('#payment_detail_hard').val(itemx.payment_detail_hard);
                                $('#editSectionProcessPayment').append('<input name="_method" type="hidden" value="PATCH">');
                                $("#createpayment").attr("action", "/webApi/payment/" + $('#order_id').val());
                            });
                        }else{
                            $('.section_payment').html('<b> soft copy</b>');
                            $('.soft_copy_payment').hide();
                            $('.hard_copy_payment').show();
                            $('#total_hard').val('');
                            $('#payment_method_hard').val('');
                            $('#payment_detail_hard').val('');
                            $('#total_soft').val('');
                            $('#payment_method_soft').val('');
                            $('#payment_detail_soft').val('');
                            $("#createpayment").attr("action", "/webApi/payment/create");
                            $('#editSectionProcessPayment').html('');
                        }
                         $('#modalProcessPayment').modal('toggle');
                    });
                }
            });
        });
        //Post new Payment
        $('#createpayment').submit(function (e) {
            //prevent from action submit default
            e.preventDefault();
            //Inserting data from Form to JSON format
            var datastring = new FormData($(this)[0]);
            //get action URL
            var $form = $(this),
              url = $form.attr("action");
            //ajax POST
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                data: datastring,
                success: function (data) { 
                    $(data).each(function (index, item) {
                        if(item.success){
                               $.toast({
                                heading: 'Pembayaran diproses',
                                text: 'Pembayaran untuk order ini berhasil diproses',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'info',
                                hideAfter: 3000, 
                                stack: 6
                              });  
                            $('#modalProcessPayment').modal('toggle');
                            $('.hard_copy_payment').hide();
                            $('#total_hard').val('');
                            $('#payment_method_hard').val('');
                            $('#payment_detail_hard').val('');
                            $('#total_soft').val('');
                            $('#payment_method_soft').val('');
                            $('#payment_detail_soft').val('');
                            window.location.reload();
                        }else{
                            $('#modalProcessPayment').modal('toggle');
                            $('#myModalerror').modal('toggle');
                        }
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    $('#modalProcessPayment').modal('toggle');
                    $('#myModalerror').modal('toggle');
                }                   
            });  
        });
        //Send Soft Copy
        $('#myTable tbody').on('click', '.btneditProcessSoft', function (e) {
            $('#modalEditSoftCopy').modal('toggle');
            e.preventDefault();
             $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/paymentbyRentID/'+this.id,
                success: function (data) {

                    $(data).each(function (index, item) {
                        $(item.data).each(function (indexx, itemx) {
                            $('#image_prove_soft').attr("src","/"+itemx.prove_phase_soft_copy);

                            $(itemx.rent).each(function (indexxx, itemxx) {
                                $('#serial_number').val(itemxx.serial_number);
                            });
                        });
                    });
                }
            });
            $('#editSectionEditSoftCopy').append('<input name="_method" type="hidden" value="PATCH">');
            $("#EditSoftCopy").attr("action", "/webApi/rent-status/soft_process/" + this.id);
        });
        //Post SoftCopy
        $('#EditSoftCopy').submit(function (e) {
            //prevent from action submit default
            e.preventDefault();
            //Inserting data from Form to JSON format
            var datastring = new FormData($(this)[0]);
            //get action URL
            var $form = $(this),
              url = $form.attr("action");
            //ajax POST
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                data: datastring,
                success: function (data) { 
                    $(data).each(function (index, item) {
                        if(item.success){
                               $.toast({
                                heading: 'Update info sewa',
                                text: 'Informasi sewa pada sertifikat diubah',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'info',
                                hideAfter: 3000, 
                                stack: 6
                              });  
                            $('#modalSendSoftCopy').modal('toggle');
                            $('#serial_number').val('');
                            window.location.reload();
                        }else{
                            $('#modalSendSoftCopy').modal('toggle');
                            $('#myModalerror').modal('toggle');
                        }
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    $('#modalSendSoftCopy').modal('toggle');
                    $('#myModalerror').modal('toggle');
                }                   
            });  
        });
        //Send Hard Copy
        $('.btneditSendHard').on('click', function (e) {
            e.preventDefault();
            $('#modalSendHardCopy').modal('toggle');
             $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/paymentbyOrderID/'+this.id,
                success: function (data) {

                    $(data).each(function (index, item) {
                        $(item.data).each(function (indexx, itemx) {
                            $(itemx.rent).each(function (indexxx, itemxx) {
                                $('#pick_up_date').val(itemxx.pick_up_date);
                                $('#pick_up_by').val(itemxx.pick_up_by);
                                $('#exact_return_date').val(itemxx.exact_return_date);
                            });
                        });
                    });
                }
            });
            $('#editSectionsendHardCopy').append('<input name="_method" type="hidden" value="PATCH">');
            $("#sendHardCopy").attr("action", "/webApi/ordersendhard/" + this.id);
        });
        //Post HardCopy
        $('#sendHardCopy').submit(function (e) {
            //prevent from action submit default
            e.preventDefault();
            //Inserting data from Form to JSON format
            var datastring = new FormData($(this)[0]);
            //get action URL
            var $form = $(this),
              url = $form.attr("action");
            //ajax POST
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                data: datastring,
                success: function (data) { 
                    $(data).each(function (index, item) {
                        if(item.success){
                            $('#modalSendHardCopy').modal('toggle');
                                $('#pick_up_date').val('');
                                $('#pick_up_by').val('');
                                $('#exact_return_date').val('');
                            window.location.reload();
                        }else{
                            $('#modalSendHardCopy').modal('toggle');
                            $('#myModalerror').modal('toggle');
                        }
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    $('#modalSendHardCopy').modal('toggle');
                    $('#myModalerror').modal('toggle');
                }                   
            });  
        });

        //cancelRent
        $('#myTable tbody').on('click', '.btncancelRent', function (e) {
            $('#myModalcancelRent').modal('toggle');
            $iddel=this.id;

        });
        $('#btnconfirmcancelrent').on('click', function (e) {
            //Delete Confirmation\
                $('#myModalcancelRent').modal('toggle');
                
                $.ajax({
                    cache: false,
                    type: 'POST',
                    url: '/webApi/request-status/cancel/'+$iddel,
                    data: { _method: 'PATCH', _token: $('input[name=_token]').val() },
                    success: function (data) {
                        window.location.reload();

                    }
                });
        });
        //cancelRent
        $('#myTable tbody').on('click', '.btnacceptRent', function (e) {
            $('#myModalacceptRent').modal('toggle');
            $iddel=this.id;

        });
        $('.btneditSendSoft').on('click', function (e) {
            $('#myModalSendSoftCopy').modal('toggle');
            $iddel=this.id;
        });
        $('#btnconfirmSendSoftCopy').on('click', function (e) {
            //Delete Confirmation\
                $('#myModalSendSoftCopy').modal('toggle');
                
                $.ajax({
                    cache: false,
                    type: 'GET',
                    url: '/webApi/ordersendsoftcopy/'+$iddel,
                    success: function (data) {
                        window.location.reload();
                    }
                });
        });
        $('.btneditProcessHard').on('click', function (e) {
            $('#myModalProcessHardCopy').modal('toggle');
            $iddel=this.id;
        });
        $('#btnconfirmProcessHardCopy').on('click', function (e) {
            //Delete Confirmation\
                $('#myModalProcessHardCopy').modal('toggle');
                
                $.ajax({
                    cache: false,
                    type: 'POST',
                    url: '/webApi/request-processhardcopy/'+$iddel,
                    data: { _method: 'PATCH', _token: $('input[name=_token]').val(), rent_ids: data },
                    success: function (data) {
                        window.location.reload();
                    }
                });
        });
        //ca
        $('#btnconfirmacceptRent').on('click', function (e) {
            //Delete Confirmation\
                $('#myModalacceptRent').modal('toggle');
                
                $.ajax({
                    cache: false,
                    type: 'POST',
                    url: '/webApi/request-status/accept/'+$iddel,
                    data: { _method: 'PATCH', _token: $('input[name=_token]').val() },
                    success: function (data) {
                        window.location.reload();
                    }
                });
        });
        //cancelRent
        $('.btncancelOrder').on('click', function (e) {
            $('#myModalcancelOrder').modal('toggle');
            $iddel=this.id;

        });
        $('#btnconfirmcancelorder').on('click', function (e) {
            //Delete Confirmation\
                $('#myModalcancelOrder').modal('toggle');
                
                $.ajax({
                    cache: false,
                    type: 'GET',
                    url: '/webApi/ordercancel/'+$iddel,
                    success: function (data) {
                        window.location.reload();
                    }
                });
        });
        //cancelRent
        $('.btndeleteOrder').on('click', function (e) {
            $('#myModaldeleteOrder').modal('toggle');
            $iddel=this.id;

        });
        $('#btnconfirmdelete').on('click', function (e) {
            //Delete Confirmation\
                $('#myModaldeleteOrder').modal('toggle');
                
                $.ajax({
                    cache: false,
                    type: 'GET',
                    url: '/webApi/ordercancel/'+$iddel,
                    success: function (data) {
                        window.location.replace("/history/request");
                    }
                });
        });
        //upload Payment soft

        //upload payment hard


        //search sertifiakt
        $('#searchCertificateButton').on('click', function(event) {
            event.preventDefault();
            insideval="";
                $.ajax({
                    cache: false,
                    type: 'GET',
                    url: '/webApi/usercertificate-search/'+$('#keyword').val(),
                    success: function (data) {
                       $(data).each(function (index, item) {
                        if(item.success){
                            $(item.data).each(function(index, itemxx) {
                                insideval=insideval+'<tr>'
                                                        +'<td>'+itemxx.registration_number+'</td>'
                                                        +'<td>'+itemxx.name+'</td>'
                                                        +'<td>'+itemxx.certificate_classification+'</td>'
                                                        +'<td>'+itemxx.qualification+'</td>'
                                                        +'<td><a href="javascript:void(0)" class="icon text-success btncancelRent" id="" data-toggle="tooltip" data-placement="top" title="Sematkan Sertifikat"><i class="mdi mdi-bookmark-check"></i></a></td>'
                                                    +'</tr>';
                                                    
                            });
                            $('#searchInsideResult').html(insideval);
                        }else{
                            $('#myModalerror').modal('toggle');
                        }
                    });
                    }
                });
        });
    </script>
@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-8 col-8 align-self-center">
                        <h3 class="text-themecolor">Detail Order</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Sewa</a></li>
                            <li class="breadcrumb-item"><a href="/history/request">Permohonan Sewa</a></li>
                            <li class="breadcrumb-item active">{{$order->invoice}}</li>
                        </ol>
                    </div>
                </div>
@endsection

@section('content')
                
                <!-- Row -->
                <div class="row">
                    <!-- /Column -->
                    <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs profile-tab" role="tablist">
                                    <li class="nav-item col-md-3 col-sm-6 text-center"> <a class="nav-link active" data-toggle="tab" href="#general_info" role="tab">Order</a> </li>
                                    <li class="nav-item col-md-3 col-sm-6 text-center"> <a class="nav-link" data-toggle="tab" href="#payment_info" role="tab">Pembayaran</a> </li>
                                    <li class="nav-item col-md-3 col-sm-6 text-center"> <a class="nav-link" data-toggle="tab" href="#history" role="tab">Permohonan</a> </li>
                                    <li class="nav-item col-md-3 col-sm-6 text-center"> <a class="nav-link" data-toggle="tab" href="#certificate" role="tab">Sertifikat</a> </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general_info" role="tabpanel">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center flex-row m-t-30">
                                                <div class="p-2 display-5 text-info"><span>{{ $order->invoice }}</span></div>
                                            </div>
                                            <table class="table no-border">
                                                <tbody>
                                                    <tr>
                                                        <td width="40%">User Penyewa</td>
                                                        <td class="font-medium">{{ $order->user_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Status</td>
                                                        <input type="hidden" id="order_status_view" value={{$order->status}}>
                                                        <td class="font-medium">
                                                                @if($order->status==0)
                                                                <label class="label label-warning">Menunggu konfirmasi admin</label>
                                                                @elseif($order->status==1)
                                                                <label class="label label-warning">Menunggu Pembayaran</label>
                                                                @elseif($order->status==2)
                                                                <label class="label label-success">Soft copy sedang diproses</label>
                                                                @elseif($order->status==3)
                                                                <label class="label label-info">Soft copy sudah dikirim</label>
                                                                @elseif($order->status==4)
                                                                <span class="label label-warning">Menunggu konfirmasi hard copy</span>
                                                                @elseif($order->status==5)
                                                                <span class="label label-warning">Menunggu pembayaran hard copy</span>
                                                                @elseif($order->status==6)
                                                                <span class="label label-success">Hard copy sedang diproses</span>
                                                                @elseif($order->status==7)
                                                                <span class="label label-info">Hard copy sudah diterima</span>
                                                                @elseif($order->status==8)
                                                                <label class="label label-danger">Sewa dibatalkan</label>
                                                                @else
                                                                <label class="label label-danger">Masa sewa habis</label>
                                                                @endif
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Tanggal order</td>
                                                        <td class="font-medium">{{ date("d-m-Y", strtotime($order->created_at))}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Terakhir diubah</td>
                                                        <td class="font-medium">{{ date("d-m-Y", strtotime($order->updated_at))}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Anggaran</td>
                                                        <td class="font-medium">
                                                            @if($order->budget_type=='APBD I')
                                                        {{ $order->budget_type." - ".$order->province }}
                                                            @elseif($order->budget_type=='APBD II')
                                                        {{ $order->budget_type." - ".$order->province."(".$order->city.")" }}
                                                        @else
                                                        {{ $order->budget_type }}
                                                    @endif</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Instansi</td>
                                                        <td class="font-medium">{{ $order->company }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Satuan Kerja</td>
                                                        <td class="font-medium">{{ $order->position }}</td>
                                                    </tr>
                                                    @if($order->status==7)
                                                    <tr>
                                                        <td width="40%">Tanggal pengambilan hard copy</td>
                                                        <td class="font-medium">{{ date("d-m-Y", strtotime($order->certificates->first()->pick_up_date))}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Hard copy diterima Oleh</td>
                                                        <td class="font-medium">{{$order->certificates->first()->pick_up_by}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Tanggal kembali</td>
                                                        <td class="font-medium">{{ date("d-m-Y", strtotime($order->certificates->first()->exact_return_date))}}</td>
                                                    </tr>
                                                    @endif
                                                    <tr>
                                                        <td colspan="2">Catatan penyewa</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">@if($order->note==""||$order->note==null)
                                                            Tidak ada catatan tambahan dari penyewa
                                                            @else
                                                            <blockquote>
                                                            {{$order->note}}    
                                                            </blockquote>
                                                            @endif</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                                @if(Auth::user()->hasRole('admin')||Auth::user()->hasRole('superadmin')||Auth::user()->hasRole('cs'))
                                                    @if($order->status==0)
                                                    <a href="javascript::void(0)" class="btn btn-info btneditProcessPaySoft" id="{{$order->id}}">Update data pembayaran soft copy</a>
                                                    @endif
                                                    @if($order->status==4)
                                                    <a href="javascript::void(0)" class="btn btn-success btneditProcessPayHard" id="{{$order->id}}">Update data pembayaran hard copy</a>
                                                    @endif
                                                @endif

                                                @if($order->status==0||$order->status==1||$order->status==2)
                                                <a href="javascript::void(0)" class="btn btn-warning btncancelOrder" id="{{$order->id}}">Batalkan Order</a>
                                                @endif

                                                @if($order->status==8)
                                                <a href="javascript::void(0)" class="btn btn-danger btndeleteOrder" id="{{$order->id}}">Hapus Order</a>
                                                @endif
                                        </div>
                                    </div>
                                     <div class="tab-pane" id="payment_info" role="tabpanel">
                                            @if($order->payment_detail->count()==0)
                                        <div class="card-body text-center">
                                                <h4 class="card-title">Data pembayaran tidak ditemukan</h4>
                                                <p class="card-text">Lakukan Update terhadap data pembayaran order ini</p>
                                            @else
                                        <div class="card-body">
                                            @endif
                                            <table class="table no-border">
                                                <tbody>
                                                        
                                                        @foreach ($order->payment_detail as $valuepayment)
                                                    <tr>
                                                        <td width="40%">Metode Pembayaran Soft Copy</td>
                                                        <td class="font-medium">{{ $valuepayment->payment_method_soft }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Keterangan Pembayaran Soft Copy</td>
                                                        <td class="font-medium">{{ $valuepayment->payment_detail_soft }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Total Soft Copy</td>
                                                        <td class="font-medium">Rp. {{ number_format($valuepayment->total_soft,2,",",".") }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Pembayaran Softcopy</td>
                                                        <td class="font-medium">
                                                            @if($valuepayment->phase_soft_copy==1)
                                                                <label class="label label-rounded label-info">Sudah</label>
                                                            @else
                                                                <label class="label label-rounded label-danger">Belum</label>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Metode Pembayaran Hard Copy</td>
                                                        <td class="font-medium">{{ $valuepayment->payment_method_hard }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Keterangan Pembayaran Hard Copy</td>
                                                        <td class="font-medium">{{ $valuepayment->payment_detail_hard }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Total Hard Copy</td>
                                                        <td class="font-medium">Rp. {{ number_format($valuepayment->total_hard,2,",",".") }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Pembayaran HardCopy</td>
                                                        <td class="font-medium">
                                                            @if($valuepayment->phase_hard_copy==1)
                                                                <label class="label label-rounded label-info">Sudah</label>
                                                            @else
                                                                <label class="label label-rounded label-danger">Belum</label>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                        @endforeach
                                                </tbody>
                                            </table>
                                                @if(Auth::user()->hasRole('admin')||Auth::user()->hasRole('superadmin')||Auth::user()->hasRole('cs'))
                                                <a href="javascript::void(0)" class="btn btn-info btneditProcessPaySoft" id="{{$order->id}}">Update data pembayaran soft copy</a>
                                                <a href="javascript::void(0)" class="btn btn-success btneditProcessPayHard" id="{{$order->id}}">Update data pembayaran hard copy</a>
                                                @endif
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="history" role="tabpanel">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id="myTable"  class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Keahlian/Keterampilan</th>
                                                            <th  class="text-center">Jenis Sertifikat</th>
                                                            <th>Kualifikasi</th>
                                                            <th  class="text-center">Status</th>
                                                            <th class="text-center"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if($order->request_detail->count()==0)
                                                        <tr>
                                                            <td colspan="4">TIdak ada data penyewaan</td>
                                                        </tr>
                                                        @endif
                                                        @foreach ($order->request_detail as $valuerequest)
                                                        <tr>
                                                            <td>
                                                                {{$valuerequest->certificate_name}}
                                                            </td>
                                                            <td class="text-center">
                                                                @if($valuerequest->type_id==2)
                                                                <span class="label label-warning">SKT</span>
                                                                @else
                                                                <span class="label label-info">SKA</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                {{$valuerequest->qualification}}
                                                            </td>
                                                            <td class="text-center">
                                                                @if($valuerequest->status==0)
                                                                <span class="label label-warning">Menunggu konfirmasi admin</span>
                                                                @elseif($valuerequest->status==1)
                                                                <span class="label label-warning">Menunggu Pembayaran</span>
                                                                @elseif($valuerequest->status==2)
                                                                <span class="label label-success">Soft copy sedang diproses</span>
                                                                @elseif($valuerequest->status==3)
                                                                <span class="label label-info">Soft copy sudah dikirim</span>
                                                                @elseif($valuerequest->status==4)
                                                                <span class="label label-warning">Menunggu konfirmasi hard copy</span>
                                                                @elseif($valuerequest->status==5)
                                                                <span class="label label-warning">Menunggu pembayaran hard copy</span>
                                                                @elseif($valuerequest->status==6)
                                                                <span class="label label-success">Hard copy sedang diproses</span>
                                                                @elseif($valuerequest->status==7)
                                                                <span class="label label-info">Hard copy sudah diterima</span>
                                                                @elseif($valuerequest->status==8)
                                                                <span class="label label-danger">Sewa dibatalkan</span>
                                                                @else
                                                                <span class="label label-danger">Masa sewa habis</span>
                                                                @endif
                                                            </td>

                                                            <td>
                                                                @if(Auth::user()->hasRole('admin')||Auth::user()->hasRole('superadmin')||Auth::user()->hasRole('cs'))
                                                                    @if($valuerequest->status==8||$valuerequest->status==0)
                                                                        <a href="javascript:void(0)" class="icon text-inverse btnacceptRent" id="{{$valuerequest->id}}" data-toggle="tooltip" data-placement="top" title="Terima Sewa"><i class="mdi mdi-check"></i></a>
                                                                    @endif
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="certificate" role="tabpanel">
                                        <div class="card-body">
                                            <h5>Sertifikat Tersedia <small>(Sertifikat yang telah dibuat)</small></h5>
                                            <div class="table-responsive">
                                                <table id="myTable"  class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Nomor Registrasi</th>
                                                            <th>Nama Sertifikat</th>
                                                            <th>Tanggal diterbitkan</th>
                                                            <th>Kualifikasi</th>
                                                            <th class="text-center">Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if($order->certificates->count()==0)
                                                        <tr>
                                                            <td colspan="4">TIdak ada data penyewaan</td>
                                                        </tr>
                                                        @endif
                                                        @foreach ($order->certificates as $valuecertificate)
                                                        <tr>
                                                            <td>
                                                                {{$valuecertificate->registration_number}}
                                                            </td>
                                                            <td>
                                                                {{$valuecertificate->name}}
                                                            </td>
                                                            <td class="text-center">
                                                                {{date_format($valuecertificate->date_of_registration,"j F Y G:H") }}
                                                            </td>
                                                            <td>
                                                                {{$valuecertificate->qualification}}
                                                            </td>
                                                            <td class="text-center">
                                                                @if($valuecertificate->status==0)
                                                                <span class="label label-warning">Belum diproses</span>
                                                                @elseif($valuecertificate->status==1)
                                                                <span class="label label-success">Soft copy dikirim</span>
                                                                @elseif($valuecertificate->status==2)
                                                                <span class="label label-info">Hardcopy dikirim</span>
                                                                @else
                                                                <span class="label label-danger">Masa sewa habis</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <hr>
                                            <h5>Sertifikat Diproses</h5>
                                            <div class="table-responsive">
                                                <table id="myTable"  class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Kualifikasi</th>
                                                            <th>Tipe Sertifikat</th>
                                                            <th>Kualifikasi</th>
                                                            @if($valuerequest->status==1)
                                                            <th></th>
                                                            <th>Biaya</th>
                                                            @endif
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $total_price=0;?>
                                                        @if($order->request_detail->count()==0)
                                                        <tr>
                                                            <td colspan="4">TIdak ada data penyewaan</td>
                                                        </tr>
                                                        @endif
                                                        @foreach ($order->request_detail as $valuerequest)
                                                        @if(!($valuerequest->status==0||$valuerequest->status==8||$valuerequest->status==9))
                                                        <tr>
                                                            <td>
                                                                {{$valuerequest->certificate_name}}
                                                            </td>
                                                            <td class="text-center">
                                                                @if($valuerequest->type_id==2)
                                                                <span class="label label-warning">SKT</span>
                                                                @else
                                                                <span class="label label-info">SKA</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                {{$valuerequest->qualification}}
                                                            </td>

                                                            @if($valuerequest->status==1)
                                                            <td class="text-left">
                                                                Rp. 
                                                            </td>
                                                            <td class="text-right">
                                                                @if($valuerequest->status < 4)
                                                                {{ number_format($valuerequest->certificate_price_soft,2,",",".") }}
                                                                <?php $total_price=$total_price+$valuerequest->certificate_price_soft; ?>
                                                                @elseif($valuerequest->status > 3 && $valuerequest->status < 8)
                                                                {{ number_format($valuerequest->certificate_price_hard,2,",",".") }}
                                                                <?php $total_price=$total_price+$valuerequest->certificate_price_hard; ?>
                                                                @else
                                                                -
                                                                @endif
                                                            </td>
                                                            @endif
                                                            <td>

                                                                @if(Auth::user()->hasRole('admin')||Auth::user()->hasRole('superadmin')||Auth::user()->hasRole('cs'))
                                                                    @if($valuerequest->status==2)
                                                                        <a href="javascript:void(0)" class="icon text-inverse btnsearchCertificate" id="{{$valuerequest->id}}" data-toggle="tooltip" data-placement="top" title="Cari Sertifikat"><i class="mdi mdi-file-find"></i></a>
                                                                        @elseif($valuerequest->status < 2)
                                                                        <a href="javascript:void(0)" class="icon text-danger btncancelRent" id="{{$valuerequest->id}}" data-toggle="tooltip" data-placement="top" title="Batalkan sewa"><i class="mdi mdi-close"></i></a>
                                                                    @endif
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @endif
                                                        @endforeach
                                                    </tbody>
                                                    @if($order->status==1)
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="3" class="text-right"><h6>Total</h6></td>
                                                            <td class="text-left">Rp.</td>
                                                            <input type="hidden" id="total_must_pay" name="total_must_pay" value="{{$total_price}}">
                                                            <td class="text-right">{{ number_format($total_price,2,",",".")}}</td>
                                                        </tr>
                                                    </tfoot>
                                                    @endif
                                                </table>
                                                
                                                @if(Auth::user()->hasRole('admin')||Auth::user()->hasRole('superadmin')||Auth::user()->hasRole('cs'))
                                                <a href="javascript::void(0)" class="btn btn-info btneditSendSoft" id="{{$order->id}}">Kirim Soft Copy</a>
                                                <a href="javascript::void(0)" class="btn btn-success btneditProcessHard" id="{{$order->id}}">Sewa hard copy</a>
                                                <a href="javascript::void(0)" class="btn btn-success btneditSendHard" id="{{$order->id}}">Konfirmasi Penerimaan Hard Copy</a>
                                                @if($order->status==0)
                                                <a href="javascript::void(0)" class="btn btn-danger btneditcancelRent" id="{{$order->id}}">Batalkan Sewa</a>
                                                @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- Column -->
                </div>

                @if(Auth::user()->hasRole('admin')||Auth::user()->hasRole('superadmin')||Auth::user()->hasRole('cs'))
                <!-- /Row -->
                <div class="row" id="searchSection">
                    <div class="col-sm-12 col-md-5 col-lg-5">
                            <div class="card">
                                <div class="card-body">
                                            <div class="d-flex align-items-center flex-row m-t-30">
                                                <div class="p-3 display-6 text-info"><span>REQ.2.302</span></div>
                                            </div>
                                            <table class="table no-border">
                                                <tbody>
                                                    <tr>
                                                        <td width="40%">Jenis Sertifikat</td>
                                                        <td class="font-medium"><span class="label label-info">SKA</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Keahlian / Keterampilan</td>
                                                        <td class="font-medium">Ahli Arsitekur Lansekap</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Jenjang Pendidikan</td>
                                                        <td class="font-medium text-success">2013 S1 - Desain Interior<i class="mdi mdi-check"></i></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Instansi</td>
                                                        <td class="font-medium text-success">PT harapan bahagia - Asisten Perancangan Interior <i class="mdi mdi-check"></i></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                </div>
                            </div>
                    </div>
                    <div class="col-sm-12 col-md-7 col-lg-7">
                        <div class="card">
                                <div class="card-body">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="keyword"  placeholder="Masukkan Kata Kunci">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-info" type="button" id="searchCertificateButton">Cari!</button>
                                                    </div>
                                                </div>
                                           <div class="table-responsive m-t-40">
                                                <table class="table stylish-table" id="myTable">
                                                    <thead>
                                                        <tr>
                                                            <th width="20%">Nomor Registrasi</th>
                                                            <th width="20%">Nama Sertifikat </th>
                                                            <th width="20%">Klasifikasi </th>
                                                            <th width="5%" class="text-center">Kualifikasi</th>
                                                            <th width="10%" class="text-center"></th>
                                                        </tr>
                                                    </thead>
                                                <tbody id="searchInsideResult">
                                                    <tr>
                                                        <td>123.123.123.123</td>
                                                        <td>Buman anda</td>
                                                        <td>Gudang penyimpanan</td>
                                                        <td>Muda</td>
                                                        <td><a href="javascript:void(0)" class="icon text-success btncancelRent" id="" data-toggle="tooltip" data-placement="top" title="Ganti permohonan"><i class="mdi mdi-bookmark-check"></i></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                            </div>
                    </div>
                    
                </div>
                @endif
                <!-- Modal Process Payment -->
                <div class="modal fade" id="modalProcessPayment" tabindex="-1" role="dialog" aria-labelledby="modalProcessPaymentLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            {!! Form::open(array('route' => 'payment.store','method'=>'POST','id'=>'createpayment','class'=>'form-horizontal ')) !!}
                                        <div class="row" id="editSectionProcessPayment">

                                        </div>
                            <input type="hidden" name="order_id"  id="order_id" >
                            <div class="modal-header">
                                <h4 class="modal-title" id="modalProcessPaymentLabel">Buat pembayaran untuk sewa sertifikat <span class="section_payment">softcopy</span></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                    <div class="form-group soft_copy_payment">
                                        <label for="recipient-name" class="control-label">Metode pembayaran:</label>
                                        <select class="custom-select pull-right" name="payment_method_soft" id="payment_method_soft">
                                            <option value="" selected>Pilih metode pembayaran...</option>
                                            <option value="langsung">Langsung</option>
                                            <option value="transfer">Bank Transfer</option>
                                        </select>
                                    </div>
                                    <div class="form-group soft_copy_payment">
                                        <label for="message-text" class="control-label">Detail pembayaran:</label>
                                            <textarea class="form-control" id="payment_detail_soft" name="payment_detail_soft"></textarea>
                                    </div>

                                    <div class="form-group soft_copy_payment">
                                        <label for="message-text" class="control-label">Bukti Bayar Softcopy</label>
                                        <input type="file" id="input-file-now" class="dropify" name="prove_phase_soft_copy"  id="prove_phase_soft_copy" />
                                    </div>
                                    <div class="form-group soft_copy_payment">
                                        <label for="message-text" class="control-label">Total:</label>
                                        <input type="number" class="form-control" id="total_soft" name="total_soft">
                                    </div>

                                    <div class="form-group hard_copy_payment">
                                        <label for="recipient-name" class="control-label">Metode pembayaran:</label>
                                        <select class="custom-select pull-right" name="payment_method_hard" id="payment_method_hard">
                                            <option value="" selected>Pilih metode pembayaran...</option>
                                            <option value="langsung">Langsung</option>
                                            <option value="transfer">Bank Transfer</option>
                                        </select>
                                    </div>
                                    <div class="form-group hard_copy_payment">
                                        <label for="message-text" class="control-label">Detail pembayaran:</label>
                                            <textarea class="form-control" id="payment_detail_hard" name="payment_detail_hard"></textarea>
                                    </div>

                                    <div class="form-group hard_copy_payment">
                                        <label for="message-text" class="control-label">Bukti Bayar Hardcopy</label>
                                        <input type="file" id="input-file-now" class="dropify" name="prove_phase_hard_copy"  id="prove_phase_soft_copy" />
                                    </div>
                                    <div class="form-group hard_copy_payment">
                                        <label for="message-text" class="control-label">Total:</label>
                                        <input type="number" class="form-control" id="total_hard" name="total_hard">
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <!-- Modal SendSoftFile -->

                <div class="modal fade" id="modalEditSoftCopy" tabindex="-1" role="dialog" aria-labelledby="modalEditSoftCopyLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modalEditSoftCopyLabel">Informasi sertifikat sewa </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            {!! Form::open(array('route' => 'payment.store','method'=>'POST','id'=>'EditSoftCopy','class'=>'form-horizontal ')) !!}
                            <div class="row" id="editSectionEditSoftCopy">

                            </div>
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Nomor Seri/Registrasi:</label>
                                        <input type="text" class="form-control" id="serial_number" name="serial_number">
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <!-- Modal SendHardFile -->

                <div class="modal fade" id="modalSendHardCopy" tabindex="-1" role="dialog" aria-labelledby="SendHardCopyLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="SendHardCopyLabel">Konfirmasi serah terima sertifikat hardcopy</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            {!! Form::open(array('route' => 'payment.store','method'=>'POST','id'=>'sendHardCopy','class'=>'form-horizontal ')) !!}
                                        <div class="row" id="editSectionsendHardCopy">

                                        </div>
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label for="pick_up_date" class="control-label">Tanggal Pengambilan:</label>
                                        <input type="text" class="form-control datepicker-autocloserent" id="pick_up_date" name="pick_up_date">
                                    </div>
                                    <div class="form-group">
                                        <label for="pick_up_by" class="control-label">Diterima oleh:</label>
                                        <input type="text" class="form-control" id="pick_up_by" name="pick_up_by">
                                    </div>
                                    <div class="form-group">
                                        <label for="exact_return_date" class="control-label">Tanggal Pengembalian:</label>
                                        <input type="text" class="form-control datepicker-autocloserent" id="exact_return_date" name="exact_return_date">
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Konfirmasi</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modalReturnHardCopy" tabindex="-1" role="dialog" aria-labelledby="ReturnHardCopyLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="ReturnHardCopyLabel">Konfirmasi pengembalian sertifikat hardcopy</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            {!! Form::open(array('route' => 'payment.store','method'=>'POST','id'=>'returnHardCopy','class'=>'form-horizontal ')) !!}
                                        <div class="row" id="editSectionreturnHardCopy">

                                        </div>
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label for="return_date" class="control-label">Dikembalikan pada tanggal:</label>
                                        <input type="text" class="form-control datepicker-autocloserent" id="return_date" name="return_date">
                                    </div>
                                    <div class="form-group">
                                        <label for="return_date" class="control-label">Dikembalikan oleh:</label>
                                        <input type="text" class="form-control" id="return_by" name="return_by">
                                    </div>
                            </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Konfirmasi</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>


                <!-- Modal uploadBukti -->

                <div class="modal fade" id="modaluploadSoftProve" tabindex="-1" role="dialog" aria-labelledby="modaluploadProveSoftLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modaluploadProveSoftLabel">Upload Bukti Bayar SoftCopy</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            {!! Form::open(array('route' => 'payment.store','method'=>'POST','id'=>'uploadProveSoft','class'=>'form-horizontal ')) !!}
                                        <div class="row" id="editSectionUploadProveSoft">

                                        </div>
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Bukti Bayar Softcopy</label>
                                        <input type="file" id="input-file-now" class="dropify" name="prove_phase_soft_copy"  id="prove_phase_soft_copy" />
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Kirim</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="modaluploadHardProve" tabindex="-1" role="dialog" aria-labelledby="modaluploadProveHardLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modaluploadProveHardLabel">Upload Bukti Bayar Hardcopy</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            {!! Form::open(array('route' => 'payment.store','method'=>'POST','id'=>'uploadProveHard','class'=>'form-horizontal ')) !!}
                                        <div class="row" id="editSectionUploadProveHatd">

                                        </div>
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Bukti Bayar Hardcopy</label>
                                        <input type="file" id="input-file-now" class="dropify" name="prove_phase_hard_copy"  id="prove_phase_hard_copy" />
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Kirim</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <!-- sample modal content -->
                <div id="myModalerror" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Terjadi Kesalahan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Terjadi kesalahan dalam proses pengiriman data, coba beberapa saat lagi atau hubungi admin</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">OK</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <!-- sample modal content -->
                <div id="myModalcancelOrder" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Batalkan Order</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Anda yakin membatalkan order ini ?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">Batal</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmcancelorder">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <!-- sample modal content -->
                <div id="myModalcancelRent" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Batalkan Sewa</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Anda yakin membatalkan Sewa ini ?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">Batal</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmcancelrent">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <!-- sample modal content -->
                <div id="myModaldeleteOrder" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Hapus Order</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Anda yakin menghapus Order ini?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Batal</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmdelete">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <!-- sample modal content -->
                <div id="myModalacceptRent" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Terima Permohonan Sewa</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Anda yakin menerima permohonan sewa sertifikat ini?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Batal</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmacceptRent">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <!-- sample modal content -->
                <div id="myModalfailedSend" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Terjadi Kesalahan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p id="failedmsg"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">OK</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <!-- sample modal content -->
                <div id="myModalSendSoftCopy" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalSendSoftCopyLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModaSendSoftCopylLabel">Kirim Soft Copy</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p id="failedmsg">Anda yakin akan mengirim File Soft Copy yang sudah diproses?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">Batalkan</button>
                                <button type="button" class="btn btn-info waves-effect " id="btnconfirmSendSoftCopy">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <!-- sample modal content -->
                <div id="myModalProcessHardCopy" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalProcessHardCopyLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalProcessHardCopylLabel">Konfirmasi Sewa Hard Copy</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p id="failedmsg">Anda yakin melakukan proses sewa hard copy sertifikat sewa yang dipilih?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">Batalkan</button>
                                <button type="button" class="btn btn-info waves-effect " id="btnconfirmProcessHardCopy">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
@endsection
