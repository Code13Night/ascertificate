@extends('backend.layouts.app')


@section('title')
Permintaan Penyewaan
@endsection

@section('css')


    <!-- Popup CSS -->
    <link href="{{ asset('backend/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet">
@endsection
@section('js')
    
    <!-- Magnific popup JavaScript -->
    <script src="{{ asset('backend/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('backend/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script>
@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Detail Sewa</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Sewa</a></li>
                            <li class="breadcrumb-item "><a href="/rent-request">Permohonan Sewa</a></li>
                            <li class="breadcrumb-item active">{{$rent->serial_number}}</li>
                        </ol>
                    </div>
                </div>
@endsection

@section('content')
                <div class="row">
                    
                    <!-- /Column -->
                    <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs profile-tab" role="tablist">
                                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#general_info" role="tab">Informasi Umum</a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#picture" role="tab">Sertifikat</a> </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general_info" role="tabpanel">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center flex-row m-t-30">
                                                <div class="p-2 display-5 text-info"><span>{{ $rent->serial_number }}</span></div>
                                            </div>
                                            <table class="table no-border">
                                                <tbody>
                                                    <tr>
                                                        <td width="40%">Atas Nama</td>
                                                        <td class="font-medium">{{ $rent->user_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Sertifikat</td>
                                                        <td class="font-medium">{{ $rent->profession_name.' - '.$rent->certificate_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Jenis Sertifikat</td>
                                                        <td class="font-medium">{{ $rent->certificate_type_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Kualifikasi</td>
                                                        <td class="font-medium">{{ $rent->qualification }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Status</td>
                                                        <td class="font-medium">
                                                                @if($rent->status==0)
                                                                <label class="label label-warning">Menunggu konfirmasi admin</label>
                                                                @elseif($rent->status==1)
                                                                <label class="label label-warning">Menunggu Pembayaran</label>
                                                                @elseif($rent->status==2)
                                                                <label class="label label-success">Soft copy sedang diproses</label>
                                                                @elseif($rent->status==3)
                                                                <label class="label label-info">Soft copy sudah dikirim</label>
                                                                @elseif($rent->status==4)
                                                                <label class="label label-warning">Menunggu konfirmasi hard copy</label>
                                                                @elseif($rent->status==5)
                                                                <label class="label label-warning">Menunggu pembayaran hard copy</label>
                                                                @elseif($rent->status==6)
                                                                <label class="label label-success">Hard copy sedang diproses</label>
                                                                @elseif($rent->status==7)
                                                                <label class="label label-info">Hard copy sudah diterima</label>
                                                                @elseif($rent->status==8)
                                                                <label class="label label-danger">Sewa dibatalkan</label>
                                                                @else
                                                                <label class="label label-danger">Masa sewa habis</label>
                                                                @endif
                                                    </tr>
                                                    @if($rent->status==3||$rent->status==4||$rent->status==5)
                                                    <tr>
                                                        <td width="40%">Tanggal Berlaku</td>
                                                        <td class="font-medium">{{ date("d-m-Y", strtotime($rent->date_of_issued))." sampai ".  date("d-m-Y", strtotime($rent->valid_until) )}}</td>
                                                    </tr>
                                                    @endif
                                                    <tr>
                                                        <td width="40%"><b>Informasi Pendidikan</b></td>
                                                        <td class="font-medium"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Jenjang Pendidikan</td>
                                                        <td class="font-medium">{{ $rent->education_level_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Jurusan</td>
                                                        <td class="font-medium">{{ $rent->study_program_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Tahun Lulus</td>
                                                        <td class="font-medium">{{ $rent->graduate_year }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">Catatan</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">@if($rent->note==""||$rent->note==null)
                                                            Tidak ada catatan tambahan 
                                                            @else
                                                            <blockquote>
                                                            {{$rent->note}}    
                                                            </blockquote>
                                                            @endif</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                                @if($rent->status==0||$rent->status==1||$rent->status==2||$rent->status==4||$rent->status==5)
                                                <a href="javascript::void(0)" class="btn btn-warning btncancelOrder" id="{{$rent->id}}">Batalkan Sewa</a>
                                                @endif

                                                @if($rent->status==8)
                                                <a href="javascript::void(0)" class="btn btn-danger btndeleteOrder" id="{{$rent->id}}">Hapus Sewa</a>
                                                @endif
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="picture" role="tabpanel">
                                        <div class="card-body">
                                            @if($rent->status==3||$rent->status==4||$rent->status==5||$rent->status==6||$rent->status==7)
                                            <div class="row m-t-60">
                                                <div class="col-md-12">
                                                    @if($rent->certificate_image_thumb!=null)
                                                    <a class="image-popup-no-margins" href="/{{$rent->soft_file}}"> <img src="/{{$rent->soft_file}}" alt="image" height="400px" width="auto" > </a>
                                                    <h6 class="m-t-10"></h6> </div>
                                                    @else

                                                    @if(!($rent->certificate_image_name==null||$certificate->certificate_image_name==""))
                                                    <a target="_blank" href="/{{$rent->certificate_image_path}}">Lihat Sertifikat</a>
                                                    @else
                                                    <h4 class="card-title">Sertifikat belum tersedia</h4>
                                                    <p class="card-text">Harap Hubungi admin</p>
                                                    @endif
                                                    @endif
                                            </div>
                                            @else
                                                <h4 class="card-title">Sertifikat Belum diterbitkan</h4>
                                                <p class="card-text">Harap Hubungi admin</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- Column -->
                </div>
                
@endsection
