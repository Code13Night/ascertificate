@extends('backend.layouts.app')


@section('title')
Permintaan Penyewaan
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.css') }}" />

    <link href="{{ asset('backend/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/dropify/dist/css/dropify.min.css') }}">

@endsection
@section('js')

    <!-- This is data table -->
    <script src="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="{{ asset('backend/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <!-- end - This is for export functionality only -->
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{ asset('backend/assets/plugins/html5-editor/wysihtml5-0.3.0.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <script>
        $('.dropify').dropify();

        $('.textarea_editor').wysihtml5();
        $(".select2").select2({
                width: "100%"
            });
        tablestate='request';
        var table=$('#myTable').DataTable({iDisplayLength: 10,
            responsive: true,
            lengthMenu: [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "order": [
                [0, 'asc']
            ],
            "ajax": '/webApi/allrentsbystatus-request',
            "columns": [
                    { "data": "serial_number" },
                    { "data": "user_name" },
                    { "data": "certificate_name" },
                    { "data": "type_name" },
                    { "data": "status" },
                    { "data": "id" },
            ], "columnDefs": [{
                "targets": 5,
                "data": "Action_Link",
                orderable: false,
                className: "text-center",
                "render": function (data, type, full, meta) {
                    editSection='';
                    switch(tablestate)
                      {
                        case 'request':
                            editSection='<a class="dropdown-item btneditProcessPay" href="javascript:void(0)" id=' + data + '  data-toggle="modal" data-target="#modalProcessPayment">Proses ke pembayaran</a>';
                            break;
                        case 'waiting_for_payment':
                            editSection='';
                            break;
                        case 'process_soft_copy':
                            editSection='<a class="dropdown-item btneditSendSoft" href="javascript:void(0)" id=' + data + ' data-toggle="modal" data-target="#modalSendSoftCopy">Kirim soft copy</a>';
                            break;
                        case 'process_hard_copy':
                            editSection='<a class="dropdown-item btneditSendHard" href="javascript:void(0)" id=' + data + ' data-toggle="modal" data-target="#modalSendHardCopy">Kirim hard copy</a>';
                            break;
                        default:
                            editSection='<a class="dropdown-item btneditProcessPay" href="javascript:void(0)" id=' + data + '   data-toggle="modal" data-target="#modalProcessPayment">Proses ke pembayaran</a>';
                            break;
                      }
                    return '<div class="btn-group">'+
                                    '<button type="button" class="btn btn-sm btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                        '<i class="ti-settings"></i>'+
                                    '</button>'+
                                    '<div class="dropdown-menu dropdown-menu-right">'+
                                        '<a class="dropdown-item btnshow" href="/rent-detail/'+data+'" id=' + data + ' >Tampilkan detail</a>'+
                                        editSection+
                                        '<div class="dropdown-divider"></div>'+
                                        '<a class="dropdown-item btncancel" id=' + data + ' >Batalkan pemesanan</a>'+
                                        '<div class="dropdown-divider"></div>'+
                                        '<a class="dropdown-item btndelete" href="javascript:void(0)" id=' + data + ' data-toggle="modal" data-target="#myModal">Hapus</a>'+
                                    '</div>'+
                                '</div>';

                }
            },{
                "targets": 0,
                className: "text-center",
                "render": function ( data, type, row ) {
                    
                    return '<a href="/rent-serial_number/'+data+'"  id=' + data + ' class="btnshowSerialNumber" >'+data+'</a>';
                }
            },{
                "targets": 4,
                className: "text-center",
                "render": function ( data, type, row ) {
                    if(data==1||data==0||data==5){
                        return '<span class="label label-danger">Belum dibayar</span>';
                    }else if(data==2||data==3){
                        return '<span class="label label-success">Soft copy sudah dibayar</span>';
                    }else {
                        return '<span class="label label-info">Sudah dibayar</span>';
                    }
                }
            },{
                "targets": 3,
                "data": "active",
                className: "text-center",
                "render": function (data, type, full, meta) {
                    if(data=="SKA"){
                        return '<span class="label label-info">'+data+'</span>';
                    }else{
                        return '<span class="label label-warning">'+data+'</span>';
                    }
                }
            },
            ],
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
            
        });
        //PRocess Payment
        $('#myTable tbody').on('click', '.btneditProcessPay', function (e) {
            e.preventDefault();
            $('#rent_id').val(this.id);
        });
        //Post new Payment
        $('#createpayment').submit(function (e) {
            //prevent from action submit default
            e.preventDefault();
            //Inserting data from Form to JSON format
            var datastring = $("#createpayment").serialize();
            //get action URL
            var $form = $(this),
              url = $form.attr("action");
            //ajax POST
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                data: datastring,
                success: function (data) { 
                    $(data).each(function (index, item) {
                        if(item.success){
                            table.ajax.reload();
                            $('#modalProcessPayment').modal('toggle');
                            $('#total').val('');
                            $('#payment_method').val('');
                            $('#payment_detail').val('');
                        }else{
                            $('#modalProcessPayment').modal('toggle');
                            $('#myModalerror').modal('toggle');
                        }
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    $('#modalProcessPayment').modal('toggle');
                    $('#myModalerror').modal('toggle');
                }                   
            });  
        });
        //Send Soft Copy
        $('#myTable tbody').on('click', '.btneditSendSoft', function (e) {
            e.preventDefault();
             $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/paymentbyRentID/'+this.id,
                success: function (data) {

                    $(data).each(function (index, item) {
                        $(item.data).each(function (indexx, itemx) {
                            $('#image_prove_soft').attr("src",itemx.prove_phase_soft_copy);

                            $(itemx.rent).each(function (indexxx, itemxx) {
                                $('#serial_number').val(itemxx.serial_number);
                            });
                        });
                    });
                }
            });
            $('#editSectionsendSoftCopy').append('<input name="_method" type="hidden" value="PATCH">');
            $("#sendSoftCopy").attr("action", "/webApi/rent-status/soft_sended/" + this.id);
        });
        //Post SoftCopy
        $('#sendSoftCopy').submit(function (e) {
            //prevent from action submit default
            e.preventDefault();
            //Inserting data from Form to JSON format
            var datastring = new FormData($(this)[0]);
            //get action URL
            var $form = $(this),
              url = $form.attr("action");
            //ajax POST
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                data: datastring,
                success: function (data) { 
                    $(data).each(function (index, item) {
                        if(item.success){
                            table.ajax.reload();
                            $('#modalSendSoftCopy').modal('toggle');
                            $('#serial_number').val('');
                            $('#soft_file').val('');
                        }else{
                            $('#modalSendSoftCopy').modal('toggle');
                            $('#myModalerror').modal('toggle');
                        }
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    $('#modalSendSoftCopy').modal('toggle');
                    $('#myModalerror').modal('toggle');
                }                   
            });  
        });
        //Send Hard Copy
        $('#myTable tbody').on('click', '.btneditSendHard', function (e) {
            e.preventDefault();
             $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/paymentbyRentID/'+this.id,
                success: function (data) {

                    $(data).each(function (index, item) {
                        $(item.data).each(function (indexx, itemx) {
                            $('#image_prove_hard').attr("src",itemx.prove_phase_hard_copy);

                            $(itemx.rent).each(function (indexxx, itemxx) {
                                $('#serial_number').val(itemxx.serial_number);
                            });
                        });
                    });
                }
            });
            $('#editSectionsendHardCopy').append('<input name="_method" type="hidden" value="PATCH">');
            $("#sendHardCopy").attr("action", "/webApi/rent-status/hard_sended/" + this.id);
        });
        //Post HardCopy
        $('#sendHardCopy').submit(function (e) {
            //prevent from action submit default
            e.preventDefault();
            //Inserting data from Form to JSON format
            var datastring = new FormData($(this)[0]);
            //get action URL
            var $form = $(this),
              url = $form.attr("action");
            //ajax POST
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                data: datastring,
                success: function (data) { 
                    $(data).each(function (index, item) {
                        if(item.success){
                            table.ajax.reload();
                            $('#modalSendHardCopy').modal('toggle');
                            $('#total').val('');
                            $('#payment_method').val('');
                            $('#payment_detail').val('');
                        }else{
                            $('#modalSendHardCopy').modal('toggle');
                            $('#myModalerror').modal('toggle');
                        }
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    $('#modalSendHardCopy').modal('toggle');
                    $('#myModalerror').modal('toggle');
                }                   
            });  
        });
        $('#select_rent').on('change', function() {
          switch(this.value)
          {
            case '0':
                $reloadurl='request';
                break;
            case '1':
                $reloadurl='waiting_for_payment';
                break;
            case '2':
                $reloadurl='process_soft_copy';
                break;
            case '3':
                $reloadurl='process_hard_copy';
                break;
            default:
                $reloadurl='request';
                break;
          }

            tablestate=$reloadurl;
            table.ajax.url( '/webApi/allrentsbystatus-'+$reloadurl ).load();
        });
    </script>
@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Permintaan Sewa</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Sertifikat</a></li>
                            <li class="breadcrumb-item active">Permintaan Sewa</li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>Sewa Bulan Ini</small></h6>
                                    <h4 class="m-t-0 text-info">30</h4></div>
                                <div class="spark-chart">
                                    <div id="monthchart"></div>
                                </div>
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>Pendapatan Bulan Ini</small></h6>
                                    <h4 class="m-t-0 text-primary">Rp. 40.000.000,00</h4></div>
                                <div class="spark-chart">
                                    <div id="lastmonthchart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('content')
                
                <!-- Row -->
                <div class="row" id="tableSection">

                    
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-5 col-lg-5">
                                        <h4 class="card-title">Request Sewa</h4>
                                        <h6 class="card-subtitle">Proses Request Sewa Sertifikat</h6>
                                    </div>
                                    <div class="col-sm-12 col-md-7 col-lg-7">
                                        <select class="custom-select pull-right" name="select_rent" id="select_rent">
                                            <option value="0" selected>Permohonan sewa</option>
                                            <option value="1">Proses pembayaran soft copy</option>
                                            <option value="2">Proses pembuatan soft copy</option>
                                            <option value="3">Proses pembuatan hard copy</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="20%">Nomor Seri</th>
                                                <th width="20%">User</th>
                                                <th width="20%">Sertifikat</th>
                                                <th width="10%">Tipe Sertifikat</th>
                                                <th width="10%">Status Pembayaran</th>
                                                <th width="10%" class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal Process Payment -->
                <div class="modal fade" id="modalProcessPayment" tabindex="-1" role="dialog" aria-labelledby="modalProcessPaymentLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            {!! Form::open(array('route' => 'payment.store','method'=>'POST','id'=>'createpayment','class'=>'form-horizontal ')) !!}
                            <input type="hidden" name="rent_id"  id="rent_id" >
                            <div class="modal-header">
                                <h4 class="modal-title" id="modalProcessPaymentLabel">Buat pembayaran untuk sewa sertifikat</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Metode pembayaran:</label>
                                        <select class="custom-select pull-right" name="payment_method" id="payment_method">
                                            <option value="" selected>Pilih metode pembayaran...</option>
                                            <option value="langsung">Langsung</option>
                                            <option value="transfer">Bank Transfer</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Detail pembayaran:</label>
                                            <textarea class="form-control" id="payment_detail" name="payment_detail"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Total:</label>
                                        <input type="number" class="form-control" id="total" name="total">
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <!-- Modal SendSoftFile -->

                <div class="modal fade" id="modalSendSoftCopy" tabindex="-1" role="dialog" aria-labelledby="modalSendSoftCopyLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modalSendSoftCopyLabel">Kirim soft copy sertifikat </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            {!! Form::open(array('route' => 'payment.store','method'=>'POST','id'=>'sendSoftCopy','class'=>'form-horizontal ')) !!}
                                        <div class="row" id="editSectionsendSoftCopy">

                                        </div>
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Bukti Pembayaran</label>
                                         <img class="" id="image_prove_soft" src="{{ asset('images/thumb/not-found.jpg') }}" style="width: 100%" alt="img" class="img-responsive">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Nomor Seri:</label>
                                        <input type="text" class="form-control" id="serial_number" name="serial_number">
                                    </div>
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Sertifikat</label>
                                        <input type="file" id="input-file-now" class="dropify" name="soft_file"  id="soft_file" />
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Kirim</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <!-- Modal SendHardFile -->

                <div class="modal fade" id="modalSendHardCopy" tabindex="-1" role="dialog" aria-labelledby="SendHardCopyLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="SendHardCopyLabel">Konfirmasi pengiriman sertifikat hardcopy</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            {!! Form::open(array('route' => 'payment.store','method'=>'POST','id'=>'sendHardCopy','class'=>'form-horizontal ')) !!}
                                        <div class="row" id="editSectionsendHardCopy">

                                        </div>
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Bukti Pembayaran Hard Copy</label>
                                         <img class="" id="image_prove_hard" src="{{ asset('images/thumb/not-found.jpg') }}" style="width: 100%" alt="img" class="img-responsive">
                                    </div>
                                    <p>Pembayaran telah dilunasi <br> konfirmasi serah terima / pengiriman  hard copy sertifikat sewa?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Konfirmasi</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <!-- sample modal content -->
                <div id="myModalerror" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Terjadi Kesalahan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Terjadi kesalahan dalam proses pengiriman data, coba beberapa saat lagi atau hubungi admin</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">OK</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
@endsection
