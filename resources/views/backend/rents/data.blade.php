@foreach($rent as $rentvalue)
                	<div class="col-md-4 col-sm-6 col-lg-4">
						<div class="card blog-widget">
                            <div class="card-body">
                                <h3>{{$rentvalue->classification_code." - ". $rentvalue->certificate_name}} </h3>
                                <h5>Nomor Seri: <strong>{{$rentvalue->serial_number }}</strong></h5>
                                @if($rentvalue->type_name=="SKA")
                                                    <label class="label label-rounded label-info">
                                                @else
                                                    <label class="label label-rounded label-warning">
                                                @endif
                                {{ $rentvalue->type_name." - ".$rentvalue->qualification }}
                            	</label>
                                <p class="m-t-20 m-b-20">
                                Status: <b>
                                                                @if($rentvalue->status==0)
                                                                <span class="text-warning">Menunggu konfirmasi admin</span>
                                                                @elseif($rentvalue->status==1)
                                                                <span class="text-warning">Menunggu pembayaran soft copy</span>
                                                                @elseif($rentvalue->status==2)
                                                                <span class="text-success">Soft copy sedang diproses</span>
                                                                @elseif($rentvalue->status==3)
                                                                <span class="text-info">Soft copy sudah dikirim</span>
                                                                @elseif($rentvalue->status==4)
                                                                <span class="text-warning">Menunggu konfirmasi hard copy</span>
                                                                @elseif($rentvalue->status==5)
                                                                <span class="text-warning">Menunggu pembayaran hard copy</span>
                                                                @elseif($rentvalue->status==6)
                                                                <span class="text-success">Hard copy sedang diproses</span>
                                                                @elseif($rentvalue->status==7)
                                                                <span class="text-info">Hard copy sudah diterima</span>
                                                                @elseif($rentvalue->status==8)
                                                                <span class="text-danger">Sewa dibatalkan</span>
                                                                @else
                                                                <span class="text-danger">Masa sewa habis</span>
                                                                @endif
                                                            </b> 
                                </p>
                                <div class="d-flex float-right">
                                    <div class="read "><a href="/certificate-code/{{$rentvalue->classification_code}}" class="link font-medium">Selengkapnya</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
@endforeach