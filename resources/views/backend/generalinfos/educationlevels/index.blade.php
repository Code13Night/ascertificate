@extends('backend.layouts.app')


@section('title')
Admin
@endsection

@section('css')


@endsection
@section('js')
    <!-- This is data table -->
    <script src="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
        var table=$('#myTable').DataTable({iDisplayLength: 10,
            responsive: true,
            lengthMenu: [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "order": [
                [0, 'asc']
            ],
            "ajax": '/webApi/alleducation-levels',
            "columns": [
                    { "data": "name" },
                    { "data": "total" },
            ], "columnDefs": [{
                "targets": 1,
                "data": "active",
                className: "text-center",
            }
            ],
            
        });
    </script>
@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Tingkat Pendidikan</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Informasi Umum</a></li>
                            <li class="breadcrumb-item active">Tingkat Pendidikan</li>
                        </ol>
                    </div>
                </div>
@endsection

@section('content')
                



                <!-- Row -->
                <div class="row" id="tableSection">

                    
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Tingkat Pendidikan</h4>
                                <h6 class="card-subtitle">Informasi jumlah Sertifikat berdasarkan tingkat pendidikan</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="30%">Nama</th>
                                                <th width="30%">Jumlah Sertifikat Sewa</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

@endsection
