@extends('backend.layouts.app')


@section('title')
Notifikasi
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.css') }}" />
    <link href="{{ asset('backend/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />


@endsection
@section('js')
    <!-- This is data table -->
    <script src="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="{{ asset('backend/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/assets/plugins/html5-editor/wysihtml5-0.3.0.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.js') }}"></script>
    <!-- end - This is for export functionality only -->
    <script>
        $('.textarea_editor').wysihtml5();
        $(".select2").select2({
                width: "100%"
            });
        var table=$('#myTable').DataTable({iDisplayLength: 10,
            responsive: true,
            lengthMenu: [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "order": [
                [3, 'desc']
            ],
            "ajax": '/webApi/allnotifications',
            "columns": [
                    { "data": "title" },
                    { "data": "body" },
                    { "data": "user_type" },
                    { "data": "id" },
            ], "columnDefs": [{
                "targets": 3,
                "data": "Action_Link",
                orderable: false,
                className: "text-center",
                "render": function (data, type, full, meta) {
                    return '<a href="javascript:void(0)" id=' + data + ' data-toggle="tooltip" title="Duplicate" class="btnedit icon text-warning"><i class="mdi mdi-content-duplicate"></i></a>' +
                    		'<a href="javascript:void(0)" id=' + data + ' data-toggle="modal" data-target="#myModal2"  title="Resend" class="btnresend icon text-info"><i class="mdi mdi-reply"></i></a>' +
                                     '  <a  href="javascript:void(0)" id=' + data + ' data-toggle="modal" data-target="#myModal" title="Delete" class="btndelete icon text-danger"><i class="mdi mdi-delete"></i></a>';

                }
            }
            ],
        });
        $('.hideadd').on('click', function (e) {
            $('#createSection').hide('fast');
        });
        $('.btncreate').on('click', function (e) {
            $('#createSection').removeClass('hide');
            $('#createSection').show('slow');
            $("#createuser").attr("action", "/webApi/notification/create");
            $('#editSection input').remove();
        });
        //Post new User
        $('#createuser').submit(function (e) {
            //prevent from action submit default
            e.preventDefault();
            //Inserting data from Form to JSON format
            var datastring = $("#createuser").serialize();
            //get action URL
            var $form = $(this),
              url = $form.attr("action");
            //ajax POST
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                data: datastring,
                success: function (data) {
                    $(data).each(function (index, item) {
                        if(item.success){
                            table.ajax.reload();
                            $('#body').data("wysihtml5").editor.setValue('');                        
                            $('#title').val('');
                            $('#user_id').val('').trigger('change');
                            $('#title').focus();
                            $('#createSection').hide('fast');
                            $('#editSection input').remove();
                        }else{
                            $('#myModalerror').modal('toggle');
                        }
                    });     
                }
            });
        });
        //Delete Confirmation
        $('#myTable tbody').on('click', '.btndelete', function (e) {
            e.preventDefault();
            $iddel=this.id;
        });
        //Delete Confirmation
        $('#myTable tbody').on('click', '.btnresend', function (e) {
            e.preventDefault();
            $iddel=this.id;
        });
        //edit value
        $('#myTable tbody').on('click', '.btnedit', function (e) {
            e.preventDefault();
            $idshow=this.id;
            $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/notification/'+$idshow,
                success: function (data) {
                    $(data).each(function (index, item) {
                        $(item.data).each(function (indexx, itemx) {

                            $('#body').data("wysihtml5").editor.setValue(itemx.body);                        
                            $('#title').val(itemx.title);
                            if(itemx.user_type=="android"){
                            	$('#basic_checkbox_1').prop('checked', true);
                            }else{
                            	$('#basic_checkbox_1').prop('checked', false);
                            	$('#user_id').val(itemx.user_id).trigger('change');
                            }
                            $('#title').focus();
                            $('#type').val(itemx.type);
                        });
                    });                     
                    $('#createSection').show('slow');
                }
            });
        });
        $('#basic_checkbox_1').on('click',function (e){
			if ($(this).is(':checked')) {
                $('#user_id').select2().prop('disabled', true);
                $('#user_id').val('').trigger('change');
            }else{
                $('#user_id').select2().prop('disabled', false);
            }
        });
        //Delete Confirmation
        $('#btnconfirmdelete').on('click', function (e) {
            $('#myModal').modal('toggle');
            
            $.ajax({
                cache: false,
                type: 'DELETE',
                url: '/webApi/notification/'+$iddel,
                data: { _method: 'DELETE', _token: $('input[name=_token]').val() },
                success: function (data) {
                    table.ajax.reload();
                }
            });
        });
        //Delete Confirmation
        $('#btnconfirmresend').on('click', function (e) {
            $('#myModal2').modal('toggle');
            
            $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/notification-resend/'+$iddel,
                success: function (data) {

                }
            });
        });
    </script>
@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Notifikasi</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active"><a href="javascript:void(0)">Extra</a></li>
                            <li class="breadcrumb-item active">Notifikasi</li>
                        </ol>
                    </div>
                    
                </div>
@endsection

@section('content')
                
                <!-- Row -->
                <div class="row hide" id="createSection">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">

                            <div class="card-header">
                                <button class="pull-right btn btn-sm btn-rounded btn-info hideadd"  data-toggle="tooltip" title="Tutup form notifikasi">X</button>
                                <h4 class="m-b-0 text-white" id="tileCreateSection">Kirim Notifikasi</h4>
                            </div>
                            <div class="card-body">
                                {!! Form::open(array('route' => 'notification.store','method'=>'POST','id'=>'createuser','class'=>'form-horizontal ')) !!}
                                    <div class="form-body">
                                        <div class="row" id="editSection">

                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Judul</label>
                                                    <div class="col-md-5">
                                                        {!! Form::text('title', null, array('placeholder' => 'Judul','id'=>'title','class' => 'form-control')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Isi Notifikasi</label>
                                                    <div class="col-md-9">
                                                        <textarea class="textarea_editor form-control" name="body" id="body" rows="15" placeholder="Enter text ..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">User Penerima</label>
                                                    <div class="col-md-2 col-sm-3">
                                                        <input type="checkbox" id="basic_checkbox_1"><label for="basic_checkbox_1">Semua User</label>
                                                    </div>
                                                    <div class="col-md-7 ol-sm-6">
                                                        {!! Form::select('user_id', $users,[], array('class' => 'form-control select2 m-b-10','style'=>'width: 100%' ,'id'=>'user_id')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-2">
                                            </div>
                                            <div class="col-md-10 ">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                        <button type="reset" class="btn btn-inverse">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Row -->
                <div class="row" id="tableSection">

                    
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <button class="pull-right btn btn-sm btn-rounded btn-success btncreate" >Tambah Notifikasi</button>
                                <h4 class="card-title">Notifikasi</h4>
                                <h6 class="card-subtitle">Kirim Notifikasi Ke User Penyewa</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="25%">Judul</th>
                                                <th width="30%">Isi Notifikasi</th>
                                                <th width="10%">User</th>
                                                <th width="10%"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- sample modal content -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Hapus Notifikasi</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Anda yakin menghapus Notifikasi ini?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Tutup</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmdelete">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <!-- sample modal content -->
                <div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Kirim Ulang Notifikasi ?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Anda yakin mengirim ulang notifikasi ini?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Tutup</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmresend">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
@endsection
