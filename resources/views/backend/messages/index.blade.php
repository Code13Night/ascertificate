@extends('backend.layouts.app')


@section('title')
Pesan
@endsection

@section('css')


@endsection
@section('js')

@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Pesan</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">User</li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>Sewa Bulan Ini</small></h6>
                                    <h4 class="m-t-0 text-info">30</h4></div>
                                <div class="spark-chart">
                                    <div id="monthchart"></div>
                                </div>
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>Pendapatan Bulan Ini</small></h6>
                                    <h4 class="m-t-0 text-primary">Rp. 40.000.000,00</h4></div>
                                <div class="spark-chart">
                                    <div id="lastmonthchart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('content')
                
@endsection
