@extends('backend.layouts.app')


@section('title')
Profil User
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.css') }}" />
    <link href="{{ asset('backend/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/dropify/dist/css/dropify.min.css') }}">


@endsection
@section('js')
    <script src="{{ asset('backend/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <!-- end - This is for export functionality only -->
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{ asset('backend/assets/plugins/html5-editor/wysihtml5-0.3.0.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <script type="text/javascript">
        var page = 1;
        var edittime=false;
        $(window).scroll(function() {
            if($('#rolex').val()){
                if(!edittime){
                    if($(window).scrollTop() + $(window).height() >= $(document).height()) {
                        page++;
                        loadMoreData(page);
                    }    
                }
            }
        });

        $('.dropify').dropify();

        $('.textarea_editor').wysihtml5();
        $(".select2").select2({
                width: "100%"
            });
        function loadMoreData(page){
          $.ajax(
                {
                    url: '?page=' + page,
                    type: "get",
                    beforeSend: function()
                    {
                        $('.ajax-load').show();
                    }
                })
                .done(function(data)
                {
                    if(data.html == " "){
                        edittime=true;
                        $('.ajax-load').html("No more data found");
                        return;
                    }
                    $('.ajax-load').hide();
                    $("#post-data").append(data.html);
                })
                .fail(function(jqXHR, ajaxOptions, thrownError)
                {

                });
        }
        function getSelectedText(elementId) {
            var elt = document.getElementById(elementId);

            if (elt.selectedIndex == -1)
                return null;

            return elt.options[elt.selectedIndex].text;
        }
        //Post Profile
        $('#createuser').submit(function (e) {
            //prevent from action submit default
            e.preventDefault();
            //Inserting data from Form to JSON format
            var datastring = new FormData($(this)[0]);
            //get action URL
            var $form = $(this),
              url = $form.attr("action");
            //ajax POST
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                data: datastring,
                success: function (data) { 
                    $(data).each(function (index, item) {
                        if(item.success){
                            if(item.data_image!=null){
                                $('.imageprofile').attr("src","/"+item.data_image);
                            }
                            edittime=false;
                            $('#password').val('');
                            $('#confirm-password').val('');
                            $('#profile_name').html($('#name').val());
                            if($('#rolex').val()){
                                if(getSelectedText('profession_id')!=""){
                                    $('#profile_profession').html(getSelectedText('profession_id'));
                                }else{
                                    $('#profile_profession').html('Lengkapi Profil Anda');
                                }
                                if(getSelectedText('education_level_id')!=""){
                                    $('#profile_education').html($('#graduate_year').val()+' - '+getSelectedText('education_level_id')+getSelectedText('study_program_id'));
                                }else{
                                    $('#profile_education').html('Lengkapi Profil Anda');
                                }
                                if($('#id_card_number').val()!=""){
                                    $('#profile_id_card').html($('#id_card_number').val());
                                }else{
                                    $('#profile_id_card').html('Lengkapi Profil Anda');
                                }
                                if($('#phone_number').val()){
                                    $('#profile_phone').html('+62'+$('#phone_number').val());
                                }else{
                                    $('#profile_phone').html('Lengkapi Profil Anda');
                                }
                                
                                $('#profile_address').html($('#address').val());
                            }else{
                                $('#profile_email').html($('#email').val());
                            }
                            $('#createSection').hide('fast');
                            $('.showsection').show('slow');
                            $('#editSection input').remove(); 
                        }else{
                            $('.bodymsg').html(item.message);
                            $('#myModalerrorMessage').modal('toggle'); 
                        }
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    $('#myModalerror').modal('toggle');
                }                   
            });  
        });
        $('.hideadd').on('click',function () {
            $('#createSection').hide('fast');
            $('.showsection').show('slow');
            edittime=false;
        })
        //edit value
        $('.btnedit').on('click', function (e) {
            edittime=true;
            e.preventDefault();
            $idshow=this.id;
            $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/user-profile',
                success: function (data) {
                    $(data).each(function (index, item) {
                        $(item.data).each(function (indexx, itemx) { 
                            $('#name').val(itemx.name);
                            $('#email').val(itemx.email);
                            $('#password').val('');
                            $('#confirm-password').val('');
                            if($('#rolex').val()){   
                                if(itemx.general_info!=null){
                                    $(itemx.general_info).each(function (indexxx, itemxx) { 
                                        $('#id_card_number').val(itemxx.id_card_number);
                                        $('#phone_number').val(itemxx.phone_number);
                                        $('#profession_id').val(itemxx.profession_id).trigger('change');
                                        $('#education_level_id').val(itemxx.education_level_id).trigger('change');
                                        $('#study_program_id').val(itemxx.study_program_id).trigger('change');
                                        // $('#company_id').val(itemxx.company_id).trigger('change');
                                        $('#address').data("wysihtml5").editor.setValue(itemxx.address);
                                        $('#graduate_year').val(itemxx.graduate_year);
                                    });
                                }
                            }
                        });
                    });                     
                    $('.showsection').hide('fast');
                    $('#createSection').show('slow');
                    $('#editSection').append('<input name="_method" type="hidden" value="PATCH">');
                    $("#createuser").attr("action", "/webApi/user-profileupdate");
                }
            });
        });

    </script>
@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Profil</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">User</a></li>
                            <li class="breadcrumb-item active">Profil</li>
                        </ol>
                    </div>
                </div>
@endsection

@section('content')

                <div class="row hide" id="createSection">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <button class="pull-right btn btn-sm btn-rounded btn-info hideadd"  data-toggle="tooltip" title="Tutup Ganti Profil">X</button>
                                <h4 class="m-b-0 text-white">Edit Profil</h4>
                            </div>
                            <div class="card-body">
                                {!! Form::open(array('route' => 'certificate.store','method'=>'POST','id'=>'createuser','class'=>'form-horizontal ')) !!}
                                    <div class="form-body">

                                        <div class="row" id="editSection">

                                        </div>
                                        <h3 class="box-title">Informasi Akun</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Nama</label>
                                                    <div class="col-md-9">
                                                        {!! Form::text('name', null, array('id'=>'name','class' => 'form-control')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Email</label>
                                                    <div class="col-md-9">
                                                        {!! Form::text('email', null, array('id'=>'email','class' => 'form-control')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Password</label>
                                                    <div class="col-md-9">
                                                        <input type="password" name="password" id="password" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Konfirmasi Password</label>
                                                    <div class="col-md-9">
                                                        <input type="password" name="confirm-password" id="confirm-password" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @if(Auth::user()->hasRole('user'))
                                        <h3 class="box-title">Informasi Umum</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">No. KTP</label>
                                                    <div class="col-md-9">
                                                        {!! Form::number('id_card_number', null, array('id'=>'id_card_number','class' => 'form-control')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Bidang Profesi</label>
                                                    <div class="col-md-9">
                                                        {!! Form::select('profession_id', $professions,[], array('class' => 'form-control select2 m-b-10','style'=>'width: 100%' ,'id'=>'profession_id')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Instansi</label>
                                                    <div class="col-md-7">
                                                        {!! Form::select('company_id', $companies,[], array('class' => 'form-control select2 m-b-10','style'=>'width: 100%' ,'id'=>'company_id')) !!}
                                                        <small class="form-control-feedback"> Apabila Instansi Anda tidak ada harap ditambahkan dengan klik tombol tambah disamping</small>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="pull-right btn btn-sm btn-rounded btn-info hideadd" data-toggle="tooltip" title="" data-original-title="Tambah Instansi">+</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">No Telfon</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">+62</span>
                                                            </div>
                                                            <input type="number" class="form-control" name="phone_number" id="phone_number">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-2">Alamat</label>
                                                    <div class="col-md-9">
                                                        <textarea class="textarea_editor form-control" name="address" id="address" rows="15" placeholder="Enter text ..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Foto</label>
                                                    <div class="col-md-9">
                                                        <input type="file" id="input-file-now" class="dropify" name="image"  id="image" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <h3 class="box-title">Informasi Pendidikan Terakhir</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Jenjang Pendidikan</label>
                                                    <div class="col-md-9">
                                                        {!! Form::select('education_level_id', $education_levels,[], array('class' => 'm-b-10 form-control select2 ','style'=>'width: 100%','id'=>'education_level_id')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Program Studi</label>
                                                    <div class="col-md-9">
                                                        {!! Form::select('study_program_id', $study_programs,[], array('class' => 'm-b-10 form-control select2 ','style'=>'width: 100%','id'=>'study_program_id')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Tahun Lulus</label>
                                                    <div class="col-md-9">
                                                        {!! Form::number('graduate_year', null, array('id'=>'graduate_year','class' => 'form-control')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-2">
                                            </div>
                                            <div class="col-md-10 ">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                        <button type="button" class="btn btn-inverse hideadd">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>   
                <!-- Row -->
                <div class="row showsection">
                    <!-- Column -->
                    <div class="col-lg-4 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <button class="pull-right btn btn-sm btn-rounded btn-info btnedit" >Edit Profil</button>
                                <center class="m-t-30"> 
                                    @if(count($general_info_all)>0)
                                        @if($general_info->image_thumb==""||$general_info->image_thumb==null)
                                            <img src="/images/thumb/not-found.jpg" class="img-circle imageprofile" width="150">
                                        @else
                                            <img src="/{{ $general_info->image_thumb }}" class="img-circle imageprofile" width="150">
                                        @endif
                                    @else
                                    <img src="/images/thumb/not-found.jpg" class="img-circle" width="150">
                                    @endif
                                    <h4 class="card-title m-t-10" id="profile_name">{{$user->name}}</h4>

                                     @if(Auth::user()->hasRole('user'))
                                    <h6 class="card-subtitle" id="profile_profession">@if(count($general_info_all)>0)
                                        {{$general_info->profession_name}}
                                        @else
                                        Lengkapi profil Anda
                                        @endif</h6>
                                    @else
                                    <h6 class="card-subtitle" id="profile_email">
                                        {{$user->email}}
                                    </h6>
                                    @endif
                                    @if(Auth::user()->hasRole('user'))
                                    <div class="row text-center justify-content-md-center">
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="mdi mdi-certificate"></i> <font class="font-medium">254</font></a></div>
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="mdi mdi-history"></i> <font class="font-medium">54</font></a></div>
                                    </div>
                                    @elseif(Auth::user()->hasRole('superadmin'))

                                    <div class="row text-center justify-content-md-center">
                                        <h6>Super Admin</h6>
                                    </div>
                                    @elseif(Auth::user()->hasRole('admin'))

                                    <div class="row text-center justify-content-md-center">
                                        <h6>Admin</h6>
                                    </div>
                                    @else

                                    <div class="row text-center justify-content-md-center">
                                        <h6>Costumer Service</h6>
                                    </div>
                                    @endif
                                </center>
                            </div>
                            <div>
                                <hr> </div>

                            @if(Auth::user()->hasRole('user'))
                            <div class="card-body"> 
                                <small class="text-muted">Email </small>
                                <h6 id="profil_email">{{$user->email}}</h6> 
                                <small class="text-muted p-t-30 db">No. KTP</small>
                                <h6 id="profile_id_card">@if(count($general_info_all)>0)
                                    {{$general_info->id_card_number}}
                                    @else
                                    Lengkapi Profil Anda
                                    @endif</h6> 
                                <small class="text-muted p-t-30 db">Telfon</small>
                                <h6 id="profile_phone">@if(count($general_info_all)>0)
                                    +62 {{$general_info->phone_number}}
                                    @else
                                    Lengkapi Profil Anda
                                    @endif</h6> 
                                <small class="text-muted p-t-30 db">Alamat</small>
                                <h6 id="profile_address">@if(count($general_info_all)>0)
                                    {{$general_info->address}}
                                    @else
                                    Lengkapi Profil Anda
                                    @endif</h6>
                                <small class="text-muted p-t-30 db">Pendidikan Terakhir</small>
                                <h6 id="profile_education">@if(count($general_info_all)>0)
                                    {!!"<b>".$general_info->graduate_year."</b> - ".$general_info->education_level_name." ".$general_info->study_program_name!!}
                                    @else
                                    Lengkapi Profil Anda
                                    @endif</h6>

                            </div>
                            @endif
                        </div>
                    <!-- /Row -->
                    </div>
                    <!-- /Column -->
                    <div class="col-lg-8 col-md-7">
                            <div class="card">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs profile-tab" role="tablist">
                                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#activity" role="tab">Aktifitas</a> </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="activity" role="tabpanel">
                                        <div class="card-body">
                                            <div class="table-responsive m-t-40">
                                                <table class="table stylish-table">
                                                    <thead>
                                                        <tr>
                                                            <th>Aktifitas</th>
                                                            <th>Tanggal</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if($logs->count()==0)
                                                        <tr>
                                                            <td colspan="4">
                                                                Tidak ada Aktifitas
                                                            </td>
                                                        </tr>
                                                        @endif
                                                        @foreach ($logs as $log)
                                                        <tr>
                                                            <td>{{$log->subject}}</td>
                                                            <td>{{date_format($log->created_at,"F j, Y, g:i a") }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- Column -->
                </div>
                <input type="hidden" name="rolex" id="rolex" value={{Auth::user()->hasRole('user')}}>
                <!-- /Row -->
                @if(Auth::user()->hasRole('user'))
                <!-- Row -->
                <div class="row showsection">
                    @include('backend.rents.data')
                </div>
                <!-- /Row -->
                @endif
                <div class="ajax-load float-center text-center showsection" style="display:none">
                <p><img src="{{ asset('frontend/assets/images/loading.gif') }}">Load more...</p>
                </div>

                <!-- sample modal content -->
                <div id="myModalerrorMessage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Terjadi Kesalahan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body bodymsg">
                                <p>Sertifikat dengan klasifikasi ini telah terdaftar</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">OK</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <!-- sample modal content -->
                <div id="myModalerror" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Terjadi Kesalahan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body bodymsg">
                                <p>Terjadi kesalahan pada server, Harap dicoba beberapa saat lagi</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">OK</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

@endsection
