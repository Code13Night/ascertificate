@extends('backend.layouts.app')


@section('title')
Sertifikat
@endsection

@section('css')


    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.css') }}" />
    <link href="{{ asset('backend/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/dropify/dist/css/dropify.min.css') }}">
    <link href="{{ asset('backend/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('js')

    <!-- This is data table -->
    <script src="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="{{ asset('backend/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <!-- end - This is for export functionality only -->
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{ asset('backend/assets/plugins/html5-editor/wysihtml5-0.3.0.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
        <!-- Date Picker Plugin JavaScript -->
        <script src="{{ asset('backend/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script>

        $('.datepicker-autocloserent').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        var drEvent = $('#file').dropify();
        drEvent = drEvent.data('dropify');
        function addCommas(nStr) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }
        function formatDate(date) {
          var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "June", "July",
            "Aug", "Sep", "Oct",
            "Nov", "Dec"
          ];

          var day = date.getDate();
          var monthIndex = date.getMonth();
          var year = date.getFullYear();

          return day + ' ' + monthNames[monthIndex] + ' ' + year;
        }
        $('.textarea_editor').wysihtml5();
        $(".select2").select2({
                width: "100%"
            });
        var table=$('#myTable').DataTable({iDisplayLength: 10,
            responsive: true,
            lengthMenu: [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "order": [
                [0, 'asc']
            ],
            "ajax": '/webApi/allusercertificates',
            "columns": [
                    { "data": "registration_number" },
                    { "data": "name" },
                    { "data": "certificate_classification" },
                    { "data": "certificate_type_id" },
                    { "data": "qualification" },
                    { "data": "date_of_registration" },
                    { "data": "id" },
            ], "columnDefs": [{
                "targets": 6,
                "data": "Action_Link",
                orderable: false,
                className: "text-center",
                "render": function (data, type, full, meta) {
                    return '<div class="btn-group">'+
                                    '<button type="button" class="btn btn-sm btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                        '<i class="ti-settings"></i>'+
                                    '</button>'+
                                    '<div class="dropdown-menu dropdown-menu-right">'+
                                        '<a class="dropdown-item btnshow" href="/certificate/registration-id/'+data+'" id=' + data + ' >Tampilkan Detail</a>'+
                                        '<a class="dropdown-item btnedit" href="javascript:void(0)" id=' + data + ' >Edit</a>'+
                                        '<div class="dropdown-divider"></div>'+
                                        '<a class="dropdown-item btndelete" href="javascript:void(0)" id=' + data + ' data-toggle="modal" data-target="#myModal">Hapus</a>'+
                                    '</div>'+
                                '</div>';

                }
            },{
                "targets": 0,
                className: "text-center",
                "render": function ( data, type, row ) {
                    
                    return '<a href="/certificate/registration-number/'+data+'"  id=' + data + ' class="btnshowClassificationCode" >'+data+'</a>';
                }
            },{
                "targets": 3,
                "data": "active",
                className: "text-center",
                "render": function (data, type, full, meta) {
                    if(data==1||data=="1"){
                        return '<span class="label label-info">SKA</span>';
                    }else{
                        return '<span class="label label-warning">SKT</span>';
                    }
                }
            },{
                "targets": 5,
                "data": "active",
                className: "text-center",
                "render": function (data, type, full, meta) {
                        return formatDate( new Date(data));
                }
            }
            ],
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf', 'print'
            ]
            
        });

        $('.hideadd').on('click', function (e) {
            $('#createSection').hide('fast');
        });
        toaststats=true;
        $('.btncreate').on('click', function (e) {
            $('#createSection').removeClass('hide');
            window.scrollTo(0,0);
            toaststats=true;
            $('#createSection').show('slow');
            $("#createuser").attr("action", "/webApi/usercertificate/create");
            $('#editSection input').remove();
        });
         var $radios = $('input:radio[name=type_id]');
        //Post new User
        $('#createuser').submit(function (e) {
            //prevent from action submit default
            e.preventDefault();
            NProgress.start();
            //Inserting data from Form to JSON format
            var datastring = new FormData($(this)[0]);
            //get action URL
            var $form = $(this),
              url = $form.attr("action");
            //ajax POST
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                data: datastring,
                success: function (data) { 
                    $(data).each(function (index, item) {
                        if(item.success){
                            table.ajax.reload();    
                            $('div.dataTables_filter input').val($('#name').val());              
                            $('#name').val('');                 
                            $('#image').val('');
                            $('#registration_number').val('');
                            $('#date_of_registration').val('');
                            $('#type_id').val('');
                            $('#qualification').val('').trigger('change');
                            $('#user_id').val('not_selected').trigger('change');
                            $('#profession_id').val('').trigger('change');
                            $(".dropify-clear").click();
                            NProgress.done();
                            $('#createSection').hide('fast');
                            $('#editSection input').remove();
                            $('div.dataTables_filter input').focus();
                            window.scrollTo(0,0);
                            if(toaststats){
                               $.toast({
                                heading: 'Sertifikat ditambahkan',
                                text: 'Sertifikat berhasil ditambahkan',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'info',
                                hideAfter: 3000, 
                                stack: 6
                              });  
                           }else{
                               $.toast({
                                heading: 'Sertifikat diedit',
                                text: 'Sertifikat berhasil diubah',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'warning',
                                hideAfter: 3000, 
                                stack: 6
                              });
                           }
                               
                        }else{
                            $('#myModalerror').modal('toggle');
                        }
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    $('#myModalerror').modal('toggle');
                }                   
            });  
        });

        //button change status
        $('#myTable tbody').on('click', '.btneditstatus', function (e) {
            e.preventDefault();
            $iddel=this.id;
        });

        //edit value
        $('#myTable tbody').on('click', '.btnedit', function (e) {
            e.preventDefault();
            $idshow=this.id;
            toaststats=false;
            $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/usercertificate/'+$idshow,
                success: function (data) {
                    $(data).each(function (index, item) {
                        $(item.data).each(function (indexx, itemx) {
                            $('#name').val(itemx.name);
                            $('#certificate_id').val(itemx.certificate_id).trigger('change');
                            $('#user_id').val(itemx.user_id);
                            $('#registration_number').focus();
                            $('#registration_number').val(itemx.registration_number);
                            $date=new Date(itemx.date_of_registration);
                            $('#date_of_registration').val(("0"+($date.getMonth()+1)).slice(-2)+'/'+("0" + $date.getDate()).slice(-2)+'/'+$date.getFullYear());
                            $('#qualification').val(itemx.qualification);
                            $('#initiate_image').html('');
                            $('#initiate_image').html('<input type="file" class="dropify" name="file"  id="file" />');
                            drEvent = $('#file').dropify({ defaultFile: '/'+itemx.file_path });
                        });
                    });                     
                    $('#createSection').show('slow');
                    window.scrollTo(0,0); 
                    $('#editSection').append('<input name="_method" type="hidden" value="PATCH">');
                    $("#createuser").attr("action", "/webApi/usercertificate/" + $idshow);
                }
            });
        });
        //Delete Confirmation
        $('#myTable tbody').on('click', '.btndelete', function (e) {
            e.preventDefault();
            $iddel=this.id;
        });
        //Delete Confirmation
        $('#btnconfirmdelete').on('click', function (e) {
            $('#myModal').modal('toggle');
            
            $.ajax({
                cache: false,
                type: 'DELETE',
                url: '/webApi/usercertificate/'+$iddel,
                data: { _method: 'DELETE', _token: $('input[name=_token]').val() },
                success: function (data) {
                    table.ajax.reload();
                }
            });
        });
        $('.skt_section').hide();
        $('.ska_section').show();
        var type_idfilter="all",profession_idfilter="all";
        $('#type_id1').on('click', function() {
            type_idfilter=$(this).val();
            $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/allcertificates/type-'+type_idfilter+'/profession-'+profession_idfilter,
                success: function (data) {
                    $(data).each(function (index, item) {
                        if(item.success){  
                            $('#certificate_id').empty();
                            $('#certificate_id').select2({
                                placeholder: "Select a pill",
                                data: item.data
                            });
                            $('.skt_section').hide();
                            $('.ska_section').show();
                        }
                    });
                }
            });
        });
        $('#type_id2').on('click', function() {
            type_idfilter=$(this).val();
            $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/allcertificates/type-'+type_idfilter+'/profession-'+profession_idfilter,
                success: function (data) {
                    $(data).each(function (index, item) {
                        if(item.success){  
                            $('#certificate_id').empty();
                            $('#certificate_id').select2({
                                placeholder: "Pilih Klasifikasi Sertifikat",
                                data: item.data
                            });
                            $('.skt_section').show();
                            $('.ska_section').hide();
                        }
                    });
                }
            });
        });
    </script>
@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Sertifikat</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Sertifikat</a></li>
                            <li class="breadcrumb-item active">List</li>
                        </ol>
                    </div>
                </div>
@endsection

@section('content')
             <!-- Row -->
                <div class="row hide" id="createSection">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <button class="pull-right btn btn-sm btn-rounded btn-info hideadd"  data-toggle="tooltip" title="Tutup tambah sertifikat">X</button>
                                <h4 class="m-b-0 text-white">Tambah Sertifikat</h4>
                            </div>
                            <div class="card-body">
                                {!! Form::open(array('route' => 'certificate.store','method'=>'POST','id'=>'createuser','class'=>'form-horizontal ')) !!}
                                    <div class="form-body">

                                        <div class="row" id="editSection">

                                        </div>
                                        <h3 class="box-title">Informasi Sertifikat</h3>
                                        <hr class="m-t-0 m-b-40">

                                        <!--/row-->
                                        <div class="row">
                                            <!--/span-->
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Tipe Sertifikat</label>
                                                    <div class="col-md-2">
                                                        <div class="m-b-10">
                                                            <label class="custom-control custom-radio">
                                                                <input id="type_id1" name="type_id" value=1 type="radio" class="custom-control-input type_id" checked>
                                                                <span class="custom-control-label">SKA</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="m-b-10">
                                                            <label class="custom-control custom-radio">
                                                                <input id="type_id2" name="type_id" type="radio" value=2 class="custom-control-input type_id">
                                                                <span class="custom-control-label">SKT</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Klasifikasi</label>
                                                    <div class="col-md-9">
                                                        {!! Form::select('certificate_id', $certificates,[], array('class' => 'form-control select2 m-b-10','style'=>'width: 100%' ,'id'=>'certificate_id')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">User</label>
                                                    <div class="col-md-9">
                                                        {!! Form::select('user_id', $users,[], array('class' => 'form-control select2 m-b-10','style'=>'width: 100%' ,'id'=>'user_id')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">No. Registrasi</label>
                                                    <div class="col-md-9">
                                                        {!! Form::text('registration_number', null, array('id'=>'registration_number','class' => 'form-control')) !!}
                                                         </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Nama Sertifikat</label>
                                                    <div class="col-md-9">
                                                        {!! Form::text('name', null, array('id'=>'name','class' => 'form-control')) !!}
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Kualifikasi</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control  custom-select"  name="qualification" id="qualification">
                                                            <option value="" >Pilih Kualifikasi </option>
                                                            <option value="Tingkat I" class="skt_section">Tingkat I</option>
                                                            <option value="Tingkat II" class="skt_section">Tingkat II</option>
                                                            <option value="Tingkat III" class="skt_section">Tingkat III</option>
                                                            <option value="Muda" class="ska_section">Muda</option>
                                                            <option value="Madya" class="ska_section">Madya</option>
                                                            <option value="Utama" class="ska_section">Utama</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Tanggal Berlaku</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control datepicker-autocloserent" id="date_of_registration" name="date_of_registration"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-2">Soft Copy Sertifikat</label>
                                                    <div class="col-md-9" id="initiate_image">
                                                        <input type="file" class="dropify" name="file"  id="file" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->

                                        
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-2">
                                            </div>
                                            <div class="col-md-10 ">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Simpan</button>
                                                        <button type="button" class="btn btn-inverse hideadd">Batalkan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>   
                                

                <!-- Row -->
                <div class="row" id="tableSection">

                    
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <button class="pull-right btn btn-sm btn-rounded btn-success btncreate" >Tambah Sertifikat</button>
                                <h4 class="card-title">Sertifikat</h4>
                                <h6 class="card-subtitle">Sertifikat yang tersedia untuk disewakan</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="10%">No Registrasi</th>
                                                <th width="25%">Nama Sertifikat</th>
                                                <th width="20%">Bidang Keahlian</th>
                                                <th width="10%">Tipe Sertifikat</th>
                                                <th width="10%">Kualifikasi</th>
                                                <th width="15%">Tanggal diterbitkan</th>
                                                <th width="10%" class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- sample modal content -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Hapus Sertifikat</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Anda yakin menghapus Sertifikat ini?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Tutup</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmdelete">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <!-- sample modal content -->
                <div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Ganti Status Ketersediaan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Ganti status Ketersediaan sertifikat ?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Tutup</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmstatus">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <!-- sample modal content -->
                <div id="myModalerror" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Terjadi Kesalahan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Sertifikat dengan klasifikasi ini telah terdaftar</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">OK</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
@endsection
