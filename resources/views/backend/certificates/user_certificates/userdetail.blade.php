@extends('backend.layouts.app')


@section('title')
Detail
@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.css') }}" />
    <link href="{{ asset('backend/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet">

@endsection
@section('js')
    
    <script src="{{ asset('backend/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('backend/assets/plugins/html5-editor/wysihtml5-0.3.0.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.js') }}"></script>
<!-- Magnific popup JavaScript -->
    <script src="{{ asset('backend/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('backend/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script>
@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Sertifikat</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Sertifikat</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('certificate-list') }}">List Sertifikat</a></li>
                            <li class="breadcrumb-item active">Detail</li>
                        </ol>
                    </div>
                </div>
@endsection

@section('content')
                <!-- Row -->
                <div class="row">
                    <!-- /Column -->
                    <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs profile-tab" role="tablist">
                                    <li class="nav-item col-md-6 col-sm-6 text-center"> <a class="nav-link active" data-toggle="tab" href="#general_info" role="tab">Informasi</a> </li>
                                    <li class="nav-item col-md-6 col-sm-6 text-center"> <a class="nav-link" data-toggle="tab" href="#list" role="tab">Sertifikat</a> </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general_info" role="tabpanel">
                                         <div class="card-body">
                                            <div class="d-flex align-items-center flex-row m-t-30">
                                                <div class="p-2 display-5 text-info"><span>{{ $certificate->registration_number }}</span></div>
                                                <div class="p-2">
                                                <h3 class="m-b-0">{{ $certificate->name }}</h3><small>
                                                            @if($certificate->certificate_type_id==1)
                                                                <label class="label label-rounded label-info">
                                                            @else
                                                                <label class="label label-rounded label-warning">
                                                            @endif
                                                            {{ $certificate->certificate_type_name }}</label></small></div>
                                            </div>
                                            <table class="table no-border">
                                                <tbody>
                                                    <tr>
                                                        <td width="40%">Kode Klasifikasi</td>
                                                        <td class="font-medium">{{ $certificate->certificate_classification_code }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Tipe</td>
                                                        <td class="font-medium">
                                                            @if($certificate->certificate_type_id==1)
                                                                <label class="label label-rounded label-info">
                                                            @else
                                                                <label class="label label-rounded label-warning">
                                                            @endif
                                                            {{ $certificate->certificate_type_name }}</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Bidang</td>
                                                        <td class="font-medium">{{ $certificate->profession_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Klasifikasi</td>
                                                        <td class="font-medium">{{ $certificate->certificate_classification }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Kualifikasi</td>
                                                        <td class="font-medium">{{ $certificate->qualification }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Penyewa</td>
                                                        <td class="font-medium">{{ $certificate->user_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Tanggal berlaku Mulai</td>
                                                        <td class="font-medium">{{ date("d-m-Y", strtotime($certificate->date_of_registration)) }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>


                                    </div>
                                    <div class="tab-pane" id="list" role="tabpanel">
                                        <div class="card-body">
                                        		<?php 

														$file_parts = pathinfo($certificate->file_path);

														if(strtolower($file_parts['extension'])=="jpg"||strtolower($file_parts['extension'])=="jpeg"||strtolower($file_parts['extension'])=="png"||strtolower($file_parts['extension'])=="bmp"){
															?>
															
                                            <div class="row m-t-60">
                                                <div class="col-md-12">
                                                    @if($certificate->file_path!=null)
                                                    <a class="image-popup-no-margins" href="/{{$certificate->file_path}}"> <img src="/{{$certificate->file_path}}" alt="image" height="400px" width="auto" > </a>
                                                    <h6 class="m-t-10"></h6> </div>
                                                    @else
                                                    <a class="image-popup-no-margins" href="/images/thumb/not-found.jpg"> <img src="/images/thumb/not-found.jpg" alt="image" class="img-responsive"> </a>
                                                    <h6 class="m-t-10"></h6> </div>
                                                    @endif
                                            </div>
															<?php
														}else{
															?>
														<div class="card">
			                                                <div class="d-flex flex-row">

			                                                            @if($certificate->certificate_type_id==1)
			                                                                <div class="p-10 bg-info">
			                                                            @else
			                                                                <div class="p-10 bg-warning">
			                                                            @endif
			                                                        <h3 class="text-white box m-b-0"><i class="icon-cloud-download"></i></h3></div>
			                                                    <div class="align-self-center m-l-20">
			                                                        <h3 class="m-b-0 text-inverse">{{$certificate->file_name}}</h3>
			                                                        <h5 class="text-muted m-b-0"><a target="_blank" href="/{{$certificate->file_path}}">Lihat File</a></h5></div>
			                                                </div>
			                                            </div>
															<?php
														}
                                        		?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- /Row -->

@endsection
