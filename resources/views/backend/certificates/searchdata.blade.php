@foreach($certificate as $certificatevalue)
                	<div class="col-md-4 col-sm-6 col-lg-4">
						<div class="card blog-widget">
                            <div class="card-body">
                                <div class="blog-image">
                                	@if($certificatevalue->image_name==null||$certificatevalue->image_name=='')
	                                    @if($certificatevalue->type_name=="SKA")
	                                    
	                                     <img class="" src="{{ asset('backend/assets/images/background/experted-thumb.jpg') }}" style="width: 100%" alt="img" class="img-responsive">
	                                    @else
	                                     <img class="" src="{{ asset('backend/assets/images/background/skilled-thumb.jpg') }}" style="width: 100%" alt="img" class="img-responsive">
	                                    @endif
                                @else
                                     <img class="" src="{{$certificate->image_full}}" style="width: 100%" alt="img" class="img-responsive">
                                @endif
								</div>
                                <h3>{{$certificatevalue->classification_code." - ". $certificatevalue->name}}</h3>
                                <h5><strong>{{$certificatevalue->profesion_name }}</strong></h5>
                                @if($certificatevalue->type_name=="SKA")
                                                    <label class="label label-rounded label-info">
                                                @else
                                                    <label class="label label-rounded label-warning">
                                                @endif
                                {{ $certificatevalue->type_name }}
                            	</label>
                                <p class="m-t-20 m-b-20">
                                    @if(strlen($certificatevalue->description) > 30)
                                                    {{  substr($certificatevalue->description,0,30)."..." }}
                                                @else
                                                    @if(strlen($certificatevalue->description) == 0)
                                                    Tidak ada Informasi tambahan tentang sertifikat ini
                                                    @else
                                                    $certificatevalue->description            
                                                    @endif                                  
                                                @endif   
                                </p>
                                <div class="d-flex">
                                    <div class="read"><a href="/certificate-code/{{$certificatevalue->classification_code}}" class="link font-medium">Selengkapnya</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
@endforeach