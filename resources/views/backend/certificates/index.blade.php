@extends('backend.layouts.app')


@section('title')
Sertifikat
@endsection

@section('css')


    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.css') }}" />
    <link href="{{ asset('backend/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/dropify/dist/css/dropify.min.css') }}">
@endsection
@section('js')

    <!-- This is data table -->
    <script src="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="{{ asset('backend/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <!-- end - This is for export functionality only -->
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{ asset('backend/assets/plugins/html5-editor/wysihtml5-0.3.0.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.js') }}"></script>
    <script>

        function addCommas(nStr) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }
        $('.textarea_editor').wysihtml5();
        $(".select2").select2({
                width: "100%"
            });
        var table=$('#myTable').DataTable({iDisplayLength: 10,
            responsive: true,
            lengthMenu: [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "order": [
                [0, 'asc']
            ],
            "ajax": '/webApi/allcertificates',
            "columns": [
                    { "data": "classification_code" },
                    { "data": "name" },
                    { "data": "profesion_name" },
                    { "data": "type_name" },
                    { "data": "price_soft" },
                    { "data": "status" },
                    { "data": "id" },
            ], "columnDefs": [{
                "targets": 6,
                "data": "Action_Link",
                orderable: false,
                className: "text-center",
                "render": function (data, type, full, meta) {
                    return '<div class="btn-group">'+
                                    '<button type="button" class="btn btn-sm btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                        '<i class="ti-settings"></i>'+
                                    '</button>'+
                                    '<div class="dropdown-menu dropdown-menu-right">'+
                                        '<a class="dropdown-item btnshow" href="/certificate/classification/detail/'+data+'" id=' + data + ' >Tampilkan Detail</a>'+
                                        '<a class="dropdown-item btnedit" href="javascript:void(0)" id=' + data + ' >Edit</a>'+
                                        '<a class="dropdown-item btneditstatus" href="javascript:void(0)" id=' + data + ' data-toggle="modal" data-target="#myModal2">Ganti Status Tampil</a>'+
                                        '<div class="dropdown-divider"></div>'+
                                        '<a class="dropdown-item btndelete" href="javascript:void(0)" id=' + data + ' data-toggle="modal" data-target="#myModal">Hapus</a>'+
                                    '</div>'+
                                '</div>';

                }
            },{
                "targets": 0,
                className: "text-center",
                "render": function ( data, type, row ) {
                    
                    return '<a href="/certificate/classification/code/'+data+'"  id=' + data + ' class="btnshowClassificationCode" >'+data+'</a>';
                }
            },{
                "targets": 3,
                "data": "active",
                className: "text-center",
                "render": function (data, type, full, meta) {
                    if(data=="SKA"){
                        return '<span class="label label-info">'+data+'</span>';
                    }else{
                        return '<span class="label label-warning">'+data+'</span>';
                    }
                }
            },{
                "targets": 4,
                className: "text-right",
                "render": function ( data, type, row ) {
                            return '<b>Rp<span style="opacity: 0.0;">_</span>'+ addCommas(data) +'</b>';
                }
            },{
                "targets": 5,
                "data": "active",
                className: "text-center",
                "render": function (data, type, full, meta) {
                    if(data==1){
                        return '<span class="label label-success">Tersedia</span>';
                    }else{
                        return '<span class="label label-danger">Tidak Tersedia</span>';
                    }
                }
            }
            ],
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf', 'print'
            ]
            
        });

        $('.hideadd').on('click', function (e) {
            $('#createSection').hide('fast');
        });
        toaststats=true;
        $('.btncreate').on('click', function (e) {
            $('#createSection').removeClass('hide');
            window.scrollTo(0,0);
            toaststats=true;
            $('#createSection').show('slow');
            $("#createuser").attr("action", "/webApi/certificate/create");
            $('#editSection input').remove();
        });
         var $radios = $('input:radio[name=type_id]');
        //Post new User
        $('#createuser').submit(function (e) {
            //prevent from action submit default
            e.preventDefault();
            NProgress.start();
            //Inserting data from Form to JSON format
            var datastring = new FormData($(this)[0]);
            //get action URL
            var $form = $(this),
              url = $form.attr("action");
            //ajax POST
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                data: datastring,
                success: function (data) { 
                    $(data).each(function (index, item) {
                        if(item.success){
                            table.ajax.reload();    
                            $('div.dataTables_filter input').val($('#name').val());              
                            $('#name').val('');                 
                            $('#image').val('');
                            $('#classification_code').val('');
                            $('#valid_time').val('');
                            $('#price_soft').val('');
                            $('#price_hard').val('');
                            $('#type_id').val('');
                            $('#time_spesification').val('').trigger('change');
                            $('#education_levels').val('').trigger('change');
                            $('#profession_id').val('').trigger('change');
                            $(".dropify-clear").click();
                            $('#study_programs').val('').trigger('change');
                            $('#description').data("wysihtml5").editor.setValue('');
                            NProgress.done();
                            $('#createSection').hide('fast');
                            $('#editSection input').remove();
                            $('div.dataTables_filter input').focus();
                            window.scrollTo(0,0);
                            if(toaststats){
                               $.toast({
                                heading: 'Sertifikat ditambahkan',
                                text: 'Sertifikat berhasil ditambahkan',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'info',
                                hideAfter: 3000, 
                                stack: 6
                              });  
                           }else{
                               $.toast({
                                heading: 'Sertifikat diedit',
                                text: 'Sertifikat berhasil diubah',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'warning',
                                hideAfter: 3000, 
                                stack: 6
                              });
                           }
                               
                        }else{
                            $('#myModalerror').modal('toggle');
                        }
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    $('#myModalerror').modal('toggle');
                }                   
            });  
        });

        //button change status
        $('#myTable tbody').on('click', '.btneditstatus', function (e) {
            e.preventDefault();
            $iddel=this.id;
        });

        //edit value
        $('#myTable tbody').on('click', '.btnedit', function (e) {
            e.preventDefault();
            $idshow=this.id;
            toaststats=false;
            $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/certificate/'+$idshow,
                success: function (data) {
                    $(data).each(function (index, item) {
                        $(item.data).each(function (indexx, itemx) {
                            $('#name').val(itemx.name);
                            $('#profession_id').val(itemx.profession_id).trigger('change');
                            $('#classification_code').val(itemx.classification_code);
                            $('#classification_code').focus();
                            $('#classification_code').focus();
                            $('#price_soft').val(itemx.price_soft);
                            $('#price_hard').val(itemx.price_hard);
                            $('#description').data("wysihtml5").editor.setValue(itemx.description);
                            if($radios.is(':checked') === false) {
                                $radios.filter('[value='+itemx.type_id+']').prop('checked', true);
                            }
                            var $timeval;
                            var $timespec;
                            if(itemx.period_of_validity>30){
                                if(itemx.period_of_validity>360){
                                    $timeval=itemx.period_of_validity/360;
                                    $timespec="year";
                                }else{
                                    $timeval=itemx.period_of_validity/30;
                                    $timespec="month";
                                }
                            }else {
                                $timeval=itemx.period_of_validity;
                                $timespec="day";
                            }
                            $('#valid_time').val($timeval);
                            $('#time_spesification').val($timespec).trigger('change');
                            var $study=[]; var $education=[];
                            $(itemx.study_program).each(function (indexx, itemxx) {
                               $study.push(itemxx.id);
                            });
                            $(itemx.education_level).each(function (indexx, itemxx) {
                               $education.push(itemxx.id);
                            });

                            $('#education_levels').val($education).trigger('change');
                            $('#study_programs').val($study).trigger('change');
                        });
                    });                     
                    $('#createSection').show('slow');
                    window.scrollTo(0,0); 
                    $('#editSection').append('<input name="_method" type="hidden" value="PATCH">');
                    $("#createuser").attr("action", "/webApi/certificate/" + $idshow+'-soft_sended');
                }
            });
        });
        //Confirm change status
        $('#btnconfirmstatus').on('click', function (e) {
            $('#myModal2').modal('toggle');
            
            $.ajax({
                cache: false,
                type: 'POST',
                url: '/webApi/certificate-status/'+$iddel,
                data: { _method: 'PATCH', _token: $('input[name=_token]').val() },
                success: function (data) {
                    table.ajax.reload();
                }
            });
        });
        //Delete Confirmation
        $('#myTable tbody').on('click', '.btndelete', function (e) {
            e.preventDefault();
            $iddel=this.id;
        });
        //Delete Confirmation
        $('#btnconfirmdelete').on('click', function (e) {
            $('#myModal').modal('toggle');
            
            $.ajax({
                cache: false,
                type: 'DELETE',
                url: '/webApi/certificate/'+$iddel,
                data: { _method: 'DELETE', _token: $('input[name=_token]').val() },
                success: function (data) {
                    table.ajax.reload();
                }
            });
        });
    </script>
@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Klasifikasi Sertifikat</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Sertifikat</a></li>
                            <li class="breadcrumb-item active">Klasifikasi Sertifikat</li>
                        </ol>
                    </div>
                </div>
@endsection

@section('content')
             <!-- Row -->
                <div class="row hide" id="createSection">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <button class="pull-right btn btn-sm btn-rounded btn-info hideadd"  data-toggle="tooltip" title="Tutup tambah klasifikasi sertifikat">X</button>
                                <h4 class="m-b-0 text-white">Tambah Klasifikasi Sertifikat</h4>
                            </div>
                            <div class="card-body">
                                {!! Form::open(array('route' => 'certificate.store','method'=>'POST','id'=>'createuser','class'=>'form-horizontal ')) !!}
                                    <div class="form-body">

                                        <div class="row" id="editSection">

                                        </div>
                                        <h3 class="box-title">Informasi Sertifikat</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Bidang Profesi</label>
                                                    <div class="col-md-9">
                                                        {!! Form::select('profession_id', $professions,[], array('class' => 'form-control select2 m-b-10','style'=>'width: 100%' ,'id'=>'profession_id')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">No. Klasifikasi</label>
                                                    <div class="col-md-9">
                                                        {!! Form::text('classification_code', null, array('id'=>'classification_code','class' => 'form-control')) !!}
                                                        <small class="form-control-feedback"> Tuliskan nomor klasifikasi sertifikat </small> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Klasifikasi Profesi</label>
                                                    <div class="col-md-9">
                                                        {!! Form::text('name', null, array('id'=>'name','class' => 'form-control')) !!}
                                                        <small class="form-control-feedback"> Tuliskan nama sertifikat </small> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <!--/span-->
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Berlaku Sampai</label>
                                                    <div class="col-md-4">
                                                        <input type="number" name="valid_time" id="valid_time" class="form-control" >
                                                    </div>
                                                    <div class="col-md-5">
                                                        <select class="form-control  custom-select"  name="time_spesification" id="time_spesification">
                                                            <option value="year">Tahun</option>
                                                            <option value="month">Bulan</option>
                                                            <option value="day">Hari</option>
                                                        </select>
                                                        <small class="form-control-feedback"> Pilih Jangka Waktu. </small> 
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <!--/span-->
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Tipe Sertifikat</label>
                                                    <div class="col-md-2">
                                                        <div class="m-b-10">
                                                            <label class="custom-control custom-radio">
                                                                <input id="type_id1" name="type_id" value=1 type="radio" class="custom-control-input">
                                                                <span class="custom-control-label">SKA</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="m-b-10">
                                                            <label class="custom-control custom-radio">
                                                                <input id="type_id2" name="type_id" type="radio" value=2 class="custom-control-input">
                                                                <span class="custom-control-label">SKT</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Harga soft copy</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Rp.</span>
                                                            </div>
                                                            <input type="text" class="form-control" aria-label="Jumlah " name="price_soft" id="price_soft">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">,00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Harga hard copy</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Rp.</span>
                                                            </div>
                                                            <input type="text" class="form-control" aria-label="Jumlah " name="price_hard" id="price_hard">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">,00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Jenjang Pendidikan</label>
                                                    <div class="col-md-9">
                                                        {!! Form::select('education_levels[]', $education_levels,[], array('class' => 'm-b-10 form-control select2 select2-multiple','style'=>'width: 100%','id'=>'education_levels','multiple')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Program Studi</label>
                                                    <div class="col-md-9">
                                                        {!! Form::select('study_programs[]', $study_programs,[], array('class' => 'm-b-10 form-control select2 select2-multiple','style'=>'width: 100%','id'=>'study_programs','multiple')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-2">Keterangan</label>
                                                    <div class="col-md-9">
                                                        <textarea class="textarea_editor form-control" name="description" id="description" rows="15" placeholder="Enter text ..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-2">
                                            </div>
                                            <div class="col-md-10 ">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                        <button type="button" class="btn btn-inverse hideadd">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>   
                                

                <!-- Row -->
                <div class="row" id="tableSection">

                    
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <button class="pull-right btn btn-sm btn-rounded btn-success btncreate" >Tambah Klasifikasi Sertifikat</button>
                                <h4 class="card-title">Klasifikasi Sertifikat</h4>
                                <h6 class="card-subtitle">Klasifikasi Sertifikat yang tersedia untuk disewakan</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="10%">No Klasifikasi</th>
                                                <th width="25%">Klasifikasi</th>
                                                <th width="20%">Bidang Keahlian</th>
                                                <th width="8%">Tipe Sertifikat</th>
                                                <th width="20%">Harga</th>
                                                <th width="7%">Status</th>
                                                <th width="10%" class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- sample modal content -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Hapus Klasifikasi Sertifikat</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Anda yakin menghapus Klasifikasi Sertifikat ini?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Tutup</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmdelete">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <!-- sample modal content -->
                <div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Ganti Status Ketersediaan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Ganti status Ketersediaan sertifikat ?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Tutup</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmstatus">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <!-- sample modal content -->
                <div id="myModalerror" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Terjadi Kesalahan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Sertifikat dengan klasifikasi ini telah terdaftar</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">OK</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
@endsection
