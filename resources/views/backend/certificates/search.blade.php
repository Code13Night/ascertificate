@extends('backend.layouts.app')


@section('title')
Detail
@endsection

@section('css')


@endsection
@section('js')
    <script type="text/javascript">
    	var page = 1;
		$(window).scroll(function() {
		    if($(window).scrollTop() + $(window).height() >= $(document).height()) {
		        page++;
		        loadMoreData(page);
		    }
		});
        $('.btnsearch').on('click', function (e) {
        	$searchval=$('#query_notfound').val();
        	if($searchval!=null||$searchval!=""){
        		window.location.href = '/certificate-search/'+$searchval;
        	}
				
        });

		function loadMoreData(page){
		  $.ajax(
		        {
		            url: '?page=' + page,
		            type: "get",
		            beforeSend: function()
		            {
		                $('.ajax-load').show();
		            }
		        })
		        .done(function(data)
		        {
		            if(data.html == " "){
		                $('.ajax-load').html("No more data found");
		                return;
		            }
		            $('.ajax-load').hide();
		            $("#post-data").append(data.html);
		        })
		        .fail(function(jqXHR, ajaxOptions, thrownError)
		        {
		              alert('server not responding...');
		        });
		}
    </script>
@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Sertifikat</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('certificate') }}">Sertifikat</a></li>
                            <li class="breadcrumb-item active">Cari</li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>Sertifikat ditemukan</small></h6>
                                    <h4 class="m-t-0 text-info float-right">{{$totalfound}}</h4></div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('content')
                <div class="row" id="post-data">

					@include('backend.certificates.searchdata')
                   @if($certificate->count()==0)

                	<div class="col-sm-12 col-md-12">
                		<div class="card text-center">
                            <div class="card-header">
                                Sertifikat dengan kata kunci {{ $query }} tidak ditemukan
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">Mohon mencari dengan kata kunci lain</h4>
                                <input type="text" class="form-control" name="query_notfound"  id="query_notfound" placeholder="Search new keyword">
                                <hr/> 
                           		 <a href="javascript:void(0);" class="btn btn-info btnsearch">Cari</a>
                                
                            </div>
                        </div>
                    </div>
                   @endif 
                </div>
                <div class="ajax-load float-center text-center" style="display:none">
				<p><img src="{{ asset('frontend/assets/images/loading.gif') }}">Load more...</p>
				</div>
@endsection
