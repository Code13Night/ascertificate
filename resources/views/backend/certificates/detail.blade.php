@extends('backend.layouts.app')


@section('title')
Detail
@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.css') }}" />
    <link href="{{ asset('backend/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('js')
    
    <script src="{{ asset('backend/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('backend/assets/plugins/html5-editor/wysihtml5-0.3.0.js') }}"></script>
    <script src="{{ asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.js') }}"></script>
    <script type="text/javascript">
        

        $('.textarea_editor').wysihtml5();
        $(".select2").select2({
                width: "100%"
            });
    </script>
@stop

@section('content-header')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Sertifikat</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Sertifikat</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('certificate-classification') }}">Klasifikasi Sertifikat</a></li>
                            <li class="breadcrumb-item active">Detail</li>
                        </ol>
                    </div>
                </div>
@endsection

@section('content')
                <!-- Row -->
                <div class="row">
                    <!-- /Column -->
                    <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs profile-tab" role="tablist">
                                    <li class="nav-item col-md-6 col-sm-6 text-center"> <a class="nav-link active" data-toggle="tab" href="#general_info" role="tab">Informasi Umum</a> </li>
                                    <li class="nav-item col-md-6 col-sm-6 text-center"> <a class="nav-link" data-toggle="tab" href="#list" role="tab">Daftar Sertifikat</a> </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general_info" role="tabpanel">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center flex-row m-t-30">
                                                <div class="p-2 display-5 text-info"><span>{{ $certificate->classification_code }}</span></div>
                                                <div class="p-2">
                                                <h3 class="m-b-0">{{ $certificate->name }}</h3><small>
                                                            @if($certificate->type_id==1)
                                                                <label class="label label-rounded label-info">
                                                            @else
                                                                <label class="label label-rounded label-warning">
                                                            @endif
                                                            {{ $certificate->type_name }}</label></small></div>
                                            </div>
                                            <table class="table no-border">
                                                <tbody>
                                                    <tr>
                                                        <td width="40%">Kode Klasifikasi</td>
                                                        <td class="font-medium">{{ $certificate->classification_code }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Tipe</td>
                                                        <td class="font-medium">
                                                            @if($certificate->type_id==1)
                                                                <label class="label label-rounded label-info">
                                                            @else
                                                                <label class="label label-rounded label-warning">
                                                            @endif
                                                            {{ $certificate->type_name }}</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Bidang</td>
                                                        <td class="font-medium">{{ $certificate->profession_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Sub Bidang</td>
                                                        <td class="font-medium">{{ $certificate->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Harga Soft Copy</td>
                                                        <td class="font-medium">Rp. {{ number_format($certificate->price_soft,2,",",".") }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Harga Hard Copy</td>
                                                        <td class="font-medium">Rp. {{ number_format($certificate->price_hard,2,",",".") }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Status</td>
                                                        <td class="font-medium">@if($certificate->status==1)
                                                                <label class="label label-rounded label-success">
                                                                    Tersedia
                                                            @else
                                                                <label class="label label-rounded label-danger">
                                                                    Tidak Tersedia
                                                            @endif
                                                            </label></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Masa Berlaku</td>
                                                        <td class="font-medium">@if($certificate->period_of_validity>360)
                                                                {{ $certificate->period_of_validity/360 }} Tahun
                                                            @elseif($certificate->period_of_validity>60)
                                                                {{ $certificate->period_of_validity/60 }} Bulan
                                                            @else
                                                                {{ $certificate->period_of_validity }} Hari
                                                            @endif</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Persyaratan Tingkat Pendidikan</td>
                                                        <td class="font-medium">
                                                        @foreach ($certificate->education_level as $education_level)
                                                            {{ $education_level->name }} <br/>
                                                        @endforeach
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Persyaratan Program Studi</td>
                                                        <td class="font-medium">
                                                        @foreach ($certificate->study_program as $study_program)
                                                            {{ $study_program->name }} <br/>
                                                        @endforeach
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">Informasi Umum</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">@if($certificate->description==""||$certificate->description==null)
                                                            Tidak ada Informasi tambahan tentang sertifikat ini
                                                            @else
                                                            {{$certificate->description}}
                                                            @endif</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                       <!--  @if(!($certificate->image_name==null||$certificate->image_name==""))
                                        <div class="col-md-offset-3 col-lg-offset-3 col-sm-offset-1 col-md-6 col-sm-12 col-lg-6">
                                            <div class="card">
                                                <div class="d-flex flex-row">

                                                            @if($certificate->type_id==1)
                                                                <div class="p-10 bg-info">
                                                            @else
                                                                <div class="p-10 bg-warning">
                                                            @endif
                                                        <h3 class="text-white box m-b-0"><i class="icon-cloud-download"></i></h3></div>
                                                    <div class="align-self-center m-l-20">
                                                        <h3 class="m-b-0 text-inverse">{{$certificate->image_name}}</h3>
                                                        <h5 class="text-muted m-b-0"><a target="_blank" href="/{{$certificate->image_path}}">Lihat File</a></h5></div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif -->

                                    </div>
                                    <div class="tab-pane" id="list" role="tabpanel">
                                        <div class="card-body">
                                            <div class="table-responsive m-t-40">
                                                <table class="table stylish-table">
                                                    <thead>
                                                        <tr>
                                                            <th width="25%">No. Registrasi</th>
                                                            <th width="25%">Nama Sertifikat</th>
                                                            <th width="10%" class="text-center">Kualifikasi</th>
                                                            <th width="25%" >Tanggal Terbit</th>
                                                            <th width="15%" class="text-center">Masa Berlaku</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if($rent->count()==0)
                                                        <tr>
                                                            <td colspan="5">
                                                                Tidak ada data penyewaan
                                                            </td>
                                                        </tr>
                                                        @endif
                                                        @foreach ($rent as $valuerent)
                                                        <tr>
                                                            <td>
                                                                <a href="/certificate/registration-number/{{$valuerent->registration_number}}" target="_blank">{{$valuerent->registration_number}}</a></td>
                                                            <td>{{ $valuerent->name }}</td>
                                                            <td class="text-center">{{ $valuerent->qualification }}</td>
                                                            <td>
                                                                {{ date("d-m-Y", strtotime($valuerent->date_of_registration)) }}
                                                                </td>
                                                            <td class="text-center">
                                                                @if($valuerent->period_of_validity>360)
                                                                {{$valuerent->period_of_validity/360}} Tahun
                                                                @elseif($valuerent->period_of_validity>30)
                                                                {{$valuerent->period_of_validity/30}} Bulan
                                                                @elseif($valuerent->period_of_validity>7)
                                                                {{$valuerent->period_of_validity/7}} Minggu
                                                                @else
                                                                @endif
                                                                </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- /Row -->

@endsection
