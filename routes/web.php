<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  'HomeController@welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function() {
// Route::get('/certificate-classification', 'CertificateController@index')->name('certificate-classification');
// Route::get('/certificate-list', 'CertificateController@indexUser')->name('certificate-list');;
// Route::get('/certificate-classification-detail/{id}', 'CertificateController@show');
// Route::get('/certificate-classification-code/{code}', 'CertificateController@showcode');
// Route::get('/certificate-classification-search/{query}', 'CertificateController@searchCertificate');
// Route::get('/rent-request', 'RentController@indexRequest')->name('rent-request');
// Route::get('/rent-code/{code}', 'RentController@showSerialNumber');
// Route::get('/rent-detail/{id}', 'RentController@show');
// Route::get('/cart', 'CartController@index')->name('cart');
// Route::get('/order', 'OrderController@index')->name('order');
// Route::get('/order-detail/{id}','OrderController@show');
// Route::get('/order-invoice/{code}','OrderController@showInvoice');
// Route::get('/history', 'RentController@index')->name('history');
// Route::get('/request', 'RentController@index')->name('request');

	Route::group(['prefix' => 'certificate'], function () {
		Route::get('/list', 'CertificateController@indexList')->name('certificate-list');
		Route::get('/classification', 'CertificateController@index')->name('certificate-classification');
		Route::get('/classification/detail/{id}', 'CertificateController@show');
		Route::get('/classification/code/{code}', 'CertificateController@showcode');
		Route::get('/search/{code}', 'CertificateController@searchCertificate');
		Route::get('/registration-number/{registration_number}', 'CertificateController@showRegistrationNumber');
		Route::get('/registration-id/{id}', 'CertificateController@showUserCertificateId');
		Route::get('/request', 'RequestCertificateController@indexRequest')->name('certificate-request');
	});

	Route::group(['prefix' => 'history'], function () {
		Route::get('/request', 'RequestCertificateController@index')->name('history-request');
		Route::get('/order', 'OrderController@index')->name('order');
		Route::get('/order/detail/{id}', 'OrderController@show');
		Route::get('/order/invoice/{id}', 'OrderController@showInvoice');
		Route::get('/rent', 'RequestCertificateController@indexRent')->name('rent');
	});




Route::get('/user-customer', 'UserController@indexCustomer')->name('user-customer');
Route::get('/user-admin', 'UserController@index')->name('user-admin');
Route::get('/user/profile', 'UserController@profile')->name('user-profile');
Route::get('/message', 'MessageController@index')->name('message');

	Route::group(['prefix' => 'setting'], function () {
		Route::get('/notification', 'NotificationController@index')->name('notification');
		Route::get('/page', 'PageController@index')->name('page');
		Route::get('/log', 'LogController@index')->name('log');
	});

	Route::group(['prefix' => 'general-info'], function () {
		Route::get('/company', 'GeneralInfoController@indexCompany')->name('company');
		Route::get('/study-program', 'GeneralInfoController@indexStudyProgram')->name('study-program');
		Route::get('/education-level', 'GeneralInfoController@indexEducationLevel')->name('education-level');
		Route::get('/profession', 'GeneralInfoController@indexProfession')->name('profession');
	});
});

//web API
Route::group(['prefix' => 'webApi'], function () {
	Route::get('/testing', 'Api\RequestCertificateAPIController@allRequestCertificates');
	//list api certificate
	Route::get('/allcertificates', 'Api\CertificateAPIController@allCertificatesWeb');
	Route::get('/allcertificates/type-{type_id}/profession-{profession_id}', 'Api\CertificateAPIController@allCertificatesWebFilterselect');
	Route::get('/allcity/filter_by-{province_id}', 'Api\GeneralInfoAPIController@allCityWebFilter');
	Route::get('/certificate/{id}', 'Api\CertificateAPIController@select');
	Route::get('/certificate-search/{query}-{page}', 'Api\CertificateAPIController@searchCertificate');
	//list api usercertificate
	Route::get('/allusercertificates', 'Api\CertificateAPIController@allUserCertificatesWeb');
	Route::get('/usercertificate/{id}', 'Api\CertificateAPIController@selectUserCertificate');
	Route::get('/usercertificate-code/{code}', 'Api\CertificateAPIController@selectUserCertificateCode');

	//list api category
	Route::get('/allcompanies', 'Api\GeneralInfoAPIController@allCompanies');
	Route::get('/company/{id}', 'Api\GeneralInfoAPIController@selectedCompany');

	//list api category
	Route::get('/alleducation-levels', 'Api\GeneralInfoAPIController@allEducationLevels');
	Route::get('/education-level/{id}', 'Api\GeneralInfoAPIController@selectedEducationLevel');

	//list api category
	Route::get('/allstudy-programs', 'Api\GeneralInfoAPIController@allStudyPrograms');
	Route::get('/study-program/{id}', 'Api\GeneralInfoAPIController@selectedStudyProgram');

	//list api category
	Route::get('/allprofessions', 'Api\GeneralInfoAPIController@allProfessions');
	Route::get('/profession/{id}', 'Api\GeneralInfoAPIController@selectedProfession');

	Route::post('/user-check', 'Api\UserAPIController@checkUser');
	
	Route::group(['middleware' => ['auth']], function() {
		Route::patch('/certificate-status/{id}', 'Api\CertificateAPIController@updateCertificateStatus');
		Route::post('/certificate/create',['as'=>'certificate.store','uses'=>'Api\CertificateAPIController@save','middleware' => ['role:admin|superadmin']]);
		Route::patch('/certificate/{id}',['as'=>'certificate.update','uses'=>'Api\CertificateAPIController@update','middleware' => ['role:admin|superadmin']]);
		Route::delete('/certificate/{id}',['as'=>'certificate.destroy','uses'=>'Api\CertificateAPIController@delete','middleware' => ['role:admin|superadmin']]);
		//

		Route::post('/usercertificate/create',['as'=>'usercertificate.store','uses'=>'Api\CertificateAPIController@saveUserCertificate','middleware' => ['role:admin|superadmin']]);
		Route::patch('/usercertificate/{id}',['as'=>'usercertificate.update','uses'=>'Api\CertificateAPIController@updateUserCertificate','middleware' => ['role:admin|superadmin']]);
		Route::delete('/usercertificate/{id}',['as'=>'usercertificate.destroy','uses'=>'Api\CertificateAPIController@deleteUserCertificate','middleware' => ['role:admin|superadmin']]);


		Route::get('/usercertificate-search/{query}', 'Api\CertificateAPIController@searchUserCertificate');
		Route::get('/usercertificate-detail/{id}', 'Api\CertificateAPIController@selectUserCertificate');

		//cart
		Route::get('/allcarts', 'Api\CartAPIController@allCartWeb');
		Route::post('/cart/add','Api\CartAPIController@addtoCart');
		Route::delete('/cart/{id}','Api\CartAPIController@delete');
		Route::delete('/cart-request/{id}','Api\CartAPIController@deleteRequest');
		Route::delete('/cart-bycertificate/{id}','Api\CartAPIController@deletebycertificate');
		Route::delete('/empty-cart','Api\CartAPIController@deleteAllCartUser');
		//
		Route::post('/checkout','Api\CartAPIController@checkout');

		//list api order

		Route::get('/allorder', 'Api\OrderAPIController@allOrderWeb');
		Route::get('/allorderbystatus-{status}', 'Api\OrderAPIController@allOrderWebByStatus');
		Route::get('/ordercancel/{id}','Api\OrderAPIController@cancelOrder');
		Route::get('/ordersendsoftcopy/{id}','Api\OrderAPIController@sendSoftCopyByOrder');
		Route::patch('/order-status/{type}/{id}', 'Api\RequestAPIController@updateStatusOrder');
		Route::patch('/ordersendhard/{id}', 'Api\RequestAPIController@updateSendRequestHardCopy');
		Route::delete('/order/{id}',['as'=>'order.destroy','uses'=>'Api\OrderAPIController@delete']);

		//list api rent
		// Route::get('/allrents', 'Api\RentAPIController@apiAllcategories');
		// Route::get('/allrequest', 'Api\RentAPIController@apiAllcategories');
		// Route::get('/allrentsbystatus-{status}', 'Api\RentAPIController@allRentStatus');
		// Route::get('/rent/{id}', 'Api\RentAPIController@WebapiCategorySelected');
		// Route::patch('/rent-status/{type}/{id}', 'Api\RentAPIController@updateStatusRent');
		// Route::patch('/rent-processhardcopy/{id}', 'Api\RentAPIController@updateStatusRentHardCopy');
		// Route::post('/rent/create',['as'=>'rent.store','uses'=>'Api\RentAPIController@save']);
		// Route::patch('/rent/{id}',['as'=>'rent.update','uses'=>'Api\RentAPIController@updateCategory']);
		// Route::delete('/rent/{id}',['as'=>'rent.destroy','uses'=>'RentAPIController@destroyCategory']);


		//list api Request
		Route::get('/allrequest', 'Api\RequestCertificateAPIController@allRequest');
		Route::get('/allrequestbystatus-{status}', 'Api\RequestCertificateAPIController@allRequestByStatus');
		Route::get('/request/{id}', 'Api\RequestCertificateAPIController@show');
		Route::patch('/request/{id}', 'Api\RequestCertificateAPIController@update');
		Route::patch('/request-status/{type}/{id}', 'Api\RequestCertificateAPIController@updateStatusRent');
		Route::patch('/request-processhardcopy/{id}', 'Api\RequestCertificateAPIController@updateStatusRentHardCopy');
		Route::get('/request-cancel/{id}', 'Api\RequestCertificateAPIController@cancelRequest');
		Route::delete('/request/{id}', 'Api\RequestCertificateAPIController@delete');

		Route::get('/allusercertificatesByStatus-{status}', 'Api\CertificateAPIController@allUserCertificatesWebStatus');


		//list api payment
		Route::post('/payment/create',['as'=>'payment.store','uses'=>'Api\PaymentAPIController@save']);
		Route::patch('/payment/{id}',['as'=>'payment.update','uses'=>'Api\PaymentAPIController@update']);
		Route::get('/payment/{id}','Api\PaymentAPIController@select');
		Route::get('/payment/{id}','Api\PaymentAPIController@select');
		Route::get('/paymentbyRentID/{id}','Api\PaymentAPIController@selectByRentID');
		Route::get('/paymentbyOrderID/{id}','Api\PaymentAPIController@selectByOrderID');

		//list api User
		Route::get('/allusers', 'Api\UserAPIController@allUsers');
		Route::get('/allusers-admin','Api\UserAPIController@allUserAdmin');
		Route::get('/allusers-customer','Api\UserAPIController@allUserCostumer');
		Route::get('/user/{id}', 'Api\UserAPIController@selectWeb');
		Route::get('/user-profile', 'Api\UserAPIController@getProfile');
		Route::patch('/user-profileupdate', 'Api\UserAPIController@updateProfile');
		Route::patch('/user-status/{id}', 'Api\UserAPIController@updateStatus');
		Route::post('/user/create',['as'=>'user.store','uses'=>'Api\UserAPIController@save','middleware' => ['role:admin|superadmin']]);
		Route::patch('/user/{id}',['as'=>'user.update','uses'=>'Api\UserAPIController@update','middleware' => ['role:admin|superadmin']]);
		Route::delete('/user/{id}',['as'=>'user.destroy','uses'=>'Api\UserAPIController@delete','middleware' => ['role:admin|superadmin']]);

		Route::post('/company/create',['as'=>'company.store','uses'=>'Api\GeneralInfoAPIController@saveCompanyAPI','middleware' => ['role:admin|superadmin']]);
		Route::patch('/company/{id}',['as'=>'company.edit','uses'=>'Api\GeneralInfoAPIController@updateCompany','middleware' => ['role:admin|superadmin']]);
		Route::delete('/company/{id}',['as'=>'company.destroy','uses'=>'Api\GeneralInfoAPIController@deleteCompany','middleware' => ['role:admin|superadmin']]);


		Route::post('/study-program/create',['as'=>'study-program.store','uses'=>'Api\GeneralInfoAPIController@saveStudyProgramAPI','middleware' => ['role:admin|superadmin']]);
		Route::patch('/study-program/{id}',['as'=>'study-program.edit','uses'=>'Api\GeneralInfoAPIController@updateStudyProgram','middleware' => ['role:admin|superadmin']]);
		Route::delete('/study-program/{id}',['as'=>'study-program.destroy','uses'=>'Api\GeneralInfoAPIController@deleteStudyProgram','middleware' => ['role:admin|superadmin']]);

		Route::post('/profession/create',['as'=>'profession.store','uses'=>'Api\GeneralInfoAPIController@saveProfessionAPI','middleware' => ['role:admin|superadmin']]);
		Route::patch('/profession/{id}',['as'=>'profession.edit','uses'=>'Api\GeneralInfoAPIController@updateProfession','middleware' => ['role:admin|superadmin']]);
		Route::delete('/profession/{id}',['as'=>'profession.destroy','uses'=>'Api\GeneralInfoAPIController@deleteProfession','middleware' => ['role:admin|superadmin']]);
		//list api Logs
		Route::get('/alllogs', 'LogController@allLog');
		//list api Notification
		Route::get('/allnotifications', 'NotificationController@allNotification');
		Route::get('/allnotifications-byuser', 'NotificationController@allNotificationUser');
		Route::get('/allnotifications-byuser-home', 'NotificationController@allNotificationUserHome');
		Route::get('/notification/{id}', 'NotificationController@select');
		Route::get('/notification-resend/{id}', 'NotificationController@resend');
		Route::post('/notification/create',['as'=>'notification.store','uses'=>'NotificationController@save','middleware' => ['role:admin|superadmin']]);
		Route::delete('/notification/{id}',['as'=>'notification.destroy','uses'=>'NotificationController@delete','middleware' => ['role:admin|superadmin']]);
	});
});