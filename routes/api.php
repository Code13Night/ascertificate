<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('tempdelete'
			, 'TempdeleteController@savetempdelete');
Route::get('alltempdelete'
			, 'TempdeleteController@alltempdelete');
Route::post('tempdeletefile'
			, 'TempdeleteController@filename');


Route::get('/home', 'HomeController@ApiHome');

Route::get('/allcertificatesnopage', 'Api\CertificateAPIController@allCertificatesWeb');
Route::get('/allcertificates/type-{type_id}/profession-{profession_id}', 'Api\CertificateAPIController@allCertificatesWebFilterselect');
Route::get('/allcertificates-{page}', 'Api\CertificateAPIController@allAvailableCertificates');
Route::get('/allcertificates-{page}/{order}-{sort}', 'Api\CertificateAPIController@allAvailableCertificatesOrder');
Route::get('/allcertificatesfilter-{page}/filter/{filter}-{filtervalue}', 'Api\CertificateAPIController@allCertificatesFilterBy');
Route::get('/allcertificatesfilter-{page}/filter/{filter}-{filtervalue}/{order}-{sort}', 'Api\CertificateAPIController@allCertificatesFilterByOrder');
Route::get('/certificate/{id}', 'Api\CertificateAPIController@select');
Route::get('/certificate-search/{query}-{page}', 'Api\CertificateAPIController@searchCertificate');
Route::get('/allcertificatesfilter/type-{type_id}/profession-{profession_id}', 'Api\CertificateAPIController@allCertificatesWebFilterselect');

Route::get('/allcompanies-{page}', 'Api\GeneralInfoAPIController@allCompaniesPaginate');
Route::get('/allcompanies-{page}/{order?}-{sort?}', 'Api\GeneralInfoAPIController@allCompaniesPaginateOrder');
Route::get('/company/{id}', 'Api\GeneralInfoAPIController@selectedCompany');

Route::get('/alleducation-levels-{page}', 'Api\GeneralInfoAPIController@allEducationLevelsPaginate');
Route::get('/alleducation-levels-{page}/{order?}-{sort?}', 'Api\GeneralInfoAPIController@allEducationLevelsPaginateOrder');
Route::get('/education-level/{id}', 'Api\GeneralInfoAPIController@selectedEducationLevel');

Route::get('/allstudy-programs-{page}', 'Api\GeneralInfoAPIController@allStudyProgramsPaginate');
Route::get('/allstudy-programs-{page}/{order?}-{sort?}', 'Api\GeneralInfoAPIController@allStudyProgramsPaginateOrder');
Route::get('/study-program/{id}', 'Api\GeneralInfoAPIController@selectedStudyProgram');

Route::get('/allprofessions-{page}', 'Api\GeneralInfoAPIController@allProfessionsPaginate');
Route::get('/allprofessions-{page}/{order?}-{sort?}', 'Api\GeneralInfoAPIController@allProfessionsPaginateOrder');
Route::get('/profession/{id}', 'Api\GeneralInfoAPIController@selectedProfession');


Route::get('/allprovinces', 'Api\GeneralInfoAPIController@getAllProvinces');
Route::get('/province/{id}', 'Api\GeneralInfoAPIController@getSelectedProvince');
Route::get('/allcities', 'Api\GeneralInfoAPIController@getAllCities');

//authentication costumize
Route::group(['prefix' => 'authcostum'], function () {
	Route::post('register'
		, 'Api\UserAPIController@register');
	Route::post('login'
		, 'Api\UserAPIController@login');


	Route::group(['middleware' => ['auth:api']], function() {
		//get Profile
		Route::get('/profile', 'Api\UserAPIController@getProfile');
		//Update Profile
		Route::patch('/profile/update', 'Api\UserAPIController@updateProfile');
		
		//autentication controller
		Route::post('logout'
			, 'Api\UserAPIController@destroyConnectiontoDevice');
		Route::post('logout-all'
			, 'Api\UserAPIController@destroyConnectiontoAllDevice');
		Route::post('logout-other'
			, 'Api\UserAPIController@destroyConnectiontoAllDeviceExpectCurrent');
		Route::post('check-device'
			, 'Api\UserAPIController@Checkconnection');

	});
});
	
Route::group(['middleware' => ['auth:api']], function() {
	//carts
	Route::get('/allcarts-{page}','Api\CartAPIController@allCartUser');
	Route::post('/cart/add','Api\CartAPIController@addtoCart');
	Route::delete('/cart/{id}','Api\CartAPIController@delete');
		Route::delete('/cart-request/{id}','Api\CartAPIController@deleteRequest');
		Route::delete('/cart-bycertificate/{id}','Api\CartAPIController@deletebycertificate');
	Route::delete('/empty-cart','Api\CartAPIController@deleteAllCartUser');
	//
	Route::post('/checkout','Api\CartAPIController@checkout');
	//order
	Route::get('/allorders-{page}','Api\OrderAPIController@allOrderUser');
	Route::get('/allorders-{page}/order-{orderBy}/sort-{sortBy}','Api\OrderAPIController@allOrderUser');
	Route::get('/order/{id}','Api\OrderAPIController@selectedOrder');
	Route::patch('/order/{id}','Api\OrderAPIController@updateOrder');
	Route::delete('order/{id}','Api\OrderAPIController@deleteOrder');
	//rents
	Route::get('/allrents-{type}-{page}', 'Api\RequestCertificateAPIController@allRequestUser');
	Route::get('/rent/{id}', 'Api\RequestCertificateAPIController@show');
	Route::post('/rent/create',['as'=>'rent.store','uses'=>'Api\RentAPIController@save']);
	//payments
	Route::patch('/payment/{id}',['as'=>'rent.update','uses'=>'Api\PaymentAPIController@update']);

	//message
	Route::get('/allconversation', 'Api\MessageAPIController@apiAllcategories');
	Route::get('/conversation/{id}', 'Api\MessageAPIController@WebapiCategorySelected');
	Route::post('/message/create',['as'=>'rent.store','uses'=>'Api\MessageAPIController@storeCategory','middleware' => ['role:admin|superadmin']]);
	Route::delete('/conversation/{id}',['as'=>'rent.destroy','uses'=>'Api\MessageAPIController@destroyCategory','middleware' => ['role:admin|superadmin']]);
	Route::delete('/message/{id}',['as'=>'rent.destroy','uses'=>'Api\MessageAPIController@destroyCategory','middleware' => ['role:admin|superadmin']]);
	//notifications
	Route::get('/allnotifications', 'Api\NotificationAPIController@apiAllcategories');
	Route::delete('/notification/{id}',['as'=>'notif.destroy','uses'=>'Api\NotificationAPIController@destroyCategory','middleware' => ['role:admin|superadmin']]);
	

});