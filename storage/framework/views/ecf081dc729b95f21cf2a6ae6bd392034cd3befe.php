<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo e(asset('images/icon/apple-icon-57x57.png')); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo e(asset('images/icon/apple-icon-60x60.png')); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo e(asset('images/icon/apple-icon-72x72.png')); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('images/icon/apple-icon-76x76.png')); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo e(asset('images/icon/apple-icon-114x114.png')); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(asset('images/icon/apple-icon-120x120.png')); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo e(asset('images/icon/apple-icon-144x144.png')); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(asset('images/icon/apple-icon-152x152.png')); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo e(asset('images/icon/apple-icon-180x180.png')); ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo e(asset('images/icon/android-icon-192x192.png')); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(asset('images/icon/favicon-32x32.png')); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo e(asset('images/icon/favicon-96x96.png')); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('images/icon/favicon-16x16.png')); ?>">
    <link rel="manifest" href="<?php echo e(asset('images/icon/manifest.json')); ?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo e(asset('images/icon/ms-icon-144x144.png')); ?>">
    <meta name="theme-color" content="#ffffff">
    <title>SKA SKT ID -  <?php echo $__env->yieldContent('title'); ?></title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo e(asset('backend/assets/plugins/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('backend/assets/plugins/toast-master/css/jquery.toast.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('backend/assets/plugins/nprogress/nprogress.css')); ?>" rel="stylesheet">

    <?php echo $__env->yieldContent('css'); ?>
    <!-- Custom CSS -->
    <link href="<?php echo e(asset('backend/css/style.css')); ?>" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo e(asset('backend/css/colors/blue.css')); ?>" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<?php if(Auth::user()->hasRole('user')): ?>
<body class="card-no-border">
    <?php else: ?>
<body class="fix-header fix-sidebar card-no-border">
    <?php endif; ?>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="/home">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="<?php echo e(asset('images/icon/favicon-32x32.png')); ?>" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php echo e(asset('images/icon/favicon-32x32.png')); ?>" alt="homepage" class="light-logo" />
                            <!-- AS -->
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>Project</span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown indicatorclick">
                            <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-message"></i>
                                <div class="notify activitynotify"> 
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                                <ul>
                                    <li>
                                        <div class="drop-title">Notifications</div>
                                    </li>
                                    <li>
                                        <div class="message-center notifbody">
                                            
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="<?php echo e(route('notification')); ?>"> <strong>Lihat Semua Notifikasi</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <?php if(Auth::user()->hasRole('user')): ?>
                        <!-- ============================================================== -->
                        <!-- Cart -->
                        <!-- ============================================================== -->
                        <li class="nav-item">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="/certificate/request#cartOpen"> <i class="mdi mdi-cart"></i>
                                
                                <div class="notify cartnotify"> 
                                    <?php if(Session::get('availablecart')): ?>
                                    <span class="heartbit"></span> <span class="point"></span>
                                    <?php endif; ?>
                                </div>
                                
                            </a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Cart -->
                        <!-- ============================================================== -->
                        <?php endif; ?>
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo e(asset('backend/assets/images/users/admin.png')); ?>" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php echo e(asset('backend/assets/images/users/admin.png')); ?>" alt="user"></div>
                                            <div class="u-text">
                                                <h4><?php echo e(Auth::user()->name); ?> </h4>
                                                <p class="text-muted"><?php echo e(Auth::user()->email); ?> </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<?php echo e(route('user-profile')); ?>"><i class="ti-settings"></i> Pengaturan Akun</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<?php echo e(route('logout')); ?>"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                     <i class="fa fa-power-off"></i> Logout</a>
                                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                        <?php echo csrf_field(); ?>
                                    </form>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li <?php if(Request::url()==url('/home')): ?>
                            class="active" 
                            <?php endif; ?>
                        >
                            <a href="<?php echo e(route('home')); ?>" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Dashboard </span></a>
                        </li> 
                        <?php if(Auth::user()->hasRole(['admin','superadmin','cs'])): ?>
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">Sertifikat</li>
                        <li <?php if(Request::url()==url('/certificate/classification')||Request::url()==url('/certificate/detail')||Request::url()==url('/certificate/code')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('certificate-classification')); ?>" aria-expanded="false"><i class="mdi mdi-tag-multiple"></i><span class="hide-menu">Klasifikasi Sertifikat </span></a>
                        </li>
                        <li <?php if(Request::url()==url('/certificate-list')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('certificate-list')); ?>" aria-expanded="false"><i class="mdi mdi-certificate"></i><span class="hide-menu">Daftar Sertifikat </span></a>
                        </li>
                        <?php endif; ?>
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">Sewa</li>
                        <li <?php if(Request::url()==url('/history/request')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('certificate-request')); ?>" aria-expanded="false"><i class="mdi mdi-repeat"></i><span class="hide-menu">Permohonan Sewa </span></a>
                        </li>

                        <?php if(Auth::user()->hasRole(['admin','superadmin','cs'])): ?>
                        <li <?php if(Request::url()==url('/history/rent')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('rent')); ?>" aria-expanded="false"><i class="mdi mdi-history"></i><span class="hide-menu">Riwayat Penyewaan </span></a>
                        </li>
                        <?php endif; ?>

                        <?php if(Auth::user()->hasRole(['user'])): ?>
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">Riwayat</li>

                        <li <?php if(Request::url()==url('/order')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('order')); ?>" aria-expanded="false"><i class="mdi mdi-package-down"></i><span class="hide-menu">Riwayat Order </span></a>
                        </li>
                        <li <?php if(Request::url()==url('/history/request')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('history-request')); ?>" aria-expanded="false"><i class="mdi mdi-book-open"></i><span class="hide-menu">Riwayat Permohonan</span></a>
                        </li>

                        <li <?php if(Request::url()==url('/history/rent')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('rent')); ?>" aria-expanded="false"><i class="mdi mdi-certificate"></i><span class="hide-menu">Riwayat Sewa Sertifikat </span></a>
                        </li>
                        <?php endif; ?>
                        
                        <?php if(Auth::user()->hasRole(['admin','superadmin','cs'])): ?>
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">User</li>
                        <li <?php if(Request::url()==url('/user-admin')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('user-admin')); ?>" aria-expanded="false"><i class="mdi mdi-account-star"></i><span class="hide-menu">Admin </span> </a>
                        </li>
                        <li <?php if(Request::url()==url('/user-customer')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('user-customer')); ?>" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">Penyewa </span> </a>
                        </li>
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">Informasi Umum</li>
                        <!-- <li <?php if(Request::url()==url('/general-info/company')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('company')); ?>" aria-expanded="false"><i class="mdi mdi-city"></i><span class="hide-menu">Perusahaan </span> </a>
                        </li> -->
                        <li <?php if(Request::url()==url('/general-info/profession')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('profession')); ?>" aria-expanded="false"><i class="mdi mdi-briefcase"></i><span class="hide-menu">Profesi </span>  </a>
                        </li>
                        <li <?php if(Request::url()==url('/general-info/study-program')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('study-program')); ?>" aria-expanded="false"><i class="mdi mdi-book"></i><span class="hide-menu">Program Studi </span>  </a>
                        </li>
                        <li <?php if(Request::url()==url('/general-info/education-level')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('education-level')); ?>" aria-expanded="false"><i class="mdi mdi-library"></i><span class="hide-menu">Jenjang Pendidikan </span>  </a>
                        </li>
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">Extra</li>
                        <li <?php if(Request::url()==url('/setting/notification')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('notification')); ?>" aria-expanded="false"><i class="mdi mdi-bell-ring-outline"></i><span class="hide-menu">Notifikasi </span>  </a>
                        </li>
                        <!-- <li <?php if(Request::url()==url('/setting/page')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('page')); ?>" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Konten Halaman </span>  </a>
                        </li> -->
                        <li <?php if(Request::url()==url('/setting/log')): ?>
                            class="active" 
                            <?php endif; ?>
                        > 
                            <a href="<?php echo e(route('log')); ?>" aria-expanded="false"><i class="mdi mdi-information"></i><span class="hide-menu">Log</span>  </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <!-- item--><a href="<?php echo e(route('user-profile')); ?>" class="link" data-toggle="tooltip" title="Pengaturan"><i class="ti-settings"></i></a>
                <!-- item--> <a href="<?php echo e(route('notification')); ?>" class="link" data-toggle="tooltip" title="Notifikasi"><i class="mdi mdi-message"></i></a> 
                <!-- item--><a href="<?php echo e(route('logout')); ?>"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a> </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                    <?php echo $__env->yieldContent('content-header'); ?>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                    <?php echo $__env->yieldContent('content'); ?>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2018 Project AS </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo e(asset('backend/assets/plugins/jquery/jquery.min.js')); ?>"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo e(asset('backend/assets/plugins/popper/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('backend/assets/plugins/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo e(asset('backend/js/jquery.slimscroll.js')); ?>"></script>
    <!--Wave Effects -->
    <script src="<?php echo e(asset('backend/js/waves.js')); ?>"></script>
    <!--Menu sidebar -->
    <script src="<?php echo e(asset('backend/js/sidebarmenu.js')); ?>"></script>
    <!--stickey kit -->
    <script src="<?php echo e(asset('backend/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')); ?>"></script>
    <script src="<?php echo e(asset('backend/assets/plugins/sparkline/jquery.sparkline.min.js')); ?>"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo e(asset('backend/js/custom.min.js')); ?>"></script>
    <script src="<?php echo e(asset('backend/assets/plugins/toast-master/js/jquery.toast.js')); ?>"></script>
    <script src="<?php echo e(asset('backend/assets/plugins/nprogress/nprogress.js')); ?>"></script>
    <script type="text/javascript">
            function formatDate(date) {
              var hours = date.getHours();
              var minutes = date.getMinutes();
              var ampm = hours >= 12 ? 'pm' : 'am';
              hours = hours % 12;
              hours = hours ? hours : 12; // the hour '0' should be '12'
              minutes = minutes < 10 ? '0'+minutes : minutes;
              var strTime = hours + ':' + minutes + ' ' + ampm;
              return date.getDate() + "-"+ parseInt(date.getMonth()+1) + "-" + date.getFullYear() + "  " + strTime;
            }


            var lastnumber=0;
            function callnotif() {
            var insidehtml="";
                $.ajax({
                    cache: false,
                    type: 'GET',
                    url: '/webApi/allnotifications-byuser-home',
                    success: function (data) {
                        $(data).each(function (index, item) {
                            if(item.data.length>0){
                                $(item.data).each(function (indexx, itemx) {
                                    if(itemx.id>lastnumber){
                                        lastnumber=itemx.id;
                                        $('.activitynotify').html('<span class="heartbit"></span> <span class="point"></span>');
                                    }
                                    insidehtml=insidehtml+'<!-- Message -->'+
                                                '<a href="javascript:void(0)">'+
                                                    '<div class="mail-contnet">'+
                                                        '<h5>'+itemx.title+'</h5> <span class="mail-desc">'+itemx.body+'</span> <span class="time">'+formatDate( new Date(itemx.created_at))+'</span> </div>'+
                                                '</a>';
                                });   
                            }else{         
                            lastnumber=0;
                            insidehtml='<!-- Message -->'+
                                        '<a href="javascript:void(0)">'+
                                            '<div class="mail-contnet">'+
                                                '<span class="mail-desc">Tidak ada notifikasi </span></div>'+
                                        '</a>';
                            $('.activitynotify').html('');
                            }
                        });                     
                        $('.notifbody').html(insidehtml);
                    }
                });  
            }
            callnotif();
            window.setInterval(function(){
             callnotif();
            }, 300000);
            
            $('.indicatorclick').on('click',function (e) {
                $('.activitynotify').html('');
            })
    </script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->

    <?php echo $__env->yieldContent('js'); ?>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php echo e(asset('backend/assets/plugins/styleswitcher/jQuery.style.switcher.js')); ?>"></script>
    
</body>

</html>