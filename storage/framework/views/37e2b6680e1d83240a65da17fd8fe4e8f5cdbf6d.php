<!DOCTYPE html>
<html lang="en">

<head>
    <!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"><!-- Favicon icon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo e(asset('images/icon/apple-icon-57x57.png')); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo e(asset('images/icon/apple-icon-60x60.png')); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo e(asset('images/icon/apple-icon-72x72.png')); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('images/icon/apple-icon-76x76.png')); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo e(asset('images/icon/apple-icon-114x114.png')); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(asset('images/icon/apple-icon-120x120.png')); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo e(asset('images/icon/apple-icon-144x144.png')); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(asset('images/icon/apple-icon-152x152.png')); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo e(asset('images/icon/apple-icon-180x180.png')); ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo e(asset('images/icon/android-icon-192x192.png')); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(asset('images/icon/favicon-32x32.png')); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo e(asset('images/icon/favicon-96x96.png')); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('images/icon/favicon-16x16.png')); ?>">
    <link rel="manifest" href="<?php echo e(asset('images/icon/manifest.json')); ?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo e(asset('images/icon/ms-icon-144x144.png')); ?>">
    <meta name="theme-color" content="#ffffff">
    <title>Construct Services Certification Handling and Rental</title>
    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/bootstrap/css/bootstrap.min.css')); ?>">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Neuton:400,200,700,300' rel='stylesheet' type='text/css'>
    <!-- FONT AWESOME CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/fonts/font-awesome.min.css')); ?>">
    <!-- OWL CAROSEL CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/owlcarousel/css/owl.carousel.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/owlcarousel/css/owl.theme.css')); ?>">
    <!-- LIGHTBOX CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/css/lightbox.min.css')); ?>">
    <!-- MAIN STYLE CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/css/style.css')); ?>">
    <!-- RESPONSIVE CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/css/responsive.css')); ?>">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- START PRELOADER -->
    <div class="preloader">
        <div class="status">
            <div class="status-mes"></div>
        </div>
    </div>
    <!-- / END PRELOADER -->


    <!-- START SINGLE BLOG PAGE DESIGN AREA -->
    <header id="home" class="welcome-area single-blog-area">
        <div class="header-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <!-- START LOGO DESIGN AREA -->
                        <div class="logo">
                            <a href="">AS</a>
                        </div>
                        <!-- END LOGO DESIGN AREA -->
                    </div>
                    <div class="col-md-9">
                        <!-- START MENU DESIGN AREA -->
                        <div class="mainmenu">
                            <div class="navbar navbar-nobg">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li><a  href="/home#home">Home <div class="ripple-wrapper"></div></a></li>
                                        <li><a  href="/home#about">Tentang Kami</a></li>
                                        <li><a  href="/home#service">Layanan</a></li>
                                        <li  class="active"><a  href="/home#certificate">Sertifikat</a></li>
                                        <li><a  href="/home#contact">Hubungi Kami</a></li>
                                        <?php if(auth()->guard()->guest()): ?>
                                        <li><a href="<?php echo e(route('login')); ?>">Login</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- END MENU DESIGN AREA -->
                    </div>
                </div>
            </div>
        </div>
        <div class="single-page-heading-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="single-blog-heading-text text-center">
                            <h4><a href="/">AS </a>/ Sertifikat</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
     <!-- / END SINGLE BLOG PAGE DESIGN AREA -->

    <?php echo $__env->yieldContent('content'); ?>

    <!-- START FOOTER DESIGN AREA -->
    <footer class="footer-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-text text-center">
                        <h1>AS </h1>
                        <h4>Construction Services Certification Handling and Rental</h4>
                        <h6 class="text-muted">&copy;copyright 2016 Project AS</h6>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- / END CONTACT DETAILS DESIGN AREA -->
    <!-- START SCROOL UP DESIGN AREA -->
    <div class="scroll-to-up">
        <div class="scrollup">
            <i class="fa fa-angle-up"></i>
        </div>
    </div>
    <!-- / END SCROOL UP DESIGN AREA -->

    <!-- LATEST JQUERY -->
    <script src="<?php echo e(asset('frontend/assets/js/jquery.min.js')); ?>"></script>
    <!-- BOOTSTRAP JS -->
    <script src="<?php echo e(asset('frontend/assets/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <!-- OWL CAROUSEL JS  -->
    <script src="<?php echo e(asset('frontend/assets/owlcarousel/js/owl.carousel.min.js')); ?>"></script>
    <!-- ISOTOP JS -->
    <script src="<?php echo e(asset('frontend/assets/js/jquery.mixitup.js')); ?>"></script>
    <!-- LIGHTBOX JS -->
    <script src="<?php echo e(asset('frontend/assets/js/lightbox.min.js')); ?>"></script>
    <!-- scripts js -->
    <script src="<?php echo e(asset('frontend/assets/js/scripts.js')); ?>"></script>

    <?php echo $__env->yieldContent('js'); ?>
</body>


<!-- Mirrored from wordpressboss.com/opb/demo-aisha/aisha/single-blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Feb 2018 18:16:35 GMT -->
</html>