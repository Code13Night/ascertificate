<?php $__env->startSection('title'); ?>
Detail
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>

    <link rel="stylesheet" href="<?php echo e(asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.css')); ?>" />
    <link href="<?php echo e(asset('backend/assets/plugins/select2/dist/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    
    <script src="<?php echo e(asset('backend/assets/plugins/select2/dist/js/select2.full.min.js')); ?>" type="text/javascript"></script>

    <script src="<?php echo e(asset('backend/assets/plugins/html5-editor/wysihtml5-0.3.0.js')); ?>"></script>
    <script src="<?php echo e(asset('backend/assets/plugins/html5-editor/bootstrap-wysihtml5.js')); ?>"></script>
    <script type="text/javascript">
        

        $('.textarea_editor').wysihtml5();
        $(".select2").select2({
                width: "100%"
            });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content-header'); ?>
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Sertifikat</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Sertifikat</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e(route('certificate-classification')); ?>">Klasifikasi Sertifikat</a></li>
                            <li class="breadcrumb-item active">Detail</li>
                        </ol>
                    </div>
                </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
                <!-- Row -->
                <div class="row">
                    <!-- /Column -->
                    <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs profile-tab" role="tablist">
                                    <li class="nav-item col-md-6 col-sm-6 text-center"> <a class="nav-link active" data-toggle="tab" href="#general_info" role="tab">Informasi Umum</a> </li>
                                    <li class="nav-item col-md-6 col-sm-6 text-center"> <a class="nav-link" data-toggle="tab" href="#list" role="tab">Daftar Sertifikat</a> </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general_info" role="tabpanel">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center flex-row m-t-30">
                                                <div class="p-2 display-5 text-info"><span><?php echo e($certificate->classification_code); ?></span></div>
                                                <div class="p-2">
                                                <h3 class="m-b-0"><?php echo e($certificate->name); ?></h3><small>
                                                            <?php if($certificate->type_id==1): ?>
                                                                <label class="label label-rounded label-info">
                                                            <?php else: ?>
                                                                <label class="label label-rounded label-warning">
                                                            <?php endif; ?>
                                                            <?php echo e($certificate->type_name); ?></label></small></div>
                                            </div>
                                            <table class="table no-border">
                                                <tbody>
                                                    <tr>
                                                        <td width="40%">Kode Klasifikasi</td>
                                                        <td class="font-medium"><?php echo e($certificate->classification_code); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Tipe</td>
                                                        <td class="font-medium">
                                                            <?php if($certificate->type_id==1): ?>
                                                                <label class="label label-rounded label-info">
                                                            <?php else: ?>
                                                                <label class="label label-rounded label-warning">
                                                            <?php endif; ?>
                                                            <?php echo e($certificate->type_name); ?></label></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Bidang</td>
                                                        <td class="font-medium"><?php echo e($certificate->profession_name); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Sub Bidang</td>
                                                        <td class="font-medium"><?php echo e($certificate->name); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Harga Soft Copy</td>
                                                        <td class="font-medium">Rp. <?php echo e(number_format($certificate->price_soft,2,",",".")); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Harga Hard Copy</td>
                                                        <td class="font-medium">Rp. <?php echo e(number_format($certificate->price_hard,2,",",".")); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Status</td>
                                                        <td class="font-medium"><?php if($certificate->status==1): ?>
                                                                <label class="label label-rounded label-success">
                                                                    Tersedia
                                                            <?php else: ?>
                                                                <label class="label label-rounded label-danger">
                                                                    Tidak Tersedia
                                                            <?php endif; ?>
                                                            </label></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Masa Berlaku</td>
                                                        <td class="font-medium"><?php if($certificate->period_of_validity>360): ?>
                                                                <?php echo e($certificate->period_of_validity/360); ?> Tahun
                                                            <?php elseif($certificate->period_of_validity>60): ?>
                                                                <?php echo e($certificate->period_of_validity/60); ?> Bulan
                                                            <?php else: ?>
                                                                <?php echo e($certificate->period_of_validity); ?> Hari
                                                            <?php endif; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Persyaratan Tingkat Pendidikan</td>
                                                        <td class="font-medium">
                                                        <?php $__currentLoopData = $certificate->education_level; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $education_level): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php echo e($education_level->name); ?> <br/>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Persyaratan Program Studi</td>
                                                        <td class="font-medium">
                                                        <?php $__currentLoopData = $certificate->study_program; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $study_program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php echo e($study_program->name); ?> <br/>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">Informasi Umum</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"><?php if($certificate->description==""||$certificate->description==null): ?>
                                                            Tidak ada Informasi tambahan tentang sertifikat ini
                                                            <?php else: ?>
                                                            <?php echo e($certificate->description); ?>

                                                            <?php endif; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                       <!--  <?php if(!($certificate->image_name==null||$certificate->image_name=="")): ?>
                                        <div class="col-md-offset-3 col-lg-offset-3 col-sm-offset-1 col-md-6 col-sm-12 col-lg-6">
                                            <div class="card">
                                                <div class="d-flex flex-row">

                                                            <?php if($certificate->type_id==1): ?>
                                                                <div class="p-10 bg-info">
                                                            <?php else: ?>
                                                                <div class="p-10 bg-warning">
                                                            <?php endif; ?>
                                                        <h3 class="text-white box m-b-0"><i class="icon-cloud-download"></i></h3></div>
                                                    <div class="align-self-center m-l-20">
                                                        <h3 class="m-b-0 text-inverse"><?php echo e($certificate->image_name); ?></h3>
                                                        <h5 class="text-muted m-b-0"><a target="_blank" href="/<?php echo e($certificate->image_path); ?>">Lihat File</a></h5></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?> -->

                                    </div>
                                    <div class="tab-pane" id="list" role="tabpanel">
                                        <div class="card-body">
                                            <div class="table-responsive m-t-40">
                                                <table class="table stylish-table">
                                                    <thead>
                                                        <tr>
                                                            <th width="25%">No. Registrasi</th>
                                                            <th width="25%">Nama Sertifikat</th>
                                                            <th width="10%" class="text-center">Kualifikasi</th>
                                                            <th width="25%" >Tanggal Terbit</th>
                                                            <th width="15%" class="text-center">Masa Berlaku</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php if($rent->count()==0): ?>
                                                        <tr>
                                                            <td colspan="5">
                                                                Tidak ada data penyewaan
                                                            </td>
                                                        </tr>
                                                        <?php endif; ?>
                                                        <?php $__currentLoopData = $rent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $valuerent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr>
                                                            <td>
                                                                <a href="/certificate/registration-number/<?php echo e($valuerent->registration_number); ?>" target="_blank"><?php echo e($valuerent->registration_number); ?></a></td>
                                                            <td><?php echo e($valuerent->name); ?></td>
                                                            <td class="text-center"><?php echo e($valuerent->qualification); ?></td>
                                                            <td>
                                                                <?php echo e(date("d-m-Y", strtotime($valuerent->date_of_registration))); ?>

                                                                </td>
                                                            <td class="text-center">
                                                                <?php if($valuerent->period_of_validity>360): ?>
                                                                <?php echo e($valuerent->period_of_validity/360); ?> Tahun
                                                                <?php elseif($valuerent->period_of_validity>30): ?>
                                                                <?php echo e($valuerent->period_of_validity/30); ?> Bulan
                                                                <?php elseif($valuerent->period_of_validity>7): ?>
                                                                <?php echo e($valuerent->period_of_validity/7); ?> Minggu
                                                                <?php else: ?>
                                                                <?php endif; ?>
                                                                </td>
                                                        </tr>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- /Row -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>