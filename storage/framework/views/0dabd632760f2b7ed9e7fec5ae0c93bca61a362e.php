<?php $__env->startSection('title'); ?>
Admin
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <!-- This is data table -->
    <script src="<?php echo e(asset('backend/assets/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
        var table=$('#myTable').DataTable({iDisplayLength: 10,
            responsive: true,
            lengthMenu: [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "order": [
                [0, 'asc']
            ],
            "ajax": '/webApi/allusers-admin',
            "columns": [
                    { "data": "username" },
                    { "data": "email" },
                    { "data": "display_name" },
                    { "data": "active" },
                    { "data": "user_id" },
            ], "columnDefs": [{
                "targets": 4,
                "data": "Action_Link",
                orderable: false,
                className: "text-center",
                "render": function (data, type, full, meta) {
                    return '<a href="javascript:void(0)" id=' + data + ' data-toggle="tooltip" title="Edit" class="btnedit icon text-warning"><i class="mdi mdi-lead-pencil"></i></a>' +
                                     '  <a  href="javascript:void(0)" id=' + data + ' data-toggle="modal" data-target="#myModal" title="Delete" class="btndelete icon text-danger"><i class="mdi mdi-delete"></i></a>';

                }
            },{
                "targets": 3,
                "data": "active",
                orderable: false,
                className: "text-center",
                "render": function (data, type, full, meta) {
                    if(data==1){
                        return '<span class="label label-success">Active</span>';
                    }else{
                        return '<span class="label label-danger">inactive</span>';
                    }
                }
            }
            ],
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('.hideadd').on('click', function (e) {
            $('#createSection').hide('fast');
        });
        $('.btncreate').on('click', function (e) {
            $('#createSection').removeClass('hide');
            $('#createSection').show('slow');
            $("#createuser").attr("action", "/webApi/user/create");
            $('#editSection input').remove();
        });
        //Post new User
        $('#createuser').submit(function (e) {
            //prevent from action submit default
            e.preventDefault();
            //Inserting data from Form to JSON format
            var datastring = $("#createuser").serialize();
            //get action URL
            var $form = $(this),
              url = $form.attr("action");
            //ajax POST
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                data: datastring,
                success: function (data) { 
                    $(data).each(function (index, item) {
                        if(item.success){
                            table.ajax.reload();                        
                            $('#name').val('');
                            $('#email').val('');
                            $('#roles').val('');
                            $('#confirm-password').val('');
                            $('#password').val('');
                            $('#createSection').hide('fast');
                            $('#editSection input').remove(); 
                        }else{
                            $('#myModalerror').modal('toggle');
                        }
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    $('#myModalerror').modal('toggle');
                }                   
            });  
        });

        //Delete Confirmation
        $('#myTable tbody').on('click', '.btndelete', function (e) {
            e.preventDefault();
            $iddel=this.id;
        });
        //edit value
        $('#myTable tbody').on('click', '.btnedit', function (e) {
            e.preventDefault();
            $idshow=this.id;
            $.ajax({
                cache: false,
                type: 'GET',
                url: '/webApi/user/'+$idshow,
                success: function (data) {
                    $(data).each(function (index, item) {
                        $(item.data).each(function (indexx, itemx) {
                            $('#name').val(itemx.name);
                            $('#name').focus();
                            $('#email').val(itemx.email);
                            $(itemx.role).each(function (indexx, itemxx) {
                                $('#roles').val(itemxx.role_id);
                            });
                        });
                    });                     
                    $('#createSection').show('slow');
                    $('#editSection').append('<input name="_method" type="hidden" value="PATCH">');
                    $("#createuser").attr("action", "/webApi/user/" + $idshow);
                }
            });
        });
        //Delete Confirmation
        $('#btnconfirmdelete').on('click', function (e) {
            $('#myModal').modal('toggle');
            
            $.ajax({
                cache: false,
                type: 'DELETE',
                url: '/webApi/user/'+$iddel,
                data: { _method: 'DELETE', _token: $('input[name=_token]').val() },
                success: function (data) {
                    table.ajax.reload();
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content-header'); ?>
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Admin</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">User</a></li>
                            <li class="breadcrumb-item active">Admin</li>
                        </ol>
                    </div>
                </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
                

                <!-- Row -->
                <div class="row hide" id="createSection">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">

                            <div class="card-header">
                                <button class="pull-right btn btn-sm btn-rounded btn-info hideadd"  data-toggle="tooltip" title="Tutup tambah user">X</button>
                                <h4 class="m-b-0 text-white" id="tileCreateSection">Tambah Admin</h4>
                            </div>
                            <div class="card-body">
                                <?php echo Form::open(array('route' => 'user.store','method'=>'POST','id'=>'createuser','class'=>'form-horizontal ')); ?>

                                    <div class="form-body">
                                        <div class="row" id="editSection">

                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Nama</label>
                                                    <div class="col-md-9">
                                                        <?php echo Form::text('name', null, array('placeholder' => 'Nama User','id'=>'name','class' => 'form-control')); ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Email</label>
                                                    <div class="col-md-9">
                                                        <?php echo Form::text('email', null, array('placeholder' => 'Email','id'=>'email','class' => 'form-control')); ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Password</label>
                                                    <div class="col-md-9">
                                                        <?php echo Form::password('password', array('placeholder' => 'Password','id'=>'password','class' => 'form-control')); ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Password</label>
                                                    <div class="col-md-9">
                                                        <?php echo Form::password('confirm-password', array('placeholder' => 'Confirm Password','id'=>'confirm-password','class' => 'form-control')); ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <!--/span-->
                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Role</label>
                                                    <div class="col-md-5">
                                                        <?php echo Form::select('roles[]', $roles,[], array('class' => 'form-control custom-select','id'=>'roles')); ?>

                                                        <small class="form-control-feedback"> Pilih Role Admin. </small> 
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-2">
                                            </div>
                                            <div class="col-md-10 ">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                        <button type="reset" class="btn btn-inverse">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>
                </div>


                <!-- Row -->
                <div class="row" id="tableSection">

                    
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <button class="pull-right btn btn-sm btn-rounded btn-success btncreate" >Tambah User</button>
                                <h4 class="card-title">User Admin</h4>
                                <h6 class="card-subtitle">Daftar admin yang mengelola project AS</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="30%">Nama</th>
                                                <th width="30%">Email</th>
                                                <th width="20%">Role</th>
                                                <th width="10%">Status</th>
                                                <th width="10%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- sample modal content -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Hapus User</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Anda yakin menghapus user ini?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Tutup</button>
                                <button type="button" class="btn btn-danger waves-effect " id="btnconfirmdelete">Konfirmasi</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <!-- sample modal content -->
                <div id="myModalerror" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Terjadi Kesalahan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>Email telah terdaftar</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">OK</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>