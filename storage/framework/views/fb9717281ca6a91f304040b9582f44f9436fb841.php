                    <?php $__currentLoopData = $certificates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $certificate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <!-- START SINGLE WORK DESIGN AREA -->
                    <div class="col-md-4 col-sm-4 mix <?php echo e($certificate->type_name); ?>">
                        <div class="item">

                                    <?php if($certificate->image_name==null||$certificate->image_name==''): ?>
                                        <?php if($certificate->type_name=="SKA"): ?>
                                        
                                         <img class="" src="<?php echo e(asset('backend/assets/images/background/experted-thumb.jpg')); ?>" style="width: 100%" alt="img" class="img-responsive">
                                        <?php else: ?>
                                         <img class="" src="<?php echo e(asset('backend/assets/images/background/skilled-thumb.jpg')); ?>" style="width: 100%" alt="img" class="img-responsive">
                                        <?php endif; ?>
                                <?php else: ?>
                                     <img class="" src="<?php echo e($certificate->image_full); ?>" style="width: 100%" alt="img" class="img-responsive">
                                <?php endif; ?>
                            <a <a href="/certificate-code/<?php echo e($certificate->classification_code); ?>" >
                                <div class="portfolio-caption">
                                    <h4><?php echo e($certificate->classification_code." - ". $certificate->name); ?></h4>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- END SINGLE WORK DESIGN AREA -->
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>