<?php $__currentLoopData = $rent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rentvalue): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                	<div class="col-md-4 col-sm-6 col-lg-4">
						<div class="card blog-widget">
                            <div class="card-body">
                                <h3><?php echo e($rentvalue->classification_code." - ". $rentvalue->certificate_name); ?> </h3>
                                <h5>Nomor Seri: <strong><?php echo e($rentvalue->serial_number); ?></strong></h5>
                                <?php if($rentvalue->type_name=="SKA"): ?>
                                                    <label class="label label-rounded label-info">
                                                <?php else: ?>
                                                    <label class="label label-rounded label-warning">
                                                <?php endif; ?>
                                <?php echo e($rentvalue->type_name." - ".$rentvalue->qualification); ?>

                            	</label>
                                <p class="m-t-20 m-b-20">
                                Status: <b>
                                                                <?php if($rentvalue->status==0): ?>
                                                                <span class="text-warning">Menunggu konfirmasi admin</span>
                                                                <?php elseif($rentvalue->status==1): ?>
                                                                <span class="text-warning">Menunggu Pembayaran</span>
                                                                <?php elseif($rentvalue->status==2): ?>
                                                                <span class="text-success">Soft copy sedang diproses</span>
                                                                <?php elseif($rentvalue->status==3): ?>
                                                                <span class="text-info">Soft copy sudah dikirim</span>
                                                                <?php elseif($rentvalue->status==4): ?>
                                                                <span class="text-success">Hard copy sedang diproses</span>
                                                                <?php elseif($rentvalue->status==5): ?>
                                                                <span class="text-info">Hard copy sudah dikirim</span>
                                                                <?php elseif($rentvalue->status==6): ?>
                                                                <span class="text-danger">Sewa dibatalkan</span>
                                                                <?php else: ?>
                                                                <span class="text-danger">Masa sewa habis</span>
                                                                <?php endif; ?>
                                                            </b> 
                                </p>
                                <div class="d-flex float-right">
                                    <div class="read "><a href="/certificate-code/<?php echo e($rentvalue->classification_code); ?>" class="link font-medium">Selengkapnya</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>