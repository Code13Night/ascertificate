<?php $__env->startSection('js'); ?>
    <script type="text/javascript">
    	var page = 1;
		$(window).scroll(function() {
		    if($(window).scrollTop() + $(window).height() >= $(document).height()) {
		        page++;
		        loadMoreData(page);
		    }
		});

		function loadMoreData(page){
		  $.ajax(
		        {
		            url: '?page=' + page,
		            type: "get",
		            beforeSend: function()
		            {
		            	$('#tempremove').removeClass("work-inner");
		                $('.ajax-load').show();
		            }
		        })
		        .done(function(data)
		        {
		            if(data.html == " "){
		                $('.ajax-load').html("No more data found");
		                return;
		            }
		            $('.ajax-load').hide();
		            $("#post-data").append(data.html);
		        })
		        .fail(function(jqXHR, ajaxOptions, thrownError)
		        {
		              alert('server not responding...');
		        });
		}
        $('.filter').on('click', function (e) {
		    $('#tempremove').addClass("work-inner");
        });
    </script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <!-- START WORK DESIGN AREA -->
    <section class="work section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title text-center">
                        <h2><span>Sertifikat</span> yang Tersedia</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <ul class="work">
                    <li class="filter" data-filter="all">Semua</li>
                    <li class="filter" data-filter=".SKT">SKT</li>
                    <li class="filter" data-filter=".SKA">SKA</li>
                </ul>
            </div>
            <div class="work-inner" id="tempremove">
                <div class="row work-posts" id="post-data">
                   
				<?php echo $__env->make('frontend.certificates.data', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>

                <div class="ajax-load float-center text-center" style="display:none">
				<p><img src="<?php echo e(asset('frontend/assets/images/loading.gif')); ?>">Load more...</p>
				</div>
            </div>
            <hr>
        </div>
    </section>
    <!-- START WORK DESIGN AREA -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>