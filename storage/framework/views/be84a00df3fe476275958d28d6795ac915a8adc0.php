<?php $__env->startSection('title'); ?>
Riwayat Sewa
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

    <!-- This is data table -->
    <script src="<?php echo e(asset('backend/assets/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo e(asset('backend/assets/plugins/select2/dist/js/select2.full.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
        function formatDate(date) {
          var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "June", "July",
            "Aug", "Sep", "Oct",
            "Nov", "Dec"
          ];

          var day = date.getDate();
          var monthIndex = date.getMonth();
          var year = date.getFullYear();

          return day + ' ' + monthNames[monthIndex] + ' ' + year;
        }
        var table=$('#myTable').DataTable({iDisplayLength: 10,
            responsive: true,
            lengthMenu: [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "order": [
                [0, 'asc']
            ],
            "ajax": '/webApi/allrequestbystatus-all',
            "columns": [
                    { "data": "certificate_name" },
                    { "data": "qualification" },
                    { "data": "user_name" },
                    { "data": "type_name" },
                    { "data": "status" },
            ], "columnDefs": [{
                "targets": 4,
                className: "text-center",
                "render": function ( data, type, row ) {
                    switch(data){
                        case 0:
                            return '<span class="label label-warning">Menunggu konfirmasi admin</span>';
                            break;
                        case 1:
                            return '<span class="label label-warning">Menunggu Pembayaran</span>';
                            break;
                        case 2:
                            return '<span class="label label-success">Soft copy sedang diproses</span>';
                            break;
                        case 3:
                            return '<span class="label label-info">Soft copy sudah dikirim</span>';
                            break;
                        case 4:
                            return '<span class="label label-warning">Menunggu konfirmasi hard copy</span>';
                            break;
                        case 5:
                            return '<span class="label label-warning">Menunggu pembayaran soft copy</span>';
                            break;
                        case 6:
                            return '<span class="label label-success">Hard copy sedang diproses</span>';
                            break;
                        case 7:
                            return '<span class="label label-info">Hard copy sudah diteriman</span>';
                            break;
                        case 8:
                            return '<span class="label label-danger">Sewa dibatalkan</span>';
                            break;
                        default:
                            return '<span class="label label-danger">Masa sewa habis</span>';
                            break;
                    }
                }
            },{
                "targets": 3,
                "data": "active",
                className: "text-center",
                "render": function (data, type, full, meta) {
                    if(data=="SKA"){
                        return '<span class="label label-info">'+data+'</span>';
                    }else{
                        return '<span class="label label-warning">'+data+'</span>';
                    }
                }
            },
            ],
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
            
        });
        $('#select_rent').on('change', function() {
          switch(this.value)
          {
            case '1':
                $reloadurl='request';
                break;
            case '2':
                $reloadurl='waiting_for_payment';
                break;
            case '3':
                $reloadurl='process_soft_copy';
                break;
            case '4':
                $reloadurl='soft_copy_finish';
                break;
            case '5':
                $reloadurl='request_hard_copy';
                break;
            case '6':
                $reloadurl='waiting_for_payment_hard_copy';
                break;
            case '7':
                $reloadurl='process_hard_copy';
                break;
            case '8':
                $reloadurl='hard_copy_finish';
                break;
            case '9':
                $reloadurl='cancel';
                break;
            case '10':
                $reloadurl='expired';
                break;
            default:
                $reloadurl='all';
                break;
          }
          table.ajax.url( '/webApi/allrequestbystatus-'+$reloadurl ).load();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content-header'); ?>
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Riwayat Sewa</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Sewa</a></li>
                            <li class="breadcrumb-item active">Riwayat Sewa</li>
                        </ol>
                    </div>
                </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <div class="d-flex flex-row">
                                <div class="p-10 bg-info">
                                    <h3 class="text-white box m-b-0"><i class="mdi mdi-repeat"></i></h3></div>
                                <div class="align-self-center m-l-20">
                                    <h3 class="m-b-0 text-info"><?php echo e($total_rent); ?></h3>
                                    <h5 class="text-muted m-b-0">Sewa</h5></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <div class="d-flex flex-row">
                                <div class="p-10 bg-success">
                                    <h3 class="text-white box m-b-0"><i class="mdi mdi-file"></i></h3></div>
                                <div class="align-self-center m-l-20">
                                    <h3 class="m-b-0 text-success"><?php echo e($soft_rent); ?></h3>
                                    <h5 class="text-muted m-b-0">Soft Copy</h5></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-12">
                        <div class="card">
                            <div class="d-flex flex-row">
                                <div class="p-10 bg-inverse">
                                    <h3 class="text-white box m-b-0"><i class="mdi mdi-book-open-variant"></i></h3></div>
                                <div class="align-self-center m-l-20">
                                    <h3 class="m-b-0"><?php echo e($hard_rent); ?></h3>
                                    <h5 class="text-muted m-b-0">Hard Copy</h5></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <div class="row" id="tableSection">

                    
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-5 col-lg-5">
                                        <h4 class="card-title">History Sewa</h4>
                                        <h6 class="card-subtitle">Menampilkan semua riwayat sewa user</h6>
                                    </div>
                                    <div class="col-sm-12 col-md-7 col-lg-7">
                                        <label class="col-lg-2 col-md-3 col-sm-3 col-xs-3">Filter: </label>
                                        <select class="col-lg-10 col-sm-9 col-xs-9 custom-select pull-right" name="select_rent" id="select_rent">
                                            <option value="0" selected>Semua</option>
                                            <option value="1">Permohonan sewa</option>
                                            <option value="2">Menunggu pembayaran soft copy</option>
                                            <option value="3">Soft copy sedang diproses</option>
                                            <option value="4">Soft copy sudah dikirim</option>
                                            <option value="5">Permohonan hard copu</option>
                                            <option value="6">Menunggu pembayaran hard copy</option>
                                            <option value="7">Hard copy sedang diproses</option>
                                            <option value="8">Hard copy sudah diterima</option>
                                            <option value="9">Dibatalkan</option>
                                            <option value="10">Masa sewa habis</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="20%">Sertifikat</th>
                                                <th width="8%">Kualifikasi</th>
                                                <th width="20%">User</th>
                                                <th width="7%">Tipe Sertifikat</th>
                                                <th width="10%">Status Sewa</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>         
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>