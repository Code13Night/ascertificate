<?php $__currentLoopData = $certificate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $certificatevalue): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                	<div class="col-md-4 col-sm-6 col-lg-4">
						<div class="card blog-widget">
                            <div class="card-body">
                                <div class="blog-image">
                                	<?php if($certificatevalue->image_name==null||$certificatevalue->image_name==''): ?>
	                                    <?php if($certificatevalue->type_name=="SKA"): ?>
	                                    
	                                     <img class="" src="<?php echo e(asset('backend/assets/images/background/experted-thumb.jpg')); ?>" style="width: 100%" alt="img" class="img-responsive">
	                                    <?php else: ?>
	                                     <img class="" src="<?php echo e(asset('backend/assets/images/background/skilled-thumb.jpg')); ?>" style="width: 100%" alt="img" class="img-responsive">
	                                    <?php endif; ?>
                                <?php else: ?>
                                     <img class="" src="<?php echo e($certificate->image_full); ?>" style="width: 100%" alt="img" class="img-responsive">
                                <?php endif; ?>
								</div>
                                <h3><?php echo e($certificatevalue->classification_code." - ". $certificatevalue->name); ?></h3>
                                <h5><strong><?php echo e($certificatevalue->profesion_name); ?></strong></h5>
                                <?php if($certificatevalue->type_name=="SKA"): ?>
                                                    <label class="label label-rounded label-info">
                                                <?php else: ?>
                                                    <label class="label label-rounded label-warning">
                                                <?php endif; ?>
                                <?php echo e($certificatevalue->type_name); ?>

                            	</label>
                                <p class="m-t-20 m-b-20">
                                    <?php if(strlen($certificatevalue->description) > 30): ?>
                                                    <?php echo e(substr($certificatevalue->description,0,30)."..."); ?>

                                                <?php else: ?>
                                                    <?php if(strlen($certificatevalue->description) == 0): ?>
                                                    Tidak ada Informasi tambahan tentang sertifikat ini
                                                    <?php else: ?>
                                                    $certificatevalue->description            
                                                    <?php endif; ?>                                  
                                                <?php endif; ?>   
                                </p>
                                <div class="d-flex">
                                    <div class="read"><a href="/certificate-code/<?php echo e($certificatevalue->classification_code); ?>" class="link font-medium">Selengkapnya</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>