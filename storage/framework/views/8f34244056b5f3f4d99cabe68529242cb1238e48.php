<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">


<head>
    <!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"><!-- Favicon icon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo e(asset('images/icon/apple-icon-57x57.png')); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo e(asset('images/icon/apple-icon-60x60.png')); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo e(asset('images/icon/apple-icon-72x72.png')); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('images/icon/apple-icon-76x76.png')); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo e(asset('images/icon/apple-icon-114x114.png')); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(asset('images/icon/apple-icon-120x120.png')); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo e(asset('images/icon/apple-icon-144x144.png')); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(asset('images/icon/apple-icon-152x152.png')); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo e(asset('images/icon/apple-icon-180x180.png')); ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo e(asset('images/icon/android-icon-192x192.png')); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(asset('images/icon/favicon-32x32.png')); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo e(asset('images/icon/favicon-96x96.png')); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('images/icon/favicon-16x16.png')); ?>">
    <link rel="manifest" href="<?php echo e(asset('images/icon/manifest.json')); ?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo e(asset('images/icon/ms-icon-144x144.png')); ?>">
    <meta name="theme-color" content="#ffffff">
    <title>SKA-SKT ID</title>
    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/bootstrap/css/bootstrap.min.css')); ?>">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Neuton:400,200,700,300' rel='stylesheet' type='text/css'>
    <!-- FONT AWESOME CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/fonts/font-awesome.min.css')); ?>">
    <!-- OWL CAROSEL CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/owlcarousel/css/owl.carousel.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/owlcarousel/css/owl.theme.css')); ?>">
    <!-- LIGHTBOX CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/css/lightbox.min.css')); ?>">
    <!-- MAIN STYLE CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/css/style.css')); ?>">
    <!-- RESPONSIVE CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/css/responsive.css')); ?>">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- START PRELOADER -->
    <div class="preloader">
        <div class="status">
            <div class="status-mes"></div>
        </div>
    </div>
    <!-- / END PRELOADER -->

    <!-- START HOMEPAGE DESIGN AREA -->
    <header id="home" class="welcome-area">
        <div class="header-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <!-- START LOGO DESIGN AREA -->
                        <div class="logo">
                            <a href="">SKA-SKT ID</a>
                        </div>
                        <!-- END LOGO DESIGN AREA -->
                    </div>
                    <div class="col-md-9">
                        <!-- START MENU DESIGN AREA -->
                        <div class="mainmenu">
                            <div class="navbar navbar-nobg">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="active"><a class="smoth-scroll" href="#home">Home <div class="ripple-wrapper"></div></a></li>
                                        <li><a class="smoth-scroll" href="#about">Tentang Kami</a></li>
                                        <li><a class="smoth-scroll" href="#service">Layanan</a></li>
                                        <li><a class="smoth-scroll" href="#certificate">Sertifikat</a></li>
                                        <li><a class="smoth-scroll" href="#contact">Hubungi Kami</a></li>
                                        <?php if(auth()->guard()->guest()): ?>
                                        <li><a href="<?php echo e(route('login')); ?>">Login</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- END MENU DESIGN AREA -->
                    </div>
                </div>
            </div>
        </div>
        <div class="welcome-image-area">
            <div class="display-table">
                <div class="display-table-cell">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Construction Services Certification Handling and Rental </h2>
                                <!-- <p>Siap melayani a</p> -->
                                <a class="slide-btn smoth-scroll" href="#about">Siapa Kami?</a>
                                <a class="slide-btn smoth-scroll" href="javascript:void(0);">Download</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- / END HOMEPAGE DESIGN AREA -->

    <!-- START ABOUT US DESIGN AREA -->
    <section id="about" class="about-us-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <!-- START SECTION TITLE DESIGN AREA -->
                    <div class="section-title text-center">
                        <h2><span>Tentang</span> Kami</h2>
                    </div>
                    <!-- / END SECTION TITLE DESIGN AREA -->
                </div>
            </div>
            <div class="row">
                <!-- START ABOUT IMAGE DESIGN AREA -->
                <div class="col-md-6">
                    <div class="about-image">
                        <img src="<?php echo e(asset('frontend/assets/images/about.jpg')); ?>" alt="" class="img-responsive">
                    </div>
                </div>
                <!-- / END ABOUT US IMAGE AREA -->
                <!-- START ABOUT US TEXT DESIGN AREA -->
                <div class="col-md-6">
                    <div class="about-text">
                        <h4>Tentang Kami</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentiallyIt has survived not only five centuriesLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially</p>
                    </div>
                </div>
                <!-- / END ABOUT US TEXT DESIGN AREA -->
            </div>
        </div>
    </section>
    <!-- / END ABOUT US DESIGN AREA -->

    <!-- START SERVICES DESIGN AREA -->
    <section id="service" class="service-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title text-center">
                        <h2><span>Layanan</span> Kami </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- START SINGLE SERVICES DESIGN AREA -->
                <div class="col-md-4 col-sm-6">
                    <div class="single-service">
                        <i class="fa fa-thumbs-up"></i>
                        <h4>Terpercaya</h4>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                    </div>
                </div>
                <!-- / END SINGLE SERVICES DESIGN AREA -->
                <!-- START SINGLE SERVICES DESIGN AREA -->
                <div class="col-md-4 col-sm-6">
                    <div class="single-service">
                        <i class="fa fa-laptop"></i>
                        <h4>Tersedia Online</h4>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                    </div>
                </div>
                <!-- / END SINGLE SERVICES DESIGN AREA -->
                <!-- START SINGLE SERVICES DESIGN AREA -->
                <div class="col-md-4 col-sm-6">
                    <div class="single-service">
                        <i class="fa fa-flag"></i>
                        <h4>Terdaftar Nasional</h4>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                    </div>
                </div>
                <!-- / END SINGLE SERVICES DESIGN AREA -->
                <!-- START SINGLE SERVICES DESIGN AREA -->
                <div class="col-md-4 col-sm-6">
                    <div class="single-service">
                        <i class="fa fa-connectdevelop"></i>
                        <h4>Jaringan Luas</h4>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                    </div>
                </div>
                <!-- / END SINGLE SERVICES DESIGN AREA -->
                <!-- START SINGLE SERVICES DESIGN AREA -->
                <div class="col-md-4 col-sm-6">
                    <div class="single-service">
                        <i class="fa fa-briefcase"></i>
                        <h4>Beragam Profesi</h4>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                    </div>
                </div>
                <!-- / END SINGLE SERVICES DESIGN AREA -->
                <!-- START SINGLE SERVICES DESIGN AREA -->
                <div class="col-md-4 col-sm-6">
                    <div class="single-service">
                        <i class="fa fa-users"></i>
                        <h4>Full support</h4>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                    </div>
                </div>
                <!-- / END SINGLE SERVICES DESIGN AREA -->
            </div>
        </div>
    </section>
    <!-- / END SERVICES DESIGN AREA -->


    <section id="certificate" class="team-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title text-center">
                        <h2>Sertifikat <span> yang tersedia</span></h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- START SINGLE TEAM DESIGN AREA -->
                <div class="col-md-6 col-sm-6">
                    <div class="single-team">
                        <img src="<?php echo e(asset('backend/assets/images/background/experted-thumb.jpg')); ?>"  class="img-circle" >
                        <h4>SKA</h4>
                        <h6 class="text-muted">Sertifikat Keahlian Kerja</h6>
                        <p>Sertifikat khusus sebagai bukti kompetensi tenaga ahli konstruksi. Sertifikat ini dikeluarkan oleh LPJK dengan persyaratan tertentu.</p>
                                
                        <a href="/rent-request" class="read-more">Sewa</a>
                    </div>
                </div>
                <!-- / END SINGLE TEAM DESIGN AREA -->
                <!-- START SINGLE TEAM DESIGN AREA -->
                <div class="col-md-6 col-sm-6">
                    <div class="single-team">
                        <img src="<?php echo e(asset('backend/assets/images/background/skilled-thumb.jpg')); ?>"  class="img-circle" >
                        <h4>SKT</h4>
                        <h6 class="text-muted"> Sertifikat Keterampilan Kerja</h6>
                        <p>Sertifikat khusus sebagai bukti kompetensi tenaga terampil konstruksi. Sertifikat ini dikeluarkan oleh LPJK dengan persyaratan tertentu.</p>
                        <a href="/rent-request" class="read-more">Sewa</a>
                    </div>
                </div>
                <!-- / END SINGLE TEAM DESIGN AREA -->
            </div>
        </div>
    </section>


    <!-- START CONTACT DESIGN AREA -->
    <section id="contact" class="contact-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title text-center">
                        <h2><span>Hubungi</span>Kami </h2>
                    </div>
                </div>
            </div>
            <!-- START MAP DESIGN AREA -->
            <div class="row">
                <div class="col-md-12">
                    <div class="map"></div>
                </div>
            </div>
            <!-- / END CONTACT DESIGN AREA -->
            <div class="row">
                <!-- START CONTACT DETAILS DESIGN AREA -->
                <div class="col-md-4">
                    <div class="contact-details">
                        <div class="single-contact">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <h6 class="text-muted">Surabaya, Jawa TImur, Indonesia</h6>
                        </div>
                        <div class="single-contact">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <h6 class="text-muted">+6231-111-2222-333 </h6>
                        </div>
                        <div class="single-contact">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <h6 class="text-muted">info@sitename.com </h6>
                        </div>
                        <div class="single-contact">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            <h6 class="text-muted">www.websitename.com</h6>
                        </div>
                    </div>
                </div>
                <!-- / END CONTACT DETAILS DESIGN AREA -->
                <!-- START CONTACT FORM DESIGN AREA -->
                <div class="col-md-8">
                    <div class="contact-form">
                        <div class="row">
                            <form action="http://wordpressboss.com/opb/demo-aisha/aisha/assets/php/contact.php" method="post">
                                <div class="form-group col-md-6">
                                    <input type="text" name="name" class="form-control" id="first-name" placeholder="Masukkan Nama Anda" required="required">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Masukkan Email Anda" required="required">
                                </div>
                                <div class="form-group col-md-12">
                                    <textarea rows="6" name="message" class="form-control" id="description" placeholder="Masukkan Pertanyaan" required="required"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <button>Kirim Pesan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /END  CONTACT DETAILS DESIGN AREA -->
            </div>
        </div>
    </section>
    <!-- / END CONTACT DESIGN AREA -->

    <!-- START FOOTER DESIGN AREA -->
    <footer class="footer-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-text text-center">
                        <h1>SKA-SKT ID </h1>
                        <h4>Construction Services Certification Handling and Rental</h4>
                        <h6 class="text-muted">&copy;copyright 2016 Project AS</h6>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- / END CONTACT DETAILS DESIGN AREA -->


    <!-- START SCROOL UP DESIGN AREA -->
    <div class="scroll-to-up">
        <div class="scrollup">
            <i class="fa fa-angle-up"></i>
        </div>
    </div>
    <!-- / END SCROOL UP DESIGN AREA -->

    <!-- LATEST JQUERY -->
    <script src="<?php echo e(asset('frontend/assets/js/jquery.min.js')); ?>"></script>
    <!-- BOOTSTRAP JS -->
    <script src="<?php echo e(asset('frontend/assets/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <!-- OWL CAROUSEL JS  -->
    <script src="<?php echo e(asset('frontend/assets/owlcarousel/js/owl.carousel.min.js')); ?>"></script>
    <!-- ISOTOP JS -->
    <script src="<?php echo e(asset('frontend/assets/js/jquery.mixitup.js')); ?>"></script>
    <!-- LIGHTBOX JS -->
    <script src="<?php echo e(asset('frontend/assets/js/lightbox.min.js')); ?>"></script>
    <!-- GOOGLE MAP JS -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLy4EXLgPYkRO_kbY41worB3ujTr0pSMQ"></script>
    <script src="<?php echo e(asset('frontend/assets/js/gmap3.min.js')); ?>"></script>
    <!-- scripts js -->
    <script src="<?php echo e(asset('frontend/assets/js/scripts.js')); ?>"></script>
</body>

</html>