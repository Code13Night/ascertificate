<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from wrappixel.com/demos/admin-templates/material-pro/material/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 27 Feb 2018 17:10:22 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo e(asset('images/icon/apple-icon-57x57.png')); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo e(asset('images/icon/apple-icon-60x60.png')); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo e(asset('images/icon/apple-icon-72x72.png')); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('images/icon/apple-icon-76x76.png')); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo e(asset('images/icon/apple-icon-114x114.png')); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(asset('images/icon/apple-icon-120x120.png')); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo e(asset('images/icon/apple-icon-144x144.png')); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(asset('images/icon/apple-icon-152x152.png')); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo e(asset('images/icon/apple-icon-180x180.png')); ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo e(asset('images/icon/android-icon-192x192.png')); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(asset('images/icon/favicon-32x32.png')); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo e(asset('images/icon/favicon-96x96.png')); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('images/icon/favicon-16x16.png')); ?>">
    <link rel="manifest" href="<?php echo e(asset('images/icon/manifest.json')); ?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo e(asset('images/icon/ms-icon-144x144.png')); ?>">
    <meta name="theme-color" content="#ffffff">
    <title>Construct Services Certification Handling and Rental -  Register</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo e(asset('backend/assets/plugins/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo e(asset('backend/css/style.css')); ?>" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo e(asset('backend/css/colors/blue.css')); ?>" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register login-sidebar"  style="background-image:url(<?php echo e(asset('backend/assets/images/background/login-register.jpg')); ?>);">
      <div class="login-box card">
        <div class="card-body">
           <form method="POST" class="form-horizontal form-material" id="register form" action="<?php echo e(route('register')); ?>">
            <?php echo csrf_field(); ?>
            <a href="javascript:void(0)" class="text-center db"><img src="<?php echo e(asset('images/icon/favicon-32x32.png')); ?>" alt="Home" /><hr/></a>  
            
            <h3 class="box-title m-t-40 m-b-0">Daftar Sekarang</h3><small>Buat akun anda dan nikmati layanan kami</small> 
            <div class="form-group <?php echo e($errors->has('name') ? ' has-danger ' : ''); ?>">
              <div class="col-xs-12">
                <input id="name" type="text" class="form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" name="name" value="<?php echo e(old('name')); ?>"  placeholder="Nama" required autofocus>
                <?php if($errors->has('name')): ?>
                <div class="form-control-feedback"><?php echo e($errors->first('name')); ?></div>
                <?php endif; ?>
              </div>
            </div>
            <div class="form-group <?php echo e($errors->has('email') ? ' has-danger ' : ''); ?>">
              <div class="col-xs-12">
                <input id="email" type="text" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>"  placeholder="Email" required autofocus>
                <?php if($errors->has('email')): ?>
                <div class="form-control-feedback"><?php echo e($errors->first('email')); ?></div>
                <?php endif; ?>
              </div>
            </div>
            <div class="form-group <?php echo e($errors->has('password') ? ' has-danger ' : ''); ?>">
              <div class="col-xs-12">
                <input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' form-control-danger ' : ''); ?>" name="password" placeholder="Password" required>
                <?php if($errors->has('password')): ?>
                <div class="form-control-feedback"><?php echo e($errors->first('password')); ?></div>
                <?php endif; ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12">
                    <input id="password-confirm" type="password" class="form-control" placeholder="Konfirmasi Password"  name="password_confirmation" required>
              </div>
            </div>
            <div class="form-group text-center m-t-20">
              <div class="col-xs-12">
                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Sign Up</button>
              </div>
            </div>
            <div class="form-group m-b-0">
              <div class="col-sm-12 text-center">
                <p>Sudah Punya Akun? <a href="<?php echo e(route('login')); ?>" class="text-info m-l-5"><b>Sign In</b></a></p>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo e(asset('backend/assets/plugins/jquery/jquery.min.js')); ?>"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo e(asset('backend/assets/plugins/popper/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('backend/assets/plugins/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo e(asset('backend/js/jquery.slimscroll.js')); ?>"></script>
    <!--Wave Effects -->
    <script src="<?php echo e(asset('backend/js/waves.js')); ?>"></script>
    <!--Menu sidebar -->
    <script src="<?php echo e(asset('backend/js/sidebarmenu.js')); ?>"></script>
    <!--stickey kit -->
    <script src="<?php echo e(asset('backend/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')); ?>"></script>
    <script src="<?php echo e(asset('backend/assets/plugins/sparkline/jquery.sparkline.min.js')); ?>"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo e(asset('backend/js/custom.min.js')); ?>"></script>
    <script src="<?php echo e(asset('backend/assets/plugins/styleswitcher/jQuery.style.switcher.js')); ?>"></script>
</body>


</html>
