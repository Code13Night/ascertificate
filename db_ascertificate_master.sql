/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100130
Source Host           : localhost:3306
Source Database       : db_ascertificate

Target Server Type    : MYSQL
Target Server Version : 100130
File Encoding         : 65001

Date: 2018-03-18 19:36:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for certificates
-- ----------------------------
DROP TABLE IF EXISTS `certificates`;
CREATE TABLE `certificates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `serial_number` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `study_program_id` int(10) unsigned DEFAULT NULL,
  `profession_id` int(10) unsigned DEFAULT NULL,
  `education_level_id` int(10) unsigned DEFAULT NULL,
  `description` text,
  `period_of_validity` int(11) DEFAULT '0',
  `price` double(10,2) DEFAULT '0.00',
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `certificates_ibfk_1` (`profession_id`),
  KEY `education_level_id` (`education_level_id`),
  KEY `study_program_id` (`study_program_id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `certificates_ibfk_1` FOREIGN KEY (`profession_id`) REFERENCES `professions` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `certificates_ibfk_2` FOREIGN KEY (`education_level_id`) REFERENCES `professions` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `certificates_ibfk_3` FOREIGN KEY (`study_program_id`) REFERENCES `study_programs` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `certificates_ibfk_4` FOREIGN KEY (`type_id`) REFERENCES `certificate_types` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of certificates
-- ----------------------------
INSERT INTO `certificates` VALUES ('1', 'SF20123AP2018', 'Sertifikat Apoteker Farmasi', '1', null, '4', '1', null, '360', '4500000.00', '1', '2018-03-18 11:47:51', '2018-03-18 19:35:47');
INSERT INTO `certificates` VALUES ('2', 'KT1232123BA18', 'Sertifikat Kontraktor', '2', null, '2', '1', null, '180', '2500000.00', '1', '2018-03-18 11:48:51', '2018-03-18 19:35:49');

-- ----------------------------
-- Table structure for certificate_types
-- ----------------------------
DROP TABLE IF EXISTS `certificate_types`;
CREATE TABLE `certificate_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `image_thumb` varchar(255) DEFAULT NULL,
  `total` int(10) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of certificate_types
-- ----------------------------
INSERT INTO `certificate_types` VALUES ('1', 'SKA', null, null, null, '0', '2018-03-18 19:33:58', '2018-03-18 19:34:09');
INSERT INTO `certificate_types` VALUES ('2', 'SKT', null, null, null, '0', '2018-03-18 19:34:00', '2018-03-18 19:34:10');
INSERT INTO `certificate_types` VALUES ('3', 'ISO', null, null, null, '0', '2018-03-18 19:34:02', '2018-03-18 19:34:11');

-- ----------------------------
-- Table structure for companies
-- ----------------------------
DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `total` int(10) DEFAULT '0',
  `image_name` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `image_thumb` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of companies
-- ----------------------------
INSERT INTO `companies` VALUES ('1', 'PT ABC ', '1', null, null, null, '2018-03-18 11:27:34', '2018-03-18 11:28:05');
INSERT INTO `companies` VALUES ('2', 'Jasa Konsulat Pembangunan', '2', null, null, null, '2018-03-18 11:27:49', '2018-03-18 11:28:05');
INSERT INTO `companies` VALUES ('3', 'Pharmacy Techno', '1', null, null, null, '2018-03-18 11:27:56', '2018-03-18 11:28:06');
INSERT INTO `companies` VALUES ('4', 'Mercusuar Buana', '1', null, null, null, '2018-03-18 11:28:04', '2018-03-18 11:28:07');

-- ----------------------------
-- Table structure for education_levels
-- ----------------------------
DROP TABLE IF EXISTS `education_levels`;
CREATE TABLE `education_levels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `total` int(10) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `image_name` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `image_thumb` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of education_levels
-- ----------------------------
INSERT INTO `education_levels` VALUES ('1', 'S1', '0', '2018-03-18 11:28:21', '2018-03-18 11:28:26', null, null, null);
INSERT INTO `education_levels` VALUES ('2', 'S2', '0', '2018-03-18 11:28:27', '2018-03-18 11:28:27', null, null, null);
INSERT INTO `education_levels` VALUES ('3', 'S3', '0', '2018-03-18 11:28:29', '2018-03-18 11:28:29', null, null, null);
INSERT INTO `education_levels` VALUES ('4', 'SMK', '0', '2018-03-18 11:28:36', '2018-03-18 11:28:36', null, null, null);
INSERT INTO `education_levels` VALUES ('5', 'SMA', '0', '2018-03-18 11:28:38', '2018-03-18 11:28:38', null, null, null);
INSERT INTO `education_levels` VALUES ('6', 'SMP', '0', '2018-03-18 11:28:45', '2018-03-18 11:28:45', null, null, null);
INSERT INTO `education_levels` VALUES ('7', 'SD', '0', '2018-03-18 11:28:47', '2018-03-18 11:28:47', null, null, null);
INSERT INTO `education_levels` VALUES ('8', 'Lain-Lain', '0', '2018-03-18 11:28:56', '2018-03-18 11:28:56', null, null, null);

-- ----------------------------
-- Table structure for image_certificates
-- ----------------------------
DROP TABLE IF EXISTS `image_certificates`;
CREATE TABLE `image_certificates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `certificate_id` int(10) unsigned DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `image_thumb` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `certificates_id` (`certificate_id`),
  CONSTRAINT `image_certificates_ibfk_1` FOREIGN KEY (`certificate_id`) REFERENCES `certificates` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of image_certificates
-- ----------------------------

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `content` text,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `pages_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `pages_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pages
-- ----------------------------

-- ----------------------------
-- Table structure for payments
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rent_id` int(10) unsigned NOT NULL,
  `phase_1` tinyint(1) DEFAULT '0',
  `phase_2` tinyint(1) DEFAULT '0',
  `prove_phase_1` varchar(255) DEFAULT NULL,
  `prove_phase_2` varchar(255) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_detail` text,
  `total` double(10,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `rent_id` (`rent_id`),
  CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`rent_id`) REFERENCES `rents` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of payments
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'role-list', 'List Role', 'Lihat List Role', '2017-01-30 11:21:39', '2017-01-30 11:21:39');
INSERT INTO `permissions` VALUES ('2', 'role-create', 'Buat Role', 'Buat Role Baru', '2017-01-30 11:21:39', '2017-01-30 11:21:39');
INSERT INTO `permissions` VALUES ('3', 'role-edit', 'Edit Role', 'Edit Role', '2017-01-30 11:21:39', '2017-01-30 11:21:39');
INSERT INTO `permissions` VALUES ('4', 'role-delete', 'Hapus Role', 'Hapus Role', '2017-01-30 11:21:39', '2017-01-30 11:21:39');
INSERT INTO `permissions` VALUES ('5', 'product-list', 'Lihat List Produk', 'Lihat Semua Produk', '2017-01-30 11:21:40', '2017-01-30 11:21:40');
INSERT INTO `permissions` VALUES ('6', 'product-add', 'Tambahkan Produk', 'Tambahkan Produk Baru', '2017-01-30 11:21:40', '2017-01-30 11:21:40');
INSERT INTO `permissions` VALUES ('7', 'product-edit', 'Edit Produk', 'Edit Produk', '2017-01-30 11:21:40', '2017-01-30 11:21:40');
INSERT INTO `permissions` VALUES ('8', 'product-delete', 'Hapus Produk', 'Hapus Produk', '2017-01-30 11:21:40', '2017-01-30 11:21:40');

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`) USING BTREE,
  CONSTRAINT `permission_role_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permission_role
-- ----------------------------

-- ----------------------------
-- Table structure for professions
-- ----------------------------
DROP TABLE IF EXISTS `professions`;
CREATE TABLE `professions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `total` int(10) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `image_name` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `image_thumb` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of professions
-- ----------------------------
INSERT INTO `professions` VALUES ('1', 'Guru', '0', '2018-03-18 11:30:25', '2018-03-18 11:30:25', null, null, null);
INSERT INTO `professions` VALUES ('2', 'Kontraktor', '0', '2018-03-18 11:30:28', '2018-03-18 11:30:28', null, null, null);
INSERT INTO `professions` VALUES ('3', 'Mandor', '0', '2018-03-18 11:30:32', '2018-03-18 11:30:32', null, null, null);
INSERT INTO `professions` VALUES ('4', 'Apoteker', '0', '2018-03-18 11:30:37', '2018-03-18 11:30:37', null, null, null);
INSERT INTO `professions` VALUES ('5', 'Arsitek', '0', '2018-03-18 11:30:42', '2018-03-18 11:30:42', null, null, null);
INSERT INTO `professions` VALUES ('6', 'Operator Alat Berat', '0', '2018-03-18 11:30:51', '2018-03-18 11:30:51', null, null, null);
INSERT INTO `professions` VALUES ('7', 'Teknisi Mesin', '0', '2018-03-18 11:30:59', '2018-03-18 11:30:59', null, null, null);
INSERT INTO `professions` VALUES ('8', 'Teknisi Elektronik', '0', '2018-03-18 11:31:11', '2018-03-18 11:31:11', null, null, null);
INSERT INTO `professions` VALUES ('9', 'Manajer K3', '0', '2018-03-18 11:31:36', '2018-03-18 11:31:36', null, null, null);
INSERT INTO `professions` VALUES ('10', 'Manajer Keuangan', '0', '2018-03-18 11:31:40', '2018-03-18 11:31:40', null, null, null);

-- ----------------------------
-- Table structure for rents
-- ----------------------------
DROP TABLE IF EXISTS `rents`;
CREATE TABLE `rents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `certificate_id` int(11) unsigned NOT NULL,
  `serial_number` varchar(255) DEFAULT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `date_of_issued` datetime DEFAULT NULL,
  `valid_until` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `soft_file` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `note` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `certificate_id` (`certificate_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `rents_ibfk_1` FOREIGN KEY (`certificate_id`) REFERENCES `certificates` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `rents_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rents
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin', 'Admin', null, '2018-03-18 11:42:36', '2018-03-18 11:43:00');
INSERT INTO `roles` VALUES ('2', 'superadmin', 'Super Admin', null, '2018-03-18 11:42:39', '2018-03-18 11:43:04');
INSERT INTO `roles` VALUES ('3', 'cs', 'Costumer Service', null, '2018-03-18 11:42:42', '2018-03-18 11:43:23');
INSERT INTO `roles` VALUES ('4', 'user', 'User', null, '2018-03-18 11:42:57', '2018-03-18 11:43:27');

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`) USING BTREE,
  CONSTRAINT `role_user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role_user
-- ----------------------------
INSERT INTO `role_user` VALUES ('1', '2');
INSERT INTO `role_user` VALUES ('2', '4');
INSERT INTO `role_user` VALUES ('3', '4');
INSERT INTO `role_user` VALUES ('4', '4');
INSERT INTO `role_user` VALUES ('5', '4');

-- ----------------------------
-- Table structure for study_programs
-- ----------------------------
DROP TABLE IF EXISTS `study_programs`;
CREATE TABLE `study_programs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `total` int(10) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `image_name` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `image_thumb` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of study_programs
-- ----------------------------
INSERT INTO `study_programs` VALUES ('1', 'Teknik Sipil', '0', '2018-03-18 11:29:34', '2018-03-18 11:29:34', null, null, null);
INSERT INTO `study_programs` VALUES ('2', 'Arsitektur', '1', '2018-03-18 11:29:37', '2018-03-18 11:30:12', null, null, null);
INSERT INTO `study_programs` VALUES ('3', 'Teknik Mesin', '0', '2018-03-18 11:29:44', '2018-03-18 11:29:44', null, null, null);
INSERT INTO `study_programs` VALUES ('4', 'Teknik Kimia', '2', '2018-03-18 11:29:54', '2018-03-18 11:30:12', null, null, null);
INSERT INTO `study_programs` VALUES ('5', 'Farmasi', '0', '2018-03-18 11:29:56', '2018-03-18 11:29:56', null, null, null);
INSERT INTO `study_programs` VALUES ('6', 'Teknik Fisika', '1', '2018-03-18 11:29:59', '2018-03-18 11:30:13', null, null, null);
INSERT INTO `study_programs` VALUES ('7', 'Teknik Industri', '0', '2018-03-18 11:30:11', '2018-03-18 11:30:11', null, null, null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `api_token` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`name`) USING BTREE,
  UNIQUE KEY `users_email_unique` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin ascertificate', 'admin@projectas.com', '$2y$10$D72M7iTiYABmv9bKKYmj0eOgjmHOsXRq3AqNICHDLe2HVNMvF2tsK', '1', '72gsoAfPkyJOGNUgO0MMvhP1D3v2TG5AfEa02eIOwPXdTYXpiXf6YPiAO8It', 'KPrvf5ctlIp6x2d652gA7xFSrr0aGkciQsyyg0Zno5CigvCCCrQfOOZhM7Vq', '2018-03-18 11:41:56', '2018-03-18 11:43:47');
INSERT INTO `users` VALUES ('2', 'renter company A', 'renter@test.com', '$2y$10$2FtM476dvyg3.GiKXqumNuBqBRjwSgA184cOudBCvOWUyVakPM/ga', '1', 'kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'QtAv8QfuX39THmjFUxoN9LYtjhHaAy45tbbak33y3Aie7Oclpp9vA2nxkghL', '2018-03-18 11:44:09', '2018-03-18 11:44:13');
INSERT INTO `users` VALUES ('3', 'renter company B', 'rentercompanyB@test.com', '$2y$10$pgg/Y/FNnC66QNzH43DKMOOkAKVTqLWp5waqyFUjxBaqQ5o7r3Pri', '1', 'PNH3upE4WDGaA6YI6t5riC69hp6zItGQKkxFrZb8AnflUEtKtzncF7azKEpd', 'wKPA4zOqVtBIb9B6FGmfkXjUhgzNipROLfOi2xwmQbhR6v430pvj7ezZucv4', '2018-03-18 11:44:41', '2018-03-18 11:44:46');
INSERT INTO `users` VALUES ('4', 'Kontraktor COmpany A', 'KontraktorCOmpanyA@hallo.com', '$2y$10$ZKJof0wPf1raYU0a6z1GSOlfjW1dFinuBeWRH6h/QhOEE4lJNFNnm', '1', 'EHw2uXfp8ARO0TL44lG3kYN29a3F5usX60hA7dUI46ss0xcFqrJrcx5UWbKW', null, '2018-03-18 12:03:14', '2018-03-18 12:03:14');
INSERT INTO `users` VALUES ('5', 'Kontraktor COmpany B', 'KontraktorCOmpanyB@companyB.com', '$2y$10$d6wZeVy8ZZ4xtEaRugw0qePSHYpkxRfcJTd4gkU6AWOZQx5h4fueS', '1', 'a7ZvnRXlqtf5CLubYfhVBTMxRbvTJjvZ9j8DeCRH7YLoZ3H4ZLwTBBdhCqKe', null, '2018-03-18 12:10:34', '2018-03-18 12:10:34');
INSERT INTO `users` VALUES ('6', 'Super Admin Project AS', 'su@projectas.com', '$2y$10$YfigwBWAAIiV4dt0q0ICF.VetvURL.9dMgKXapw7h.azNof8tHpje', '1', 'ob2TNY2NWlgCSvuTmDqr7Xn7cmkpJIXYZMnOoltoP53vDLlwxRyZ7yoPqjQO', null, '2018-03-18 19:18:56', '2018-03-18 19:18:56');

-- ----------------------------
-- Table structure for user_devices
-- ----------------------------
DROP TABLE IF EXISTS `user_devices`;
CREATE TABLE `user_devices` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_devices_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_devices
-- ----------------------------
INSERT INTO `user_devices` VALUES ('1', '4', '123adasdqwe123asdqwe123', '2018-03-18 12:03:14', '2018-03-18 12:03:14');
INSERT INTO `user_devices` VALUES ('2', '5', '123adasdqwe123asdqwe123', '2018-03-18 12:10:34', '2018-03-18 12:10:34');

-- ----------------------------
-- Table structure for user_general_info
-- ----------------------------
DROP TABLE IF EXISTS `user_general_info`;
CREATE TABLE `user_general_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone_number` int(10) DEFAULT NULL,
  `profession_id` int(10) unsigned DEFAULT NULL,
  `education_level_id` int(10) unsigned DEFAULT NULL,
  `study_program_id` int(10) unsigned DEFAULT NULL,
  `address` text,
  `company_id` int(10) unsigned DEFAULT NULL,
  `graduate_year` int(10) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `image_thumb` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `user_general_info_ibfk_1` (`profession_id`),
  KEY `user_general_info_ibfk_2` (`education_level_id`),
  KEY `user_general_info_ibfk_3` (`study_program_id`),
  KEY `user_general_info_ibfk_5` (`company_id`),
  CONSTRAINT `user_general_info_ibfk_1` FOREIGN KEY (`profession_id`) REFERENCES `professions` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `user_general_info_ibfk_2` FOREIGN KEY (`education_level_id`) REFERENCES `education_levels` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `user_general_info_ibfk_3` FOREIGN KEY (`study_program_id`) REFERENCES `study_programs` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `user_general_info_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `user_general_info_ibfk_5` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_general_info
-- ----------------------------
