/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100130
Source Host           : localhost:3306
Source Database       : db_ascertificate

Target Server Type    : MYSQL
Target Server Version : 100130
File Encoding         : 65001

Date: 2018-06-05 21:26:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for carts
-- ----------------------------
DROP TABLE IF EXISTS `carts`;
CREATE TABLE `carts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `profession_id` int(10) unsigned DEFAULT NULL,
  `study_program_id` int(10) unsigned DEFAULT NULL,
  `education_level_id` int(10) unsigned DEFAULT NULL,
  `graduate_year` int(10) DEFAULT NULL,
  `budget_type` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `profession_id` (`profession_id`),
  KEY `study_program_id` (`study_program_id`),
  KEY `education_level_id` (`education_level_id`),
  CONSTRAINT `carts_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `carts_ibfk_3` FOREIGN KEY (`profession_id`) REFERENCES `professions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `carts_ibfk_4` FOREIGN KEY (`study_program_id`) REFERENCES `study_programs` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `carts_ibfk_5` FOREIGN KEY (`education_level_id`) REFERENCES `education_levels` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of carts
-- ----------------------------
INSERT INTO `carts` VALUES ('1', '2', '1', '2', '1', '2013', 'APBD I', '', 'Jambi', 'PT sejahtera bahagia', 'Kontraktor');
INSERT INTO `carts` VALUES ('2', '2', '1', '2', '1', '2013', 'APBD I', '', 'Jambi', 'PT sejahtera bahagia', 'Kontraktor');

-- ----------------------------
-- Table structure for cart_certificates
-- ----------------------------
DROP TABLE IF EXISTS `cart_certificates`;
CREATE TABLE `cart_certificates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(10) unsigned DEFAULT NULL,
  `certificate_id` int(11) unsigned DEFAULT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_id` (`cart_id`),
  KEY `certificate_id` (`certificate_id`),
  CONSTRAINT `cart_certificates_ibfk_1` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `cart_certificates_ibfk_2` FOREIGN KEY (`certificate_id`) REFERENCES `certificates` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cart_certificates
-- ----------------------------
INSERT INTO `cart_certificates` VALUES ('1', '1', '1', 'Muda');
INSERT INTO `cart_certificates` VALUES ('2', '1', '2', 'Muda');
INSERT INTO `cart_certificates` VALUES ('3', '2', '4', 'Muda');

-- ----------------------------
-- Table structure for certificates
-- ----------------------------
DROP TABLE IF EXISTS `certificates`;
CREATE TABLE `certificates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classification_code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `profession_id` int(10) unsigned DEFAULT NULL,
  `description` text,
  `period_of_validity` int(11) DEFAULT '0',
  `price_soft` double(20,2) NOT NULL DEFAULT '0.00',
  `price_hard` double(20,2) DEFAULT '0.00',
  `status` tinyint(1) DEFAULT '1',
  `image_name` varchar(255) DEFAULT NULL,
  `image_thumb` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `certificates_ibfk_1` (`profession_id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `certificates_ibfk_1` FOREIGN KEY (`profession_id`) REFERENCES `professions` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `certificates_ibfk_4` FOREIGN KEY (`type_id`) REFERENCES `certificate_types` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=231 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of certificates
-- ----------------------------
INSERT INTO `certificates` VALUES ('1', '101', 'Arsitek', '1', '1', 'Informasi mengenai sertifikat ini adalah', '360', '2500000.00', '3000000.00', '1', 'SKA_101_Arsitek.txt', null, 'images/full/certificate/SKA_101_Arsitek.txt', '2018-03-18 11:47:51', '2018-04-20 13:11:24');
INSERT INTO `certificates` VALUES ('2', '102', 'Ahli Desain Interior', '1', '1', null, '360', '2500000.00', '0.00', '1', null, null, null, '2018-03-18 11:48:51', '2018-03-22 21:45:56');
INSERT INTO `certificates` VALUES ('3', '103', 'Ahli Arsitekur Lansekap', '1', '1', null, '360', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:01:52', '2018-03-22 21:49:37');
INSERT INTO `certificates` VALUES ('4', '104', 'Teknik Iluminasi', '1', '1', null, '360', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:01:54', '2018-03-22 21:49:40');
INSERT INTO `certificates` VALUES ('5', '201', 'Ahli Teknik Bangunan Gedung', '1', '5', 'x', '360', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:01:56', '2018-04-19 13:07:30');
INSERT INTO `certificates` VALUES ('6', '202', 'Ahli Teknik Jalan', '1', '5', null, '360', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:01:57', '2018-03-22 21:49:42');
INSERT INTO `certificates` VALUES ('7', '203', '	Ahli Teknik Jembatan', '1', '5', null, '360', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:01:58', '2018-03-22 21:49:42');
INSERT INTO `certificates` VALUES ('8', '204', 'Ahli Keselamatan Jalan', '1', '5', null, '360', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:00', '2018-03-22 21:49:43');
INSERT INTO `certificates` VALUES ('9', '205', 'Ahli Teknik Terowongan', '1', '5', 'x', '360', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:01', '2018-04-19 13:08:06');
INSERT INTO `certificates` VALUES ('10', '206', '	Ahli Teknik Landasan Terbang', '1', '5', null, '360', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:02', '2018-03-22 21:49:44');
INSERT INTO `certificates` VALUES ('11', '207', 'Ahli Teknik Jalan Rel', '1', '5', null, '60', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:04', '2018-03-22 21:49:44');
INSERT INTO `certificates` VALUES ('12', '208', 'Ahli Teknik Dermaga', '1', '5', null, '60', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:06', '2018-03-22 21:49:44');
INSERT INTO `certificates` VALUES ('13', '209', 'Ahli Teknik Bangunan Lepas Pantai', '1', '5', null, '60', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:12', '2018-03-22 21:49:45');
INSERT INTO `certificates` VALUES ('14', '210', 'Ahli Teknik Bendungan Besar', '1', '5', null, '60', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:14', '2018-03-22 21:49:45');
INSERT INTO `certificates` VALUES ('15', '211', '	Ahli Teknik Sumber Daya Air', '1', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:16', '2018-03-22 21:49:46');
INSERT INTO `certificates` VALUES ('16', '214', 'Ahli Teknik Pembongkaran Bangunan', '1', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:22', '2018-03-22 21:49:46');
INSERT INTO `certificates` VALUES ('17', '215', '	Ahli Pemeliharaan dan Perawatan Bangunan', '1', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:31', '2018-03-22 21:49:48');
INSERT INTO `certificates` VALUES ('18', '216', 'Ahli Geoteknik', '1', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:33', '2018-03-22 21:49:47');
INSERT INTO `certificates` VALUES ('19', '217', 'Ahli Geodesi', '1', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:34', '2018-03-22 21:49:48');
INSERT INTO `certificates` VALUES ('20', '301', 'Ahli Teknik Mekanikal', '1', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:36', '2018-03-22 21:49:49');
INSERT INTO `certificates` VALUES ('21', '302', '	Ahli Teknik Sistem Tata Udara dan Refrigerasi', '1', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:44', '2018-03-22 21:49:50');
INSERT INTO `certificates` VALUES ('22', '303', 'Ahli Teknik Plambing dan Pompa Mekanik', '1', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:46', '2018-03-22 21:49:50');
INSERT INTO `certificates` VALUES ('23', '304', 'Ahli Teknik Proteksi Kebakaran', '1', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:48', '2018-03-22 21:49:54');
INSERT INTO `certificates` VALUES ('24', '305', '	Ahli Teknik Transportasi Dalam Gedung', '1', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:51', '2018-03-22 21:49:57');
INSERT INTO `certificates` VALUES ('25', '401', '	Ahli Teknik Tenaga Listrik', '1', '2', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:53', '2018-03-22 21:49:58');
INSERT INTO `certificates` VALUES ('26', '405', 'Ahli Teknik Elektronika dan Telekomunikasi Dalam Gedung', '1', '2', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:02:55', '2018-03-22 21:50:00');
INSERT INTO `certificates` VALUES ('27', '406', 'Ahli Teknik Sistem Sinyal Telekomunikasi Kereta Api', '1', '2', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:03:02', '2018-03-22 21:49:59');
INSERT INTO `certificates` VALUES ('28', '501', 'Ahli Teknik Lingkungan', '1', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:03:04', '2018-03-22 21:50:00');
INSERT INTO `certificates` VALUES ('29', '502', 'Ahli Perencanaan Wilayah dan Kota', '1', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:03:06', '2018-03-22 21:50:03');
INSERT INTO `certificates` VALUES ('30', '503', 'Ahli Teknik Sanitasi dan Limbah', '1', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:03:08', '2018-03-22 21:50:04');
INSERT INTO `certificates` VALUES ('31', '504', 'Ahli Teknik Air Minum', '1', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:03:10', '2018-03-22 21:50:06');
INSERT INTO `certificates` VALUES ('32', '601', 'Ahli Manajemen Konstruksi', '1', '7', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:03:12', '2018-03-22 21:50:07');
INSERT INTO `certificates` VALUES ('33', '602', 'Ahli Manajemen Proyek', '1', '7', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:03:14', '2018-03-22 21:50:08');
INSERT INTO `certificates` VALUES ('34', '603', 'Ahli K3 Konstruksi', '1', '7', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:03:16', '2018-03-22 21:50:08');
INSERT INTO `certificates` VALUES ('35', '604', 'Ahli Sistem Manajemen Mutu', '1', '7', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:03:22', '2018-03-22 21:50:10');
INSERT INTO `certificates` VALUES ('36', 'TA 003', '	Juru Gambar / Draftman Arsitektur', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:03:29', '2018-03-22 21:50:11');
INSERT INTO `certificates` VALUES ('37', 'TA 004', 'Tukang Pasang Bata / Dinding / Bricklayer / Bricklaying (Tukang Bata)', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:03:46', '2018-03-22 21:50:11');
INSERT INTO `certificates` VALUES ('38', 'TA 005', 'Tukang Pasang Batu / Stone (Rubble) Mason (Tukang Bangunan Umum)', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:03:50', '2018-03-22 21:50:12');
INSERT INTO `certificates` VALUES ('39', 'TA 006', 'Tukang Plesteran / Plesterer / Solid Plesterer', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:03:53', '2018-03-22 21:50:12');
INSERT INTO `certificates` VALUES ('40', 'TA 007', 'Tukang Pasang Keramik (Lantai dan Dinding)', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:03:57', '2018-03-22 21:50:13');
INSERT INTO `certificates` VALUES ('41', 'TA 008', 'Tukang Pasang Lantai Tegel / Ubin / Marmer', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:04:07', '2018-03-22 21:50:14');
INSERT INTO `certificates` VALUES ('42', 'TA 009', 'Tukang Kayu / Carpenter (Termasuk Kayu Bangunan)', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:04:12', '2018-03-22 21:50:14');
INSERT INTO `certificates` VALUES ('43', 'TA 011', 'Tukang Pasang Plafon / Ceiling Fixer / Ceiling Fixing', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:04:16', '2018-03-22 21:50:15');
INSERT INTO `certificates` VALUES ('44', 'TA 012', 'Tukang Pasang Dinding Gypsum', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:04:21', '2018-03-22 21:50:15');
INSERT INTO `certificates` VALUES ('45', 'TA 013', 'Tukang Pasang Plafon Gypsum', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:04:24', '2018-03-22 21:50:17');
INSERT INTO `certificates` VALUES ('46', 'TA 014', '	Tukang Cat Bangunan', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:04:27', '2018-03-22 21:50:18');
INSERT INTO `certificates` VALUES ('47', 'TA 015', '	Tukang Taman / Landscape', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:04:30', '2018-03-22 21:50:19');
INSERT INTO `certificates` VALUES ('48', 'TA 016', '	Pelaksana Lapangan Pekerjaan Plambing', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:04:35', '2018-03-22 21:50:20');
INSERT INTO `certificates` VALUES ('49', 'TA 017', 'Supervisor Perawatan Gedung Bertingkat', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:04:39', '2018-03-22 21:50:20');
INSERT INTO `certificates` VALUES ('50', 'TA 018', 'Tukang Pelitur Kayu', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:04:42', '2018-03-22 21:50:21');
INSERT INTO `certificates` VALUES ('51', 'TA 019', 'Tukang Kusen Pintu dan Jendela Bertingkat', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:04:46', '2018-03-22 21:50:21');
INSERT INTO `certificates` VALUES ('52', 'TA 020', '	Pelaksana Lapangan Pekerjaan Perumahan dan Gedung', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:04:50', '2018-03-22 21:50:22');
INSERT INTO `certificates` VALUES ('53', 'TA 021', 'Pelaksana Lapangan Pekerjaan Finishing Bangunan Gedung Bertingkat Tinggi', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:04:54', '2018-03-22 21:50:22');
INSERT INTO `certificates` VALUES ('54', 'TA 022', '	Pelaksana Bangunan Gedung / Pekerjaan Gedung', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:05:01', '2018-03-22 21:50:23');
INSERT INTO `certificates` VALUES ('55', 'TA 023', 'Pelaksana Bangunan Perumahan / Pemukiman', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:05:20', '2018-03-22 21:50:24');
INSERT INTO `certificates` VALUES ('56', 'TA 024', 'Pengawas Bangunan Gedung', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:05:23', '2018-03-22 21:50:24');
INSERT INTO `certificates` VALUES ('57', 'TA 025', 'Pengawas Bangunan Perumahan', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:05:26', '2018-03-22 21:50:25');
INSERT INTO `certificates` VALUES ('58', 'TA 026', '	Pelaksana Penata Taman', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:05:29', '2018-03-22 21:50:25');
INSERT INTO `certificates` VALUES ('59', 'TA 027', 'Juru Ukur Kuantitas Bangunan Gedung', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:05:32', '2018-03-22 21:50:27');
INSERT INTO `certificates` VALUES ('60', 'TA 028', '	Pengawas Mutu Pelaksanaan Konstruksi Bangunan Gedung', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:05:35', '2018-03-22 21:50:28');
INSERT INTO `certificates` VALUES ('61', 'TA 029', '	Penata Taman / Lanscape', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:05:38', '2018-03-22 21:50:29');
INSERT INTO `certificates` VALUES ('62', 'TA 030', '	Pelaksana Madya Perawatan Bangunan Gedung', '2', '1', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:05:43', '2018-03-22 21:50:30');
INSERT INTO `certificates` VALUES ('63', 'TE 021', '	Teknisi Instalasi Penerangan Dan Daya Fasa Satu', '2', '2', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:06:00', '2018-03-22 21:50:30');
INSERT INTO `certificates` VALUES ('64', 'TE 022', 'Teknisi Instalasi Penerangan dan Daya Fasa Tiga', '2', '2', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:06:03', '2018-03-22 21:50:31');
INSERT INTO `certificates` VALUES ('65', 'TE 024', '	Teknisi Instalasi Sistem Penangkal Petir', '2', '2', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:06:18', '2018-03-22 21:50:31');
INSERT INTO `certificates` VALUES ('66', 'TE 055', 'Teknisi Instalasi Kontrol Terprogram ( Berbasis PLC )', '2', '2', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:06:24', '2018-03-22 21:50:32');
INSERT INTO `certificates` VALUES ('67', 'TE 057', '	Teknisi Instalasi Otomasi Industri', '2', '2', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:06:29', '2018-03-22 21:50:32');
INSERT INTO `certificates` VALUES ('68', 'TE 058', 'Teknisi Instalasi Motor Listrik, Kontrol dan Instrumen', '2', '2', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:06:34', '2018-03-22 21:50:34');
INSERT INTO `certificates` VALUES ('69', 'TE 059', '	Teknisi Instalasi Alat Pengukur dan Pembatas ( APP )', '2', '2', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:06:37', '2018-03-22 21:50:33');
INSERT INTO `certificates` VALUES ('70', 'TE 060', 'Teknisi Instalasi Jaringan Tegangan Rendah ( JTR )', '2', '2', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:06:40', '2018-03-22 21:50:34');
INSERT INTO `certificates` VALUES ('71', 'TE 061', 'Teknisi Instalasi Jaringan Tegangan Menengah (JTM)', '2', '2', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:06:44', '2018-03-22 21:50:35');
INSERT INTO `certificates` VALUES ('72', 'TL 002', '	Estimator / Biaya Jalan', '2', '3', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:06:52', '2018-03-22 21:50:35');
INSERT INTO `certificates` VALUES ('73', 'TL 003', 'Quantity surveyor', '2', '3', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:06:56', '2018-03-22 21:50:36');
INSERT INTO `certificates` VALUES ('74', 'TL 005', 'Mandor Tukang Batu / Bata / Beton', '2', '3', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:00', '2018-03-22 21:50:37');
INSERT INTO `certificates` VALUES ('75', 'TL 006', 'Mandor Tukang Kayu', '2', '3', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:02', '2018-03-22 21:50:37');
INSERT INTO `certificates` VALUES ('76', 'TL 007', 'Mandor Batu Belah', '2', '3', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:06', '2018-03-22 21:50:38');
INSERT INTO `certificates` VALUES ('77', 'TL 008', 'Mandor Tanah', '2', '3', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:08', '2018-03-22 21:50:39');
INSERT INTO `certificates` VALUES ('78', 'TL 009', 'Mandor Besi / Pembesian / Penulangan Beton', '2', '3', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:13', '2018-03-22 21:50:40');
INSERT INTO `certificates` VALUES ('79', 'TM 003', 'Juru gambar / Draftman ? Mekanikal', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:15', '2018-03-22 21:50:43');
INSERT INTO `certificates` VALUES ('80', 'TM 004', 'Operator Bulldozer', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:18', '2018-03-22 21:50:43');
INSERT INTO `certificates` VALUES ('81', 'TM 005', 'Operator Motor Grader', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:21', '2018-03-22 21:50:44');
INSERT INTO `certificates` VALUES ('82', 'TM 006', 'Operator Mesin Excavator', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:30', '2018-03-22 21:50:45');
INSERT INTO `certificates` VALUES ('83', 'TM 007', 'Operator Tangga Intake Dam', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:32', '2018-03-22 21:50:45');
INSERT INTO `certificates` VALUES ('84', 'TM 008', 'Operator Road Roller / Road Roller Paver Operator', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:35', '2018-03-22 21:50:46');
INSERT INTO `certificates` VALUES ('85', 'TM 009', 'Operator Wheel Loader', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:39', '2018-03-22 21:50:46');
INSERT INTO `certificates` VALUES ('86', 'TM 010', 'Operator Crowler Crane', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:41', '2018-03-22 21:50:46');
INSERT INTO `certificates` VALUES ('87', 'TM 011', 'Operator Rough Terrain Crane', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:45', '2018-03-22 21:50:47');
INSERT INTO `certificates` VALUES ('88', 'TM 012', 'Operator Truk Mounted Crane', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:48', '2018-03-22 21:50:48');
INSERT INTO `certificates` VALUES ('89', 'TM 013', 'Operator Tower Crane', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:07:51', '2018-03-22 21:50:48');
INSERT INTO `certificates` VALUES ('90', 'TM 014', 'Operator Wheel Crane', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:07', '2018-03-22 21:50:49');
INSERT INTO `certificates` VALUES ('91', 'TM 015', 'Operator Backhoe', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:11', '2018-03-22 21:50:49');
INSERT INTO `certificates` VALUES ('92', 'TM 016', 'Operator Pile Hammer', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:14', '2018-03-22 21:50:50');
INSERT INTO `certificates` VALUES ('93', 'TM 017', '	Operator Mobil Pengaduk Beton', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:18', '2018-03-22 21:50:50');
INSERT INTO `certificates` VALUES ('94', 'TM 018', 'Operator Crowler Tractor Bulldozer', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:20', '2018-03-22 21:50:51');
INSERT INTO `certificates` VALUES ('95', 'TM 019', 'Operator Dump Truck', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:22', '2018-03-22 21:50:52');
INSERT INTO `certificates` VALUES ('96', 'TM 020', 'Operator Forklif', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:25', '2018-03-22 21:50:54');
INSERT INTO `certificates` VALUES ('97', 'TM 021', 'Operator Specialist Equipment Plant', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:27', '2018-03-22 21:50:55');
INSERT INTO `certificates` VALUES ('98', 'TM 022', '	Operator Mobile Elevating Work Platform', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:30', '2018-03-22 21:50:55');
INSERT INTO `certificates` VALUES ('99', 'TM 023', 'Operator Concrete Pump Equipment', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:32', '2018-03-22 21:50:56');
INSERT INTO `certificates` VALUES ('100', 'TM 024', 'Operator Slingging & Rigging', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:36', '2018-03-22 21:50:56');
INSERT INTO `certificates` VALUES ('101', 'TM 025', 'Operator Mesin Bor', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:38', '2018-03-22 21:50:57');
INSERT INTO `certificates` VALUES ('102', 'TM 026', '	Operator Mesin Bubut', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:41', '2018-03-22 21:50:58');
INSERT INTO `certificates` VALUES ('103', 'TM 027', 'Mekanik Alat-alat Berat', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:44', '2018-03-22 21:50:58');
INSERT INTO `certificates` VALUES ('104', 'TM 028', 'Tukang Las / Welder / Gas & Electric Welder', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:47', '2018-03-22 21:50:58');
INSERT INTO `certificates` VALUES ('105', 'TM 029', 'Tukang Bubut / Mesin Pemangkas', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:08:49', '2018-03-22 21:50:59');
INSERT INTO `certificates` VALUES ('106', 'TM 030', '	Operator Mesin Pencampur Aspal', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:04', '2018-03-22 21:51:00');
INSERT INTO `certificates` VALUES ('107', 'TM 031', 'Operator Aspal Paver / Operator Mesin Penggelar Aspal', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:07', '2018-03-22 21:51:01');
INSERT INTO `certificates` VALUES ('108', 'TM 032', '	Operator Mesin Penyemprot Aspal', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:10', '2018-03-22 21:51:02');
INSERT INTO `certificates` VALUES ('109', 'TM 033', 'Pelaksana Produksi Hotmix', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:14', '2018-03-22 21:51:02');
INSERT INTO `certificates` VALUES ('110', 'TM 034', 'Sheep Foot Vibrating Compactor Operator', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:16', '2018-03-22 21:51:04');
INSERT INTO `certificates` VALUES ('111', 'TM 035', 'Juru Las Oxyactylene', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:19', '2018-03-22 21:51:07');
INSERT INTO `certificates` VALUES ('112', 'TM 036', '	Operator Mesin Gergaji Presisi', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:22', '2018-03-22 21:51:07');
INSERT INTO `certificates` VALUES ('113', 'TM 037', 'Operator Mesin Derek', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:25', '2018-03-22 21:51:08');
INSERT INTO `certificates` VALUES ('114', 'TM 038', 'Tukang Pasang Pipa', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:28', '2018-03-22 21:51:08');
INSERT INTO `certificates` VALUES ('115', 'TM 039', 'Tukang Las Konstruksi Plat dan Pipa', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:31', '2018-03-22 21:51:10');
INSERT INTO `certificates` VALUES ('116', 'TM 040', 'Tukang Las MID (CO2) Posisi Bawah Tangan', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:35', '2018-03-22 21:51:11');
INSERT INTO `certificates` VALUES ('117', 'TM 041', 'Tukang Las TIG Posisi bawah Tangan', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:42', '2018-03-22 21:51:11');
INSERT INTO `certificates` VALUES ('118', 'TM 042', '	Operator Mesin Bubut Kayu', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:44', '2018-03-22 21:51:13');
INSERT INTO `certificates` VALUES ('119', 'TM 043', '	Operator Pengeboran Minyak', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:47', '2018-03-22 21:51:12');
INSERT INTO `certificates` VALUES ('120', 'TM 044', 'Pelaksana Lapangan Pekerjaan ME Bangunan Gedung Bertingkat Tinggi', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:49', '2018-03-22 21:51:15');
INSERT INTO `certificates` VALUES ('121', 'TM 045', '	Pelaksana Lapangan Pekerjaan Setting Out Bangunan Gedung Bertingkat', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:51', '2018-03-22 21:51:14');
INSERT INTO `certificates` VALUES ('122', 'TM 046', '	Operatot Mesin Grader', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:54', '2018-03-22 21:51:14');
INSERT INTO `certificates` VALUES ('123', 'TM 047', 'Operator Mesin Pemecah batu', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:09:57', '2018-03-22 21:51:20');
INSERT INTO `certificates` VALUES ('124', 'TM 048', '	Pelaksana Perawatan Instalasi Sistem transportasi Vertikal Dalam Gedung', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:10:01', '2018-03-22 21:51:17');
INSERT INTO `certificates` VALUES ('125', 'TM 049', '	Concrete Paver Operator (Operator Mesin Penghampar Beton Semen)', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:10:04', '2018-03-22 21:51:22');
INSERT INTO `certificates` VALUES ('126', 'TM 050', '	Operator Cold Milling Machine', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:10:08', '2018-03-22 21:51:22');
INSERT INTO `certificates` VALUES ('127', 'TM 051', 'Tukang Las Listrik', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:10:11', '2018-03-22 21:51:23');
INSERT INTO `certificates` VALUES ('128', 'TM 052', 'Mekanik Tower Crane', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:10:15', '2018-03-22 21:51:24');
INSERT INTO `certificates` VALUES ('129', 'TM 053', '	Operator Batching Plant', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:10:18', '2018-03-22 21:51:24');
INSERT INTO `certificates` VALUES ('130', 'TM 054', 'Mekanik Campuran Aspal Panas', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:10:20', '2018-03-22 21:51:25');
INSERT INTO `certificates` VALUES ('131', 'TM 055', 'Meanik Heating Ventilation dan Air Condition (HVAC)', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:10:23', '2018-03-22 21:51:26');
INSERT INTO `certificates` VALUES ('132', 'TM 056', '	Operator Gondola Pada Bangunan Gedung', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:10:27', '2018-03-22 21:51:25');
INSERT INTO `certificates` VALUES ('133', 'TM 057', 'Teknisi Fire Alarm', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:10:30', '2018-03-22 21:51:26');
INSERT INTO `certificates` VALUES ('134', 'TM 058', 'Mekanik Kapal Keruk', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:10:32', '2018-03-22 21:51:27');
INSERT INTO `certificates` VALUES ('135', 'TM 059', 'Mekanik Engine Alat Berat', '2', '4', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:10:35', '2018-03-22 21:51:27');
INSERT INTO `certificates` VALUES ('136', 'TS 003', '	Juru Gambar / Draftman -Sipil', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:02', '2018-03-22 21:51:28');
INSERT INTO `certificates` VALUES ('137', 'TS 004', '	Juru Ukur / Teknisi Survey Pemetaan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:05', '2018-03-22 21:51:28');
INSERT INTO `certificates` VALUES ('138', 'TS 006', 'Teknisi Laboratorium Beton', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:08', '2018-03-22 21:51:29');
INSERT INTO `certificates` VALUES ('139', 'TS 007', '	Teknisi Laboratorium Tanah', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:11', '2018-03-22 21:51:29');
INSERT INTO `certificates` VALUES ('140', 'TS 008', '	Teknisi Laboratorium Aspal', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:14', '2018-03-22 21:51:32');
INSERT INTO `certificates` VALUES ('141', 'TS 009', '	Operator Alat Penyelidikan Tanah/Soil Investagation OPERATOR', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:17', '2018-03-22 21:51:31');
INSERT INTO `certificates` VALUES ('142', 'TS 010', '	Tukang Pekerjaan Pondasi / Fondation Work', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:19', '2018-03-22 21:51:33');
INSERT INTO `certificates` VALUES ('143', 'TS 011', 'Tukang Pekerjaan Tanah / Earth Moving', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:23', '2018-03-22 21:51:33');
INSERT INTO `certificates` VALUES ('144', 'TS 012', '	Tukang Besi-beton / Barbender / Bar bending', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:27', '2018-03-22 21:51:34');
INSERT INTO `certificates` VALUES ('145', 'TS 013', '	Tukang Cor Beton / Concretor / Concrete Operations', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:30', '2018-03-22 21:51:35');
INSERT INTO `certificates` VALUES ('146', 'TS 014', '	Tukang Pasang Perancah / Formworker / Formwork', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:32', '2018-03-22 21:51:38');
INSERT INTO `certificates` VALUES ('147', 'TS 015', 'Tukang Pasang Scaffolding / Scaffolder / Scaffolding', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:34', '2018-03-22 21:51:36');
INSERT INTO `certificates` VALUES ('148', 'TS 016', 'Tukang Pasang Pipa Gas / Gas Pipe Fitter', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:36', '2018-03-22 21:51:38');
INSERT INTO `certificates` VALUES ('149', 'TS 017', '	Tukang Perkerasan Jalan / Paving', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:40', '2018-03-22 21:51:39');
INSERT INTO `certificates` VALUES ('150', 'TS 018', 'Tukang Pasang Konstruksi Rig / Piling Rigger / Rigger', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:44', '2018-03-22 21:51:40');
INSERT INTO `certificates` VALUES ('151', 'TS 019', 'Tukang \"Boring\" / Boring and Driving', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:46', '2018-03-22 21:51:41');
INSERT INTO `certificates` VALUES ('152', 'TS 020', '	Tukang Pekerjaan Baja', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:48', '2018-03-22 21:51:43');
INSERT INTO `certificates` VALUES ('153', 'TS 021', 'Pekerja Aspal Jalan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:50', '2018-03-22 21:51:43');
INSERT INTO `certificates` VALUES ('154', 'TS 022', '	Mandor Produksi Campuran Aspal Panas', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:53', '2018-03-22 21:51:44');
INSERT INTO `certificates` VALUES ('155', 'TS 023', '	Mandor Perkerasan Jalan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:56', '2018-03-22 21:51:44');
INSERT INTO `certificates` VALUES ('156', 'TS 024', '	Teknisi Pekerjaan Jalan dan Jembatan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:11:59', '2018-03-22 21:51:45');
INSERT INTO `certificates` VALUES ('157', 'TS 025', 'Juru Ukur Kuantitas Pekerjaan Jalan dan Jembatan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:01', '2018-03-22 21:51:45');
INSERT INTO `certificates` VALUES ('158', 'TS 026', '	Tukang Perancah Besi', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:03', '2018-03-22 21:51:46');
INSERT INTO `certificates` VALUES ('159', 'TS 027', 'Tukang Konstruksi Baja & Plat (dan Tukang Pasang Menara)', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:06', '2018-03-22 21:51:46');
INSERT INTO `certificates` VALUES ('160', 'TS 028', '	Pelaksana Lapangan Pekerjaan Jalan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:09', '2018-03-22 21:51:47');
INSERT INTO `certificates` VALUES ('161', 'TS 029', '	Pelaksana Lapangan Pekerjaan Jembatan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:18', '2018-03-22 21:51:47');
INSERT INTO `certificates` VALUES ('162', 'TS 030', 'Pelaksana Lapangan Pekerjaan Jaringan Irigasi', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:24', '2018-03-22 21:51:48');
INSERT INTO `certificates` VALUES ('163', 'TS 031', '	Pelaksana Saluran Irigasi', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:29', '2018-03-22 21:51:48');
INSERT INTO `certificates` VALUES ('164', 'TS 032', '	Pelaksana Bangunan Irigrasi', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:31', '2018-03-22 21:51:48');
INSERT INTO `certificates` VALUES ('165', 'TS 033', '	Pelaksana Bendungan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:33', '2018-03-22 21:51:49');
INSERT INTO `certificates` VALUES ('166', 'TS 034', '	Pelaksana Terowongan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:36', '2018-03-22 21:51:49');
INSERT INTO `certificates` VALUES ('167', 'TS 035', '	Teknisi Penghitung Kuantitas Pekerjaan Sumber Daya Air', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:40', '2018-03-22 21:51:50');
INSERT INTO `certificates` VALUES ('168', 'TS 036', '	Pengawas Bendungan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:46', '2018-03-22 21:51:51');
INSERT INTO `certificates` VALUES ('169', 'TS 037', '	Pengawas Bangunan Irigasi', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:49', '2018-03-22 21:51:52');
INSERT INTO `certificates` VALUES ('170', 'TS 038', '	Pengawas Saluran Irigasi', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:52', '2018-03-22 21:51:53');
INSERT INTO `certificates` VALUES ('172', 'TS 040', 'Pengawas Lapangan Pekerjaan Jalan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:12:57', '2018-03-22 21:51:54');
INSERT INTO `certificates` VALUES ('173', 'TS 041', '	Pengawas Lapangan Pekerjaan Jembatan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:01', '2018-03-22 21:51:54');
INSERT INTO `certificates` VALUES ('174', 'TS 042', 'Teknisi Pengerukan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:03', '2018-03-22 21:51:55');
INSERT INTO `certificates` VALUES ('175', 'TS 043', 'Teknisi Survey Teknik Sipil', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:06', '2018-03-22 21:51:55');
INSERT INTO `certificates` VALUES ('176', 'TS 044', 'Pelaksana Pekerjaan Jembatan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:08', '2018-03-22 21:51:55');
INSERT INTO `certificates` VALUES ('177', 'TS 045', '	Pelaksana Pekerjaan Jalan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:10', '2018-03-22 21:51:58');
INSERT INTO `certificates` VALUES ('178', 'TS 046', '	Kepala Pengawas Pekerjaan Jalan dan Jembatan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:13', '2018-03-22 21:51:56');
INSERT INTO `certificates` VALUES ('179', 'TS 047', '	Juru Hitung Kuantitas', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:21', '2018-03-22 21:51:58');
INSERT INTO `certificates` VALUES ('180', 'TS 048', '	Juru Ukur Pekerjaan Jalan / Jembatan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:25', '2018-03-22 21:51:59');
INSERT INTO `certificates` VALUES ('181', 'TS 049', 'Teknisi Penghitung Kuantitas Pekerjaan Jalan / Jembatan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:29', '2018-03-22 21:52:01');
INSERT INTO `certificates` VALUES ('182', 'TS 050', '	Steel Erector of Bridge', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:35', '2018-03-22 21:51:59');
INSERT INTO `certificates` VALUES ('183', 'TS 051', '	Pelaksana Bangunan Gedung / Pekerjaan Gedung', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:43', '2018-03-22 21:52:00');
INSERT INTO `certificates` VALUES ('184', 'TS 052', '	Pelaksana Lapangan Pekerjaan Gedung', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:49', '2018-03-22 21:52:00');
INSERT INTO `certificates` VALUES ('185', 'TS 053', '	Tukang Kayu Bekisting', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:52', '2018-03-22 21:52:03');
INSERT INTO `certificates` VALUES ('186', 'TS 054', 'Tukang Pasang Beton Pra Cetak', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:55', '2018-03-22 21:52:04');
INSERT INTO `certificates` VALUES ('187', 'TS 055', '	Tukang Rangka Alumunium', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:57', '2018-03-22 21:52:05');
INSERT INTO `certificates` VALUES ('188', 'TS 056', '	Mandor Pemasangan Rangka Atap Baja Ringan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:13:59', '2018-03-22 21:52:05');
INSERT INTO `certificates` VALUES ('189', 'TS 057', 'Mandor Pemasangan Rangka Baja Jembatan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:14:02', '2018-03-22 21:52:06');
INSERT INTO `certificates` VALUES ('190', 'TS 058', '	Pelaksana Lapangan Pekerjaan Pemasangan Jembatan Rangka Baja', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:14:05', '2018-03-22 21:52:07');
INSERT INTO `certificates` VALUES ('191', 'TS 059', '	Juru Gambar Pekerjaan Jalan dan Jembatan', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:14:10', '2018-03-22 21:52:09');
INSERT INTO `certificates` VALUES ('192', 'TS 060', 'Tukang Bekisting (acuan) dan Perancah Bidang Sumber Daya Air', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:14:12', '2018-03-22 21:52:12');
INSERT INTO `certificates` VALUES ('193', 'TS 061', '	Mandor Pekerjaan Perkerasan Aspal', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:14:15', '2018-03-22 21:52:10');
INSERT INTO `certificates` VALUES ('194', 'TS 062', 'Mandor Tukang Pasang Beton Precast', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:14:18', '2018-03-22 21:52:13');
INSERT INTO `certificates` VALUES ('195', 'TS 063', 'Asisten Teknisi Laboratorium Jalan (Campuran Beton Beraspal)', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:14:21', '2018-03-22 21:52:13');
INSERT INTO `certificates` VALUES ('196', 'TS 064', 'Asisten Teknisi Laboratorium Beton', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:14:26', '2018-03-22 21:52:14');
INSERT INTO `certificates` VALUES ('197', 'TS 065', '	Asisten Teknisi Laboratorium Mekanika Tanah', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:14:29', '2018-03-22 21:52:14');
INSERT INTO `certificates` VALUES ('198', 'TS 066', 'Teknisi Geoteknik', '2', '5', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:14:33', '2018-03-22 21:52:16');
INSERT INTO `certificates` VALUES ('199', 'TT 001', 'Pelaksana Plambing / Pekerjaan Plambing', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:10', '2018-03-22 21:52:17');
INSERT INTO `certificates` VALUES ('200', 'TT 002', '	Pengawas Plambing / Pekerjaan Plambing', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:12', '2018-03-22 21:52:17');
INSERT INTO `certificates` VALUES ('201', 'TT 003', 'Juru gambar / Draftman - Tata lingkungan', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:14', '2018-03-22 21:52:18');
INSERT INTO `certificates` VALUES ('202', 'TT 004', '	Tukang Sanitary', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:16', '2018-03-22 21:52:21');
INSERT INTO `certificates` VALUES ('203', 'TT 005 ', 'Tukang Pipa Air / Plumber', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:19', '2018-03-22 21:52:22');
INSERT INTO `certificates` VALUES ('204', 'TT 006', 'Tukang Pipa Gas', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:21', '2018-03-22 21:52:22');
INSERT INTO `certificates` VALUES ('205', 'TT 007', 'Tukang Pipa Bangunan', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:24', '2018-03-22 21:52:23');
INSERT INTO `certificates` VALUES ('206', 'TT 008', 'Tukang Filter Pipa', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:32', '2018-03-22 21:52:23');
INSERT INTO `certificates` VALUES ('207', 'TT 009', 'Juru Pengeboran Air Tanah', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:34', '2018-03-22 21:52:25');
INSERT INTO `certificates` VALUES ('208', 'TT 010', 'Montir Generator', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:36', '2018-03-22 21:52:24');
INSERT INTO `certificates` VALUES ('209', 'TT 011', 'Pelaksana Perpipaan Air Bersih', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:38', '2018-03-22 21:52:24');
INSERT INTO `certificates` VALUES ('210', 'TT 012', 'Pelaksana Pembuatan Fasilitas Sampah dan Limbah', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:40', '2018-03-22 21:52:26');
INSERT INTO `certificates` VALUES ('211', 'TT 013', 'Pelaksana Pengeboran Air Tanah', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:42', '2018-03-22 21:52:27');
INSERT INTO `certificates` VALUES ('212', 'TT 014', 'Pengawas Perpipaan Air Bersih', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:44', '2018-03-22 21:52:27');
INSERT INTO `certificates` VALUES ('213', 'TT 015', 'Pengawas Pengeboran Air Tanah', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:46', '2018-03-22 21:52:28');
INSERT INTO `certificates` VALUES ('214', 'TT 016', 'Tukang Plambing', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:50', '2018-03-22 21:52:30');
INSERT INTO `certificates` VALUES ('215', 'TT 017', 'Mandor Plambing', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:52', '2018-03-22 21:52:30');
INSERT INTO `certificates` VALUES ('216', 'TT 018', 'Pelaksana Pengujian Kualitas Air Minum SPAM', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:55', '2018-03-22 21:52:31');
INSERT INTO `certificates` VALUES ('217', 'TT 019', 'Pelaksana Pemasangan Pintu Air', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:15:58', '2018-03-22 21:52:31');
INSERT INTO `certificates` VALUES ('218', 'TT 020', 'Pelaksana Lapangan Perpipaan Air Madya', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:16:02', '2018-03-22 21:52:32');
INSERT INTO `certificates` VALUES ('219', 'TT 021', 'Pelaksana Lapangan TK II Pekerjaan Perpipaan', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:16:04', '2018-03-22 21:52:33');
INSERT INTO `certificates` VALUES ('220', 'TT 022', 'Pelaksana Pemasangan Pipa Leachate (Lindo dan Gas di TPA)', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:16:08', '2018-03-22 21:52:33');
INSERT INTO `certificates` VALUES ('221', 'TT 023', 'Pelaksana Pekerjaan Bangunan Limbah Permukiman', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:16:10', '2018-03-22 21:52:34');
INSERT INTO `certificates` VALUES ('222', 'TT 024', 'Pelaksana Pekerjaan Lapisan Kedap Air di empat Pemproses TPA', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:16:12', '2018-03-22 21:52:34');
INSERT INTO `certificates` VALUES ('223', 'TT 025', 'Teknisi Soundir', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:16:15', '2018-03-22 21:52:35');
INSERT INTO `certificates` VALUES ('224', 'TT 206', 'Teknisi Geologi Teknik', '2', '6', null, '90', '3000000.00', '0.00', '1', null, null, null, '2018-03-22 21:16:18', '2018-03-22 21:52:37');
INSERT INTO `certificates` VALUES ('225', 'XASa', 'asd', '2', '1', null, '360', '118.00', '0.00', '1', null, null, null, '2018-04-17 13:10:11', '2018-04-17 13:10:11');
INSERT INTO `certificates` VALUES ('226', 'asd', '123', '2', '2', 'asd', '30', '12.00', '0.00', '1', null, null, null, '2018-04-17 13:11:15', '2018-04-17 13:11:15');
INSERT INTO `certificates` VALUES ('227', 'Test', 'test', '1', '1', 'Halo', '360', '38.00', '0.00', '1', 'SKA_Test_test.txt', 'images/thumb/certificate/SKA_Test_test.jpg', 'images/full/certificate/SKA_Test_test.txt', '2018-04-19 04:04:25', '2018-04-19 12:53:06');
INSERT INTO `certificates` VALUES ('228', 'Test dudex', 'haikx', '1', '1', 'Test', '4320', '120000.00', '0.00', '1', 'FQaQw.docx', null, 'images/full/certificate/FQaQw.docx', '2018-04-19 11:18:24', '2018-04-19 11:46:14');
INSERT INTO `certificates` VALUES ('229', 'tesxx', 'sda', '2', '1', 'asd', '4680', '1222222.00', '0.00', '1', 'SKT_tesxx_sda.txt', null, 'images/full/certificate/SKT_tesxx_sda.txt', '2018-04-19 12:10:34', '2018-04-19 12:42:22');
INSERT INTO `certificates` VALUES ('230', '321', 'Ksda', '2', '2', 'asd', '960', '100000.00', '100000.00', '1', null, null, null, '2018-05-08 04:03:45', '2018-05-08 04:03:45');

-- ----------------------------
-- Table structure for certificate_education_levels
-- ----------------------------
DROP TABLE IF EXISTS `certificate_education_levels`;
CREATE TABLE `certificate_education_levels` (
  `certificate_id` int(10) unsigned DEFAULT NULL,
  `education_level_id` int(10) unsigned DEFAULT NULL,
  KEY `certificate_id` (`certificate_id`),
  KEY `education_level_id` (`education_level_id`),
  CONSTRAINT `certificate_education_levels_ibfk_1` FOREIGN KEY (`certificate_id`) REFERENCES `certificates` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `certificate_education_levels_ibfk_2` FOREIGN KEY (`education_level_id`) REFERENCES `education_levels` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of certificate_education_levels
-- ----------------------------
INSERT INTO `certificate_education_levels` VALUES ('224', '4');
INSERT INTO `certificate_education_levels` VALUES ('224', '5');
INSERT INTO `certificate_education_levels` VALUES ('223', '4');
INSERT INTO `certificate_education_levels` VALUES ('223', '5');
INSERT INTO `certificate_education_levels` VALUES ('223', '4');
INSERT INTO `certificate_education_levels` VALUES ('2', '1');
INSERT INTO `certificate_education_levels` VALUES ('3', '1');
INSERT INTO `certificate_education_levels` VALUES ('4', '1');
INSERT INTO `certificate_education_levels` VALUES ('6', '1');
INSERT INTO `certificate_education_levels` VALUES ('7', '1');
INSERT INTO `certificate_education_levels` VALUES ('8', '1');
INSERT INTO `certificate_education_levels` VALUES ('225', '1');
INSERT INTO `certificate_education_levels` VALUES ('226', '1');
INSERT INTO `certificate_education_levels` VALUES ('228', '1');
INSERT INTO `certificate_education_levels` VALUES ('229', '4');
INSERT INTO `certificate_education_levels` VALUES ('227', '1');
INSERT INTO `certificate_education_levels` VALUES ('5', '1');
INSERT INTO `certificate_education_levels` VALUES ('9', '1');
INSERT INTO `certificate_education_levels` VALUES ('1', '1');
INSERT INTO `certificate_education_levels` VALUES ('230', '1');

-- ----------------------------
-- Table structure for certificate_study_programs
-- ----------------------------
DROP TABLE IF EXISTS `certificate_study_programs`;
CREATE TABLE `certificate_study_programs` (
  `certificate_id` int(10) unsigned DEFAULT NULL,
  `study_program_id` int(10) unsigned DEFAULT NULL,
  KEY `certificate_id` (`certificate_id`),
  KEY `study_program_id` (`study_program_id`),
  CONSTRAINT `certificate_study_programs_ibfk_1` FOREIGN KEY (`certificate_id`) REFERENCES `certificates` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `certificate_study_programs_ibfk_2` FOREIGN KEY (`study_program_id`) REFERENCES `study_programs` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of certificate_study_programs
-- ----------------------------
INSERT INTO `certificate_study_programs` VALUES ('2', '1');
INSERT INTO `certificate_study_programs` VALUES ('3', '1');
INSERT INTO `certificate_study_programs` VALUES ('2', '2');
INSERT INTO `certificate_study_programs` VALUES ('3', '3');
INSERT INTO `certificate_study_programs` VALUES ('4', '1');
INSERT INTO `certificate_study_programs` VALUES ('4', '4');
INSERT INTO `certificate_study_programs` VALUES ('4', '5');
INSERT INTO `certificate_study_programs` VALUES ('4', '6');
INSERT INTO `certificate_study_programs` VALUES ('6', '4');
INSERT INTO `certificate_study_programs` VALUES ('6', '4');
INSERT INTO `certificate_study_programs` VALUES ('225', '1');
INSERT INTO `certificate_study_programs` VALUES ('226', '1');
INSERT INTO `certificate_study_programs` VALUES ('228', '1');
INSERT INTO `certificate_study_programs` VALUES ('227', '1');
INSERT INTO `certificate_study_programs` VALUES ('227', '2');
INSERT INTO `certificate_study_programs` VALUES ('5', '4');
INSERT INTO `certificate_study_programs` VALUES ('1', '1');
INSERT INTO `certificate_study_programs` VALUES ('230', '1');

-- ----------------------------
-- Table structure for certificate_types
-- ----------------------------
DROP TABLE IF EXISTS `certificate_types`;
CREATE TABLE `certificate_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `total` int(10) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of certificate_types
-- ----------------------------
INSERT INTO `certificate_types` VALUES ('1', 'SKA', '0', '2018-03-18 19:33:58', '2018-03-23 14:26:18');
INSERT INTO `certificate_types` VALUES ('2', 'SKT', '0', '2018-03-18 19:34:00', '2018-03-23 14:25:58');
INSERT INTO `certificate_types` VALUES ('3', 'ISO', '0', '2018-03-18 19:34:02', '2018-03-18 19:34:11');

-- ----------------------------
-- Table structure for cities
-- ----------------------------
DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities` (
  `id` int(4) unsigned NOT NULL,
  `province_id` int(2) unsigned NOT NULL,
  `name` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cities
-- ----------------------------
INSERT INTO `cities` VALUES ('1101', '11', 'KAB. ACEH SELATAN');
INSERT INTO `cities` VALUES ('1102', '11', 'KAB. ACEH TENGGARA');
INSERT INTO `cities` VALUES ('1103', '11', 'KAB. ACEH TIMUR');
INSERT INTO `cities` VALUES ('1104', '11', 'KAB. ACEH TENGAH');
INSERT INTO `cities` VALUES ('1105', '11', 'KAB. ACEH BARAT');
INSERT INTO `cities` VALUES ('1106', '11', 'KAB. ACEH BESAR');
INSERT INTO `cities` VALUES ('1107', '11', 'KAB. PIDIE');
INSERT INTO `cities` VALUES ('1108', '11', 'KAB. ACEH UTARA');
INSERT INTO `cities` VALUES ('1109', '11', 'KAB. SIMEULUE');
INSERT INTO `cities` VALUES ('1110', '11', 'KAB. ACEH SINGKIL');
INSERT INTO `cities` VALUES ('1111', '11', 'KAB. BIREUEN');
INSERT INTO `cities` VALUES ('1112', '11', 'KAB. ACEH BARAT DAYA');
INSERT INTO `cities` VALUES ('1113', '11', 'KAB. GAYO LUES');
INSERT INTO `cities` VALUES ('1114', '11', 'KAB. ACEH JAYA');
INSERT INTO `cities` VALUES ('1115', '11', 'KAB. NAGAN RAYA');
INSERT INTO `cities` VALUES ('1116', '11', 'KAB. ACEH TAMIANG');
INSERT INTO `cities` VALUES ('1117', '11', 'KAB. BENER MERIAH');
INSERT INTO `cities` VALUES ('1118', '11', 'KAB. PIDIE JAYA');
INSERT INTO `cities` VALUES ('1171', '11', 'KOTA BANDA ACEH');
INSERT INTO `cities` VALUES ('1172', '11', 'KOTA SABANG');
INSERT INTO `cities` VALUES ('1173', '11', 'KOTA LHOKSEUMAWE');
INSERT INTO `cities` VALUES ('1174', '11', 'KOTA LANGSA');
INSERT INTO `cities` VALUES ('1175', '11', 'KOTA SUBULUSSALAM');
INSERT INTO `cities` VALUES ('1201', '12', 'KAB. TAPANULI TENGAH');
INSERT INTO `cities` VALUES ('1202', '12', 'KAB. TAPANULI UTARA');
INSERT INTO `cities` VALUES ('1203', '12', 'KAB. TAPANULI SELATAN');
INSERT INTO `cities` VALUES ('1204', '12', 'KAB. NIAS');
INSERT INTO `cities` VALUES ('1205', '12', 'KAB. LANGKAT');
INSERT INTO `cities` VALUES ('1206', '12', 'KAB. KARO');
INSERT INTO `cities` VALUES ('1207', '12', 'KAB. DELI SERDANG');
INSERT INTO `cities` VALUES ('1208', '12', 'KAB. SIMALUNGUN');
INSERT INTO `cities` VALUES ('1209', '12', 'KAB. ASAHAN');
INSERT INTO `cities` VALUES ('1210', '12', 'KAB. LABUHANBATU');
INSERT INTO `cities` VALUES ('1211', '12', 'KAB. DAIRI');
INSERT INTO `cities` VALUES ('1212', '12', 'KAB. TOBA SAMOSIR');
INSERT INTO `cities` VALUES ('1213', '12', 'KAB. MANDAILING NATAL');
INSERT INTO `cities` VALUES ('1214', '12', 'KAB. NIAS SELATAN');
INSERT INTO `cities` VALUES ('1215', '12', 'KAB. PAKPAK BHARAT');
INSERT INTO `cities` VALUES ('1216', '12', 'KAB. HUMBANG HASUNDUTAN');
INSERT INTO `cities` VALUES ('1217', '12', 'KAB. SAMOSIR');
INSERT INTO `cities` VALUES ('1218', '12', 'KAB. SERDANG BEDAGAI');
INSERT INTO `cities` VALUES ('1219', '12', 'KAB. BATU BARA');
INSERT INTO `cities` VALUES ('1220', '12', 'KAB. PADANG LAWAS UTARA');
INSERT INTO `cities` VALUES ('1221', '12', 'KAB. PADANG LAWAS');
INSERT INTO `cities` VALUES ('1222', '12', 'KAB. LABUHANBATU SELATAN');
INSERT INTO `cities` VALUES ('1223', '12', 'KAB. LABUHANBATU UTARA');
INSERT INTO `cities` VALUES ('1224', '12', 'KAB. NIAS UTARA');
INSERT INTO `cities` VALUES ('1225', '12', 'KAB. NIAS BARAT');
INSERT INTO `cities` VALUES ('1271', '12', 'KOTA MEDAN');
INSERT INTO `cities` VALUES ('1272', '12', 'KOTA PEMATANG SIANTAR');
INSERT INTO `cities` VALUES ('1273', '12', 'KOTA SIBOLGA');
INSERT INTO `cities` VALUES ('1274', '12', 'KOTA TANJUNG BALAI');
INSERT INTO `cities` VALUES ('1275', '12', 'KOTA BINJAI');
INSERT INTO `cities` VALUES ('1276', '12', 'KOTA TEBING TINGGI');
INSERT INTO `cities` VALUES ('1277', '12', 'KOTA PADANGSIDIMPUAN');
INSERT INTO `cities` VALUES ('1278', '12', 'KOTA GUNUNGSITOLI');
INSERT INTO `cities` VALUES ('1301', '13', 'KAB. PESISIR SELATAN');
INSERT INTO `cities` VALUES ('1302', '13', 'KAB. SOLOK');
INSERT INTO `cities` VALUES ('1303', '13', 'KAB. SIJUNJUNG');
INSERT INTO `cities` VALUES ('1304', '13', 'KAB. TANAH DATAR');
INSERT INTO `cities` VALUES ('1305', '13', 'KAB. PADANG PARIAMAN');
INSERT INTO `cities` VALUES ('1306', '13', 'KAB. AGAM');
INSERT INTO `cities` VALUES ('1307', '13', 'KAB. LIMA PULUH KOTA');
INSERT INTO `cities` VALUES ('1308', '13', 'KAB. PASAMAN');
INSERT INTO `cities` VALUES ('1309', '13', 'KAB. KEPULAUAN MENTAWAI');
INSERT INTO `cities` VALUES ('1310', '13', 'KAB. DHARMASRAYA');
INSERT INTO `cities` VALUES ('1311', '13', 'KAB. SOLOK SELATAN');
INSERT INTO `cities` VALUES ('1312', '13', 'KAB. PASAMAN BARAT');
INSERT INTO `cities` VALUES ('1371', '13', 'KOTA PADANG');
INSERT INTO `cities` VALUES ('1372', '13', 'KOTA SOLOK');
INSERT INTO `cities` VALUES ('1373', '13', 'KOTA SAWAHLUNTO');
INSERT INTO `cities` VALUES ('1374', '13', 'KOTA PADANG PANJANG');
INSERT INTO `cities` VALUES ('1375', '13', 'KOTA BUKITTINGGI');
INSERT INTO `cities` VALUES ('1376', '13', 'KOTA PAYAKUMBUH');
INSERT INTO `cities` VALUES ('1377', '13', 'KOTA PARIAMAN');
INSERT INTO `cities` VALUES ('1401', '14', 'KAB. KAMPAR');
INSERT INTO `cities` VALUES ('1402', '14', 'KAB. INDRAGIRI HULU');
INSERT INTO `cities` VALUES ('1403', '14', 'KAB. BENGKALIS');
INSERT INTO `cities` VALUES ('1404', '14', 'KAB. INDRAGIRI HILIR');
INSERT INTO `cities` VALUES ('1405', '14', 'KAB. PELALAWAN');
INSERT INTO `cities` VALUES ('1406', '14', 'KAB. ROKAN HULU');
INSERT INTO `cities` VALUES ('1407', '14', 'KAB. ROKAN HILIR');
INSERT INTO `cities` VALUES ('1408', '14', 'KAB. SIAK');
INSERT INTO `cities` VALUES ('1409', '14', 'KAB. KUANTAN SINGINGI');
INSERT INTO `cities` VALUES ('1410', '14', 'KAB. KEPULAUAN MERANTI');
INSERT INTO `cities` VALUES ('1471', '14', 'KOTA PEKANBARU');
INSERT INTO `cities` VALUES ('1472', '14', 'KOTA DUMAI');
INSERT INTO `cities` VALUES ('1501', '15', 'KAB. KERINCI');
INSERT INTO `cities` VALUES ('1502', '15', 'KAB. MERANGIN');
INSERT INTO `cities` VALUES ('1503', '15', 'KAB. SAROLANGUN');
INSERT INTO `cities` VALUES ('1504', '15', 'KAB. BATANGHARI');
INSERT INTO `cities` VALUES ('1505', '15', 'KAB. MUARO JAMBI');
INSERT INTO `cities` VALUES ('1506', '15', 'KAB. TANJUNG JABUNG BARAT');
INSERT INTO `cities` VALUES ('1507', '15', 'KAB. TANJUNG JABUNG TIMUR');
INSERT INTO `cities` VALUES ('1508', '15', 'KAB. BUNGO');
INSERT INTO `cities` VALUES ('1509', '15', 'KAB. TEBO');
INSERT INTO `cities` VALUES ('1571', '15', 'KOTA JAMBI');
INSERT INTO `cities` VALUES ('1572', '15', 'KOTA SUNGAI PENUH');
INSERT INTO `cities` VALUES ('1601', '16', 'KAB. OGAN KOMERING ULU');
INSERT INTO `cities` VALUES ('1602', '16', 'KAB. OGAN KOMERING ILIR');
INSERT INTO `cities` VALUES ('1603', '16', 'KAB. MUARA ENIM');
INSERT INTO `cities` VALUES ('1604', '16', 'KAB. LAHAT');
INSERT INTO `cities` VALUES ('1605', '16', 'KAB. MUSI RAWAS');
INSERT INTO `cities` VALUES ('1606', '16', 'KAB. MUSI BANYUASIN');
INSERT INTO `cities` VALUES ('1607', '16', 'KAB. BANYUASIN');
INSERT INTO `cities` VALUES ('1608', '16', 'KAB. OGAN KOMERING ULU TIMUR');
INSERT INTO `cities` VALUES ('1609', '16', 'KAB. OGAN KOMERING ULU SELATAN');
INSERT INTO `cities` VALUES ('1610', '16', 'KAB. OGAN ILIR');
INSERT INTO `cities` VALUES ('1611', '16', 'KAB. EMPAT LAWANG');
INSERT INTO `cities` VALUES ('1612', '16', 'KAB. PENUKAL ABAB LEMATANG ILIR');
INSERT INTO `cities` VALUES ('1613', '16', 'KAB. MUSI RAWAS UTARA');
INSERT INTO `cities` VALUES ('1671', '16', 'KOTA PALEMBANG');
INSERT INTO `cities` VALUES ('1672', '16', 'KOTA PAGAR ALAM');
INSERT INTO `cities` VALUES ('1673', '16', 'KOTA LUBUK LINGGAU');
INSERT INTO `cities` VALUES ('1674', '16', 'KOTA PRABUMULIH');
INSERT INTO `cities` VALUES ('1701', '17', 'KAB. BENGKULU SELATAN');
INSERT INTO `cities` VALUES ('1702', '17', 'KAB. REJANG LEBONG');
INSERT INTO `cities` VALUES ('1703', '17', 'KAB. BENGKULU UTARA');
INSERT INTO `cities` VALUES ('1704', '17', 'KAB. KAUR');
INSERT INTO `cities` VALUES ('1705', '17', 'KAB. SELUMA');
INSERT INTO `cities` VALUES ('1706', '17', 'KAB. MUKO MUKO');
INSERT INTO `cities` VALUES ('1707', '17', 'KAB. LEBONG');
INSERT INTO `cities` VALUES ('1708', '17', 'KAB. KEPAHIANG');
INSERT INTO `cities` VALUES ('1709', '17', 'KAB. BENGKULU TENGAH');
INSERT INTO `cities` VALUES ('1771', '17', 'KOTA BENGKULU');
INSERT INTO `cities` VALUES ('1801', '18', 'KAB. LAMPUNG SELATAN');
INSERT INTO `cities` VALUES ('1802', '18', 'KAB. LAMPUNG TENGAH');
INSERT INTO `cities` VALUES ('1803', '18', 'KAB. LAMPUNG UTARA');
INSERT INTO `cities` VALUES ('1804', '18', 'KAB. LAMPUNG BARAT');
INSERT INTO `cities` VALUES ('1805', '18', 'KAB. TULANG BAWANG');
INSERT INTO `cities` VALUES ('1806', '18', 'KAB. TANGGAMUS');
INSERT INTO `cities` VALUES ('1807', '18', 'KAB. LAMPUNG TIMUR');
INSERT INTO `cities` VALUES ('1808', '18', 'KAB. WAY KANAN');
INSERT INTO `cities` VALUES ('1809', '18', 'KAB. PESAWARAN');
INSERT INTO `cities` VALUES ('1810', '18', 'KAB. PRINGSEWU');
INSERT INTO `cities` VALUES ('1811', '18', 'KAB. MESUJI');
INSERT INTO `cities` VALUES ('1812', '18', 'KAB. TULANG BAWANG BARAT');
INSERT INTO `cities` VALUES ('1813', '18', 'KAB. PESISIR BARAT');
INSERT INTO `cities` VALUES ('1871', '18', 'KOTA BANDAR LAMPUNG');
INSERT INTO `cities` VALUES ('1872', '18', 'KOTA METRO');
INSERT INTO `cities` VALUES ('1901', '19', 'KAB. BANGKA');
INSERT INTO `cities` VALUES ('1902', '19', 'KAB. BELITUNG');
INSERT INTO `cities` VALUES ('1903', '19', 'KAB. BANGKA SELATAN');
INSERT INTO `cities` VALUES ('1904', '19', 'KAB. BANGKA TENGAH');
INSERT INTO `cities` VALUES ('1905', '19', 'KAB. BANGKA BARAT');
INSERT INTO `cities` VALUES ('1906', '19', 'KAB. BELITUNG TIMUR');
INSERT INTO `cities` VALUES ('1971', '19', 'KOTA PANGKAL PINANG');
INSERT INTO `cities` VALUES ('2101', '21', 'KAB. BINTAN');
INSERT INTO `cities` VALUES ('2102', '21', 'KAB. KARIMUN');
INSERT INTO `cities` VALUES ('2103', '21', 'KAB. NATUNA');
INSERT INTO `cities` VALUES ('2104', '21', 'KAB. LINGGA');
INSERT INTO `cities` VALUES ('2105', '21', 'KAB. KEPULAUAN ANAMBAS');
INSERT INTO `cities` VALUES ('2171', '21', 'KOTA BATAM');
INSERT INTO `cities` VALUES ('2172', '21', 'KOTA TANJUNG PINANG');
INSERT INTO `cities` VALUES ('3101', '31', 'KAB. ADM. KEP. SERIBU');
INSERT INTO `cities` VALUES ('3171', '31', 'KOTA ADM. JAKARTA PUSAT');
INSERT INTO `cities` VALUES ('3172', '31', 'KOTA ADM. JAKARTA UTARA');
INSERT INTO `cities` VALUES ('3173', '31', 'KOTA ADM. JAKARTA BARAT');
INSERT INTO `cities` VALUES ('3174', '31', 'KOTA ADM. JAKARTA SELATAN');
INSERT INTO `cities` VALUES ('3175', '31', 'KOTA ADM. JAKARTA TIMUR');
INSERT INTO `cities` VALUES ('3201', '32', 'KAB. BOGOR');
INSERT INTO `cities` VALUES ('3202', '32', 'KAB. SUKABUMI');
INSERT INTO `cities` VALUES ('3203', '32', 'KAB. CIANJUR');
INSERT INTO `cities` VALUES ('3204', '32', 'KAB. BANDUNG');
INSERT INTO `cities` VALUES ('3205', '32', 'KAB. GARUT');
INSERT INTO `cities` VALUES ('3206', '32', 'KAB. TASIKMALAYA');
INSERT INTO `cities` VALUES ('3207', '32', 'KAB. CIAMIS');
INSERT INTO `cities` VALUES ('3208', '32', 'KAB. KUNINGAN');
INSERT INTO `cities` VALUES ('3209', '32', 'KAB. CIREBON');
INSERT INTO `cities` VALUES ('3210', '32', 'KAB. MAJALENGKA');
INSERT INTO `cities` VALUES ('3211', '32', 'KAB. SUMEDANG');
INSERT INTO `cities` VALUES ('3212', '32', 'KAB. INDRAMAYU');
INSERT INTO `cities` VALUES ('3213', '32', 'KAB. SUBANG');
INSERT INTO `cities` VALUES ('3214', '32', 'KAB. PURWAKARTA');
INSERT INTO `cities` VALUES ('3215', '32', 'KAB. KARAWANG');
INSERT INTO `cities` VALUES ('3216', '32', 'KAB. BEKASI');
INSERT INTO `cities` VALUES ('3217', '32', 'KAB. BANDUNG BARAT');
INSERT INTO `cities` VALUES ('3218', '32', 'KAB. PANGANDARAN');
INSERT INTO `cities` VALUES ('3271', '32', 'KOTA BOGOR');
INSERT INTO `cities` VALUES ('3272', '32', 'KOTA SUKABUMI');
INSERT INTO `cities` VALUES ('3273', '32', 'KOTA BANDUNG');
INSERT INTO `cities` VALUES ('3274', '32', 'KOTA CIREBON');
INSERT INTO `cities` VALUES ('3275', '32', 'KOTA BEKASI');
INSERT INTO `cities` VALUES ('3276', '32', 'KOTA DEPOK');
INSERT INTO `cities` VALUES ('3277', '32', 'KOTA CIMAHI');
INSERT INTO `cities` VALUES ('3278', '32', 'KOTA TASIKMALAYA');
INSERT INTO `cities` VALUES ('3279', '32', 'KOTA BANJAR');
INSERT INTO `cities` VALUES ('3301', '33', 'KAB. CILACAP');
INSERT INTO `cities` VALUES ('3302', '33', 'KAB. BANYUMAS');
INSERT INTO `cities` VALUES ('3303', '33', 'KAB. PURBALINGGA');
INSERT INTO `cities` VALUES ('3304', '33', 'KAB. BANJARNEGARA');
INSERT INTO `cities` VALUES ('3305', '33', 'KAB. KEBUMEN');
INSERT INTO `cities` VALUES ('3306', '33', 'KAB. PURWOREJO');
INSERT INTO `cities` VALUES ('3307', '33', 'KAB. WONOSOBO');
INSERT INTO `cities` VALUES ('3308', '33', 'KAB. MAGELANG');
INSERT INTO `cities` VALUES ('3309', '33', 'KAB. BOYOLALI');
INSERT INTO `cities` VALUES ('3310', '33', 'KAB. KLATEN');
INSERT INTO `cities` VALUES ('3311', '33', 'KAB. SUKOHARJO');
INSERT INTO `cities` VALUES ('3312', '33', 'KAB. WONOGIRI');
INSERT INTO `cities` VALUES ('3313', '33', 'KAB. KARANGANYAR');
INSERT INTO `cities` VALUES ('3314', '33', 'KAB. SRAGEN');
INSERT INTO `cities` VALUES ('3315', '33', 'KAB. GROBOGAN');
INSERT INTO `cities` VALUES ('3316', '33', 'KAB. BLORA');
INSERT INTO `cities` VALUES ('3317', '33', 'KAB. REMBANG');
INSERT INTO `cities` VALUES ('3318', '33', 'KAB. PATI');
INSERT INTO `cities` VALUES ('3319', '33', 'KAB. KUDUS');
INSERT INTO `cities` VALUES ('3320', '33', 'KAB. JEPARA');
INSERT INTO `cities` VALUES ('3321', '33', 'KAB. DEMAK');
INSERT INTO `cities` VALUES ('3322', '33', 'KAB. SEMARANG');
INSERT INTO `cities` VALUES ('3323', '33', 'KAB. TEMANGGUNG');
INSERT INTO `cities` VALUES ('3324', '33', 'KAB. KENDAL');
INSERT INTO `cities` VALUES ('3325', '33', 'KAB. BATANG');
INSERT INTO `cities` VALUES ('3326', '33', 'KAB. PEKALONGAN');
INSERT INTO `cities` VALUES ('3327', '33', 'KAB. PEMALANG');
INSERT INTO `cities` VALUES ('3328', '33', 'KAB. TEGAL');
INSERT INTO `cities` VALUES ('3329', '33', 'KAB. BREBES');
INSERT INTO `cities` VALUES ('3371', '33', 'KOTA MAGELANG');
INSERT INTO `cities` VALUES ('3372', '33', 'KOTA SURAKARTA');
INSERT INTO `cities` VALUES ('3373', '33', 'KOTA SALATIGA');
INSERT INTO `cities` VALUES ('3374', '33', 'KOTA SEMARANG');
INSERT INTO `cities` VALUES ('3375', '33', 'KOTA PEKALONGAN');
INSERT INTO `cities` VALUES ('3376', '33', 'KOTA TEGAL');
INSERT INTO `cities` VALUES ('3401', '34', 'KAB. KULON PROGO');
INSERT INTO `cities` VALUES ('3402', '34', 'KAB. BANTUL');
INSERT INTO `cities` VALUES ('3403', '34', 'KAB. GUNUNG KIDUL');
INSERT INTO `cities` VALUES ('3404', '34', 'KAB. SLEMAN');
INSERT INTO `cities` VALUES ('3471', '34', 'KOTA YOGYAKARTA');
INSERT INTO `cities` VALUES ('3501', '35', 'KAB. PACITAN');
INSERT INTO `cities` VALUES ('3502', '35', 'KAB. PONOROGO');
INSERT INTO `cities` VALUES ('3503', '35', 'KAB. TRENGGALEK');
INSERT INTO `cities` VALUES ('3504', '35', 'KAB. TULUNGAGUNG');
INSERT INTO `cities` VALUES ('3505', '35', 'KAB. BLITAR');
INSERT INTO `cities` VALUES ('3506', '35', 'KAB. KEDIRI');
INSERT INTO `cities` VALUES ('3507', '35', 'KAB. MALANG');
INSERT INTO `cities` VALUES ('3508', '35', 'KAB. LUMAJANG');
INSERT INTO `cities` VALUES ('3509', '35', 'KAB. JEMBER');
INSERT INTO `cities` VALUES ('3510', '35', 'KAB. BANYUWANGI');
INSERT INTO `cities` VALUES ('3511', '35', 'KAB. BONDOWOSO');
INSERT INTO `cities` VALUES ('3512', '35', 'KAB. SITUBONDO');
INSERT INTO `cities` VALUES ('3513', '35', 'KAB. PROBOLINGGO');
INSERT INTO `cities` VALUES ('3514', '35', 'KAB. PASURUAN');
INSERT INTO `cities` VALUES ('3515', '35', 'KAB. SIDOARJO');
INSERT INTO `cities` VALUES ('3516', '35', 'KAB. MOJOKERTO');
INSERT INTO `cities` VALUES ('3517', '35', 'KAB. JOMBANG');
INSERT INTO `cities` VALUES ('3518', '35', 'KAB. NGANJUK');
INSERT INTO `cities` VALUES ('3519', '35', 'KAB. MADIUN');
INSERT INTO `cities` VALUES ('3520', '35', 'KAB. MAGETAN');
INSERT INTO `cities` VALUES ('3521', '35', 'KAB. NGAWI');
INSERT INTO `cities` VALUES ('3522', '35', 'KAB. BOJONEGORO');
INSERT INTO `cities` VALUES ('3523', '35', 'KAB. TUBAN');
INSERT INTO `cities` VALUES ('3524', '35', 'KAB. LAMONGAN');
INSERT INTO `cities` VALUES ('3525', '35', 'KAB. GRESIK');
INSERT INTO `cities` VALUES ('3526', '35', 'KAB. BANGKALAN');
INSERT INTO `cities` VALUES ('3527', '35', 'KAB. SAMPANG');
INSERT INTO `cities` VALUES ('3528', '35', 'KAB. PAMEKASAN');
INSERT INTO `cities` VALUES ('3529', '35', 'KAB. SUMENEP');
INSERT INTO `cities` VALUES ('3571', '35', 'KOTA KEDIRI');
INSERT INTO `cities` VALUES ('3572', '35', 'KOTA BLITAR');
INSERT INTO `cities` VALUES ('3573', '35', 'KOTA MALANG');
INSERT INTO `cities` VALUES ('3574', '35', 'KOTA PROBOLINGGO');
INSERT INTO `cities` VALUES ('3575', '35', 'KOTA PASURUAN');
INSERT INTO `cities` VALUES ('3576', '35', 'KOTA MOJOKERTO');
INSERT INTO `cities` VALUES ('3577', '35', 'KOTA MADIUN');
INSERT INTO `cities` VALUES ('3578', '35', 'KOTA SURABAYA');
INSERT INTO `cities` VALUES ('3579', '35', 'KOTA BATU');
INSERT INTO `cities` VALUES ('3601', '36', 'KAB. PANDEGLANG');
INSERT INTO `cities` VALUES ('3602', '36', 'KAB. LEBAK');
INSERT INTO `cities` VALUES ('3603', '36', 'KAB. TANGERANG');
INSERT INTO `cities` VALUES ('3604', '36', 'KAB. SERANG');
INSERT INTO `cities` VALUES ('3671', '36', 'KOTA TANGERANG');
INSERT INTO `cities` VALUES ('3672', '36', 'KOTA CILEGON');
INSERT INTO `cities` VALUES ('3673', '36', 'KOTA SERANG');
INSERT INTO `cities` VALUES ('3674', '36', 'KOTA TANGERANG SELATAN');
INSERT INTO `cities` VALUES ('5101', '51', 'KAB. JEMBRANA');
INSERT INTO `cities` VALUES ('5102', '51', 'KAB. TABANAN');
INSERT INTO `cities` VALUES ('5103', '51', 'KAB. BADUNG');
INSERT INTO `cities` VALUES ('5104', '51', 'KAB. GIANYAR');
INSERT INTO `cities` VALUES ('5105', '51', 'KAB. KLUNGKUNG');
INSERT INTO `cities` VALUES ('5106', '51', 'KAB. BANGLI');
INSERT INTO `cities` VALUES ('5107', '51', 'KAB. KARANGASEM');
INSERT INTO `cities` VALUES ('5108', '51', 'KAB. BULELENG');
INSERT INTO `cities` VALUES ('5171', '51', 'KOTA DENPASAR');
INSERT INTO `cities` VALUES ('5201', '52', 'KAB. LOMBOK BARAT');
INSERT INTO `cities` VALUES ('5202', '52', 'KAB. LOMBOK TENGAH');
INSERT INTO `cities` VALUES ('5203', '52', 'KAB. LOMBOK TIMUR');
INSERT INTO `cities` VALUES ('5204', '52', 'KAB. SUMBAWA');
INSERT INTO `cities` VALUES ('5205', '52', 'KAB. DOMPU');
INSERT INTO `cities` VALUES ('5206', '52', 'KAB. BIMA');
INSERT INTO `cities` VALUES ('5207', '52', 'KAB. SUMBAWA BARAT');
INSERT INTO `cities` VALUES ('5208', '52', 'KAB. LOMBOK UTARA');
INSERT INTO `cities` VALUES ('5271', '52', 'KOTA MATARAM');
INSERT INTO `cities` VALUES ('5272', '52', 'KOTA BIMA');
INSERT INTO `cities` VALUES ('5301', '53', 'KAB. KUPANG');
INSERT INTO `cities` VALUES ('5302', '53', 'KAB TIMOR TENGAH SELATAN');
INSERT INTO `cities` VALUES ('5303', '53', 'KAB. TIMOR TENGAH UTARA');
INSERT INTO `cities` VALUES ('5304', '53', 'KAB. BELU');
INSERT INTO `cities` VALUES ('5305', '53', 'KAB. ALOR');
INSERT INTO `cities` VALUES ('5306', '53', 'KAB. FLORES TIMUR');
INSERT INTO `cities` VALUES ('5307', '53', 'KAB. SIKKA');
INSERT INTO `cities` VALUES ('5308', '53', 'KAB. ENDE');
INSERT INTO `cities` VALUES ('5309', '53', 'KAB. NGADA');
INSERT INTO `cities` VALUES ('5310', '53', 'KAB. MANGGARAI');
INSERT INTO `cities` VALUES ('5311', '53', 'KAB. SUMBA TIMUR');
INSERT INTO `cities` VALUES ('5312', '53', 'KAB. SUMBA BARAT');
INSERT INTO `cities` VALUES ('5313', '53', 'KAB. LEMBATA');
INSERT INTO `cities` VALUES ('5314', '53', 'KAB. ROTE NDAO');
INSERT INTO `cities` VALUES ('5315', '53', 'KAB. MANGGARAI BARAT');
INSERT INTO `cities` VALUES ('5316', '53', 'KAB. NAGEKEO');
INSERT INTO `cities` VALUES ('5317', '53', 'KAB. SUMBA TENGAH');
INSERT INTO `cities` VALUES ('5318', '53', 'KAB. SUMBA BARAT DAYA');
INSERT INTO `cities` VALUES ('5319', '53', 'KAB. MANGGARAI TIMUR');
INSERT INTO `cities` VALUES ('5320', '53', 'KAB. SABU RAIJUA');
INSERT INTO `cities` VALUES ('5321', '53', 'KAB. MALAKA');
INSERT INTO `cities` VALUES ('5371', '53', 'KOTA KUPANG');
INSERT INTO `cities` VALUES ('6101', '61', 'KAB. SAMBAS');
INSERT INTO `cities` VALUES ('6102', '61', 'KAB. MEMPAWAH');
INSERT INTO `cities` VALUES ('6103', '61', 'KAB. SANGGAU');
INSERT INTO `cities` VALUES ('6104', '61', 'KAB. KETAPANG');
INSERT INTO `cities` VALUES ('6105', '61', 'KAB. SINTANG');
INSERT INTO `cities` VALUES ('6106', '61', 'KAB. KAPUAS HULU');
INSERT INTO `cities` VALUES ('6107', '61', 'KAB. BENGKAYANG');
INSERT INTO `cities` VALUES ('6108', '61', 'KAB. LANDAK');
INSERT INTO `cities` VALUES ('6109', '61', 'KAB. SEKADAU');
INSERT INTO `cities` VALUES ('6110', '61', 'KAB. MELAWI');
INSERT INTO `cities` VALUES ('6111', '61', 'KAB. KAYONG UTARA');
INSERT INTO `cities` VALUES ('6112', '61', 'KAB. KUBU RAYA');
INSERT INTO `cities` VALUES ('6171', '61', 'KOTA PONTIANAK');
INSERT INTO `cities` VALUES ('6172', '61', 'KOTA SINGKAWANG');
INSERT INTO `cities` VALUES ('6201', '62', 'KAB. KOTAWARINGIN BARAT');
INSERT INTO `cities` VALUES ('6202', '62', 'KAB. KOTAWARINGIN TIMUR');
INSERT INTO `cities` VALUES ('6203', '62', 'KAB. KAPUAS');
INSERT INTO `cities` VALUES ('6204', '62', 'KAB. BARITO SELATAN');
INSERT INTO `cities` VALUES ('6205', '62', 'KAB. BARITO UTARA');
INSERT INTO `cities` VALUES ('6206', '62', 'KAB. KATINGAN');
INSERT INTO `cities` VALUES ('6207', '62', 'KAB. SERUYAN');
INSERT INTO `cities` VALUES ('6208', '62', 'KAB. SUKAMARA');
INSERT INTO `cities` VALUES ('6209', '62', 'KAB. LAMANDAU');
INSERT INTO `cities` VALUES ('6210', '62', 'KAB. GUNUNG MAS');
INSERT INTO `cities` VALUES ('6211', '62', 'KAB. PULANG PISAU');
INSERT INTO `cities` VALUES ('6212', '62', 'KAB. MURUNG RAYA');
INSERT INTO `cities` VALUES ('6213', '62', 'KAB. BARITO TIMUR');
INSERT INTO `cities` VALUES ('6271', '62', 'KOTA PALANGKARAYA');
INSERT INTO `cities` VALUES ('6301', '63', 'KAB. TANAH LAUT');
INSERT INTO `cities` VALUES ('6302', '63', 'KAB. KOTABARU');
INSERT INTO `cities` VALUES ('6303', '63', 'KAB. BANJAR');
INSERT INTO `cities` VALUES ('6304', '63', 'KAB. BARITO KUALA');
INSERT INTO `cities` VALUES ('6305', '63', 'KAB. TAPIN');
INSERT INTO `cities` VALUES ('6306', '63', 'KAB. HULU SUNGAI SELATAN');
INSERT INTO `cities` VALUES ('6307', '63', 'KAB. HULU SUNGAI TENGAH');
INSERT INTO `cities` VALUES ('6308', '63', 'KAB. HULU SUNGAI UTARA');
INSERT INTO `cities` VALUES ('6309', '63', 'KAB. TABALONG');
INSERT INTO `cities` VALUES ('6310', '63', 'KAB. TANAH BUMBU');
INSERT INTO `cities` VALUES ('6311', '63', 'KAB. BALANGAN');
INSERT INTO `cities` VALUES ('6371', '63', 'KOTA BANJARMASIN');
INSERT INTO `cities` VALUES ('6372', '63', 'KOTA BANJARBARU');
INSERT INTO `cities` VALUES ('6401', '64', 'KAB. PASER');
INSERT INTO `cities` VALUES ('6402', '64', 'KAB. KUTAI KARTANEGARA');
INSERT INTO `cities` VALUES ('6403', '64', 'KAB. BERAU');
INSERT INTO `cities` VALUES ('6407', '64', 'KAB. KUTAI BARAT');
INSERT INTO `cities` VALUES ('6408', '64', 'KAB. KUTAI TIMUR');
INSERT INTO `cities` VALUES ('6409', '64', 'KAB. PENAJAM PASER UTARA');
INSERT INTO `cities` VALUES ('6411', '64', 'KAB. MAHAKAM ULU');
INSERT INTO `cities` VALUES ('6471', '64', 'KOTA BALIKPAPAN');
INSERT INTO `cities` VALUES ('6472', '64', 'KOTA SAMARINDA');
INSERT INTO `cities` VALUES ('6474', '64', 'KOTA BONTANG');
INSERT INTO `cities` VALUES ('6501', '65', 'KAB. BULUNGAN');
INSERT INTO `cities` VALUES ('6502', '65', 'KAB. MALINAU');
INSERT INTO `cities` VALUES ('6503', '65', 'KAB. NUNUKAN');
INSERT INTO `cities` VALUES ('6504', '65', 'KAB. TANA TIDUNG');
INSERT INTO `cities` VALUES ('6571', '65', 'KOTA TARAKAN');
INSERT INTO `cities` VALUES ('7101', '71', 'KAB. BOLAANG MONGONDOW');
INSERT INTO `cities` VALUES ('7102', '71', 'KAB. MINAHASA');
INSERT INTO `cities` VALUES ('7103', '71', 'KAB. KEPULAUAN SANGIHE');
INSERT INTO `cities` VALUES ('7104', '71', 'KAB. KEPULAUAN TALAUD');
INSERT INTO `cities` VALUES ('7105', '71', 'KAB. MINAHASA SELATAN');
INSERT INTO `cities` VALUES ('7106', '71', 'KAB. MINAHASA UTARA');
INSERT INTO `cities` VALUES ('7107', '71', 'KAB. MINAHASA TENGGARA');
INSERT INTO `cities` VALUES ('7108', '71', 'KAB. BOLAANG MONGONDOW UTARA');
INSERT INTO `cities` VALUES ('7109', '71', 'KAB. KEP. SIAU TAGULANDANG BIARO');
INSERT INTO `cities` VALUES ('7110', '71', 'KAB. BOLAANG MONGONDOW TIMUR');
INSERT INTO `cities` VALUES ('7111', '71', 'KAB. BOLAANG MONGONDOW SELATAN');
INSERT INTO `cities` VALUES ('7171', '71', 'KOTA MANADO');
INSERT INTO `cities` VALUES ('7172', '71', 'KOTA BITUNG');
INSERT INTO `cities` VALUES ('7173', '71', 'KOTA TOMOHON');
INSERT INTO `cities` VALUES ('7174', '71', 'KOTA KOTAMOBAGU');
INSERT INTO `cities` VALUES ('7201', '72', 'KAB. BANGGAI');
INSERT INTO `cities` VALUES ('7202', '72', 'KAB. POSO');
INSERT INTO `cities` VALUES ('7203', '72', 'KAB. DONGGALA');
INSERT INTO `cities` VALUES ('7204', '72', 'KAB. TOLI TOLI');
INSERT INTO `cities` VALUES ('7205', '72', 'KAB. BUOL');
INSERT INTO `cities` VALUES ('7206', '72', 'KAB. MOROWALI');
INSERT INTO `cities` VALUES ('7207', '72', 'KAB. BANGGAI KEPULAUAN');
INSERT INTO `cities` VALUES ('7208', '72', 'KAB. PARIGI MOUTONG');
INSERT INTO `cities` VALUES ('7209', '72', 'KAB. TOJO UNA UNA');
INSERT INTO `cities` VALUES ('7210', '72', 'KAB. SIGI');
INSERT INTO `cities` VALUES ('7211', '72', 'KAB. BANGGAI LAUT');
INSERT INTO `cities` VALUES ('7212', '72', 'KAB. MOROWALI UTARA');
INSERT INTO `cities` VALUES ('7271', '72', 'KOTA PALU');
INSERT INTO `cities` VALUES ('7301', '73', 'KAB. KEPULAUAN SELAYAR');
INSERT INTO `cities` VALUES ('7302', '73', 'KAB. BULUKUMBA');
INSERT INTO `cities` VALUES ('7303', '73', 'KAB. BANTAENG');
INSERT INTO `cities` VALUES ('7304', '73', 'KAB. JENEPONTO');
INSERT INTO `cities` VALUES ('7305', '73', 'KAB. TAKALAR');
INSERT INTO `cities` VALUES ('7306', '73', 'KAB. GOWA');
INSERT INTO `cities` VALUES ('7307', '73', 'KAB. SINJAI');
INSERT INTO `cities` VALUES ('7308', '73', 'KAB. BONE');
INSERT INTO `cities` VALUES ('7309', '73', 'KAB. MAROS');
INSERT INTO `cities` VALUES ('7310', '73', 'KAB. PANGKAJENE KEPULAUAN');
INSERT INTO `cities` VALUES ('7311', '73', 'KAB. BARRU');
INSERT INTO `cities` VALUES ('7312', '73', 'KAB. SOPPENG');
INSERT INTO `cities` VALUES ('7313', '73', 'KAB. WAJO');
INSERT INTO `cities` VALUES ('7314', '73', 'KAB. SIDENRENG RAPPANG');
INSERT INTO `cities` VALUES ('7315', '73', 'KAB. PINRANG');
INSERT INTO `cities` VALUES ('7316', '73', 'KAB. ENREKANG');
INSERT INTO `cities` VALUES ('7317', '73', 'KAB. LUWU');
INSERT INTO `cities` VALUES ('7318', '73', 'KAB. TANA TORAJA');
INSERT INTO `cities` VALUES ('7322', '73', 'KAB. LUWU UTARA');
INSERT INTO `cities` VALUES ('7324', '73', 'KAB. LUWU TIMUR');
INSERT INTO `cities` VALUES ('7326', '73', 'KAB. TORAJA UTARA');
INSERT INTO `cities` VALUES ('7371', '73', 'KOTA MAKASSAR');
INSERT INTO `cities` VALUES ('7372', '73', 'KOTA PARE PARE');
INSERT INTO `cities` VALUES ('7373', '73', 'KOTA PALOPO');
INSERT INTO `cities` VALUES ('7401', '74', 'KAB. KOLAKA');
INSERT INTO `cities` VALUES ('7402', '74', 'KAB. KONAWE');
INSERT INTO `cities` VALUES ('7403', '74', 'KAB. MUNA');
INSERT INTO `cities` VALUES ('7404', '74', 'KAB. BUTON');
INSERT INTO `cities` VALUES ('7405', '74', 'KAB. KONAWE SELATAN');
INSERT INTO `cities` VALUES ('7406', '74', 'KAB. BOMBANA');
INSERT INTO `cities` VALUES ('7407', '74', 'KAB. WAKATOBI');
INSERT INTO `cities` VALUES ('7408', '74', 'KAB. KOLAKA UTARA');
INSERT INTO `cities` VALUES ('7409', '74', 'KAB. KONAWE UTARA');
INSERT INTO `cities` VALUES ('7410', '74', 'KAB. BUTON UTARA');
INSERT INTO `cities` VALUES ('7411', '74', 'KAB. KOLAKA TIMUR');
INSERT INTO `cities` VALUES ('7412', '74', 'KAB. KONAWE KEPULAUAN');
INSERT INTO `cities` VALUES ('7413', '74', 'KAB. MUNA BARAT');
INSERT INTO `cities` VALUES ('7414', '74', 'KAB. BUTON TENGAH');
INSERT INTO `cities` VALUES ('7415', '74', 'KAB. BUTON SELATAN');
INSERT INTO `cities` VALUES ('7471', '74', 'KOTA KENDARI');
INSERT INTO `cities` VALUES ('7472', '74', 'KOTA BAU BAU');
INSERT INTO `cities` VALUES ('7501', '75', 'KAB. GORONTALO');
INSERT INTO `cities` VALUES ('7502', '75', 'KAB. BOALEMO');
INSERT INTO `cities` VALUES ('7503', '75', 'KAB. BONE BOLANGO');
INSERT INTO `cities` VALUES ('7504', '75', 'KAB. PAHUWATO');
INSERT INTO `cities` VALUES ('7505', '75', 'KAB. GORONTALO UTARA');
INSERT INTO `cities` VALUES ('7571', '75', 'KOTA GORONTALO');
INSERT INTO `cities` VALUES ('7601', '76', 'KAB. MAMUJU UTARA');
INSERT INTO `cities` VALUES ('7602', '76', 'KAB. MAMUJU');
INSERT INTO `cities` VALUES ('7603', '76', 'KAB. MAMASA');
INSERT INTO `cities` VALUES ('7604', '76', 'KAB. POLEWALI MANDAR');
INSERT INTO `cities` VALUES ('7605', '76', 'KAB. MAJENE');
INSERT INTO `cities` VALUES ('7606', '76', 'KAB. MAMUJU TENGAH');
INSERT INTO `cities` VALUES ('8101', '81', 'KAB. MALUKU TENGAH');
INSERT INTO `cities` VALUES ('8102', '81', 'KAB. MALUKU TENGGARA');
INSERT INTO `cities` VALUES ('8103', '81', 'KAB MALUKU TENGGARA BARAT');
INSERT INTO `cities` VALUES ('8104', '81', 'KAB. BURU');
INSERT INTO `cities` VALUES ('8105', '81', 'KAB. SERAM BAGIAN TIMUR');
INSERT INTO `cities` VALUES ('8106', '81', 'KAB. SERAM BAGIAN BARAT');
INSERT INTO `cities` VALUES ('8107', '81', 'KAB. KEPULAUAN ARU');
INSERT INTO `cities` VALUES ('8108', '81', 'KAB. MALUKU BARAT DAYA');
INSERT INTO `cities` VALUES ('8109', '81', 'KAB. BURU SELATAN');
INSERT INTO `cities` VALUES ('8171', '81', 'KOTA AMBON');
INSERT INTO `cities` VALUES ('8172', '81', 'KOTA TUAL');
INSERT INTO `cities` VALUES ('8201', '82', 'KAB. HALMAHERA BARAT');
INSERT INTO `cities` VALUES ('8202', '82', 'KAB. HALMAHERA TENGAH');
INSERT INTO `cities` VALUES ('8203', '82', 'KAB. HALMAHERA UTARA');
INSERT INTO `cities` VALUES ('8204', '82', 'KAB. HALMAHERA SELATAN');
INSERT INTO `cities` VALUES ('8205', '82', 'KAB. KEPULAUAN SULA');
INSERT INTO `cities` VALUES ('8206', '82', 'KAB. HALMAHERA TIMUR');
INSERT INTO `cities` VALUES ('8207', '82', 'KAB. PULAU MOROTAI');
INSERT INTO `cities` VALUES ('8208', '82', 'KAB. PULAU TALIABU');
INSERT INTO `cities` VALUES ('8271', '82', 'KOTA TERNATE');
INSERT INTO `cities` VALUES ('8272', '82', 'KOTA TIDORE KEPULAUAN');
INSERT INTO `cities` VALUES ('9101', '91', 'KAB. MERAUKE');
INSERT INTO `cities` VALUES ('9102', '91', 'KAB. JAYAWIJAYA');
INSERT INTO `cities` VALUES ('9103', '91', 'KAB. JAYAPURA');
INSERT INTO `cities` VALUES ('9104', '91', 'KAB. NABIRE');
INSERT INTO `cities` VALUES ('9105', '91', 'KAB. KEPULAUAN YAPEN');
INSERT INTO `cities` VALUES ('9106', '91', 'KAB. BIAK NUMFOR');
INSERT INTO `cities` VALUES ('9107', '91', 'KAB. PUNCAK JAYA');
INSERT INTO `cities` VALUES ('9108', '91', 'KAB. PANIAI');
INSERT INTO `cities` VALUES ('9109', '91', 'KAB. MIMIKA');
INSERT INTO `cities` VALUES ('9110', '91', 'KAB. SARMI');
INSERT INTO `cities` VALUES ('9111', '91', 'KAB. KEEROM');
INSERT INTO `cities` VALUES ('9112', '91', 'KAB PEGUNUNGAN BINTANG');
INSERT INTO `cities` VALUES ('9113', '91', 'KAB. YAHUKIMO');
INSERT INTO `cities` VALUES ('9114', '91', 'KAB. TOLIKARA');
INSERT INTO `cities` VALUES ('9115', '91', 'KAB. WAROPEN');
INSERT INTO `cities` VALUES ('9116', '91', 'KAB. BOVEN DIGOEL');
INSERT INTO `cities` VALUES ('9117', '91', 'KAB. MAPPI');
INSERT INTO `cities` VALUES ('9118', '91', 'KAB. ASMAT');
INSERT INTO `cities` VALUES ('9119', '91', 'KAB. SUPIORI');
INSERT INTO `cities` VALUES ('9120', '91', 'KAB. MAMBERAMO RAYA');
INSERT INTO `cities` VALUES ('9121', '91', 'KAB. MAMBERAMO TENGAH');
INSERT INTO `cities` VALUES ('9122', '91', 'KAB. YALIMO');
INSERT INTO `cities` VALUES ('9123', '91', 'KAB. LANNY JAYA');
INSERT INTO `cities` VALUES ('9124', '91', 'KAB. NDUGA');
INSERT INTO `cities` VALUES ('9125', '91', 'KAB. PUNCAK');
INSERT INTO `cities` VALUES ('9126', '91', 'KAB. DOGIYAI');
INSERT INTO `cities` VALUES ('9127', '91', 'KAB. INTAN JAYA');
INSERT INTO `cities` VALUES ('9128', '91', 'KAB. DEIYAI');
INSERT INTO `cities` VALUES ('9171', '91', 'KOTA JAYAPURA');
INSERT INTO `cities` VALUES ('9201', '92', 'KAB. SORONG');
INSERT INTO `cities` VALUES ('9202', '92', 'KAB. MANOKWARI');
INSERT INTO `cities` VALUES ('9203', '92', 'KAB. FAK FAK');
INSERT INTO `cities` VALUES ('9204', '92', 'KAB. SORONG SELATAN');
INSERT INTO `cities` VALUES ('9205', '92', 'KAB. RAJA AMPAT');
INSERT INTO `cities` VALUES ('9206', '92', 'KAB. TELUK BINTUNI');
INSERT INTO `cities` VALUES ('9207', '92', 'KAB. TELUK WONDAMA');
INSERT INTO `cities` VALUES ('9208', '92', 'KAB. KAIMANA');
INSERT INTO `cities` VALUES ('9209', '92', 'KAB. TAMBRAUW');
INSERT INTO `cities` VALUES ('9210', '92', 'KAB. MAYBRAT');
INSERT INTO `cities` VALUES ('9211', '92', 'KAB. MANOKWARI SELATAN');
INSERT INTO `cities` VALUES ('9212', '92', 'KAB. PEGUNUNGAN ARFAK');
INSERT INTO `cities` VALUES ('9271', '92', 'KOTA SORONG');

-- ----------------------------
-- Table structure for companies
-- ----------------------------
DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `total` int(10) DEFAULT '0',
  `type` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of companies
-- ----------------------------
INSERT INTO `companies` VALUES ('1', 'PT ABC ', '1', null, '1', '2018-03-18 11:27:34', '2018-03-18 11:28:05');
INSERT INTO `companies` VALUES ('2', 'Jasa Konsulat Pembangunan', '2', null, '1', '2018-03-18 11:27:49', '2018-03-18 11:28:05');
INSERT INTO `companies` VALUES ('3', 'Pharmacy Techno', '1', null, '1', '2018-03-18 11:27:56', '2018-03-18 11:28:06');
INSERT INTO `companies` VALUES ('4', 'Mercusuar Buana', '1', null, '1', '2018-03-18 11:28:04', '2018-03-18 11:28:07');
INSERT INTO `companies` VALUES ('5', 'PT abadi sejahterah', '0', 'Konstruksi', '1', '2018-03-21 09:28:05', '2018-03-21 09:28:05');
INSERT INTO `companies` VALUES ('6', 'Damai Bahagia Kimia', '0', 'Industri Manufaktur Baru', '1', '2018-03-21 09:29:11', '2018-03-21 09:44:49');
INSERT INTO `companies` VALUES ('7', 'Jasa Konstruksi', '0', 'Konstruksi', '1', '2018-03-21 09:29:44', '2018-03-21 09:29:44');
INSERT INTO `companies` VALUES ('9', 'Super admin LAin Bagus', '0', 'lol', '1', '2018-03-21 09:32:17', '2018-03-21 10:19:12');
INSERT INTO `companies` VALUES ('10', 'test', '0', 'qwe', '1', '2018-03-21 09:33:15', '2018-03-21 09:33:15');
INSERT INTO `companies` VALUES ('11', 'HALO', '0', 'Bebas', '1', '2018-03-21 14:19:36', '2018-03-21 14:19:36');

-- ----------------------------
-- Table structure for education_levels
-- ----------------------------
DROP TABLE IF EXISTS `education_levels`;
CREATE TABLE `education_levels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `total` int(10) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of education_levels
-- ----------------------------
INSERT INTO `education_levels` VALUES ('1', 'S1', '0', '2018-03-18 11:28:21', '2018-03-18 11:28:26');
INSERT INTO `education_levels` VALUES ('2', 'S2', '0', '2018-03-18 11:28:27', '2018-03-18 11:28:27');
INSERT INTO `education_levels` VALUES ('3', 'S3', '0', '2018-03-18 11:28:29', '2018-03-18 11:28:29');
INSERT INTO `education_levels` VALUES ('4', 'SMK', '0', '2018-03-18 11:28:36', '2018-03-18 11:28:36');
INSERT INTO `education_levels` VALUES ('5', 'SMA', '0', '2018-03-18 11:28:38', '2018-03-18 11:28:38');
INSERT INTO `education_levels` VALUES ('6', 'SMP', '0', '2018-03-18 11:28:45', '2018-03-18 11:28:45');
INSERT INTO `education_levels` VALUES ('7', 'SD', '0', '2018-03-18 11:28:47', '2018-03-18 11:28:47');
INSERT INTO `education_levels` VALUES ('8', 'Lain-Lain', '0', '2018-03-18 11:28:56', '2018-03-18 11:28:56');

-- ----------------------------
-- Table structure for logs
-- ----------------------------
DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `agent` varchar(255) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=449 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of logs
-- ----------------------------
INSERT INTO `logs` VALUES ('1', 'Edit informasi umum User: Peminjam A', 'http://localhost:8000/api/authcostum/profile/update?api_token=a7ZvnRXlqtf5CLubYfhVBTMxRbvTJjvZ9j8DeCRH7YLoZ3H4ZLwTBBdhCqKe', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '5', '2018-03-20 03:30:11', '2018-03-20 03:30:11');
INSERT INTO `logs` VALUES ('2', 'Super Admin Project AS menambahkan perusahaan Jasa Konstruksi', 'http://localhost:8000/webApi/company/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 09:32:17', '2018-03-21 09:32:17');
INSERT INTO `logs` VALUES ('3', 'Super Admin Project AS menambahkan perusahaan test', 'http://localhost:8000/webApi/company/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 09:33:15', '2018-03-21 09:33:15');
INSERT INTO `logs` VALUES ('4', 'Super Admin Project AS mengubah perusahaan Damai Bahagia Kimia', 'http://localhost:8000/webApi/company/6', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 09:44:49', '2018-03-21 09:44:49');
INSERT INTO `logs` VALUES ('5', 'Super Admin Project AS menghapus perusahaan Halo Perusahaan', 'http://localhost:8000/webApi/company/8', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 09:49:28', '2018-03-21 09:49:28');
INSERT INTO `logs` VALUES ('6', 'Super Admin Project AS menambahkan profesi Baru profesi', 'http://localhost:8000/webApi/profession/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 09:54:13', '2018-03-21 09:54:13');
INSERT INTO `logs` VALUES ('7', 'Super Admin Project AS mengubah profesi Baru profesi', 'http://localhost:8000/webApi/profession/11', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 09:54:40', '2018-03-21 09:54:40');
INSERT INTO `logs` VALUES ('8', 'Super Admin Project AS menambahkan profesi Ada', 'http://localhost:8000/webApi/profession/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 09:54:54', '2018-03-21 09:54:54');
INSERT INTO `logs` VALUES ('9', 'Super Admin Project AS mengubah profesi Ada', 'http://localhost:8000/webApi/profession/12', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 09:55:15', '2018-03-21 09:55:15');
INSERT INTO `logs` VALUES ('10', 'Super Admin Project AS menghapus profesi Mandor Baru', 'http://localhost:8000/webApi/profession/12', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 09:55:21', '2018-03-21 09:55:21');
INSERT INTO `logs` VALUES ('11', 'Super Admin Project AS menambahkan program studi Teknik Industri', 'http://localhost:8000/webApi/study-program/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:00:17', '2018-03-21 10:00:17');
INSERT INTO `logs` VALUES ('12', 'Super Admin Project AS menghapus program studi Teknik Industri', 'http://localhost:8000/webApi/study-program/7', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:00:21', '2018-03-21 10:00:21');
INSERT INTO `logs` VALUES ('13', 'Super Admin Project AS mengubah program studi Teknik Industri', 'http://localhost:8000/webApi/study-program/8', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:00:35', '2018-03-21 10:00:35');
INSERT INTO `logs` VALUES ('14', 'Super Admin Project AS menambahkan User: Super admin LAin', 'http://localhost:8000/webApi/user/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:08:42', '2018-03-21 10:08:42');
INSERT INTO `logs` VALUES ('15', 'Super Admin Project AS mengubah perusahaan Jasa Konstruksi', 'http://localhost:8000/webApi/company/9', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:19:12', '2018-03-21 10:19:12');
INSERT INTO `logs` VALUES ('16', 'Super Admin Project AS mengubah informasi  user: admin ascertificate 2 2', 'http://localhost:8000/webApi/user/7', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:19:58', '2018-03-21 10:19:58');
INSERT INTO `logs` VALUES ('17', 'Super Admin Project AS mengubah Status  user: Kontraktor COmpany A menjadi tidak aktif', 'http://localhost:8000/webApi/user-status/4', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:27:57', '2018-03-21 10:27:57');
INSERT INTO `logs` VALUES ('18', 'Super Admin Project AS mengubah Status  user: Kontraktor COmpany A menjadi tidak aktif', 'http://localhost:8000/webApi/user-status/4', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:28:41', '2018-03-21 10:28:41');
INSERT INTO `logs` VALUES ('19', 'Super Admin Project AS mengubah Status  user: Peminjam A menjadi tidak aktif', 'http://localhost:8000/webApi/user-status/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:29:08', '2018-03-21 10:29:08');
INSERT INTO `logs` VALUES ('20', 'Super Admin Project AS mengubah Status  user: Kontraktor COmpany A menjadi tidak aktif', 'http://localhost:8000/webApi/user-status/4', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:29:48', '2018-03-21 10:29:48');
INSERT INTO `logs` VALUES ('21', 'Super Admin Project AS mengubah Status  user: Kontraktor COmpany A menjadi aktif', 'http://localhost:8000/webApi/user-status/4', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:29:52', '2018-03-21 10:29:52');
INSERT INTO `logs` VALUES ('22', 'Super Admin Project AS mengubah Status  user: Peminjam A menjadi tidak aktif', 'http://localhost:8000/webApi/user-status/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:48:31', '2018-03-21 10:48:31');
INSERT INTO `logs` VALUES ('23', 'Super Admin Project AS mengubah Status  user: Peminjam A menjadi aktif', 'http://localhost:8000/webApi/user-status/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:48:37', '2018-03-21 10:48:37');
INSERT INTO `logs` VALUES ('24', 'Super Admin Project AS mengubah Status  user: Peminjam A menjadi tidak aktif', 'http://localhost:8000/webApi/user-status/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:48:42', '2018-03-21 10:48:42');
INSERT INTO `logs` VALUES ('25', 'Super Admin Project AS mengubah Status  user: Peminjam A menjadi aktif', 'http://localhost:8000/webApi/user-status/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 10:48:45', '2018-03-21 10:48:45');
INSERT INTO `logs` VALUES ('26', 'Super Admin Project AS menambahkan program studi test', 'http://localhost:8000/webApi/study-program/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 14:05:06', '2018-03-21 14:05:06');
INSERT INTO `logs` VALUES ('27', 'Super Admin Project AS menambahkan program studi test', 'http://localhost:8000/webApi/study-program/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 14:06:03', '2018-03-21 14:06:03');
INSERT INTO `logs` VALUES ('28', 'Super Admin Project AS menambahkan program studi test', 'http://localhost:8000/webApi/study-program/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 14:06:44', '2018-03-21 14:06:44');
INSERT INTO `logs` VALUES ('29', 'Super Admin Project AS menambahkan program studi test1', 'http://localhost:8000/webApi/study-program/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 14:13:00', '2018-03-21 14:13:00');
INSERT INTO `logs` VALUES ('30', 'Super Admin Project AS menambahkan perusahaan HALO', 'http://localhost:8000/webApi/company/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 14:19:36', '2018-03-21 14:19:36');
INSERT INTO `logs` VALUES ('31', 'Super Admin Project AS menambahkan User: halo user', 'http://localhost:8000/webApi/user/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-21 18:12:52', '2018-03-21 18:12:52');
INSERT INTO `logs` VALUES ('32', 'Kontraktor COmpany A Berhasil login', 'http://localhost:8000/api/authcostum/login', 'POST', '127.0.0.1', 'PostmanRuntime/7.1.1', '4', '2018-03-21 22:04:19', '2018-03-21 22:04:19');
INSERT INTO `logs` VALUES ('33', 'Kontraktor COmpany A Berhasil Logout', 'http://localhost:8000/api/authcostum/logout?api_token=EHw2uXfp8ARO0TL44lG3kYN29a3F5usX60hA7dUI46ss0xcFqrJrcx5UWbKW', 'POST', '127.0.0.1', 'PostmanRuntime/7.1.1', '4', '2018-03-21 22:05:09', '2018-03-21 22:05:09');
INSERT INTO `logs` VALUES ('34', 'Super Admin Project AS mengubah status sertifikat 101 - 	Arsitek menjadi tampil', 'http://localhost:8000/webApi/certificate-status/1', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 15:29:14', '2018-03-23 15:29:14');
INSERT INTO `logs` VALUES ('35', 'Super Admin Project AS mengubah status sertifikat 101 - 	Arsitek menjadi sembunyi', 'http://localhost:8000/webApi/certificate-status/1', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 15:29:18', '2018-03-23 15:29:18');
INSERT INTO `logs` VALUES ('36', 'Super Admin Project AS mengubah status sertifikat 101 - 	Arsitek menjadi tampil', 'http://localhost:8000/webApi/certificate-status/1', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 15:29:25', '2018-03-23 15:29:25');
INSERT INTO `logs` VALUES ('37', 'Super Admin Project AS membuat sertifikat 0001AXAA - Memasang Tower', 'http://localhost:8000/webApi/certificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 16:20:07', '2018-03-23 16:20:07');
INSERT INTO `logs` VALUES ('38', 'Super Admin Project AS menghapus sertifikat 0001AXA - Memasang Tower', 'http://localhost:8000/webApi/certificate/225', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 16:29:35', '2018-03-23 16:29:35');
INSERT INTO `logs` VALUES ('39', 'Super Admin Project AS menghapus sertifikat 0001AXA - Memasang Tower', 'http://localhost:8000/webApi/certificate/225', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 16:30:57', '2018-03-23 16:30:57');
INSERT INTO `logs` VALUES ('40', 'Super Admin Project AS mengubah informasi sertifikat 0001AXAA - Memasang Tower', 'http://localhost:8000/webApi/certificate/226', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 17:07:55', '2018-03-23 17:07:55');
INSERT INTO `logs` VALUES ('41', 'Super Admin Project AS mengubah informasi sertifikat 0001AXAA - Memasang Tower', 'http://localhost:8000/webApi/certificate/226', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 17:09:10', '2018-03-23 17:09:10');
INSERT INTO `logs` VALUES ('42', 'Super Admin Project AS mengubah informasi sertifikat 0001AXAAA - Memasang TowerX', 'http://localhost:8000/webApi/certificate/226', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 17:11:36', '2018-03-23 17:11:36');
INSERT INTO `logs` VALUES ('43', 'Super Admin Project AS mengubah informasi sertifikat 0001AXAAA - Memasang TowerX', 'http://localhost:8000/webApi/certificate/226', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 17:17:18', '2018-03-23 17:17:18');
INSERT INTO `logs` VALUES ('44', 'Super Admin Project AS membuat sertifikat 00232AL - profesi baru', 'http://localhost:8000/webApi/certificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 17:17:56', '2018-03-23 17:17:56');
INSERT INTO `logs` VALUES ('45', 'Super Admin Project AS menghapus sertifikat 00232AL - profesi baru', 'http://localhost:8000/webApi/certificate/227', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 17:26:59', '2018-03-23 17:26:59');
INSERT INTO `logs` VALUES ('46', 'Super Admin Project AS membuat sertifikat 03132AX23 - Baru dengan gambar', 'http://localhost:8000/webApi/certificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 17:36:46', '2018-03-23 17:36:46');
INSERT INTO `logs` VALUES ('47', 'Super Admin Project AS membuat sertifikat Halo Baru - Kemudian', 'http://localhost:8000/webApi/certificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 17:38:42', '2018-03-23 17:38:42');
INSERT INTO `logs` VALUES ('48', 'Super Admin Project AS menghapus sertifikat 03132AX23 - Baru dengan gambar', 'http://localhost:8000/webApi/certificate/228', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 17:42:04', '2018-03-23 17:42:04');
INSERT INTO `logs` VALUES ('49', 'Super Admin Project AS membuat sertifikat asd22 - asdasd', 'http://localhost:8000/webApi/certificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 18:02:06', '2018-03-23 18:02:06');
INSERT INTO `logs` VALUES ('50', 'Super Admin Project AS mengubah informasi sertifikat asd22 - asdasd', 'http://localhost:8000/webApi/certificate/230', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 18:03:47', '2018-03-23 18:03:47');
INSERT INTO `logs` VALUES ('51', 'Super Admin Project AS mengubah status sertifikat 101 - 	Arsitek menjadi sembunyi', 'http://localhost:8000/webApi/certificate-status/1', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 19:39:52', '2018-03-23 19:39:52');
INSERT INTO `logs` VALUES ('52', 'Super Admin Project AS mengubah status sertifikat 101 - 	Arsitek menjadi tampil', 'http://localhost:8000/webApi/certificate-status/1', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-23 19:39:57', '2018-03-23 19:39:57');
INSERT INTO `logs` VALUES ('53', 'Super Admin Project AS Menyewa Sertifikat', 'http://localhost:8000/webApi/rent/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-24 09:23:38', '2018-03-24 09:23:38');
INSERT INTO `logs` VALUES ('54', 'Super Admin Project AS Menyewa Sertifikat', 'http://localhost:8000/webApi/rent/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-24 09:24:24', '2018-03-24 09:24:24');
INSERT INTO `logs` VALUES ('55', 'Super Admin Project AS Menyewa Sertifikat', 'http://localhost:8000/webApi/rent/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '6', '2018-03-24 09:26:03', '2018-03-24 09:26:03');
INSERT INTO `logs` VALUES ('56', 'Super Admin Project AS mengubah Status  user: Kontraktor COmpany A menjadi tidak aktif', 'http://localhost:8000/webApi/user-status/4', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Mobile Safari/537.36', '6', '2018-03-25 20:22:34', '2018-03-25 20:22:34');
INSERT INTO `logs` VALUES ('57', 'Super Admin Project AS membuat pembayaran untuk penyewaan Request20180324092424', 'http://localhost:8000/webApi/payment/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Mobile Safari/537.36', '6', '2018-03-25 22:47:40', '2018-03-25 22:47:40');
INSERT INTO `logs` VALUES ('58', 'Super Admin Project AS membuat pembayaran untuk penyewaan Request2018-03-24092338', 'http://localhost:8000/webApi/payment/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Mobile Safari/537.36', '6', '2018-03-25 22:49:35', '2018-03-25 22:49:35');
INSERT INTO `logs` VALUES ('59', 'Super Admin Project AS update status penyewaan Request2018-03-24092317 menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/8', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 00:37:51', '2018-03-26 00:37:51');
INSERT INTO `logs` VALUES ('60', 'Super Admin Project AS update status penyewaan Request2018-03-24092338 menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/9', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 00:39:23', '2018-03-26 00:39:23');
INSERT INTO `logs` VALUES ('61', 'Super Admin Project AS update status penyewaan SN32313231 menjadi hard copy dikirim', 'http://localhost:8000/webApi/rent-status/hard_sended/7', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 00:39:33', '2018-03-26 00:39:33');
INSERT INTO `logs` VALUES ('62', 'Super Admin Project AS membuat pembayaran untuk penyewaan Request20180324092424', 'http://localhost:8000/webApi/payment/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 00:40:56', '2018-03-26 00:40:56');
INSERT INTO `logs` VALUES ('63', 'renter company A mengubah pembayaran penyewaan Request2018-03-24092317', 'http://localhost:8000/api/payment/8?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-03-26 10:04:34', '2018-03-26 10:04:34');
INSERT INTO `logs` VALUES ('64', 'renter company A mengubah pembayaran penyewaan Request2018-03-24092317', 'http://localhost:8000/api/payment/8?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-03-26 10:05:00', '2018-03-26 10:05:00');
INSERT INTO `logs` VALUES ('65', 'renter company A mengubah pembayaran penyewaan Request2018-03-24092338', 'http://localhost:8000/api/payment/9?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-03-26 10:05:17', '2018-03-26 10:05:17');
INSERT INTO `logs` VALUES ('66', 'renter company A mengubah pembayaran penyewaan Request20180324092424', 'http://localhost:8000/api/payment/10?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-03-26 10:05:32', '2018-03-26 10:05:32');
INSERT INTO `logs` VALUES ('67', 'renter company A mengubah pembayaran penyewaan Request20180324092424', 'http://localhost:8000/api/payment/10?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-03-26 10:10:59', '2018-03-26 10:10:59');
INSERT INTO `logs` VALUES ('68', 'renter company A mengubah pembayaran penyewaan Request20180324092424', 'http://localhost:8000/api/payment/10?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-03-26 10:13:56', '2018-03-26 10:13:56');
INSERT INTO `logs` VALUES ('69', 'renter company A mengubah pembayaran penyewaan Request20180324092424', 'http://localhost:8000/api/payment/10?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-03-26 10:15:54', '2018-03-26 10:15:54');
INSERT INTO `logs` VALUES ('70', 'renter company A mengubah pembayaran penyewaan Request20180324092424', 'http://localhost:8000/api/payment/10?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-03-26 10:16:08', '2018-03-26 10:16:08');
INSERT INTO `logs` VALUES ('71', 'renter company A mengubah pembayaran penyewaan Request20180324092424', 'http://localhost:8000/api/payment/10?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-03-26 10:16:39', '2018-03-26 10:16:39');
INSERT INTO `logs` VALUES ('72', 'renter company A mengubah pembayaran penyewaan Request20180324092424', 'http://localhost:8000/api/payment/10?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-03-26 10:17:45', '2018-03-26 10:17:45');
INSERT INTO `logs` VALUES ('73', 'Super Admin Project AS update status penyewaan  menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/10', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 11:25:51', '2018-03-26 11:25:51');
INSERT INTO `logs` VALUES ('74', 'Super Admin Project AS update status penyewaan XADDA12313 menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/10', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 11:38:33', '2018-03-26 11:38:33');
INSERT INTO `logs` VALUES ('75', 'Super Admin Project AS update status penyewaan XADDA12313 menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/10', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 11:39:47', '2018-03-26 11:39:47');
INSERT INTO `logs` VALUES ('76', 'Super Admin Project AS update status penyewaan  menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/10', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 11:41:20', '2018-03-26 11:41:20');
INSERT INTO `logs` VALUES ('77', 'Super Admin Project AS update status penyewaan  menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/10', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 11:42:43', '2018-03-26 11:42:43');
INSERT INTO `logs` VALUES ('78', 'Super Admin Project AS update status penyewaan  menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/10', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 11:43:47', '2018-03-26 11:43:47');
INSERT INTO `logs` VALUES ('79', 'Super Admin Project AS update status penyewaan XAdasd menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/10', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 11:46:04', '2018-03-26 11:46:04');
INSERT INTO `logs` VALUES ('80', 'Super Admin Project AS update status penyewaan XAdasd AS menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/10', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 11:46:35', '2018-03-26 11:46:35');
INSERT INTO `logs` VALUES ('81', 'Super Admin Project AS update status penyewaan XAdasd AS menjadi hard copy dikirim', 'http://localhost:8000/webApi/rent-status/hard_sended/10', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 11:56:04', '2018-03-26 11:56:04');
INSERT INTO `logs` VALUES ('82', 'Super Admin Project AS membuat pembayaran untuk penyewaan Request2018-03-24092317', 'http://localhost:8000/webApi/payment/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 11:57:55', '2018-03-26 11:57:55');
INSERT INTO `logs` VALUES ('83', 'renter company A mengubah pembayaran penyewaan Request2018-03-24092317', 'http://localhost:8000/api/payment/8?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-03-26 11:58:21', '2018-03-26 11:58:21');
INSERT INTO `logs` VALUES ('84', 'Super Admin Project AS update status penyewaan 31321312AXB132 menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/8', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-03-26 11:58:54', '2018-03-26 11:58:54');
INSERT INTO `logs` VALUES ('85', 'renter company A mengubah pembayaran penyewaan 31321312AXB132', 'http://localhost:8000/api/payment/8?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-03-26 11:59:16', '2018-03-26 11:59:16');
INSERT INTO `logs` VALUES ('86', 'renter company A mengubah pembayaran penyewaan 31321312AXB132', 'http://localhost:8000/api/payment/8?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-03-26 12:28:05', '2018-03-26 12:28:05');
INSERT INTO `logs` VALUES ('87', 'renter company A Menyewa Sertifikat', 'http://localhost:8000/api/rent/create?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'POST', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-03-26 12:32:43', '2018-03-26 12:32:43');
INSERT INTO `logs` VALUES ('88', 'Edit informasi umum User: renter company A', 'http://localhost:8000/api/authcostum/profile/update?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 13:21:11', '2018-04-05 13:21:11');
INSERT INTO `logs` VALUES ('89', 'renter company A Memasukkan sertifikat 	Arsitek kedalam cart', 'http://localhost:8000/api/cart/add?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'POST', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 14:16:46', '2018-04-05 14:16:46');
INSERT INTO `logs` VALUES ('90', 'renter company A Memasukkan sertifikat Ahli Desain Interior kedalam cart', 'http://localhost:8000/api/cart/add?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'POST', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 14:16:58', '2018-04-05 14:16:58');
INSERT INTO `logs` VALUES ('91', 'renter company A Memasukkan sertifikat Ahli Arsitekur Lansekap kedalam cart', 'http://localhost:8000/api/cart/add?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'POST', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 14:17:02', '2018-04-05 14:17:02');
INSERT INTO `logs` VALUES ('92', 'renter company A menghapus cart ', 'http://localhost:8000/api/cart/1?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'DELETE', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 14:20:02', '2018-04-05 14:20:02');
INSERT INTO `logs` VALUES ('93', 'renter company A Mengosongkan cart ', 'http://localhost:8000/api/empty-cart?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'DELETE', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 14:21:44', '2018-04-05 14:21:44');
INSERT INTO `logs` VALUES ('94', 'Edit informasi umum User: renter company A', 'http://localhost:8000/api/authcostum/profile/update?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 14:28:59', '2018-04-05 14:28:59');
INSERT INTO `logs` VALUES ('95', 'Edit informasi umum User: renter company A', 'http://localhost:8000/api/authcostum/profile/update?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 14:29:56', '2018-04-05 14:29:56');
INSERT INTO `logs` VALUES ('96', 'Edit informasi umum User: renter company A', 'http://localhost:8000/api/authcostum/profile/update?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 14:30:14', '2018-04-05 14:30:14');
INSERT INTO `logs` VALUES ('97', 'renter company A Memasukkan sertifikat Ahli Arsitekur Lansekap kedalam cart', 'http://localhost:8000/api/cart/add?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'POST', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 16:05:36', '2018-04-05 16:05:36');
INSERT INTO `logs` VALUES ('98', 'renter company A Memasukkan sertifikat Ahli Desain Interior kedalam cart', 'http://localhost:8000/api/cart/add?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'POST', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 16:05:47', '2018-04-05 16:05:47');
INSERT INTO `logs` VALUES ('99', 'renter company A Memasukkan sertifikat 	Arsitek kedalam cart', 'http://localhost:8000/api/cart/add?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'POST', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 16:05:54', '2018-04-05 16:05:54');
INSERT INTO `logs` VALUES ('100', 'renter company A checkout sertifikat, Order INV20180405161205 Berhasil dibuat', 'http://localhost:8000/api/checkout?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'POST', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 16:12:05', '2018-04-05 16:12:05');
INSERT INTO `logs` VALUES ('101', 'renter company A checkout sertifikat, Order INV20180405161354 Berhasil dibuat', 'http://localhost:8000/api/checkout?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'POST', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 16:13:55', '2018-04-05 16:13:55');
INSERT INTO `logs` VALUES ('102', 'renter company A checkout sertifikat, Order INV20180405161511 Berhasil dibuat', 'http://localhost:8000/api/checkout?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'POST', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 16:15:11', '2018-04-05 16:15:11');
INSERT INTO `logs` VALUES ('103', 'Edit informasi umum User: renter company A', 'http://localhost:8000/api/authcostum/profile/update?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'PATCH', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-05 18:41:43', '2018-04-05 18:41:43');
INSERT INTO `logs` VALUES ('104', 'Super Admin Project AS membuat sertifikat 01212 - TEst', 'http://localhost:8000/webApi/certificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-06 08:46:39', '2018-04-06 08:46:39');
INSERT INTO `logs` VALUES ('105', 'Super Admin Project AS mengubah status sertifikat 01212 - TEst menjadi sembunyi', 'http://localhost:8000/webApi/certificate-status/225', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-06 08:46:46', '2018-04-06 08:46:46');
INSERT INTO `logs` VALUES ('106', 'Super Admin Project AS menghapus sertifikat 01212 - TEst', 'http://localhost:8000/webApi/certificate/225', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-06 08:46:49', '2018-04-06 08:46:49');
INSERT INTO `logs` VALUES ('107', 'Super Admin Project AS membatalkan order MAMA', 'http://localhost:8000/webApi/ordercancel/2?_=1522985884937', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-06 10:38:30', '2018-04-06 10:38:30');
INSERT INTO `logs` VALUES ('108', 'Super Admin Project AS membatalkan order XAMMS', 'http://localhost:8000/webApi/ordercancel/3?_=1522985953479', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-06 10:39:33', '2018-04-06 10:39:33');
INSERT INTO `logs` VALUES ('109', 'renter company A Memasukkan sertifikat 	Arsitek kedalam cart', 'http://localhost:8000/api/cart/add?api_token=kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', 'POST', '127.0.0.1', 'PostmanRuntime/7.1.1', '2', '2018-04-06 13:14:56', '2018-04-06 13:14:56');
INSERT INTO `logs` VALUES ('110', 'renter company A Memasukkan sertifikat 	Teknisi Instalasi Penerangan Dan Daya Fasa Satu kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:28:32', '2018-04-06 14:28:32');
INSERT INTO `logs` VALUES ('111', 'renter company A Memasukkan sertifikat Teknisi Instalasi Penerangan dan Daya Fasa Tiga kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:29:57', '2018-04-06 14:29:57');
INSERT INTO `logs` VALUES ('112', 'renter company A Memasukkan sertifikat Tukang Pasang Bata / Dinding / Bricklayer / Bricklaying (Tukang Bata) kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:32:16', '2018-04-06 14:32:16');
INSERT INTO `logs` VALUES ('113', 'renter company A Memasukkan sertifikat 	Teknisi Instalasi Penerangan Dan Daya Fasa Satu kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:34:12', '2018-04-06 14:34:12');
INSERT INTO `logs` VALUES ('114', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/37', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:36:47', '2018-04-06 14:36:47');
INSERT INTO `logs` VALUES ('115', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart-bycertificate/37', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:38:55', '2018-04-06 14:38:55');
INSERT INTO `logs` VALUES ('116', 'renter company A Memasukkan sertifikat 	Ahli Teknik Tenaga Listrik kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:39:21', '2018-04-06 14:39:21');
INSERT INTO `logs` VALUES ('117', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart-bycertificate/63', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:40:38', '2018-04-06 14:40:38');
INSERT INTO `logs` VALUES ('118', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart-bycertificate/25', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:40:41', '2018-04-06 14:40:41');
INSERT INTO `logs` VALUES ('119', 'renter company A Memasukkan sertifikat 	Ahli Teknik Tenaga Listrik kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:40:46', '2018-04-06 14:40:46');
INSERT INTO `logs` VALUES ('120', 'renter company A Memasukkan sertifikat Ahli Teknik Elektronika dan Telekomunikasi Dalam Gedung kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:41:05', '2018-04-06 14:41:05');
INSERT INTO `logs` VALUES ('121', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart-bycertificate/25', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:50:36', '2018-04-06 14:50:36');
INSERT INTO `logs` VALUES ('122', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart-bycertificate/26', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:50:39', '2018-04-06 14:50:39');
INSERT INTO `logs` VALUES ('123', 'renter company A Memasukkan sertifikat 	Ahli Teknik Tenaga Listrik kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:51:44', '2018-04-06 14:51:44');
INSERT INTO `logs` VALUES ('124', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart-bycertificate/25', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:52:04', '2018-04-06 14:52:04');
INSERT INTO `logs` VALUES ('125', 'renter company A Memasukkan sertifikat 	Ahli Teknik Tenaga Listrik kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 14:56:33', '2018-04-06 14:56:33');
INSERT INTO `logs` VALUES ('126', 'renter company A Memasukkan sertifikat 	Teknisi Instalasi Penerangan Dan Daya Fasa Satu kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 15:20:04', '2018-04-06 15:20:04');
INSERT INTO `logs` VALUES ('127', 'renter company A checkout sertifikat, Order INV20180406153139 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-06 15:31:39', '2018-04-06 15:31:39');
INSERT INTO `logs` VALUES ('128', 'renter company A Memasukkan sertifikat 	Ahli Teknik Tenaga Listrik kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-10 11:40:04', '2018-04-10 11:40:04');
INSERT INTO `logs` VALUES ('129', 'renter company A checkout sertifikat, Order INV20180410114052 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-10 11:40:52', '2018-04-10 11:40:52');
INSERT INTO `logs` VALUES ('130', 'renter company A Memasukkan sertifikat 	Ahli Teknik Tenaga Listrik kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-10 11:46:11', '2018-04-10 11:46:11');
INSERT INTO `logs` VALUES ('131', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart-bycertificate/25', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-10 11:46:18', '2018-04-10 11:46:18');
INSERT INTO `logs` VALUES ('132', 'renter company A Memasukkan sertifikat 	Ahli Teknik Tenaga Listrik kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-10 11:50:42', '2018-04-10 11:50:42');
INSERT INTO `logs` VALUES ('133', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart-bycertificate/25', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-10 11:50:50', '2018-04-10 11:50:50');
INSERT INTO `logs` VALUES ('134', 'Super Admin Project AS membuat pembayaran untuk Order INV20180410114052', 'http://localhost:8000/webApi/payment/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-11 10:36:51', '2018-04-11 10:36:51');
INSERT INTO `logs` VALUES ('135', 'Super Admin Project AS mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-11 10:54:04', '2018-04-11 10:54:04');
INSERT INTO `logs` VALUES ('136', 'Super Admin Project AS membuat pembayaran untuk Order INV20180410114052', 'http://localhost:8000/webApi/payment/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-11 10:57:59', '2018-04-11 10:57:59');
INSERT INTO `logs` VALUES ('137', 'Super Admin Project AS membuat pembayaran untuk Order INV20180410114052', 'http://localhost:8000/webApi/payment/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-11 11:04:05', '2018-04-11 11:04:05');
INSERT INTO `logs` VALUES ('138', 'Edit informasi umum User: Super Admin Project AS', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 14:40:26', '2018-04-13 14:40:26');
INSERT INTO `logs` VALUES ('139', 'Edit informasi umum User: Super Admin Project AS', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 14:44:01', '2018-04-13 14:44:01');
INSERT INTO `logs` VALUES ('140', 'Edit informasi umum User: Super Admin Project AS', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 14:45:19', '2018-04-13 14:45:19');
INSERT INTO `logs` VALUES ('141', 'Edit informasi umum User: Super Admin SKA SKT', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 14:48:00', '2018-04-13 14:48:00');
INSERT INTO `logs` VALUES ('142', 'Edit informasi umum User: Super Admin SKA', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 14:57:58', '2018-04-13 14:57:58');
INSERT INTO `logs` VALUES ('143', 'Edit informasi umum User: Super Admin SKA SKT', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 14:58:20', '2018-04-13 14:58:20');
INSERT INTO `logs` VALUES ('144', 'Edit informasi umum User: renter company A', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-13 15:17:16', '2018-04-13 15:17:16');
INSERT INTO `logs` VALUES ('145', 'Edit informasi umum User: renter company A', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-13 15:18:15', '2018-04-13 15:18:15');
INSERT INTO `logs` VALUES ('146', 'Edit informasi umum User: renter company A', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-13 15:18:52', '2018-04-13 15:18:52');
INSERT INTO `logs` VALUES ('147', 'Edit informasi umum User: renter company A', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-13 15:23:31', '2018-04-13 15:23:31');
INSERT INTO `logs` VALUES ('148', 'Edit informasi umum User: renter company A', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-13 15:26:55', '2018-04-13 15:26:55');
INSERT INTO `logs` VALUES ('149', 'Edit informasi umum User: renter company A', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-13 15:31:49', '2018-04-13 15:31:49');
INSERT INTO `logs` VALUES ('150', 'renter company Aupdate profil', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-13 15:38:59', '2018-04-13 15:38:59');
INSERT INTO `logs` VALUES ('151', 'renter company Aupdate profil', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-13 15:39:57', '2018-04-13 15:39:57');
INSERT INTO `logs` VALUES ('152', 'renter company Aupdate profil', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-13 15:41:51', '2018-04-13 15:41:51');
INSERT INTO `logs` VALUES ('153', 'renter company Aupdate profil', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-13 15:45:36', '2018-04-13 15:45:36');
INSERT INTO `logs` VALUES ('154', 'renter company Aupdate profil', 'http://localhost:8000/webApi/user-profileupdate', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '2', '2018-04-13 15:46:09', '2018-04-13 15:46:09');
INSERT INTO `logs` VALUES ('155', 'Super Admin SKA SKT mengirimnkan notifikasi Kirim dari Web', 'http://localhost:8000/webApi/notification/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 17:25:05', '2018-04-13 17:25:05');
INSERT INTO `logs` VALUES ('156', 'Super Admin SKA SKT mengirimnkan notifikasi Test dari web', 'http://localhost:8000/webApi/notification/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 17:29:01', '2018-04-13 17:29:01');
INSERT INTO `logs` VALUES ('157', 'Super Admin SKA SKT mengirimnkan notifikasi test', 'http://localhost:8000/webApi/notification/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 17:31:25', '2018-04-13 17:31:25');
INSERT INTO `logs` VALUES ('158', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 18:50:47', '2018-04-13 18:50:47');
INSERT INTO `logs` VALUES ('159', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 18:51:02', '2018-04-13 18:51:02');
INSERT INTO `logs` VALUES ('160', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 19:41:38', '2018-04-13 19:41:38');
INSERT INTO `logs` VALUES ('161', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 19:41:54', '2018-04-13 19:41:54');
INSERT INTO `logs` VALUES ('162', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 19:44:01', '2018-04-13 19:44:01');
INSERT INTO `logs` VALUES ('163', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 19:44:22', '2018-04-13 19:44:22');
INSERT INTO `logs` VALUES ('164', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 19:44:42', '2018-04-13 19:44:42');
INSERT INTO `logs` VALUES ('165', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 19:46:44', '2018-04-13 19:46:44');
INSERT INTO `logs` VALUES ('166', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 19:46:52', '2018-04-13 19:46:52');
INSERT INTO `logs` VALUES ('167', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 19:49:40', '2018-04-13 19:49:40');
INSERT INTO `logs` VALUES ('168', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 19:51:20', '2018-04-13 19:51:20');
INSERT INTO `logs` VALUES ('169', 'Super Admin SKA SKT update status penyewaan XADDA12313 menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/13', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 19:51:43', '2018-04-13 19:51:43');
INSERT INTO `logs` VALUES ('170', 'Super Admin SKA SKT update status penyewaan  menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/14', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 19:54:52', '2018-04-13 19:54:52');
INSERT INTO `logs` VALUES ('171', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 19:56:25', '2018-04-13 19:56:25');
INSERT INTO `logs` VALUES ('172', 'Super Admin SKA SKT update status penyewaan XADDA12313 menjadi hard copy dikirim', 'http://localhost:8000/webApi/rent-status/hard_sended/13', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 19:56:37', '2018-04-13 19:56:37');
INSERT INTO `logs` VALUES ('173', 'Super Admin SKA SKT update status penyewaan  menjadi hard copy dikirim', 'http://localhost:8000/webApi/rent-status/hard_sended/14', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 19:56:42', '2018-04-13 19:56:42');
INSERT INTO `logs` VALUES ('174', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 20:47:10', '2018-04-13 20:47:10');
INSERT INTO `logs` VALUES ('175', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 20:48:17', '2018-04-13 20:48:17');
INSERT INTO `logs` VALUES ('176', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 20:48:37', '2018-04-13 20:48:37');
INSERT INTO `logs` VALUES ('177', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 20:51:57', '2018-04-13 20:51:57');
INSERT INTO `logs` VALUES ('178', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 20:52:09', '2018-04-13 20:52:09');
INSERT INTO `logs` VALUES ('179', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 20:52:59', '2018-04-13 20:52:59');
INSERT INTO `logs` VALUES ('180', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 20:53:49', '2018-04-13 20:53:49');
INSERT INTO `logs` VALUES ('181', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 20:54:10', '2018-04-13 20:54:10');
INSERT INTO `logs` VALUES ('182', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 20:54:41', '2018-04-13 20:54:41');
INSERT INTO `logs` VALUES ('183', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161354', 'http://localhost:8000/webApi/payment/5', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-13 20:56:11', '2018-04-13 20:56:11');
INSERT INTO `logs` VALUES ('184', 'Super Admin SKA SKT update status penyewaan REQ225 menjadi dibatalkan', 'http://localhost:8000/webApi/rent-status/cancel/16', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-14 05:26:46', '2018-04-14 05:26:46');
INSERT INTO `logs` VALUES ('185', 'Super Admin SKA SKT update status penyewaan REQ263 menjadi dibatalkan', 'http://localhost:8000/webApi/rent-status/cancel/17', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-14 05:33:00', '2018-04-14 05:33:00');
INSERT INTO `logs` VALUES ('186', 'Super Admin SKA SKT update status penyewaan REQ225 menjadi dibatalkan', 'http://localhost:8000/webApi/rent-status/cancel/16', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-14 05:33:51', '2018-04-14 05:33:51');
INSERT INTO `logs` VALUES ('187', 'Super Admin SKA SKT membatalkan order INV20180405161354', 'http://localhost:8000/webApi/ordercancel/5?_=1523697068418', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-14 16:11:14', '2018-04-14 16:11:14');
INSERT INTO `logs` VALUES ('188', 'Super Admin SKA SKT membatalkan order INV20180405161354', 'http://localhost:8000/webApi/ordercancel/5?_=1523697130356', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-14 16:12:14', '2018-04-14 16:12:14');
INSERT INTO `logs` VALUES ('189', 'Super Admin SKA SKT update status penyewaan REQ225 menjadi hard copy dibayar', 'http://localhost:8000/webApi/rent-status/accept/16', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-14 16:13:35', '2018-04-14 16:13:35');
INSERT INTO `logs` VALUES ('190', 'Super Admin SKA SKT update status penyewaan REQ263 menjadi hard copy dibayar', 'http://localhost:8000/webApi/rent-status/accept/17', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-14 16:13:41', '2018-04-14 16:13:41');
INSERT INTO `logs` VALUES ('191', 'Super Admin SKA SKT membuat pembayaran untuk Order INV20180406153139', 'http://localhost:8000/webApi/payment/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-14 16:14:02', '2018-04-14 16:14:02');
INSERT INTO `logs` VALUES ('192', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180406153139', 'http://localhost:8000/webApi/payment/8', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-14 16:14:18', '2018-04-14 16:14:18');
INSERT INTO `logs` VALUES ('193', 'Super Admin SKA SKT update status penyewaan 231323 menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/16', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-14 16:14:43', '2018-04-14 16:14:43');
INSERT INTO `logs` VALUES ('194', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161511', 'http://localhost:8000/webApi/payment/6', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-15 17:45:57', '2018-04-15 17:45:57');
INSERT INTO `logs` VALUES ('195', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161511', 'http://localhost:8000/webApi/payment/6', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-15 17:46:43', '2018-04-15 17:46:43');
INSERT INTO `logs` VALUES ('196', 'Super Admin SKA SKT update status penyewaan  menjadi soft copy dikirim', 'http://localhost:8000/webApi/rent-status/soft_sended/15', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-15 17:49:48', '2018-04-15 17:49:48');
INSERT INTO `logs` VALUES ('197', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180405161511', 'http://localhost:8000/webApi/payment/6', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-15 17:50:39', '2018-04-15 17:50:39');
INSERT INTO `logs` VALUES ('198', 'Super Admin SKA SKT update status penyewaan  menjadi hard copy dikirim', 'http://localhost:8000/webApi/rent-status/hard_sended/15', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-15 17:53:31', '2018-04-15 17:53:31');
INSERT INTO `logs` VALUES ('199', 'Super Admin SKA SKT membuat sertifikat XASa - asd', 'http://localhost:8000/webApi/certificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-17 13:10:12', '2018-04-17 13:10:12');
INSERT INTO `logs` VALUES ('200', 'Super Admin SKA SKT membuat sertifikat asd - 123', 'http://localhost:8000/webApi/certificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '6', '2018-04-17 13:11:16', '2018-04-17 13:11:16');
INSERT INTO `logs` VALUES ('201', 'Super Admin SKA SKT membuat sertifikat Test - test', 'http://localhost:8000/webApi/certificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 04:04:26', '2018-04-19 04:04:26');
INSERT INTO `logs` VALUES ('202', 'Super Admin SKA SKT membuat sertifikat Test dude - haikx', 'http://localhost:8000/webApi/certificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 11:18:24', '2018-04-19 11:18:24');
INSERT INTO `logs` VALUES ('203', 'Super Admin SKA SKT mengubah informasi sertifikat Test dude - haikx', 'http://localhost:8000/webApi/certificate/228-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 11:45:50', '2018-04-19 11:45:50');
INSERT INTO `logs` VALUES ('204', 'Super Admin SKA SKT mengubah informasi sertifikat Test dude - haikx', 'http://localhost:8000/webApi/certificate/228-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 11:46:14', '2018-04-19 11:46:14');
INSERT INTO `logs` VALUES ('205', 'Super Admin SKA SKT mengubah informasi sertifikat Test dudex - haikx', 'http://localhost:8000/webApi/certificate/228-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 11:46:22', '2018-04-19 11:46:22');
INSERT INTO `logs` VALUES ('206', 'Super Admin SKA SKT membuat sertifikat tesxx - sda', 'http://localhost:8000/webApi/certificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:10:34', '2018-04-19 12:10:34');
INSERT INTO `logs` VALUES ('207', 'Super Admin SKA SKT mengubah informasi sertifikat Test dudex - haikx', 'http://localhost:8000/webApi/certificate/228-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:24:41', '2018-04-19 12:24:41');
INSERT INTO `logs` VALUES ('208', 'Super Admin SKA SKT mengubah informasi sertifikat Test dudex - haikx', 'http://localhost:8000/webApi/certificate/228-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:24:56', '2018-04-19 12:24:56');
INSERT INTO `logs` VALUES ('209', 'Super Admin SKA SKT mengubah informasi sertifikat Test - test', 'http://localhost:8000/webApi/certificate/227-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:25:09', '2018-04-19 12:25:09');
INSERT INTO `logs` VALUES ('210', 'Super Admin SKA SKT mengubah informasi sertifikat Test - test', 'http://localhost:8000/webApi/certificate/227-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:27:35', '2018-04-19 12:27:35');
INSERT INTO `logs` VALUES ('211', 'Super Admin SKA SKT mengubah informasi sertifikat Test - test', 'http://localhost:8000/webApi/certificate/227-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:29:45', '2018-04-19 12:29:45');
INSERT INTO `logs` VALUES ('212', 'Super Admin SKA SKT mengubah informasi sertifikat Test - test', 'http://localhost:8000/webApi/certificate/227-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:31:38', '2018-04-19 12:31:38');
INSERT INTO `logs` VALUES ('213', 'Super Admin SKA SKT mengubah informasi sertifikat Test - test', 'http://localhost:8000/webApi/certificate/227-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:32:02', '2018-04-19 12:32:02');
INSERT INTO `logs` VALUES ('214', 'Super Admin SKA SKT mengubah informasi sertifikat Test - test', 'http://localhost:8000/webApi/certificate/227-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:32:23', '2018-04-19 12:32:23');
INSERT INTO `logs` VALUES ('215', 'Super Admin SKA SKT mengubah informasi sertifikat tesxx - sda', 'http://localhost:8000/webApi/certificate/229-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:34:20', '2018-04-19 12:34:20');
INSERT INTO `logs` VALUES ('216', 'Super Admin SKA SKT mengubah informasi sertifikat tesxx - sda', 'http://localhost:8000/webApi/certificate/229-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:35:03', '2018-04-19 12:35:03');
INSERT INTO `logs` VALUES ('217', 'Super Admin SKA SKT mengubah informasi sertifikat tesxx - sda', 'http://localhost:8000/webApi/certificate/229-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:36:25', '2018-04-19 12:36:25');
INSERT INTO `logs` VALUES ('218', 'Super Admin SKA SKT mengubah informasi sertifikat tesxx - sda', 'http://localhost:8000/webApi/certificate/229-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:36:40', '2018-04-19 12:36:40');
INSERT INTO `logs` VALUES ('219', 'Super Admin SKA SKT mengubah informasi sertifikat tesxx - sda', 'http://localhost:8000/webApi/certificate/229-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:42:22', '2018-04-19 12:42:22');
INSERT INTO `logs` VALUES ('220', 'Super Admin SKA SKT mengubah informasi sertifikat Test - test', 'http://localhost:8000/webApi/certificate/227-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:52:54', '2018-04-19 12:52:54');
INSERT INTO `logs` VALUES ('221', 'Super Admin SKA SKT mengubah informasi sertifikat Test - test', 'http://localhost:8000/webApi/certificate/227-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 12:53:06', '2018-04-19 12:53:06');
INSERT INTO `logs` VALUES ('222', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - 	Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 12:57:18', '2018-04-19 12:57:18');
INSERT INTO `logs` VALUES ('223', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 12:58:22', '2018-04-19 12:58:22');
INSERT INTO `logs` VALUES ('224', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:00:33', '2018-04-19 13:00:33');
INSERT INTO `logs` VALUES ('225', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:01:50', '2018-04-19 13:01:50');
INSERT INTO `logs` VALUES ('226', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:02:34', '2018-04-19 13:02:34');
INSERT INTO `logs` VALUES ('227', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:04:24', '2018-04-19 13:04:24');
INSERT INTO `logs` VALUES ('228', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:06:59', '2018-04-19 13:06:59');
INSERT INTO `logs` VALUES ('229', 'Super Admin SKA SKT mengubah informasi sertifikat 201 - Ahli Teknik Bangunan Gedung', 'http://localhost:8000/webApi/certificate/5-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:07:30', '2018-04-19 13:07:30');
INSERT INTO `logs` VALUES ('230', 'Super Admin SKA SKT mengubah informasi sertifikat 201 - Ahli Teknik Bangunan Gedung', 'http://localhost:8000/webApi/certificate/5-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 13:07:52', '2018-04-19 13:07:52');
INSERT INTO `logs` VALUES ('231', 'Super Admin SKA SKT mengubah informasi sertifikat 205 - Ahli Teknik Terowongan', 'http://localhost:8000/webApi/certificate/9-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36', '6', '2018-04-19 13:08:06', '2018-04-19 13:08:06');
INSERT INTO `logs` VALUES ('232', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:12:45', '2018-04-19 13:12:45');
INSERT INTO `logs` VALUES ('233', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:14:10', '2018-04-19 13:14:10');
INSERT INTO `logs` VALUES ('234', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:19:04', '2018-04-19 13:19:04');
INSERT INTO `logs` VALUES ('235', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:19:45', '2018-04-19 13:19:45');
INSERT INTO `logs` VALUES ('236', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:20:05', '2018-04-19 13:20:05');
INSERT INTO `logs` VALUES ('237', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:22:59', '2018-04-19 13:22:59');
INSERT INTO `logs` VALUES ('238', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:23:40', '2018-04-19 13:23:40');
INSERT INTO `logs` VALUES ('239', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:25:08', '2018-04-19 13:25:08');
INSERT INTO `logs` VALUES ('240', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:25:52', '2018-04-19 13:25:52');
INSERT INTO `logs` VALUES ('241', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:26:30', '2018-04-19 13:26:30');
INSERT INTO `logs` VALUES ('242', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:28:05', '2018-04-19 13:28:05');
INSERT INTO `logs` VALUES ('243', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:28:15', '2018-04-19 13:28:15');
INSERT INTO `logs` VALUES ('244', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:28:22', '2018-04-19 13:28:22');
INSERT INTO `logs` VALUES ('245', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:28:31', '2018-04-19 13:28:31');
INSERT INTO `logs` VALUES ('246', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:28:56', '2018-04-19 13:28:56');
INSERT INTO `logs` VALUES ('247', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-19 13:30:27', '2018-04-19 13:30:27');
INSERT INTO `logs` VALUES ('248', 'Super Admin SKA SKT membuat pembayaran untuk Order MAMA', 'http://localhost:8000/webApi/payment/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 09:17:14', '2018-04-20 09:17:14');
INSERT INTO `logs` VALUES ('249', 'Super Admin SKA SKT mengubah pembayaran penyewaan MAMA', 'http://localhost:8000/webApi/payment/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 09:23:34', '2018-04-20 09:23:34');
INSERT INTO `logs` VALUES ('250', 'Super Admin SKA SKT membuat pembayaran untuk Order MAMA', 'http://localhost:8000/webApi/payment/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 09:26:09', '2018-04-20 09:26:09');
INSERT INTO `logs` VALUES ('251', 'Super Admin SKA SKT membuat pembayaran untuk Order MAMA', 'http://localhost:8000/webApi/payment/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 09:45:25', '2018-04-20 09:45:25');
INSERT INTO `logs` VALUES ('252', 'Super Admin SKA SKT mengubah pembayaran penyewaan MAMA', 'http://localhost:8000/webApi/payment/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 10:01:40', '2018-04-20 10:01:40');
INSERT INTO `logs` VALUES ('253', 'Super Admin SKA SKT update informasi penyewaan 3213223313', 'http://localhost:8000/webApi/rent-status/soft_process/6', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 10:02:00', '2018-04-20 10:02:00');
INSERT INTO `logs` VALUES ('254', 'Super Admin SKA SKT update informasi penyewaan XADDA12313', 'http://localhost:8000/webApi/rent-status/soft_process/9', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 10:07:57', '2018-04-20 10:07:57');
INSERT INTO `logs` VALUES ('255', 'Super Admin SKA SKT update status penyewaan 3213223313 menjadi hardcopy menunggu konfirmasi admin', 'http://localhost:8000/webApi/rent-processhardcopy/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 10:28:14', '2018-04-20 10:28:14');
INSERT INTO `logs` VALUES ('256', 'Super Admin SKA SKT update status penyewaan 3213223313 menjadi hardcopy menunggu konfirmasi admin', 'http://localhost:8000/webApi/rent-processhardcopy/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 10:29:00', '2018-04-20 10:29:00');
INSERT INTO `logs` VALUES ('257', 'Super Admin SKA SKT mengubah pembayaran penyewaan MAMA', 'http://localhost:8000/webApi/payment/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 10:31:12', '2018-04-20 10:31:12');
INSERT INTO `logs` VALUES ('258', 'Super Admin SKA SKT mengubah pembayaran penyewaan MAMA', 'http://localhost:8000/webApi/payment/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 10:31:44', '2018-04-20 10:31:44');
INSERT INTO `logs` VALUES ('259', 'Super Admin SKA SKT update status penyewaan 3213223313 menjadi hardcopy menunggu konfirmasi admin', 'http://localhost:8000/webApi/rent-processhardcopy/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 10:39:06', '2018-04-20 10:39:06');
INSERT INTO `logs` VALUES ('260', 'Super Admin SKA SKT mengubah pembayaran penyewaan MAMA', 'http://localhost:8000/webApi/payment/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 10:40:14', '2018-04-20 10:40:14');
INSERT INTO `logs` VALUES ('261', 'Super Admin SKA SKT mengubah pembayaran penyewaan MAMA', 'http://localhost:8000/webApi/payment/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 10:42:17', '2018-04-20 10:42:17');
INSERT INTO `logs` VALUES ('262', 'Super Admin SKA SKT update status penyewaan 3213223313 menjadi hardcopy diterima penyewa', 'http://localhost:8000/webApi/ordersendhard/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 10:51:11', '2018-04-20 10:51:11');
INSERT INTO `logs` VALUES ('263', 'Super Admin SKA SKT update status penyewaan 3213223313 menjadi hardcopy diterima penyewa', 'http://localhost:8000/webApi/ordersendhard/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 11:04:51', '2018-04-20 11:04:51');
INSERT INTO `logs` VALUES ('264', 'Super Admin SKA SKT update status penyewaan 3213223313 menjadi hardcopy diterima penyewa', 'http://localhost:8000/webApi/ordersendhard/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 11:06:37', '2018-04-20 11:06:37');
INSERT INTO `logs` VALUES ('265', 'Super Admin SKA SKT update status penyewaan 3213223313 menjadi hardcopy diterima penyewa', 'http://localhost:8000/webApi/ordersendhard/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 11:35:39', '2018-04-20 11:35:39');
INSERT INTO `logs` VALUES ('266', 'Super Admin SKA SKT update status penyewaan 3213223313 menjadi hardcopy diterima penyewa', 'http://localhost:8000/webApi/ordersendhard/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 12:48:06', '2018-04-20 12:48:06');
INSERT INTO `logs` VALUES ('267', 'Super Admin SKA SKT update status penyewaan 3213223313 menjadi hardcopy diterima penyewa', 'http://localhost:8000/webApi/ordersendhard/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 12:50:06', '2018-04-20 12:50:06');
INSERT INTO `logs` VALUES ('268', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 13:06:37', '2018-04-20 13:06:37');
INSERT INTO `logs` VALUES ('269', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 13:06:54', '2018-04-20 13:06:54');
INSERT INTO `logs` VALUES ('270', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 13:07:41', '2018-04-20 13:07:41');
INSERT INTO `logs` VALUES ('271', 'Super Admin SKA SKT mengubah informasi sertifikat 101 - Arsitek', 'http://localhost:8000/webApi/certificate/1-soft_sended', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 13:11:24', '2018-04-20 13:11:24');
INSERT INTO `logs` VALUES ('272', 'Super Admin SKA SKT membuat pembayaran untuk Order MAMA', 'http://localhost:8000/webApi/payment/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 15:54:11', '2018-04-20 15:54:11');
INSERT INTO `logs` VALUES ('273', 'Super Admin SKA SKT mengubah pembayaran penyewaan MAMA', 'http://localhost:8000/webApi/payment/2', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-20 15:54:23', '2018-04-20 15:54:23');
INSERT INTO `logs` VALUES ('274', 'renter company A Memasukkan sertifikat Ahli Teknik Bangunan Gedung kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '2', '2018-04-21 11:02:34', '2018-04-21 11:02:34');
INSERT INTO `logs` VALUES ('275', 'renter company A checkout sertifikat, Order INV20180421110518 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '2', '2018-04-21 11:05:18', '2018-04-21 11:05:18');
INSERT INTO `logs` VALUES ('276', 'renter company A menghapus order INV20180406131443', 'http://localhost:8000/webApi/order/7', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '2', '2018-04-21 11:06:45', '2018-04-21 11:06:45');
INSERT INTO `logs` VALUES ('277', 'renter company A menghapus order INV20180410114052', 'http://localhost:8000/webApi/order/9', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '2', '2018-04-21 11:06:50', '2018-04-21 11:06:50');
INSERT INTO `logs` VALUES ('278', 'Super Admin SKA SKT membuat pembayaran untuk Order INV20180421110518', 'http://localhost:8000/webApi/payment/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-21 11:38:44', '2018-04-21 11:38:44');
INSERT INTO `logs` VALUES ('279', 'Super Admin SKA SKT update informasi penyewaan SN3123.23123..223', 'http://localhost:8000/webApi/rent-status/soft_process/19', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-21 11:39:17', '2018-04-21 11:39:17');
INSERT INTO `logs` VALUES ('280', 'Super Admin SKA SKT update status penyewaan SN3123.23123..223 menjadi hardcopy menunggu konfirmasi admin', 'http://localhost:8000/webApi/rent-processhardcopy/10', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-21 11:40:36', '2018-04-21 11:40:36');
INSERT INTO `logs` VALUES ('281', 'Super Admin SKA SKT update status penyewaan SN3123.23123..223 menjadi hardcopy menunggu konfirmasi admin', 'http://localhost:8000/webApi/rent-processhardcopy/10', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-21 11:42:22', '2018-04-21 11:42:22');
INSERT INTO `logs` VALUES ('282', 'Super Admin SKA SKT mengubah pembayaran penyewaan INV20180421110518', 'http://localhost:8000/webApi/payment/10', 'PATCH', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', '6', '2018-04-21 11:43:20', '2018-04-21 11:43:20');
INSERT INTO `logs` VALUES ('283', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-07 21:44:01', '2018-05-07 21:44:01');
INSERT INTO `logs` VALUES ('284', 'renter company A Memasukkan sertifikat 	Ahli Teknik Sistem Tata Udara dan Refrigerasi kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-07 21:44:52', '2018-05-07 21:44:52');
INSERT INTO `logs` VALUES ('285', 'renter company A Memasukkan sertifikat 	Ahli Teknik Transportasi Dalam Gedung kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 02:12:24', '2018-05-08 02:12:24');
INSERT INTO `logs` VALUES ('286', 'renter company A Memasukkan sertifikat Ahli Teknik Proteksi Kebakaran kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 02:21:45', '2018-05-08 02:21:45');
INSERT INTO `logs` VALUES ('287', 'renter company A checkout sertifikat, Order INV20180508023611 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 02:36:11', '2018-05-08 02:36:11');
INSERT INTO `logs` VALUES ('288', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 02:38:52', '2018-05-08 02:38:52');
INSERT INTO `logs` VALUES ('289', 'renter company A checkout sertifikat, Order INV20180508023903 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 02:39:03', '2018-05-08 02:39:03');
INSERT INTO `logs` VALUES ('290', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 02:39:20', '2018-05-08 02:39:20');
INSERT INTO `logs` VALUES ('291', 'renter company A checkout sertifikat, Order INV20180508024047 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 02:40:47', '2018-05-08 02:40:47');
INSERT INTO `logs` VALUES ('292', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 02:43:52', '2018-05-08 02:43:52');
INSERT INTO `logs` VALUES ('293', 'renter company A checkout sertifikat, Order INV20180508024500 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 02:45:00', '2018-05-08 02:45:00');
INSERT INTO `logs` VALUES ('294', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 02:45:06', '2018-05-08 02:45:06');
INSERT INTO `logs` VALUES ('295', 'renter company A checkout sertifikat, Order INV20180508024513 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 02:45:13', '2018-05-08 02:45:13');
INSERT INTO `logs` VALUES ('296', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 02:45:56', '2018-05-08 02:45:56');
INSERT INTO `logs` VALUES ('297', 'renter company A checkout sertifikat, Order INV20180508024601 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 02:46:01', '2018-05-08 02:46:01');
INSERT INTO `logs` VALUES ('298', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 03:02:05', '2018-05-08 03:02:05');
INSERT INTO `logs` VALUES ('299', 'renter company A checkout sertifikat, Order INV20180508030210 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 03:02:10', '2018-05-08 03:02:10');
INSERT INTO `logs` VALUES ('300', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 03:02:38', '2018-05-08 03:02:38');
INSERT INTO `logs` VALUES ('301', 'renter company A checkout sertifikat, Order INV20180508030243 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 03:02:44', '2018-05-08 03:02:44');
INSERT INTO `logs` VALUES ('302', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 03:04:21', '2018-05-08 03:04:21');
INSERT INTO `logs` VALUES ('303', 'renter company A checkout sertifikat, Order INV20180508030427 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 03:04:27', '2018-05-08 03:04:27');
INSERT INTO `logs` VALUES ('304', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 03:04:42', '2018-05-08 03:04:42');
INSERT INTO `logs` VALUES ('305', 'renter company A Memasukkan sertifikat 	Ahli Teknik Sistem Tata Udara dan Refrigerasi kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 03:04:50', '2018-05-08 03:04:50');
INSERT INTO `logs` VALUES ('306', 'renter company A checkout sertifikat, Order INV20180508030456 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 03:04:56', '2018-05-08 03:04:56');
INSERT INTO `logs` VALUES ('307', 'renter company A Memasukkan sertifikat 	Ahli Teknik Sistem Tata Udara dan Refrigerasi kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 03:05:21', '2018-05-08 03:05:21');
INSERT INTO `logs` VALUES ('308', 'renter company A Memasukkan sertifikat Ahli Teknik Proteksi Kebakaran kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 03:05:52', '2018-05-08 03:05:52');
INSERT INTO `logs` VALUES ('309', 'renter company A Memasukkan sertifikat Ahli Teknik Plambing dan Pompa Mekanik kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 03:10:05', '2018-05-08 03:10:05');
INSERT INTO `logs` VALUES ('310', 'Super Admin SKA SKT membuat sertifikat 321 - Ksda', 'http://localhost:8000/webApi/certificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '6', '2018-05-08 04:03:45', '2018-05-08 04:03:45');
INSERT INTO `logs` VALUES ('311', 'renter company A checkout sertifikat, Order INV20180508065206 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 06:52:07', '2018-05-08 06:52:07');
INSERT INTO `logs` VALUES ('312', 'Super Admin SKA SKT membuat sertifikat asd123123 - asdasd', 'http://localhost:8000/webApi/usercertificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '6', '2018-05-08 06:59:11', '2018-05-08 06:59:11');
INSERT INTO `logs` VALUES ('313', 'Super Admin SKA SKT menghapus sertifikat asd123123 - asdasd', 'http://localhost:8000/webApi/usercertificate/2', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '6', '2018-05-08 07:26:50', '2018-05-08 07:26:50');
INSERT INTO `logs` VALUES ('314', 'Super Admin SKA SKT membuat sertifikat xasd - 13asd', 'http://localhost:8000/webApi/usercertificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '6', '2018-05-08 07:31:27', '2018-05-08 07:31:27');
INSERT INTO `logs` VALUES ('315', 'Super Admin SKA SKT membuat sertifikat xasd - asd', 'http://localhost:8000/webApi/usercertificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '6', '2018-05-08 07:36:20', '2018-05-08 07:36:20');
INSERT INTO `logs` VALUES ('316', 'Super Admin SKA SKT membuat sertifikat xasd - asdsd', 'http://localhost:8000/webApi/usercertificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '6', '2018-05-08 07:37:10', '2018-05-08 07:37:10');
INSERT INTO `logs` VALUES ('317', 'Super Admin SKA SKT membuat sertifikat xasd - asdas', 'http://localhost:8000/webApi/usercertificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '6', '2018-05-08 07:37:59', '2018-05-08 07:37:59');
INSERT INTO `logs` VALUES ('318', 'Super Admin SKA SKT membuat sertifikat 31232aasdx.asdasa - asdasd', 'http://localhost:8000/webApi/usercertificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '6', '2018-05-08 07:41:10', '2018-05-08 07:41:10');
INSERT INTO `logs` VALUES ('319', 'Super Admin SKA SKT membuat sertifikat asd123123 - asdasd', 'http://localhost:8000/webApi/usercertificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '6', '2018-05-08 08:45:17', '2018-05-08 08:45:17');
INSERT INTO `logs` VALUES ('320', 'Super Admin SKA SKT membuat sertifikat asd123123 - asdxx', 'http://localhost:8000/webApi/usercertificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '6', '2018-05-08 08:51:34', '2018-05-08 08:51:34');
INSERT INTO `logs` VALUES ('321', 'Super Admin SKA SKT membuat sertifikat asd123123 - asd', 'http://localhost:8000/webApi/usercertificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '6', '2018-05-08 08:53:31', '2018-05-08 08:53:31');
INSERT INTO `logs` VALUES ('322', 'Super Admin SKA SKT menghapus sertifikat asd123123 - asd', 'http://localhost:8000/webApi/usercertificate/13', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '6', '2018-05-08 08:55:15', '2018-05-08 08:55:15');
INSERT INTO `logs` VALUES ('323', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 09:09:53', '2018-05-08 09:09:53');
INSERT INTO `logs` VALUES ('324', 'renter company A Memasukkan sertifikat Ahli Teknik Plambing dan Pompa Mekanik kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 09:10:03', '2018-05-08 09:10:03');
INSERT INTO `logs` VALUES ('325', 'renter company A Memasukkan sertifikat Ahli Teknik Plambing dan Pompa Mekanik kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 09:11:52', '2018-05-08 09:11:52');
INSERT INTO `logs` VALUES ('326', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 09:11:58', '2018-05-08 09:11:58');
INSERT INTO `logs` VALUES ('327', 'renter company A checkout sertifikat, Order INV20180508091205 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 09:12:05', '2018-05-08 09:12:05');
INSERT INTO `logs` VALUES ('328', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 10:09:01', '2018-05-08 10:09:01');
INSERT INTO `logs` VALUES ('329', 'renter company A Memasukkan sertifikat 	Ahli Teknik Sistem Tata Udara dan Refrigerasi kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 10:32:35', '2018-05-08 10:32:35');
INSERT INTO `logs` VALUES ('330', 'renter company A Memasukkan sertifikat 	Ahli Teknik Transportasi Dalam Gedung kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 10:32:41', '2018-05-08 10:32:41');
INSERT INTO `logs` VALUES ('331', 'renter company A Memasukkan sertifikat Operator Crowler Crane kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-08 10:32:49', '2018-05-08 10:32:49');
INSERT INTO `logs` VALUES ('332', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 04:26:42', '2018-05-10 04:26:42');
INSERT INTO `logs` VALUES ('333', 'renter company A Memasukkan sertifikat 	Ahli Teknik Transportasi Dalam Gedung kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 04:28:22', '2018-05-10 04:28:22');
INSERT INTO `logs` VALUES ('334', 'renter company A Memasukkan sertifikat Operator Pile Hammer kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 10:32:35', '2018-05-10 10:32:35');
INSERT INTO `logs` VALUES ('335', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/22', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 10:37:29', '2018-05-10 10:37:29');
INSERT INTO `logs` VALUES ('336', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/23', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 10:37:32', '2018-05-10 10:37:32');
INSERT INTO `logs` VALUES ('337', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/24', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 10:37:34', '2018-05-10 10:37:34');
INSERT INTO `logs` VALUES ('338', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/25', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 10:37:37', '2018-05-10 10:37:37');
INSERT INTO `logs` VALUES ('339', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/26', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 10:37:43', '2018-05-10 10:37:43');
INSERT INTO `logs` VALUES ('340', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/27', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 10:37:47', '2018-05-10 10:37:47');
INSERT INTO `logs` VALUES ('341', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/28', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 10:37:50', '2018-05-10 10:37:50');
INSERT INTO `logs` VALUES ('342', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 10:50:28', '2018-05-10 10:50:28');
INSERT INTO `logs` VALUES ('343', 'renter company A Memasukkan sertifikat 	Ahli Teknik Sistem Tata Udara dan Refrigerasi kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 10:51:38', '2018-05-10 10:51:38');
INSERT INTO `logs` VALUES ('344', 'renter company A Memasukkan sertifikat 	Ahli Teknik Transportasi Dalam Gedung kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 10:54:25', '2018-05-10 10:54:25');
INSERT INTO `logs` VALUES ('345', 'renter company A Memasukkan sertifikat Ahli Teknik Plambing dan Pompa Mekanik kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 10:54:53', '2018-05-10 10:54:53');
INSERT INTO `logs` VALUES ('346', 'renter company A Memasukkan sertifikat Ahli Teknik Proteksi Kebakaran kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:07:40', '2018-05-10 11:07:40');
INSERT INTO `logs` VALUES ('347', 'renter company A checkout sertifikat, Order INV20180510110839 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:08:40', '2018-05-10 11:08:40');
INSERT INTO `logs` VALUES ('348', 'renter company A checkout sertifikat, Order INV20180510111706 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:17:06', '2018-05-10 11:17:06');
INSERT INTO `logs` VALUES ('349', 'renter company A Memasukkan sertifikat Ahli Teknik Plambing dan Pompa Mekanik kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:25:56', '2018-05-10 11:25:56');
INSERT INTO `logs` VALUES ('350', 'renter company A checkout sertifikat, Order INV20180510112614 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:26:15', '2018-05-10 11:26:15');
INSERT INTO `logs` VALUES ('351', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:44:23', '2018-05-10 11:44:23');
INSERT INTO `logs` VALUES ('352', 'renter company A Memasukkan sertifikat Juru gambar / Draftman ? Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:44:31', '2018-05-10 11:44:31');
INSERT INTO `logs` VALUES ('353', 'renter company A Memasukkan sertifikat Operator Tangga Intake Dam kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:44:52', '2018-05-10 11:44:52');
INSERT INTO `logs` VALUES ('354', 'renter company A Memasukkan sertifikat Ahli Teknik Proteksi Kebakaran kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:46:53', '2018-05-10 11:46:53');
INSERT INTO `logs` VALUES ('355', 'renter company A Memasukkan sertifikat Juru gambar / Draftman ? Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:46:57', '2018-05-10 11:46:57');
INSERT INTO `logs` VALUES ('356', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:50:42', '2018-05-10 11:50:42');
INSERT INTO `logs` VALUES ('357', 'renter company A Memasukkan sertifikat 	Ahli Teknik Sistem Tata Udara dan Refrigerasi kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:51:36', '2018-05-10 11:51:36');
INSERT INTO `logs` VALUES ('358', 'renter company A Memasukkan sertifikat Juru gambar / Draftman ? Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:51:44', '2018-05-10 11:51:44');
INSERT INTO `logs` VALUES ('359', 'renter company A Memasukkan sertifikat Operator Bulldozer kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:51:53', '2018-05-10 11:51:53');
INSERT INTO `logs` VALUES ('360', 'renter company A Memasukkan sertifikat Ahli Teknik Proteksi Kebakaran kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:53:34', '2018-05-10 11:53:34');
INSERT INTO `logs` VALUES ('361', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:54:45', '2018-05-10 11:54:45');
INSERT INTO `logs` VALUES ('362', 'renter company A Memasukkan sertifikat Ahli Teknik Proteksi Kebakaran kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:56:08', '2018-05-10 11:56:08');
INSERT INTO `logs` VALUES ('363', 'renter company A Memasukkan sertifikat Operator Mesin Excavator kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 11:56:14', '2018-05-10 11:56:14');
INSERT INTO `logs` VALUES ('364', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/41', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:01:10', '2018-05-10 12:01:10');
INSERT INTO `logs` VALUES ('365', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/40', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:01:14', '2018-05-10 12:01:14');
INSERT INTO `logs` VALUES ('366', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/38', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:01:17', '2018-05-10 12:01:17');
INSERT INTO `logs` VALUES ('367', 'renter company A Memasukkan sertifikat 	Ahli Teknik Transportasi Dalam Gedung kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:01:31', '2018-05-10 12:01:31');
INSERT INTO `logs` VALUES ('368', 'renter company A Memasukkan sertifikat 	Ahli Teknik Sistem Tata Udara dan Refrigerasi kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:01:36', '2018-05-10 12:01:36');
INSERT INTO `logs` VALUES ('369', 'renter company A Memasukkan sertifikat Operator Motor Grader kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:01:41', '2018-05-10 12:01:41');
INSERT INTO `logs` VALUES ('370', 'renter company A Memasukkan sertifikat Ahli Teknik Proteksi Kebakaran kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:01:49', '2018-05-10 12:01:49');
INSERT INTO `logs` VALUES ('371', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/39', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:10:40', '2018-05-10 12:10:40');
INSERT INTO `logs` VALUES ('372', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/42', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:10:43', '2018-05-10 12:10:43');
INSERT INTO `logs` VALUES ('373', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/43', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:10:47', '2018-05-10 12:10:47');
INSERT INTO `logs` VALUES ('374', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/44', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:10:50', '2018-05-10 12:10:50');
INSERT INTO `logs` VALUES ('375', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/45', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:10:55', '2018-05-10 12:10:55');
INSERT INTO `logs` VALUES ('376', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:11:03', '2018-05-10 12:11:03');
INSERT INTO `logs` VALUES ('377', 'renter company A Memasukkan sertifikat Juru gambar / Draftman ? Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:11:08', '2018-05-10 12:11:08');
INSERT INTO `logs` VALUES ('378', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:20:30', '2018-05-10 12:20:30');
INSERT INTO `logs` VALUES ('379', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:30:10', '2018-05-10 12:30:10');
INSERT INTO `logs` VALUES ('380', 'renter company A Memasukkan sertifikat Ahli Teknik Proteksi Kebakaran kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:45:55', '2018-05-10 12:45:55');
INSERT INTO `logs` VALUES ('381', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/47', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:47:24', '2018-05-10 12:47:24');
INSERT INTO `logs` VALUES ('382', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/46', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:47:27', '2018-05-10 12:47:27');
INSERT INTO `logs` VALUES ('383', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:47:35', '2018-05-10 12:47:35');
INSERT INTO `logs` VALUES ('384', 'renter company A Memasukkan sertifikat Ahli Teknik Plambing dan Pompa Mekanik kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:49:40', '2018-05-10 12:49:40');
INSERT INTO `logs` VALUES ('385', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/48', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:50:37', '2018-05-10 12:50:37');
INSERT INTO `logs` VALUES ('386', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/49', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:50:39', '2018-05-10 12:50:39');
INSERT INTO `logs` VALUES ('387', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:50:48', '2018-05-10 12:50:48');
INSERT INTO `logs` VALUES ('388', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/50', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:53:15', '2018-05-10 12:53:15');
INSERT INTO `logs` VALUES ('389', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/51', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:53:20', '2018-05-10 12:53:20');
INSERT INTO `logs` VALUES ('390', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:53:49', '2018-05-10 12:53:49');
INSERT INTO `logs` VALUES ('391', 'renter company A Memasukkan sertifikat Ahli Teknik Proteksi Kebakaran kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:55:27', '2018-05-10 12:55:27');
INSERT INTO `logs` VALUES ('392', 'renter company A menghapus request ', 'http://localhost:8000/webApi/cart-request/undefined', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:55:30', '2018-05-10 12:55:30');
INSERT INTO `logs` VALUES ('393', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/52', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:56:51', '2018-05-10 12:56:51');
INSERT INTO `logs` VALUES ('394', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/53', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:56:54', '2018-05-10 12:56:54');
INSERT INTO `logs` VALUES ('395', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:57:00', '2018-05-10 12:57:00');
INSERT INTO `logs` VALUES ('396', 'renter company A menghapus request ', 'http://localhost:8000/webApi/cart-request/undefined', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:57:04', '2018-05-10 12:57:04');
INSERT INTO `logs` VALUES ('397', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:57:44', '2018-05-10 12:57:44');
INSERT INTO `logs` VALUES ('398', 'renter company A menghapus request ', 'http://localhost:8000/webApi/cart-request/undefined', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:57:47', '2018-05-10 12:57:47');
INSERT INTO `logs` VALUES ('399', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/54', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:58:11', '2018-05-10 12:58:11');
INSERT INTO `logs` VALUES ('400', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/55', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:58:16', '2018-05-10 12:58:16');
INSERT INTO `logs` VALUES ('401', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:58:21', '2018-05-10 12:58:21');
INSERT INTO `logs` VALUES ('402', 'renter company A menghapus request ', 'http://localhost:8000/webApi/cart-request/39', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 12:58:25', '2018-05-10 12:58:25');
INSERT INTO `logs` VALUES ('403', 'renter company A Memasukkan sertifikat Ahli Teknik Plambing dan Pompa Mekanik kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:00:36', '2018-05-10 13:00:36');
INSERT INTO `logs` VALUES ('404', 'renter company A menghapus request ', 'http://localhost:8000/webApi/cart-request/40', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:00:39', '2018-05-10 13:00:39');
INSERT INTO `logs` VALUES ('405', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:01:46', '2018-05-10 13:01:46');
INSERT INTO `logs` VALUES ('406', 'renter company A menghapus request ', 'http://localhost:8000/webApi/cart-request/41', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:01:49', '2018-05-10 13:01:49');
INSERT INTO `logs` VALUES ('407', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:05:08', '2018-05-10 13:05:08');
INSERT INTO `logs` VALUES ('408', 'renter company A menghapus request ', 'http://localhost:8000/webApi/cart-request/42', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:05:10', '2018-05-10 13:05:10');
INSERT INTO `logs` VALUES ('409', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:09:06', '2018-05-10 13:09:06');
INSERT INTO `logs` VALUES ('410', 'renter company A menghapus request ', 'http://localhost:8000/webApi/cart-request/43', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:09:10', '2018-05-10 13:09:10');
INSERT INTO `logs` VALUES ('411', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:09:57', '2018-05-10 13:09:57');
INSERT INTO `logs` VALUES ('412', 'renter company A menghapus request ', 'http://localhost:8000/webApi/cart-request/44', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:09:59', '2018-05-10 13:09:59');
INSERT INTO `logs` VALUES ('413', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:11:05', '2018-05-10 13:11:05');
INSERT INTO `logs` VALUES ('414', 'renter company A menghapus request ', 'http://localhost:8000/webApi/cart-request/45', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:11:07', '2018-05-10 13:11:07');
INSERT INTO `logs` VALUES ('415', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:16:27', '2018-05-10 13:16:27');
INSERT INTO `logs` VALUES ('416', 'renter company A menghapus request ', 'http://localhost:8000/webApi/cart-request/47', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:16:30', '2018-05-10 13:16:30');
INSERT INTO `logs` VALUES ('417', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:16:32', '2018-05-10 13:16:32');
INSERT INTO `logs` VALUES ('418', 'renter company A menghapus request ', 'http://localhost:8000/webApi/cart-request/48', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:16:34', '2018-05-10 13:16:34');
INSERT INTO `logs` VALUES ('419', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:16:37', '2018-05-10 13:16:37');
INSERT INTO `logs` VALUES ('420', 'renter company A menghapus request ', 'http://localhost:8000/webApi/cart-request/49', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:16:38', '2018-05-10 13:16:38');
INSERT INTO `logs` VALUES ('421', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:16:40', '2018-05-10 13:16:40');
INSERT INTO `logs` VALUES ('422', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/67', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:16:45', '2018-05-10 13:16:45');
INSERT INTO `logs` VALUES ('423', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:23:13', '2018-05-10 13:23:13');
INSERT INTO `logs` VALUES ('424', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/68', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:25:28', '2018-05-10 13:25:28');
INSERT INTO `logs` VALUES ('425', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:26:12', '2018-05-10 13:26:12');
INSERT INTO `logs` VALUES ('426', 'renter company A menghapus request ', 'http://localhost:8000/webApi/cart-request/52', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:26:21', '2018-05-10 13:26:21');
INSERT INTO `logs` VALUES ('427', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:26:33', '2018-05-10 13:26:33');
INSERT INTO `logs` VALUES ('428', 'renter company A menghapus cart ', 'http://localhost:8000/webApi/cart/70', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:26:38', '2018-05-10 13:26:38');
INSERT INTO `logs` VALUES ('429', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:47:25', '2018-05-10 13:47:25');
INSERT INTO `logs` VALUES ('430', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:47:35', '2018-05-10 13:47:35');
INSERT INTO `logs` VALUES ('431', 'renter company A Memasukkan sertifikat 	Ahli Teknik Sistem Tata Udara dan Refrigerasi kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:47:39', '2018-05-10 13:47:39');
INSERT INTO `logs` VALUES ('432', 'renter company A checkout sertifikat, Order INV20180510134755 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:47:55', '2018-05-10 13:47:55');
INSERT INTO `logs` VALUES ('433', 'renter company A membatalkan order INV20180510134755', 'http://localhost:8000/webApi/ordercancel/35?_=1525934880430', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:51:44', '2018-05-10 13:51:44');
INSERT INTO `logs` VALUES ('434', 'renter company A menghapus order INV20180510134755', 'http://localhost:8000/webApi/order/35', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 13:51:51', '2018-05-10 13:51:51');
INSERT INTO `logs` VALUES ('435', 'renter company A Memasukkan sertifikat Ahli Teknik Mekanikal kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 14:25:37', '2018-05-10 14:25:37');
INSERT INTO `logs` VALUES ('436', 'renter company A Memasukkan sertifikat 	Ahli Teknik Sistem Tata Udara dan Refrigerasi kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 14:25:44', '2018-05-10 14:25:44');
INSERT INTO `logs` VALUES ('437', 'renter company A checkout sertifikat, Order INV20180510142556 Berhasil dibuat', 'http://localhost:8000/webApi/checkout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-10 14:25:56', '2018-05-10 14:25:56');
INSERT INTO `logs` VALUES ('438', 'renter company A Memasukkan sertifikat Arsitek kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-16 20:21:04', '2018-05-16 20:21:04');
INSERT INTO `logs` VALUES ('439', 'renter company A Memasukkan sertifikat Ahli Desain Interior kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-16 20:21:44', '2018-05-16 20:21:44');
INSERT INTO `logs` VALUES ('440', 'renter company A Memasukkan sertifikat Teknik Iluminasi kedalam cart', 'http://localhost:8000/webApi/cart/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', '2', '2018-05-16 20:28:50', '2018-05-16 20:28:50');
INSERT INTO `logs` VALUES ('441', 'Super Admin SKA SKT membuat sertifikat 312.123/123.31232.123 - Ada', 'http://localhost:8000/webApi/usercertificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', '6', '2018-06-05 14:45:19', '2018-06-05 14:45:19');
INSERT INTO `logs` VALUES ('442', 'Super Admin SKA SKT membuat sertifikat 4343.123.323123 - sda', 'http://localhost:8000/webApi/usercertificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', '6', '2018-06-05 14:47:53', '2018-06-05 14:47:53');
INSERT INTO `logs` VALUES ('443', 'Super Admin SKA SKT menghapus sertifikat 312.123/123.31232.123 - Ada', 'http://localhost:8000/webApi/usercertificate/4', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', '6', '2018-06-05 14:52:38', '2018-06-05 14:52:38');
INSERT INTO `logs` VALUES ('444', 'Super Admin SKA SKT menghapus sertifikat 32.23132.312 - renter company A', 'http://localhost:8000/webApi/usercertificate/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', '6', '2018-06-05 14:52:42', '2018-06-05 14:52:42');
INSERT INTO `logs` VALUES ('445', 'Super Admin SKA SKT menghapus sertifikat 4343.123.323123 - sda', 'http://localhost:8000/webApi/usercertificate/5', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', '6', '2018-06-05 14:52:46', '2018-06-05 14:52:46');
INSERT INTO `logs` VALUES ('446', 'Super Admin SKA SKT membuat sertifikat 312.123/123.31232.123 - sda', 'http://localhost:8000/webApi/usercertificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', '6', '2018-06-05 14:53:47', '2018-06-05 14:53:47');
INSERT INTO `logs` VALUES ('447', 'Super Admin SKA SKT membuat sertifikat asd123123 - test', 'http://localhost:8000/webApi/usercertificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', '6', '2018-06-05 15:00:32', '2018-06-05 15:00:32');
INSERT INTO `logs` VALUES ('448', 'Super Admin SKA SKT membuat sertifikat 123ss12322 - sda', 'http://localhost:8000/webApi/usercertificate/create', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', '6', '2018-06-05 15:01:27', '2018-06-05 15:01:27');

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `last_message` text,
  `is_read` tinyint(1) DEFAULT '0',
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of messages
-- ----------------------------

-- ----------------------------
-- Table structure for message_details
-- ----------------------------
DROP TABLE IF EXISTS `message_details`;
CREATE TABLE `message_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned DEFAULT NULL,
  `message` text,
  `sender_id` int(10) unsigned DEFAULT NULL,
  `receiver_id` int(10) unsigned DEFAULT NULL,
  `is_read` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`),
  KEY `receiver_id` (`receiver_id`),
  KEY `message_id` (`message_id`),
  CONSTRAINT `message_details_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `message_details_ibfk_2` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `message_details_ibfk_3` FOREIGN KEY (`message_id`) REFERENCES `messages` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of message_details
-- ----------------------------

-- ----------------------------
-- Table structure for notifications
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `body` varchar(255) DEFAULT NULL,
  `data_type` varchar(255) DEFAULT NULL,
  `data_id` int(11) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of notifications
-- ----------------------------
INSERT INTO `notifications` VALUES ('1', 'test', 'Halo renterA', '', null, 'renter company A', '2', '2018-04-13 13:01:29', '2018-04-13 13:01:29');
INSERT INTO `notifications` VALUES ('2', 'Test kedua', 'Halo renter', null, null, 'renter company', '2', '2018-04-13 13:24:59', '2018-04-13 13:24:59');
INSERT INTO `notifications` VALUES ('3', 'Kirim dari Web', 'test dari web', null, null, 'android', null, '2018-04-13 17:25:05', '2018-04-13 17:25:05');
INSERT INTO `notifications` VALUES ('4', 'Test dari web', 'Halo', null, null, 'android', null, '2018-04-13 17:29:01', '2018-04-13 17:29:01');
INSERT INTO `notifications` VALUES ('5', 'test', 'qwe2', null, null, 'android', null, '2018-04-13 17:31:25', '2018-04-13 17:31:25');
INSERT INTO `notifications` VALUES ('6', 'Order Diterima', 'Order INV20180405161354 telah diterima oleh admin', 'order_detail', '5', 'renter company A', '2', '2018-04-13 20:54:10', '2018-04-13 20:54:10');
INSERT INTO `notifications` VALUES ('7', 'Order Diterima', 'Order INV20180405161354 telah diterima oleh admin', 'order_detail', '5', 'renter company A', '2', '2018-04-13 20:56:11', '2018-04-13 20:56:11');
INSERT INTO `notifications` VALUES ('8', 'Sewa Sertifikat Dibatalkan', 'Sertifikat  telah dibatalkan', 'rent_detail', '16', 'renter company A', '2', '2018-04-14 05:33:51', '2018-04-14 05:33:51');
INSERT INTO `notifications` VALUES ('9', 'Order Diterima', 'Order INV20180406153139 telah diterima oleh admin', 'order_detail', '8', 'renter company A', '2', '2018-04-14 16:14:02', '2018-04-14 16:14:02');
INSERT INTO `notifications` VALUES ('10', 'Pembayaran Softcopy', 'Pembayaran Softcopy Order INV20180406153139 telah diverifikasi oleh admin ', 'order_detail', '8', 'renter company A', '2', '2018-04-14 16:14:19', '2018-04-14 16:14:19');
INSERT INTO `notifications` VALUES ('11', 'Soft copy Sertifikat Dikirim', 'Soft copy sertifikat  telah dikirim', 'rent_detail', '16', 'renter company A', '2', '2018-04-14 16:14:43', '2018-04-14 16:14:43');
INSERT INTO `notifications` VALUES ('12', 'Order Diterima', 'Order INV20180405161511 telah diterima oleh admin', 'order_detail', '6', 'renter company A', '2', '2018-04-15 17:45:58', '2018-04-15 17:45:58');
INSERT INTO `notifications` VALUES ('13', 'Pembayaran Softcopy', 'Pembayaran Softcopy Order INV20180405161511 telah diverifikasi oleh admin ', 'order_detail', '6', 'renter company A', '2', '2018-04-15 17:46:45', '2018-04-15 17:46:45');
INSERT INTO `notifications` VALUES ('14', 'Soft copy Sertifikat Dikirim', 'Soft copy sertifikat  telah dikirim', 'rent_detail', '15', 'renter company A', '2', '2018-04-15 17:49:48', '2018-04-15 17:49:48');
INSERT INTO `notifications` VALUES ('15', 'Pembayaran Hardcopy', 'Pembayaran Hardcopy Order INV20180405161511 telah diverifikasi oleh admin ', 'order_detail', '6', 'renter company A', '2', '2018-04-15 17:50:40', '2018-04-15 17:50:40');
INSERT INTO `notifications` VALUES ('16', 'Soft copy Sertifikat Dikirim', 'Soft copy sertifikat  telah dikirim', 'rent_detail', '15', 'renter company A', '2', '2018-04-15 17:53:31', '2018-04-15 17:53:31');
INSERT INTO `notifications` VALUES ('17', 'Order Diterima', 'Order MAMA telah diterima oleh admin', 'order_detail', '2', 'renter company B', '3', '2018-04-20 09:17:14', '2018-04-20 09:17:14');
INSERT INTO `notifications` VALUES ('18', 'Order Diterima', 'Order MAMA telah diterima oleh admin', 'order_detail', '2', 'renter company B', '3', '2018-04-20 09:23:34', '2018-04-20 09:23:34');
INSERT INTO `notifications` VALUES ('19', 'Order Diterima', 'Order MAMA telah diterima oleh admin', 'order_detail', '2', 'renter company B', '3', '2018-04-20 09:26:09', '2018-04-20 09:26:09');
INSERT INTO `notifications` VALUES ('20', 'Softcopy dikirim', 'Sertifikat Soft copy untuk orderINV20180406153139 telah telah dikirim ', 'order_detail', '8', 'renter company A', '2', '2018-04-20 09:30:38', '2018-04-20 09:30:38');
INSERT INTO `notifications` VALUES ('21', 'Softcopy dikirim', 'Sertifikat Soft copy untuk orderINV20180406153139 telah telah dikirim ', 'order_detail', '8', 'renter company A', '2', '2018-04-20 09:32:25', '2018-04-20 09:32:25');
INSERT INTO `notifications` VALUES ('22', 'Softcopy dikirim', 'Sertifikat Soft copy untuk orderINV20180406153139 telah telah dikirim ', 'order_detail', '8', 'renter company A', '2', '2018-04-20 09:35:17', '2018-04-20 09:35:17');
INSERT INTO `notifications` VALUES ('23', 'Order Diterima', 'Order MAMA telah diterima oleh admin', 'order_detail', '2', 'renter company A', '2', '2018-04-20 09:45:25', '2018-04-20 09:45:25');
INSERT INTO `notifications` VALUES ('24', 'Pembayaran Softcopy', 'Pembayaran Softcopy Order MAMA telah diverifikasi oleh admin ', 'order_detail', '2', 'renter company A', '2', '2018-04-20 10:01:43', '2018-04-20 10:01:43');
INSERT INTO `notifications` VALUES ('25', 'Order Diterima', 'Order MAMA telah diterima oleh admin', 'order_detail', '2', 'renter company A', '2', '2018-04-20 10:31:17', '2018-04-20 10:31:17');
INSERT INTO `notifications` VALUES ('26', 'Pembayaran Hardcopy', 'Pembayaran Hardcopy Order MAMA telah diverifikasi oleh admin ', 'order_detail', '2', 'renter company A', '2', '2018-04-20 10:31:44', '2018-04-20 10:31:44');
INSERT INTO `notifications` VALUES ('27', 'Order Diterima', 'Order MAMA telah diterima oleh admin', 'order_detail', '2', 'renter company A', '2', '2018-04-20 10:40:19', '2018-04-20 10:40:19');
INSERT INTO `notifications` VALUES ('28', 'Pembayaran Hardcopy', 'Pembayaran Hardcopy Order MAMA telah diverifikasi oleh admin ', 'order_detail', '2', 'renter company A', '2', '2018-04-20 10:42:17', '2018-04-20 10:42:17');
INSERT INTO `notifications` VALUES ('29', 'Order Diterima', 'Order MAMA telah diterima oleh admin', 'order_detail', '2', 'renter company A', '2', '2018-04-20 15:54:11', '2018-04-20 15:54:11');
INSERT INTO `notifications` VALUES ('30', 'Pembayaran Softcopy', 'Pembayaran Softcopy Order MAMA telah diverifikasi oleh admin ', 'order_detail', '2', 'renter company A', '2', '2018-04-20 15:54:24', '2018-04-20 15:54:24');
INSERT INTO `notifications` VALUES ('31', 'Pembayaran Softcopy', 'Pembayaran Softcopy Order INV20180421110518 telah diverifikasi oleh admin ', 'order_detail', '10', 'renter company A', '2', '2018-04-21 11:38:44', '2018-04-21 11:38:44');
INSERT INTO `notifications` VALUES ('32', 'Pembayaran Hardcopy', 'Pembayaran Hardcopy Order INV20180421110518 telah diverifikasi oleh admin ', 'order_detail', '10', 'renter company A', '2', '2018-04-21 11:43:21', '2018-04-21 11:43:21');
INSERT INTO `notifications` VALUES ('33', 'Order Dibatalkan', 'Order INV20180510134755 telah dibatalkan oleh admin', 'order_detail', '35', 'renter company A', '2', '2018-05-10 13:51:44', '2018-05-10 13:51:44');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice` varchar(255) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `note` text,
  `budget_type` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('36', 'INV20180510142556', '2', '1', '', 'APBN', '', '', 'kkk', 'opp', '2018-05-10 14:25:56', '2018-06-05 20:38:03');

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `content` text,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `pages_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `pages_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pages
-- ----------------------------

-- ----------------------------
-- Table structure for payments
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `phase_soft_copy` tinyint(1) DEFAULT '0',
  `phase_hard_copy` tinyint(1) DEFAULT '0',
  `prove_phase_soft_copy` varchar(255) DEFAULT NULL,
  `prove_phase_hard_copy` varchar(255) DEFAULT NULL,
  `payment_method_soft` varchar(255) DEFAULT NULL,
  `payment_detail_soft` text,
  `total_soft` double(20,2) DEFAULT '0.00',
  `payment_method_hard` varchar(255) DEFAULT NULL,
  `payment_detail_hard` varchar(255) DEFAULT NULL,
  `total_hard` double(20,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of payments
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'role-list', 'List Role', 'Lihat List Role', '2017-01-30 11:21:39', '2017-01-30 11:21:39');
INSERT INTO `permissions` VALUES ('2', 'role-create', 'Buat Role', 'Buat Role Baru', '2017-01-30 11:21:39', '2017-01-30 11:21:39');
INSERT INTO `permissions` VALUES ('3', 'role-edit', 'Edit Role', 'Edit Role', '2017-01-30 11:21:39', '2017-01-30 11:21:39');
INSERT INTO `permissions` VALUES ('4', 'role-delete', 'Hapus Role', 'Hapus Role', '2017-01-30 11:21:39', '2017-01-30 11:21:39');
INSERT INTO `permissions` VALUES ('5', 'product-list', 'Lihat List Produk', 'Lihat Semua Produk', '2017-01-30 11:21:40', '2017-01-30 11:21:40');
INSERT INTO `permissions` VALUES ('6', 'product-add', 'Tambahkan Produk', 'Tambahkan Produk Baru', '2017-01-30 11:21:40', '2017-01-30 11:21:40');
INSERT INTO `permissions` VALUES ('7', 'product-edit', 'Edit Produk', 'Edit Produk', '2017-01-30 11:21:40', '2017-01-30 11:21:40');
INSERT INTO `permissions` VALUES ('8', 'product-delete', 'Hapus Produk', 'Hapus Produk', '2017-01-30 11:21:40', '2017-01-30 11:21:40');

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`) USING BTREE,
  CONSTRAINT `permission_role_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permission_role
-- ----------------------------

-- ----------------------------
-- Table structure for professions
-- ----------------------------
DROP TABLE IF EXISTS `professions`;
CREATE TABLE `professions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `total` int(10) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `image_name` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `image_thumb` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of professions
-- ----------------------------
INSERT INTO `professions` VALUES ('1', 'Arsitek', '34', '2018-03-18 11:30:25', '2018-03-22 20:53:19', null, null, null);
INSERT INTO `professions` VALUES ('2', 'Elektrikal', '12', '2018-03-18 11:30:28', '2018-03-22 20:53:17', null, null, null);
INSERT INTO `professions` VALUES ('3', 'Lain-Lain', '7', '2018-03-18 11:30:32', '2018-03-22 20:52:43', null, null, null);
INSERT INTO `professions` VALUES ('4', 'Mekanikal', '62', '2018-03-18 11:30:37', '2018-03-22 20:52:41', null, null, null);
INSERT INTO `professions` VALUES ('5', 'Sipil', '78', '2018-03-18 11:30:42', '2018-03-22 20:52:29', null, null, null);
INSERT INTO `professions` VALUES ('6', 'Tata Lingkungan', '29', '2018-03-18 11:30:51', '2018-03-22 20:51:30', null, null, null);
INSERT INTO `professions` VALUES ('7', 'Manajemen Pelaksanaan', '4', '2018-03-22 20:51:26', '2018-03-22 20:52:05', null, null, null);

-- ----------------------------
-- Table structure for provinces
-- ----------------------------
DROP TABLE IF EXISTS `provinces`;
CREATE TABLE `provinces` (
  `id` int(2) unsigned NOT NULL,
  `name` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of provinces
-- ----------------------------
INSERT INTO `provinces` VALUES ('11', 'Aceh');
INSERT INTO `provinces` VALUES ('12', 'Sumatera Utara');
INSERT INTO `provinces` VALUES ('13', 'Sumatera Barat');
INSERT INTO `provinces` VALUES ('14', 'Riau');
INSERT INTO `provinces` VALUES ('15', 'Jambi');
INSERT INTO `provinces` VALUES ('16', 'Sumatera Selatan');
INSERT INTO `provinces` VALUES ('17', 'Bengkulu');
INSERT INTO `provinces` VALUES ('18', 'Lampung');
INSERT INTO `provinces` VALUES ('19', 'Kepulauan Bangka Belitung');
INSERT INTO `provinces` VALUES ('21', 'Kepulauan Riau');
INSERT INTO `provinces` VALUES ('31', 'DKI Jakarta');
INSERT INTO `provinces` VALUES ('32', 'Jawa Barat');
INSERT INTO `provinces` VALUES ('33', 'Jawa Tengah');
INSERT INTO `provinces` VALUES ('34', 'DI Yogyakarta');
INSERT INTO `provinces` VALUES ('35', 'Jawa Timur');
INSERT INTO `provinces` VALUES ('36', 'Banten');
INSERT INTO `provinces` VALUES ('51', 'Bali');
INSERT INTO `provinces` VALUES ('52', 'Nusa Tenggara Barat');
INSERT INTO `provinces` VALUES ('53', 'Nusa Tenggara Timur');
INSERT INTO `provinces` VALUES ('61', 'Kalimantan Barat');
INSERT INTO `provinces` VALUES ('62', 'Kalimantan Tengah');
INSERT INTO `provinces` VALUES ('63', 'Kalimantan Selatan');
INSERT INTO `provinces` VALUES ('64', 'Kalimantan Timur');
INSERT INTO `provinces` VALUES ('65', 'Kalimantan Utara');
INSERT INTO `provinces` VALUES ('71', 'Sulawesi Utara');
INSERT INTO `provinces` VALUES ('72', 'Sulawesi Tengah');
INSERT INTO `provinces` VALUES ('73', 'Sulawesi Selatan');
INSERT INTO `provinces` VALUES ('74', 'Sulawesi Tenggara');
INSERT INTO `provinces` VALUES ('75', 'Gorontalo');
INSERT INTO `provinces` VALUES ('76', 'Sulawesi Barat');
INSERT INTO `provinces` VALUES ('81', 'Maluku');
INSERT INTO `provinces` VALUES ('82', 'Maluku Utara');
INSERT INTO `provinces` VALUES ('91', 'Papua Barat');
INSERT INTO `provinces` VALUES ('92', 'Papua');

-- ----------------------------
-- Table structure for requests
-- ----------------------------
DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `study_program_id` int(10) unsigned DEFAULT NULL,
  `education_level_id` int(11) unsigned DEFAULT NULL,
  `profession_id` int(10) unsigned DEFAULT NULL,
  `graduate_year` int(10) DEFAULT NULL,
  `order_id` int(10) unsigned DEFAULT NULL,
  `note` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `rents_ibfk_3` (`study_program_id`),
  KEY `rents_ibfk_4` (`education_level_id`),
  KEY `rents_ibfk_5` (`profession_id`),
  KEY `rents_ibfk_6` (`order_id`),
  CONSTRAINT `rents_ibfk_3` FOREIGN KEY (`study_program_id`) REFERENCES `study_programs` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `rents_ibfk_4` FOREIGN KEY (`education_level_id`) REFERENCES `education_levels` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `rents_ibfk_5` FOREIGN KEY (`profession_id`) REFERENCES `professions` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `rents_ibfk_6` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of requests
-- ----------------------------
INSERT INTO `requests` VALUES ('47', '2', '1', '4', '2013', '36', null, '2018-05-10 14:25:56', '2018-05-10 14:25:56');

-- ----------------------------
-- Table structure for request_certificates
-- ----------------------------
DROP TABLE IF EXISTS `request_certificates`;
CREATE TABLE `request_certificates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(10) unsigned DEFAULT NULL,
  `certificate_id` int(11) unsigned DEFAULT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `request_id` (`request_id`),
  KEY `certificate_id` (`certificate_id`),
  CONSTRAINT `request_certificates_ibfk_1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `request_certificates_ibfk_2` FOREIGN KEY (`certificate_id`) REFERENCES `certificates` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of request_certificates
-- ----------------------------
INSERT INTO `request_certificates` VALUES ('11', '47', '20', 'Madya', '1');
INSERT INTO `request_certificates` VALUES ('12', '47', '21', 'Madya', '1');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin', 'Admin', null, '2018-03-18 11:42:36', '2018-03-18 11:43:00');
INSERT INTO `roles` VALUES ('2', 'superadmin', 'Super Admin', null, '2018-03-18 11:42:39', '2018-03-18 11:43:04');
INSERT INTO `roles` VALUES ('3', 'cs', 'Costumer Service', null, '2018-03-18 11:42:42', '2018-03-18 11:43:23');
INSERT INTO `roles` VALUES ('4', 'user', 'User', null, '2018-03-18 11:42:57', '2018-03-18 11:43:27');

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`) USING BTREE,
  CONSTRAINT `role_user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role_user
-- ----------------------------
INSERT INTO `role_user` VALUES ('2', '4');
INSERT INTO `role_user` VALUES ('3', '4');
INSERT INTO `role_user` VALUES ('4', '4');
INSERT INTO `role_user` VALUES ('5', '4');
INSERT INTO `role_user` VALUES ('6', '2');
INSERT INTO `role_user` VALUES ('7', '1');
INSERT INTO `role_user` VALUES ('9', '2');
INSERT INTO `role_user` VALUES ('10', '1');

-- ----------------------------
-- Table structure for study_programs
-- ----------------------------
DROP TABLE IF EXISTS `study_programs`;
CREATE TABLE `study_programs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `total` int(10) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of study_programs
-- ----------------------------
INSERT INTO `study_programs` VALUES ('1', 'Arsitektur', '0', '2018-03-18 11:29:34', '2018-03-22 20:55:18');
INSERT INTO `study_programs` VALUES ('2', 'Desain Interior', '0', '2018-03-18 11:29:37', '2018-03-22 21:01:03');
INSERT INTO `study_programs` VALUES ('3', 'Arsitek Lansekap', '0', '2018-03-18 11:29:44', '2018-03-22 20:55:37');
INSERT INTO `study_programs` VALUES ('4', 'Teknik Sipil', '0', '2018-03-18 11:29:54', '2018-03-22 21:01:05');
INSERT INTO `study_programs` VALUES ('5', 'Teknik Elektro', '0', '2018-03-18 11:29:56', '2018-03-22 20:55:54');
INSERT INTO `study_programs` VALUES ('6', 'Teknik Fisika', '0', '2018-03-18 11:29:59', '2018-03-22 21:01:07');
INSERT INTO `study_programs` VALUES ('8', 'Teknik Geologi', '0', '2018-03-21 10:00:17', '2018-03-22 20:57:32');
INSERT INTO `study_programs` VALUES ('9', 'Teknik Pertambangan', '0', '2018-03-21 14:05:05', '2018-03-22 20:56:17');
INSERT INTO `study_programs` VALUES ('10', 'Teknik Mesin', '0', '2018-03-21 14:06:03', '2018-03-22 20:56:24');
INSERT INTO `study_programs` VALUES ('11', 'Teknik Geodesi', '0', '2018-03-21 14:06:44', '2018-03-22 20:57:36');
INSERT INTO `study_programs` VALUES ('12', 'Teknik Pengairan', '0', '2018-03-21 14:13:00', '2018-03-22 20:57:01');
INSERT INTO `study_programs` VALUES ('13', 'Teknik lingkungan', '0', '2018-03-22 20:56:48', '2018-03-22 20:56:48');
INSERT INTO `study_programs` VALUES ('14', 'Teknik Kelautan', '0', '2018-03-22 20:56:52', '2018-03-22 20:56:52');
INSERT INTO `study_programs` VALUES ('15', 'Sumber Daya Air', '0', '2018-03-22 20:57:14', '2018-03-22 20:57:14');
INSERT INTO `study_programs` VALUES ('16', 'Geologi', '0', '2018-03-22 20:57:39', '2018-03-22 20:57:39');
INSERT INTO `study_programs` VALUES ('17', 'Geodesi', '0', '2018-03-22 20:57:42', '2018-03-22 20:57:42');
INSERT INTO `study_programs` VALUES ('18', 'Geoteknik', '0', '2018-03-22 20:57:57', '2018-03-22 20:57:57');
INSERT INTO `study_programs` VALUES ('19', 'Geomatika', '0', '2018-03-22 20:58:15', '2018-03-22 20:58:15');
INSERT INTO `study_programs` VALUES ('20', 'Oseanografi', '0', '2018-03-22 20:58:21', '2018-03-22 20:58:21');
INSERT INTO `study_programs` VALUES ('21', 'Teknik Tenaga Listrik', '0', '2018-03-22 20:58:43', '2018-03-22 20:58:43');
INSERT INTO `study_programs` VALUES ('22', 'Teknik Elektronika', '0', '2018-03-22 20:58:51', '2018-03-22 20:58:51');
INSERT INTO `study_programs` VALUES ('23', 'Teknik Telekomunikasi', '0', '2018-03-22 20:58:58', '2018-03-22 20:58:58');
INSERT INTO `study_programs` VALUES ('24', 'Teknik Informatika', '0', '2018-03-22 20:59:14', '2018-03-22 20:59:14');
INSERT INTO `study_programs` VALUES ('25', 'Teknik Persinyalan Kereta Api', '0', '2018-03-22 20:59:25', '2018-03-22 20:59:25');
INSERT INTO `study_programs` VALUES ('26', 'Tata Kota', '0', '2018-03-22 20:59:40', '2018-03-22 20:59:40');
INSERT INTO `study_programs` VALUES ('27', 'Planologi', '0', '2018-03-22 20:59:44', '2018-03-22 20:59:44');
INSERT INTO `study_programs` VALUES ('28', 'Teknik Penyehatan', '0', '2018-03-22 20:59:51', '2018-03-22 20:59:51');
INSERT INTO `study_programs` VALUES ('29', 'Teknik Kimia', '0', '2018-03-22 20:59:57', '2018-03-22 20:59:57');
INSERT INTO `study_programs` VALUES ('30', 'Semua Jurusan Teknik', '0', '2018-03-22 21:00:07', '2018-03-22 21:00:07');

-- ----------------------------
-- Table structure for temp_delete
-- ----------------------------
DROP TABLE IF EXISTS `temp_delete`;
CREATE TABLE `temp_delete` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_path` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of temp_delete
-- ----------------------------
INSERT INTO `temp_delete` VALUES ('2', 'haox', 'xasd');
INSERT INTO `temp_delete` VALUES ('3', null, null);
INSERT INTO `temp_delete` VALUES ('4', null, null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `api_token` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('0', 'WENDI RACHMAN SUSANTO, ST.\r\n', 'wendi@penyewa1.com', '', '1', '', null, '2018-03-23 15:18:03', '2018-06-05 14:33:26');
INSERT INTO `users` VALUES ('1', 'TRI WAHYU NUR WIJAYANTO, ST.\r\n', 'tri@penyewa1.com', '', '1', '', null, '2018-03-23 15:17:40', '2018-06-05 14:34:14');
INSERT INTO `users` VALUES ('2', 'renter company A', 'vianendra@gmail.com', '$2y$10$bamKKsxVsUgvigzVg2DcvuHcHDlyCVTduRFo7GyNwcY2tLwnSQjH2', '1', 'kElFX87Q4Q0eOzOLlpGVpzbBKyekpbp7evDxKJuP8eddTcXNVcOEw3yNZSo5', '1OFxeqlcimDRa2i5yu6eHTCXCbDMCZXJcY1tzmYAMvlM28jtHHl0LGlLNFPI', '2018-03-18 11:44:09', '2018-06-05 14:08:23');
INSERT INTO `users` VALUES ('3', 'renter company B', 'rentercompanyB@test.com', '$2y$10$pgg/Y/FNnC66QNzH43DKMOOkAKVTqLWp5waqyFUjxBaqQ5o7r3Pri', '1', 'PNH3upE4WDGaA6YI6t5riC69hp6zItGQKkxFrZb8AnflUEtKtzncF7azKEpd', 'wKPA4zOqVtBIb9B6FGmfkXjUhgzNipROLfOi2xwmQbhR6v430pvj7ezZucv4', '2018-03-18 11:44:41', '2018-03-18 11:44:46');
INSERT INTO `users` VALUES ('4', 'Kontraktor COmpany A', 'KontraktorCOmpanyA@hallo.com', '$2y$10$ZKJof0wPf1raYU0a6z1GSOlfjW1dFinuBeWRH6h/QhOEE4lJNFNnm', '0', 'EHw2uXfp8ARO0TL44lG3kYN29a3F5usX60hA7dUI46ss0xcFqrJrcx5UWbKW', null, '2018-03-18 12:03:14', '2018-03-25 20:22:33');
INSERT INTO `users` VALUES ('5', 'Peminjam A', 'peminjam@companyA.com', '$2y$10$TEqSE8i4Rh0NI3JCvMN7GOWc/26Mb0Xk35F1i77XpUFMOXWoqAmtm', '1', 'a7ZvnRXlqtf5CLubYfhVBTMxRbvTJjvZ9j8DeCRH7YLoZ3H4ZLwTBBdhCqKe', null, '2018-03-18 12:10:34', '2018-03-21 10:48:44');
INSERT INTO `users` VALUES ('6', 'Super Admin SKA SKT', 'su@projectas.com', '$2y$10$wcjUYlPn/5S7N0TbskkNR.BVMBBWgvNCYEdDA5q01uymX1/MoWC1u', '1', 'ob2TNY2NWlgCSvuTmDqr7Xn7cmkpJIXYZMnOoltoP53vDLlwxRyZ7yoPqjQO', 'mSyxyi9M1LlRPfgcllqeDpaEowTMCB7eN7boCp7mP0Uq7B9MLBC5bUr9LGqq', '2018-03-18 19:18:56', '2018-05-11 12:03:17');
INSERT INTO `users` VALUES ('7', 'admin ascertificate 2 2', 'admin23@projectAS.com', '$2y$10$AkCp6yGAdAeInoTAathTw.Nn3fhz9wTXQFxvl/eyyb7SFK2/UUUk.', '1', '28ldl96azBll5P6DxKSheLxUg42mg81bs668h2oe8pNhrkKbrpnxSN8cZk70', null, '2018-03-19 11:16:11', '2018-03-21 10:19:58');
INSERT INTO `users` VALUES ('9', 'Super admin LAin', 'halo@projectas.com', '$2y$10$5eWCgfzEs8YDYMDNb3pEAOv009DQ7u7Hv4OGQlBj4A5pjeIhrFrBG', '1', 'W3Ew26cY2pBYYEwe0NBe9L3Jd3E7CAcC209upO1GGIeEe1k90gS2V7bVsTf2', null, '2018-03-21 10:08:41', '2018-03-21 10:08:41');
INSERT INTO `users` VALUES ('10', 'halo user', 'admin@projectas.com', '$2y$10$J11sZMD0OQOzWFDrOSpJZ.OC6s6Gqe8H6nD5/Wl/zspuIle3YXqAG', '1', 'Yz0JBXClIApCM0iPELHWkY5CI91rZTS2SRfPZ96lTifsDfd0yL5XQXPYig2c', null, '2018-03-21 18:12:52', '2018-03-21 18:12:52');
INSERT INTO `users` VALUES ('11', 'Penyewa 1', 'email@penyewa1.com', '$2y$10$o2QofMzbuihjH8o1xxjTFuLxlujul0IxYU/uEI2wG8fjUDHPj2qau', '1', 'psgCvhTycsA9YgzCnxEXRruM6bj7eg5spP8fqwmMHkdBzyvQfUbEHK7HgKL4', '5s0GGBcBonqQjmrYk0EGLyT9r5CJJoJLoNBucNoPdAESgvthjjz4YHViKj4u', '2018-03-23 13:43:02', '2018-03-23 13:43:11');
INSERT INTO `users` VALUES ('12', 'TAUFIK NURHADI, ST.\r\n', 'taufik@penyewa1.com', '', '1', '', null, '2018-03-23 15:17:10', null);
INSERT INTO `users` VALUES ('13', 'TEDY OCA SETAWAN', 'tedy@penyewa1.com', '', '1', '', null, '2018-03-23 15:17:30', '2018-03-23 15:17:57');
INSERT INTO `users` VALUES ('18', 'WIJIYANTO, A.Md.\r\n', 'wiji@penyewa1.com', '', '1', '', null, '2018-03-23 15:18:15', null);

-- ----------------------------
-- Table structure for user_certificates
-- ----------------------------
DROP TABLE IF EXISTS `user_certificates`;
CREATE TABLE `user_certificates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `certificate_id` int(11) unsigned DEFAULT NULL,
  `order_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `registration_number` varchar(255) DEFAULT NULL,
  `serial_number` varchar(255) DEFAULT NULL,
  `address` text,
  `qualification` varchar(255) DEFAULT NULL,
  `registration_place` varchar(255) DEFAULT NULL,
  `date_of_registration` datetime DEFAULT NULL,
  `period_of_validity` int(11) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `file_thumb` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `hard_copy` tinyint(1) DEFAULT '0',
  `pick_up_date` datetime DEFAULT NULL,
  `exact_return_date` datetime DEFAULT NULL,
  `return_date` datetime DEFAULT NULL,
  `pick_up_by` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `return_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `certificate_id` (`certificate_id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `user_certificates_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_certificates_ibfk_2` FOREIGN KEY (`certificate_id`) REFERENCES `certificates` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `user_certificates_ibfk_3` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_certificates
-- ----------------------------
INSERT INTO `user_certificates` VALUES ('6', null, '1', null, 'sda', '312.123/123.31232.123', null, null, 'Muda', null, '2018-06-26 14:53:46', '360', 'not_assign_bh7yV_SKA_1_sda_Muda.jpg', 'images/full/certificate/not_assign_bh7yV_SKA_1_sda_Muda.jpg', 'images/thumb/certificate/not_assign_bh7yV_SKA_1_sda_Muda.jpg', '2018-06-05 14:53:47', '2018-06-05 14:53:47', '0', null, null, null, null, null, null);
INSERT INTO `user_certificates` VALUES ('7', null, '1', null, 'test', 'asd123123', null, null, 'Muda', null, '2018-05-15 15:00:31', '360', 'not_assign__asd123123SKA_1_test_Muda.jpg', 'images/full/certificate/not_assign__asd123123SKA_1_test_Muda.jpg', 'images/thumb/certificate/not_assign__asd123123SKA_1_test_Muda.jpg', '2018-06-05 15:00:32', '2018-06-05 15:00:32', '0', null, null, null, null, null, null);
INSERT INTO `user_certificates` VALUES ('8', null, '1', null, 'sda', '123ss12322', null, null, 'Madya', null, '2018-05-23 15:01:26', '360', 'not_assign__123ss12322SKA_1_sda_Madya.jpg', 'images/full/certificate/not_assign__123ss12322SKA_1_sda_Madya.jpg', 'images/thumb/certificate/not_assign__123ss12322SKA_1_sda_Madya.jpg', '2018-06-05 15:01:27', '2018-06-05 15:01:27', '0', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for user_devices
-- ----------------------------
DROP TABLE IF EXISTS `user_devices`;
CREATE TABLE `user_devices` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_devices_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_devices
-- ----------------------------
INSERT INTO `user_devices` VALUES ('1', '4', '123adasdqwe123asdqwe123', '2018-03-18 12:03:14', '2018-03-18 12:03:14');
INSERT INTO `user_devices` VALUES ('2', '5', '123adasdqwe123asdqwe123', '2018-03-18 12:10:34', '2018-03-18 12:10:34');

-- ----------------------------
-- Table structure for user_general_info
-- ----------------------------
DROP TABLE IF EXISTS `user_general_info`;
CREATE TABLE `user_general_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `id_card_number` double(255,0) NOT NULL,
  `phone_number` double(255,0) DEFAULT NULL,
  `profession_id` int(10) unsigned DEFAULT NULL,
  `education_level_id` int(10) unsigned DEFAULT NULL,
  `study_program_id` int(10) unsigned DEFAULT NULL,
  `address` text,
  `company_id` int(10) unsigned DEFAULT NULL,
  `graduate_year` int(10) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `image_thumb` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `user_general_info_ibfk_1` (`profession_id`),
  KEY `user_general_info_ibfk_2` (`education_level_id`),
  KEY `user_general_info_ibfk_3` (`study_program_id`),
  KEY `user_general_info_ibfk_5` (`company_id`),
  CONSTRAINT `user_general_info_ibfk_1` FOREIGN KEY (`profession_id`) REFERENCES `professions` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `user_general_info_ibfk_2` FOREIGN KEY (`education_level_id`) REFERENCES `education_levels` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `user_general_info_ibfk_3` FOREIGN KEY (`study_program_id`) REFERENCES `study_programs` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `user_general_info_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `user_general_info_ibfk_5` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_general_info
-- ----------------------------
INSERT INTO `user_general_info` VALUES ('3', '5', '2512019231312', '2512019231312', '2', '1', '3', 'Rumah saya dimana Ops', '2', '2017', null, null, null, '2018-03-20 03:29:16', '2018-03-20 03:29:16');
INSERT INTO `user_general_info` VALUES ('7', '6', '0', null, null, null, null, null, null, null, null, null, null, '2018-04-13 14:40:26', '2018-04-13 14:40:26');
INSERT INTO `user_general_info` VALUES ('8', '2', '345132421424231', '82313223123', '4', '1', '2', 'Rumah saya berada dijalan', null, '2013', 'cbevw.png', 'images/full/profile/cbevw.png', 'images/thumb/profile/cbevw.png', '2018-04-13 15:41:51', '2018-04-13 15:46:09');
