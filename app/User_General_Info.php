<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_General_Info extends Model
{
    protected $table = 'user_general_info';
    public $fillable = ['user_id','id_card_number','phone_number','profession_id','education_level_id','study_program_id','address','company_id','graduate_year','image_name','image_path','image_thumb'];
}
