<?php

namespace App\Model\Message_Detail;

use Illuminate\Database\Eloquent\Model;

class Message_Detail extends Model
{
    protected $table = 'message_details';
    public $fillable = ['message_id','message','sender_id','receiver_id','is_read'];
}
