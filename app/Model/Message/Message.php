<?php

namespace App\Model\Message;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';
    public $fillable = ['title','last_message','is_read','user_id'];
}
