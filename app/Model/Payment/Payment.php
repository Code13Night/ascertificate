<?php

namespace App\Model\Payment;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';
    public $fillable = ['order_id','phase_soft_copy','phase_hard_copy','prove_phase_soft_copy','prove_phase_hard_copy','payment_method_soft','payment_detail_soft','total_soft','payment_method_hard','payment_detail_hard','total_hard'];
}
