<?php

namespace App\Model\General_Info;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    public $fillable = ['name','title','sub_title','content'];
}
