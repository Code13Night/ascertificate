<?php

namespace App\Model\General_Info;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
    public $fillable = ['province_id','name'];
}
