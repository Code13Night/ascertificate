<?php

namespace App\Model\General_Info;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';
    public $fillable = ['name'];
}
