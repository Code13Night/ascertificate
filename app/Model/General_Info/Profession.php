<?php

namespace App\Model\General_Info;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    protected $table = 'professions';
    public $fillable = ['name','total'];
}
