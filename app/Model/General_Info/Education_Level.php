<?php

namespace App\Model\General_Info;

use Illuminate\Database\Eloquent\Model;

class Education_Level extends Model
{
    protected $table = 'education_levels';
    public $fillable = ['name','total'];
}
