<?php

namespace App\Model\General_Info;

use Illuminate\Database\Eloquent\Model;

class Study_Program extends Model
{
    protected $table = 'study_programs';
    public $fillable = ['name','total'];
}
