<?php

namespace App\Model\Certificate;

use Illuminate\Database\Eloquent\Model;
use DB;

class Certificate_Study_Program extends Model
{
    protected $table = 'certificate_study_programs';
    public $fillable = ['certificate_id','study_program_id'];
    public $timestamps = false;
}
