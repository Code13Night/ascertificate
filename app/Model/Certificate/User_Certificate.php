<?php

namespace App\Model\Certificate;

use Illuminate\Database\Eloquent\Model;
use DB;

class User_Certificate extends Model
{
    
    protected $table = 'user_certificates';
    public $fillable = ['name','user_id','certificate_id','registration_number','serial_number','address','qualification','registration_place','date_of_registration','period_of_validity','file_name','file_path','file_thumb','hard_copy','pick_up_date','exact_return_date','return_date','pick_up_by','return_by','status','order_id'];
    public static function allCertificates(){
        return DB::table('user_certificates')
            ->leftjoin('certificates', 'user_certificates.certificate_id', '=', 'certificates.id')
            ->leftjoin('certificate_types', 'certificates.type_id', '=', 'certificate_types.id')
            ->leftjoin('professions', 'certificates.profession_id', '=', 'professions.id')
            ->leftjoin('users', 'user_certificates.user_id', '=', 'users.id')
            ->select('user_certificates.id as id','user_certificates.name as name','user_id','certificate_id','registration_number','serial_number','address','qualification','registration_place','date_of_registration','user_certificates.period_of_validity','file_name','file_path','file_thumb','certificates.name as certificate_classification','certificates.type_id as certificate_type_id','certificates.classification_code as certificate_classification_code','certificate_types.name as certificate_type_name','certificate_types.id as certificate_type_id', 'users.name as user_name','professions.name as profession_name');
    }
}
