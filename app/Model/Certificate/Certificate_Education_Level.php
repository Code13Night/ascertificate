<?php

namespace App\Model\Certificate;

use Illuminate\Database\Eloquent\Model;
use DB;

class Certificate_Education_Level extends Model
{
    protected $table = 'certificate_education_levels';
    public $fillable = ['certificate_id','education_level_id'];
    public $timestamps = false;
}
