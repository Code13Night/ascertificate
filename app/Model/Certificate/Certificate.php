<?php

namespace App\Model\Certificate;

use Illuminate\Database\Eloquent\Model;
use DB;

class Certificate extends Model
{
    protected $table = 'certificates';
    public $fillable = ['classification_code','name','type_id','profession_id','description','period_of_validity','price_soft','price_hard','status'];

    public static function allCertificates(){
        return DB::table('certificates')
            ->leftjoin('professions', 'certificates.profession_id', '=', 'professions.id')
            ->leftjoin('certificate_types', 'certificates.type_id', '=', 'certificate_types.id')
            ->select('certificates.id as id','classification_code', 'certificates.name','certificate_types.name as type_name','professions.name as profesion_name', 'description', 'period_of_validity','price_soft','price_hard', 'status');
    }
    public static function searchCertificate($query){
        return DB::table('certificates')
        	->where('certificates.classification_code','like','%'.$query.'%')
        	->orWhere('certificates.name','like','%'.$query.'%')
        	->orWhere('professions.name','like','%'.$query.'%')
        	->orWhere('certificate_types.name','like','%'.$query.'%')
            ->leftjoin('professions', 'certificates.profession_id', '=', 'professions.id')
            ->leftjoin('certificate_types', 'certificates.type_id', '=', 'certificate_types.id')
            ->select('certificates.id as id','classification_code', 'certificates.name','certificate_types.name as type_name','professions.name as profesion_name', 'description', 'period_of_validity','price_soft','price_hard', 'status');
    }
}
