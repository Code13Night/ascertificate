<?php

namespace App\Model\Certificate;

use Illuminate\Database\Eloquent\Model;

class Certificate_Type extends Model
{
    protected $table = 'certificate_types';
    public $fillable = ['name','total'];
}
