<?php

namespace App\Model\Rent;

use Illuminate\Database\Eloquent\Model;
use DB;
class RequestCertificate extends Model
{
    protected $table = 'requests';
    public $fillable = ['graduate_year','order_id','education_level_id','study_program_id','profession_id','graduate_year'];
 	public static function allRequests(){
        return DB::table('requests')->groupBy('requests.id')
            ->leftjoin('request_certificates', 'requests.id', '=', 'request_certificates.request_id')
            ->leftjoin('orders', 'requests.order_id', '=', 'orders.id')
            ->leftjoin('certificates', 'request_certificates.certificate_id', '=', 'certificates.id')
            ->leftjoin('certificate_types', 'certificates.type_id', '=', 'certificate_types.id')
            ->leftjoin('professions', 'certificates.profession_id', '=', 'professions.id')
            ->leftjoin('education_levels', 'requests.education_level_id', '=', 'education_levels.id')
            ->leftjoin('study_programs', 'requests.study_program_id', '=', 'study_programs.id')
            ->leftjoin('users', 'orders.user_id', '=', 'users.id')
            ->select('requests.id as id',DB::raw('GROUP_CONCAT(certificates.name) as certificate_classification'),DB::raw('GROUP_CONCAT(certificates.type_id) as certificate_type_id'),DB::raw('GROUP_CONCAT(request_certificates.status) as request_status'),DB::raw('GROUP_CONCAT(request_certificates.qualification) as request_qualification'),DB::raw('GROUP_CONCAT(certificate_types.name) as certificate_type'),'professions.name as profession_name','professions.id as profession_id','study_programs.name as study_program_name','study_programs.id as study_program_id','education_levels.name as education_level_name','education_levels.id as education_level_id','orders.invoice','users.name as user_name','requests.graduate_year');
    }
    public static function allRequestsByCertificate(){
        return DB::table('request_certificates')
            ->leftjoin('requests', 'requests.id', '=', 'request_certificates.request_id')
            ->leftjoin('orders', 'requests.order_id', '=', 'orders.id')
            ->leftjoin('certificates', 'request_certificates.certificate_id', '=', 'certificates.id')
            ->leftjoin('certificate_types', 'certificates.type_id', '=', 'certificate_types.id')
            ->leftjoin('professions', 'certificates.profession_id', '=', 'professions.id')
            ->leftjoin('education_levels', 'requests.education_level_id', '=', 'education_levels.id')
            ->leftjoin('study_programs', 'requests.study_program_id', '=', 'study_programs.id')
            ->leftjoin('users', 'orders.user_id', '=', 'users.id')
            ->select('request_certificates.id as id','request_certificates.qualification','request_certificates.certificate_id','certificates.classification_code','certificates.name as certificate_name','certificates.status as certificate_status','certificates.type_id','certificate_types.name as type_name','professions.name as profesion_name', 'users.name as user_name','education_levels.name as education_level_name', 'study_programs.name as study_program_name', 'description as certificate_description', 'request_certificates.status','certificates.price_soft as certificate_price_soft','certificates.price_hard as certificate_price_hard','order_id');
    }
    public static function allRequestsCertificate(){
        return DB::table('requests')->groupBy('request_certificates.certificate_id')
            ->leftjoin('request_certificates', 'requests.id', '=', 'request_certificates.request_id')
            ->leftjoin('orders', 'requests.order_id', '=', 'orders.id')
            ->leftjoin('certificates', 'request_certificates.certificate_id', '=', 'certificates.id')
            ->leftjoin('certificate_types', 'certificates.type_id', '=', 'certificate_types.id')
            ->leftjoin('professions', 'certificates.profession_id', '=', 'professions.id')
            ->leftjoin('education_levels', 'requests.education_level_id', '=', 'education_levels.id')
            ->leftjoin('study_programs', 'requests.study_program_id', '=', 'study_programs.id')
            ->leftjoin('users', 'orders.user_id', '=', 'users.id')
            ->select('requests.id as id','certificates.classification_code as certificate_classification_code','certificates.name as certificate_classification','certificates.type_id as certificate_type_id','request_certificates.status as request_status','certificate_types.name as certificate_type','request_certificates.qualification as request_qualification','professions.name as profession_name','professions.id as profession_id','study_programs.name as study_program_name','study_programs.id as study_program_id','education_levels.name as education_level_name','education_levels.id as education_level_id','users.name as user_name','requests.graduate_year');
    }
}
