<?php

namespace App\Model\Rent;

use Illuminate\Database\Eloquent\Model;
use DB;

class Rent extends Model
{
    protected $table = 'rents';
    public $fillable = ['user_certificate_id','certificate_id','user_name','graduate_year','serial_number','order_id','qualification','date_of_issued','valid_until','hard_copy','pick_up_date','exact_return_date','return_date','pick_up_by','return_by','status','note'];
 	public static function allRents(){
        return DB::table('rents')
            ->leftjoin('certificates', 'rents.certificate_id', '=', 'certificates.id')
            ->leftjoin('orders', 'rents.order_id', '=', 'orders.id')
            ->leftjoin('certificate_types', 'certificates.type_id', '=', 'certificate_types.id')
            ->leftjoin('professions', 'certificates.profession_id', '=', 'professions.id')
            ->leftjoin('education_levels', 'rents.education_level_id', '=', 'education_levels.id')
            ->leftjoin('study_programs', 'rents.study_program_id', '=', 'study_programs.id')
            ->leftjoin('users', 'orders.user_id', '=', 'users.id')
            ->select('rents.id as id','rents.serial_number','rents.qualification','rents.certificate_id','certificates.classification_code','certificates.name as certificate_name','certificates.image_name as certificate_image_name','certificates.image_path as certificate_image_path','certificates.image_thumb as certificate_image_thumb','certificates.status as certificate_status','certificates.price_soft as certificate_price_soft','certificates.price_hard as certificate_price_hard','type_id','certificate_types.name as type_name','professions.name as profesion_name', 'user_name','education_levels.name as education_level_name', 'study_programs.name as study_program_name', 'description as certificate_description', 'date_of_issued','pick_up_date','exact_return_date','return_date','pick_up_by','return_by','valid_until', 'rents.status','graduate_year');
    }
    public static function allRentsByUser(){
        return DB::table('rents')
            ->leftjoin('certificates', 'rents.certificate_id', '=', 'certificates.id')
            ->leftjoin('certificate_types', 'certificates.type_id', '=', 'certificate_types.id')
            ->leftjoin('professions', 'certificates.profession_id', '=', 'professions.id')
            ->leftjoin('education_levels', 'rents.education_level_id', '=', 'education_levels.id')
            ->leftjoin('study_programs', 'rents.study_program_id', '=', 'study_programs.id')
            ->leftjoin('orders', 'rents.order_id', '=', 'orders.id')
            ->leftjoin('users', 'orders.user_id', '=', 'users.id')
            ->select('rents.id as id','rents.serial_number','rents.qualification','rents.certificate_id','certificates.classification_code','certificates.name as certificate_name','certificates.image_name as certificate_image_name','certificates.image_path as certificate_image_path','certificates.image_thumb as certificate_image_thumb','certificates.status as certificate_status','certificates.price_soft as certificate_price_soft','certificates.price_hard as certificate_price_hard','type_id','certificate_types.name as type_name', 'user_name','professions.name as profesion_name','education_levels.name as education_level_name', 'study_programs.name as study_program_name','pick_up_date','exact_return_date','return_date','pick_up_by','return_by', 'date_of_issued','valid_until', 'rents.status','graduate_year');
    }

}
