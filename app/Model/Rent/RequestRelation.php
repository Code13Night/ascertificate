<?php

namespace App\Model\Rent;

use Illuminate\Database\Eloquent\Model;

class RequestRelation extends Model
{
    protected $table = 'request_certificates';
    public $fillable = ['request_id','certificate_id','qualification','status'];
    public $timestamps = false;
}
