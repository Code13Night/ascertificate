<?php

namespace App\Model\Rent;

use Illuminate\Database\Eloquent\Model;
use DB;

class Cart extends Model
{
    protected $table = 'carts';
    public $fillable = ['user_name','graduate_year','user_id','profession_id','study_program_id','education_level_id','budget_type','province','city','company','position'];
    public $timestamps = false;

 	public static function allCarts(){
        return DB::table('carts')
            ->leftjoin('cart_certificates', 'cart_certificates.cart_id', '=', 'carts.id')
            ->leftjoin('certificates', 'cart_certificates.certificate_id', '=', 'certificates.id')
            ->leftjoin('certificate_types', 'certificates.type_id', '=', 'certificate_types.id')
            ->leftjoin('professions', 'carts.profession_id', '=', 'professions.id')
            ->leftjoin('education_levels', 'carts.education_level_id', '=', 'education_levels.id')
            ->leftjoin('study_programs', 'carts.study_program_id', '=', 'study_programs.id')
            ->leftjoin('users', 'carts.user_id', '=', 'users.id')
            ->groupBy('carts.id')
            ->select('carts.id as id','carts.user_id','carts.profession_id','carts.study_program_id','carts.education_level_id',DB::raw('GROUP_CONCAT(cart_certificates.qualification) as qualification'),DB::raw('GROUP_CONCAT(certificates.classification_code) as classification_code'),'cart_certificates.certificate_id',DB::raw('GROUP_CONCAT(certificates.name) as certificate_classification'),DB::raw('GROUP_CONCAT(certificates.type_id) as certificate_type_id'),DB::raw('GROUP_CONCAT(certificate_types.name) as type_name'),'professions.name as profesion_name','education_levels.name as education_level_name', 'study_programs.name as study_program_name','graduate_year');
    }
}
