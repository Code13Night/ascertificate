<?php

namespace App\Model\Rent;

use Illuminate\Database\Eloquent\Model;

class CartRelation extends Model
{
    protected $table = 'cart_certificates';
    public $fillable = ['cart_id','certificate_id','qualification'];
    public $timestamps = false;
}
