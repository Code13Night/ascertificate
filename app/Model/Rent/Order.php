<?php

namespace App\Model\Rent;

use Illuminate\Database\Eloquent\Model;
use DB;

class Order extends Model
{

    protected $table = 'orders';
    public $fillable = ['invoice','user_id','status','budget_type','city','province','company','position','note'];
 	public static function allOrders(){
        return DB::table('orders')->groupBy('orders.id')
            ->leftjoin('requests', 'requests.order_id', '=', 'orders.id')
            ->leftjoin('request_certificates', 'request_certificates.request_id', '=', 'requests.id')
            ->leftjoin('users', 'orders.user_id', '=', 'users.id')
            ->select('orders.id','orders.invoice',DB::raw('count(request_certificates.id) as total_request'),'users.name as username','orders.status','orders.note','orders.created_at','orders.updated_at');
    }
}
