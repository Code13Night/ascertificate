<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model;
use DB;

class Log extends Model
{
    protected $table = 'logs';
    public $fillable = ['subject','url','method','ip','agent','user_id'];

    public static function allLog(){
        return DB::table('logs')
            ->leftjoin('users', 'logs.user_id', '=', 'users.id')
            ->select('subject','url','method','ip','agent','users.name','logs.created_at as created_at','users.email');
    }
}
