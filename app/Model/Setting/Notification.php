<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';
    public $fillable = ['title','body','data_type','data_id','user_type','user_id'];
}
