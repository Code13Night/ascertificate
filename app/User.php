<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use DB;

class User extends Authenticatable
{
    use EntrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','api_token','active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','api_token','active'
    ];

    public static function AllUsers(){
        return DB::table('users')->groupBy(['users.id','users.name', 'email'])
            ->leftjoin('role_user', 'users.id', '=', 'role_user.user_id')
            ->leftjoin('roles', 'role_user.role_id', '=', 'roles.id')
            ->select('users.name', 'email', 'active',DB::raw('GROUP_CONCAT(display_name) as display_name'),'users.id as user_id');
    }
    public static function AllUsersByRoles($id){
        return DB::table('users')->groupBy(['users.id','users.name', 'email','active'])->whereIn('roles.id',$id)
            ->leftjoin('role_user', 'users.id', '=', 'role_user.user_id')
            ->leftjoin('roles', 'role_user.role_id', '=', 'roles.id')
            ->select('users.name as username', 'email', 'active',DB::raw('GROUP_CONCAT(display_name) as display_name'),'users.id as user_id');
    }
}
