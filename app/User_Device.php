<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Device extends Model
{
    protected $table = 'user_devices';
    public $fillable = ['user_id','device_id'];
}
