<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Hash;
use Image;
use Storage;
use Carbon\Carbon;
use LogActivity;
use App\User;
use App\User_General_Info;
use App\Model\Certificate\Certificate;

use App\Model\General_Info\City;
use App\Model\General_Info\Province;
use App\Model\General_Info\Company;
use App\Model\General_Info\Study_Program;
use App\Model\General_Info\Profession;
use App\Model\General_Info\Education_Level;
use App\Model\Rent\Cart;
use App\Model\Rent\Rent;

class RentController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin','cs'])){
                $total_rent=Rent::whereIn('status',[3,4,5,6,7])->count();
                $soft_rent=Rent::whereIn('status',[3,4])->count();
                $hard_rent=Rent::whereIn('status',[6,7])->count();
                return view('backend.rents.index',compact('total_rent','soft_rent','hard_rent'));
            }else{
            return view('frontend.rents.index');
            } 
        }else{
            return redirect('/login');
        } 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRequest(Request $request)
    {
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin','cs'])){
                return view('backend.rents.requests.index');
            }else{
                if(Cart::where('user_id',$user->id)->count()>0){
                    session(['availablecart' =>  true]);
                    session(['budget_type'=>Cart::where('user_id',$user->id)->first()->budget_type]);
                    session(['province'=>Cart::where('user_id',$user->id)->first()->province]);
                    session(['city'=>Cart::where('user_id',$user->id)->first()->city]);
                    session(['company'=>Cart::where('user_id',$user->id)->first()->company]);
                    session(['position'=>Cart::where('user_id',$user->id)->first()->position]);
                }else{
                    session(['availablecart' =>  false]);
                }
                $general_info_user=User_General_Info::where('user_id',$user->id);
                if($general_info_user->count()>0){
                    $general_info_user=$general_info_user->first();
                    session(['profession_id' =>  $general_info_user->profession_id]);
                    session(['study_program_id' =>  $general_info_user->study_program_id]);
                    session(['education_level_id' =>  $general_info_user->education_level_id]);
                    session(['graduate_year' =>  $general_info_user->graduate_year]);
                }
                session(['username' => $user->name]);
                $certificates=Certificate::select(DB::raw('CONCAT( classification_code," - ",name) as code_name'),'id')->where('type_id',1);
                if($request->session()->has('profession_id')){
                    $certificates=$certificates->where('profession_id',session('profession_id'));
                }
                $certificates=$certificates->pluck('code_name','id');
                $study_programs=Study_Program::pluck('name','id');
                $professions=Profession::pluck('name','id');
                $provinces=Province::pluck('name','name');
                $cities=City::pluck('name','name');
                $education_levels=Education_Level::pluck('name','id');
                return view('frontend.rents.requests.index',compact('study_programs','professions','cities','provinces','education_levels','certificates'));
            } 
        }else{
            return redirect('/login');
        } 
    }
    public function show($id)
    {
        if(Auth::check()){
            $user=User::find(Auth::id());
            $rent=Rent::find($id);
            if($rent->profession_id!=null){
                $rent->profession_name=Profession::find($rent->profession_id)->name;
            }else{
                $rent->profession_name="";
            }

            if($rent->education_level_id!=null){
                $rent->education_level_name=Education_Level::find($rent->education_level_id)->name;
            }else{
                $rent->education_level_name="";
            }

            if($rent->study_program_id!=null){
                 $rent->study_program_name=Study_Program::find($rent->study_program_id)->name;
            }else{
                $rent->study_program_name="";
            }

            $rent->certificate_name=Certificate::find($rent->certificate_id)->name;
            if(Certificate::find($rent->certificate_id)->type_id==1){
                $name_type="SKA";
            }else{
                $name_type="SKT";
            }
            $rent->certificate_type_name=$name_type;
            if($user->hasRole(['admin', 'superadmin','cs'])){
                return view('backend.rents.detail',compact('rent'));
            }else{
                return view('backend.rents.detail',compact('rent'));
            } 
        }else{
            return redirect('/login');
        } 
    }
    
    public function showSerialNumber($code)
    {
        if(Auth::check()){
            $user=User::find(Auth::id());
            $rent=Rent::where('serial_number',$code)->first();;
            if($rent->profession_id!=null){
                $rent->profession_name=Profession::find($rent->profession_id)->name;
            }else{
                $rent->profession_name="";
            }

            if($rent->education_level_id!=null){
                $rent->education_level_name=Education_Level::find($rent->education_level_id)->name;
            }else{
                $rent->education_level_name="";
            }

            if($rent->study_program_id!=null){
                 $rent->study_program_name=Study_Program::find($rent->study_program_id)->name;
            }else{
                $rent->study_program_name="";
            }

            $rent->certificate_name=Certificate::find($rent->certificate_id)->name;
            if(Certificate::find($rent->certificate_id)->type_id==1){
                $name_type="SKA";
            }else{
                $name_type="SKT";
            }
            $rent->certificate_type_name=$name_type;
            if($user->hasRole(['admin', 'superadmin','cs'])){
                return view('backend.rents.detail',compact('rent'));
            }else{
                return view('backend.rents.detail',compact('rent'));
            } 
        }else{
            return redirect('/login');
        } 
    }
}
