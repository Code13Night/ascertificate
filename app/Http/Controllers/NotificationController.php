<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Hash;
use Image;
use Storage;
use Carbon\Carbon;
use App\Model\Setting\Notification;
use LogActivity;
use App\User;
use App\User_Device;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin'])){
                $users=User::AllUsersByRoles([4])->pluck('username','user_id');
                return view('backend.notifications.index',compact('users'));
            }else{
                return view('backend.notifications.index');
            } 
        }else{
            return redirect('/login');
        } 
    }
    public function save(Request $request)
    {

        $input = $request->all();
        
        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($request->input('title'));
        $notificationBuilder->setBody(strip_tags($request->input('body')))
                            ->setSound('default');
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' => $request->input('title'),'body' => strip_tags($request->input('body')),'notif_type'=>'general','notification_data'=>'','notification_title'=>'']);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = "famUG_YyUyQ:APA91bFbx5HjoRjbprV5Gr_8RgTsfsWDjaCc17RwjKJ2YaBzAbHFIJmu5c0Ix3acmHooOOlgA3F1wEreZqnPLf6yjFRMKUEfq3yVB2PucBU936HP7AInSieD5ouNWW7n93M0dAi7Fe1u";
        if($request->has('user_id')||$request->input('user_id')!=null){
            $token=User_Device::where('user_id',$request->input('user_id'))->pluck('device_id')->toArray();
            if(User_Device::where('user_id',$request->input('user_id'))->count()>0){
            $username=User::find($request->input('user_id'))->name;
            $input['user_type']=$username;
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            //return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete(); 

            //return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify(); 

            //return Array - you should try to resend the message to the tokens in the array
            $downstreamResponse->tokensToRetry();

            // return Array (key:token, value:errror) - in production you should remove from your database the tokens
            $downstreamResponse->tokensWithError();
            }
        }else{
            $input['user_type']='android';
            $tokens = User_Device::pluck('device_id')->toArray();
            if(User_Device::count()>0){
            $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            //return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete();

            //return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify();

            //return Array - you should try to resend the message to the tokens in the array
            $downstreamResponse->tokensToRetry();

            // return Array (key:token, value:errror) - in production you should remove from your database the tokens present in this array
            $downstreamResponse->tokensWithError();
            }
        }
        $notification = Notification::create($input);
        LogActivity::addToLog(User::find(Auth::id())->name.' mengirimnkan notifikasi '. $input['title']);
        return response()->json(['success' => true, 'message' => 'Notifikasi Dikirim', 'type' => 'create']);
    }
    public function allNotification()
    {
    	$notification=Notification::get();
        return response()->json(['success' => true, 'message' => 'semua notifikasi berhasil ditampilkan', 'data' => $notification]);
    }
    public function allNotificationUser()
    {
        $notification=Notification::where('user_id',Auth::id())->get();
        return response()->json(['success' => true, 'message' => 'semua notifikasi berhasil ditampilkan', 'data' => $notification]);
    }
    public function allNotificationUserHome()
    {
        $notification=Notification::where('user_id',Auth::id())->orWhere('user_type','android')->orderBy('created_at','DESC')->limit(4)->get();
        return response()->json(['success' => true, 'message' => 'semua notifikasi berhasil ditampilkan', 'data' => $notification]);
    }
    public function allNotificationPage($page)
    {
    	$notification=Notification::orderBy('created_at','DESC')->paginate($page);
        return response()->json(['success' => true, 'message' => 'semua notifikasi berhasil ditampilkan', 'data' => $notification]);
    }
    public function select($id)
    {
    	$notification=Notification::find($id);
        return response()->json(['success' => true, 'message' => 'notifikasi berhasil ditampilkan', 'data' => $notification]);
    }
    public function resend($id)
    {
    	$notification_data=Notification::find($id);
        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($notification_data->title);
        $notificationBuilder->setBody(strip_tags($notification_data->body))
                            ->setSound('default');
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' => $notification_data->title,'body' => strip_tags($notification_data->body),'notif_type'=>'general','notification_data'=>'','notification_title'=>'']);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = "famUG_YyUyQ:APA91bFbx5HjoRjbprV5Gr_8RgTsfsWDjaCc17RwjKJ2YaBzAbHFIJmu5c0Ix3acmHooOOlgA3F1wEreZqnPLf6yjFRMKUEfq3yVB2PucBU936HP7AInSieD5ouNWW7n93M0dAi7Fe1u";
        if($notification_data->user_type!="android"){
            $token=User_Device::where('user_id',$notification_data->user_id)->pluck('device_id')->toArray();
            $username=User::find($notification_data->user_id)->name;
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            //return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete(); 
            
            //return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify(); 

            //return Array - you should try to resend the message to the tokens in the array
            $downstreamResponse->tokensToRetry();

            // return Array (key:token, value:errror) - in production you should remove from your database the tokens
            $downstreamResponse->tokensWithError();
        }else{
            $tokens = User_Device::pluck('device_id')->toArray();

            $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            //return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete();

            //return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify();

            //return Array - you should try to resend the message to the tokens in the array
            $downstreamResponse->tokensToRetry();

            // return Array (key:token, value:errror) - in production you should remove from your database the tokens present in this array
            $downstreamResponse->tokensWithError();
        }
        return response()->json(['success' => true, 'message' => 'notifikasi berhasil dikirimkebali', 'data' => $notification_data]);
    }
    public function delete($id)
    {
        $notification=Notification::find($id);
        LogActivity::addToLog(User::find(Auth::id())->name.' menghapus notifikasi '. $input['title']);
        $value=$notification->delete();
        return response()->json(['success' => $value, 'message' => 'Notifikasi dihapus ']);
    }
    public function sendNotif($notificationdata)
    {
    	# code...
    }
}
