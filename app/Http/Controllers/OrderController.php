<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;
use Auth;
use DB;
use Hash;
use Image;
use Storage;
use Carbon\Carbon;
use App\User;
use App\Model\Rent\Rent;
use App\Model\Rent\Order;
use App\Model\Rent\Cart;
use App\Model\General_Info\Company;
use App\Model\General_Info\Study_Program;
use App\Model\General_Info\Profession;
use App\Model\General_Info\Education_Level;
use App\Http\Controllers\Api\OrderAPIController;
use App\Http\Controllers\Api\RentAPIController;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin', 'cs'])){
                $study_programs=Study_Program::pluck('name','id');
                $professions=Profession::pluck('name','id');
                $education_levels=Education_Level::pluck('name','id');
                redirect('/rent-request');
            }else{
                return view('frontend.orders.index');
            } 
        }else{
            return redirect('/login');
        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Order::where('id',$id)->count()>0){
            return redirect('/history/order/invoice/'.Order::find($id)->invoice);
        }else{
            abort(404, 'Not Found');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showInvoice($code)
    {
        if(Order::where('invoice',$code)->count()>0){
        $apiOrder= new OrderAPIController();
        $order= $apiOrder->showCode($code);
            if(Auth::check()){
                $user=User::find(Auth::id());
                if($user->hasRole(['admin', 'superadmin', 'cs'])){
                    return view('backend.order.detail',compact('order'));
                }else{
                    if($order->user_id==Auth::id()){
                        return view('backend.order.detail',compact('order')); 
                    }else{
                        return view('505');
                    }
                } 
            }else{
                return redirect('/login');
            }
        }else{
            abort(404, 'Not Found');
        }
    }

}
