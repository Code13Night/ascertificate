<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;
use Auth;
use DB;
use Hash;
use Image;
use Storage;
use Carbon\Carbon;
use App\User;
use App\Model\Certificate\Certificate;
use App\Model\Certificate\User_Certificate;
use App\Model\Certificate\Certificate_Education_Level;
use App\Model\Certificate\Certificate_Study_Program;
use App\Model\Certificate\Certificate_Type;
use App\Model\General_Info\Company;
use App\Model\General_Info\Study_Program;
use App\Model\General_Info\Profession;
use App\Model\General_Info\Education_Level;
use App\Http\Controllers\Api\CertificateAPIController;
use App\Http\Controllers\Api\RentAPIController;


class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin', 'cs'])){
                $study_programs=Study_Program::pluck('name','id');
                $professions=Profession::pluck('name','id');
                $education_levels=Education_Level::pluck('name','id');
                return view('backend.certificates.index',compact('study_programs','professions','education_levels'));
            }else{
                return view('505');
            } 
        }else{
            return redirect('/login');
        }   
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexList(Request $request)
    {
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin', 'cs'])){
                $certificates=Certificate::select(DB::raw('CONCAT( classification_code,"-",name) as code_name'),'id')->pluck('code_name','id');
                $users=User::pluck('name','id');
                $users->prepend('Pilih user','not_selected');
                return view('backend.certificates.user_certificates.index',compact('certificates','users'));
            }else{
                return view('505');
            } 
        }else{
            return redirect('/login');
        }   
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $apiCertificate= new CertificateAPIController();
        $certificate= $apiCertificate->show($id);
        $rent= User_Certificate::allCertificates()->where('user_certificates.certificate_id',$id)->get();
        $study_programs=Study_Program::pluck('name','id');
        $education_levels=Education_Level::pluck('name','id');
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin', 'cs'])){
                return view('backend.certificates.detail',compact('certificate','rent','study_programs','education_levels'));
            }else{
                return view('505');
            } 
        }else{
            return redirect('/login');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showcode($code)
    {
        $apiCertificate= new CertificateAPIController();
        $certificate= $apiCertificate->showbyCode($code);
        $rent= User_Certificate::allCertificates()->where('certificates.classification_code',$code)->get();
        if($certificate->type_id==1){
            $qualification_val=collect([['name'=>'Muda', 'value'=>'Muda'],['name'=>'Madya', 'value'=>'Madya'],['name'=>'Utama', 'value'=>'Utama']]);
        }else{ 
            $qualification_val=collect([['name'=>'Tingkat 1', 'value'=>'Tingkat 1'],['name'=>'Tingkat 2', 'value'=>'Tingkat 2'],['name'=>'Tingkat 3', 'value'=>'Tingkat 3']]);
        }
        $education_certificate=Certificate_Education_Level::where('certificate_id',$certificate->id)->select('education_level_id as id')->get();
        $study_certificate=Certificate_Study_Program::where('certificate_id',$certificate->id)->select('study_program_id as id')->get();
        $qualifications=$qualification_val->pluck('name','value');
        $study_programs=Study_Program::whereIn('id',$study_certificate)->pluck('name','id');
        $education_levels=Education_Level::whereIn('id',$education_certificate)->pluck('name','id');
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin', 'cs'])){
                return view('backend.certificates.detail',compact('certificate','rent','study_programs','education_levels','qualifications'));
            }else{
                return view('505');
            } 
        }else{
            return redirect('/login');
        } 
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showUserCertificateId($id)
    {
        $user_certificate=User_Certificate::find($id);
        return redirect('/certificate/registration-number/'.$user_certificate->registration_number); 
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationNumber($registration_number)
    {
        $certificate=User_Certificate::allCertificates()->where('registration_number',$registration_number);
        if($certificate->count()>0){
            $certificate=$certificate->first();
            if(Auth::check()){
                $user=User::find(Auth::id());
                return view('backend.certificates.user_certificates.userdetail',compact('certificate')); 
            }else{
                return redirect('/login');
            }
        }else{
            return view('505');
        }  
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function searchCertificate(Request $request,$query)
    {
        $order="certificates.created_at";
        $sort="DESC";
        $certificate=Certificate::searchCertificate($query)->where('status')->orderby($order,$sort)->paginate(6);
        $totalfound=Certificate::searchCertificate($query)->where('status')->count();
        if ($request->ajax()) {
        $view = view('backend.certificates.searchdata',compact('certificate'))->render();
                return response()->json(['html'=>$view]);
        }
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin', 'cs'])){
                return view('backend.certificates.search',compact('query','certificate','totalfound'));
            }else{
                return view('505');
            } 
        }else{
            return redirect('/login');
        } 
    }

}
