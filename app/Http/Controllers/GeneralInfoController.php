<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Hash;
use Image;
use Storage;
use Carbon\Carbon;
use App\User;

class GeneralInfoController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexCompany()
    {

        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin', 'cs'])){
                return view('backend.generalinfos.companies.index');
            }else{
                return view('505');
            } 
        }else{
            return redirect('/login');
        } 
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexStudyProgram()
    {
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin', 'cs'])){
                return view('backend.generalinfos.studyprograms.index');
            }else{
                return view('505');
            } 
        }else{
            return redirect('/login');
        }
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexEducationLevel()
    {
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin', 'cs'])){
                return view('backend.generalinfos.educationlevels.index');
            }else{
                return view('505');
            } 
        }else{
            return redirect('/login');
        }
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexProfession()
    {
         if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin', 'cs'])){
                return view('backend.generalinfos.professions.index');
            }else{
                return view('505');
            } 
        }else{
            return redirect('/login');
        }
    }
}
