<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Hash;
use Image;
use Storage;
use Carbon\Carbon;
use App\User;
use App\Model\Setting\Log;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin', 'cs'])){
                return view('backend.logs.index');
            }else{
                return view('505');
            } 
        }else{
            return redirect('/login');
        } 
    }
    public function allLog()
    {
    	$log=Log::allLog()->get();
        return response()->json(['success' => true, 'message' => 'semua log berhasil ditampilkan', 'data' => $log]);
    }
}
