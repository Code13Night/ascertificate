<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use DB;
use Hash;
use Image;
use Storage;
use Carbon\Carbon;
use App\Model\Certificate\Certificate;
use App\Model\General_Info\Company;
use App\Model\General_Info\Study_Program;
use App\Model\General_Info\Profession;
use App\Model\General_Info\Education_Level;
use App\Model\Rent\Cart;
use App\Model\Rent\Rent;
use App\Model\Rent\RequestCertificate;
use App\Model\Rent\RequestRelation;
use App\Model\Rent\Order;
use App\Model\Setting\Log;
use App\Model\Setting\Notification;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Auth::check()){
            $user=User::find(Auth::id());
            if(Cart::where('user_id',$user->id)->count()>0){
                session(['availablecart' =>  true]);
            }else{
                session(['availablecart' =>  false]);
            }
            if($user->hasRole(['admin', 'superadmin', 'cs'])){
                $get_rented=RequestRelation::whereIn('status',[3,4,5])->where('certificate_id','!=',null)->groupBy('certificate_id')->select(DB::raw('count(id) as total_rent'),'certificate_id')->get();
                if(count($get_rented)>0){

                    $most_rented=$get_rented->where('total_rent',$get_rented->max('total_rent'))->first();
                    $most_rented_certificate=Certificate::find($most_rented->certificate_id);
                    $most_rented_certificate->profession_name=Profession::find($most_rented_certificate->profession_id)->name;
                    if($most_rented_certificate->type_id==1){
                        $most_rented_certificate->type_name="SKA";
                    }
                    if($most_rented_certificate->type_id==2){
                        $most_rented_certificate->type_name="SKT";
                    }
                }else{
                    $most_rented_certificate=Certificate::get()->first();
                    $most_rented_certificate->profession_name=Profession::find($most_rented_certificate->profession_id)->name;
                    if($most_rented_certificate->type_id==1){
                        $most_rented_certificate->type_name="SKA";
                    }
                    if($most_rented_certificate->type_id==2){
                        $most_rented_certificate->type_name="SKT";
                    }
                }
                    $logs=Log::orderby('created_at','DESC')->limit(6)->get();
                    $total_order=Order::count();
                    $total_profession=Profession::count();
                    $order_process=Order::allOrders()->whereIn('orders.status',[0,1,2,4,5,6])->limit(10)->get();
                    $total_order_process=Order::whereIn('status',[0,1,2,4,5,6])->count();
                    $total_order_new=Order::where('status',0)->count();
                    $total_rent=RequestRelation::count();
                    $total_rent_new=RequestRelation::where('status',0)->count();
                return view('home',compact('most_rented_certificate','logs','total_order','total_order_process','total_profession','order_process','total_order_new','total_rent','total_rent_new'));
            }else{
                $id=Auth::id();
                //logs
                $logs=Log::where('user_id',$id)->orderby('created_at','DESC')->limit(6)->get();
                //coutning
                $total_order=Order::where('user_id',$id)->count();

                $total_order_new=Order::where('user_id',$id)->where('status',0)->count();
                $total_rent=RequestCertificate::allRequestsCertificate()->where('orders.user_id',$id)->count();
                $total_rent_new=RequestCertificate::allRequestsCertificate()->where('orders.user_id',$id)->where('request_certificates.status',0)->count();
                $total_rent_process=RequestCertificate::allRequestsCertificate()->where('orders.user_id',$id)->whereIn('request_certificates.status',[0,1,2,4,5,6])->count();
                $total_rent_available=RequestCertificate::allRequestsCertificate()->where('orders.user_id',$id)->whereIn('request_certificates.status',[3,4,5,6,7])->count();
                //getdata

                $order_process=Order::where('user_id',$id)->whereIn('status',[0,1,2,4,5,6])->get();
                $rent_process=RequestCertificate::allRequestsCertificate()->where('orders.user_id',$id)->whereIn('request_certificates.status',[0,1,2,4,5,6])->limit(6)->get();
                $rent_history=RequestCertificate::allRequestsCertificate()->where('orders.user_id',$id)->limit(6)->get();
                $rent_have=RequestCertificate::allRequestsCertificate()->where('orders.user_id',$id)->whereIn('request_certificates.status',[3,4,5,6,7])->limit(6)->get();
                return view('home',compact('logs','rent_process','total_rent_process','rent_history','rent_have','total_order','order_process','total_order_new','total_rent','total_rent_new','total_rent_available'));
            } 
        }else{
            return redirect('/');
        }
    }
    public function welcome()
    {
        return view('welcome');
    }
    public function ApiHome()
    {
        
        $certificate=Certificate::allCertificates()->limit(10)->inRandomOrder()->get();
        $company=Company::limit(10)->inRandomOrder()->get();
        $profession=Profession::limit(10)->inRandomOrder()->get();
        $education_level=Education_Level::limit(10)->inRandomOrder()->get();
        $study_program=Study_Program::limit(10)->inRandomOrder()->get();
        if (Auth::check()) {
            $rent=RequestCertificate::allRequests()->where('user_id',Auth::id())->limit(10)->orderby("requests.created_at","DESC")->get();
        }else{
            $rent=null;
        }
        return response()->json(['success' => true, 'message' => 'API Home ditampilkan', 'data_certificate' => $certificate,'data_company'=>$company,'data_profession'=>$profession,'data_education_level'=>$education_level,'data_study_program'=>$study_program,'data_rent'=>$rent]);
    }
}
