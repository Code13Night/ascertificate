<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Hash;
use Image;
use Storage;
use Carbon\Carbon;
use LogActivity;
use App\User;
use App\User_General_Info;
use App\Model\Certificate\Certificate;
use App\Model\General_Info\City;
use App\Model\General_Info\Province;
use App\Model\General_Info\Company;
use App\Model\General_Info\Study_Program;
use App\Model\General_Info\Profession;
use App\Model\General_Info\Education_Level;
use App\Model\Rent\Cart;
use App\Model\Rent\RequestCertificate;
use App\Model\Rent\RequestRelation;


class RequestCertificateController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin','cs'])){
                return view('backend.rents.requests.index');
            }else{
            	return view('frontend.rents.requests.indexList');
            } 
        }else{
            return redirect('/login');
        } 
    }
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRent()
    {
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin','cs'])){
                $total_rent=RequestRelation::whereIn('status',[3,4,5,6,7])->count();
                $soft_rent=RequestRelation::whereIn('status',[3,4])->count();
                $hard_rent=RequestRelation::whereIn('status',[6,7])->count();
                return view('backend.rents.index',compact('total_rent','soft_rent','hard_rent'));
            }else{
            return view('frontend.rents.index');
            } 
        }else{
            return redirect('/login');
        } 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRequest(Request $request)
    {
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin','cs'])){
               	return redirect('history/request');
            }else{
                if(Cart::where('user_id',$user->id)->count()>0){
                    session(['availablecart' =>  true]);
                    session(['budget_type'=>Cart::where('user_id',$user->id)->first()->budget_type]);
                    session(['province'=>Cart::where('user_id',$user->id)->first()->province]);
                    session(['city'=>Cart::where('user_id',$user->id)->first()->city]);
                    session(['company'=>Cart::where('user_id',$user->id)->first()->company]);
                    session(['position'=>Cart::where('user_id',$user->id)->first()->position]);
                }else{
                    session(['availablecart' =>  false]);
                }
                $general_info_user=User_General_Info::where('user_id',$user->id);
                if($general_info_user->count()>0){
                    $general_info_user=$general_info_user->first();
                    session(['profession_id' =>  $general_info_user->profession_id]);
                    session(['study_program_id' =>  $general_info_user->study_program_id]);
                    session(['education_level_id' =>  $general_info_user->education_level_id]);
                    session(['graduate_year' =>  $general_info_user->graduate_year]);
                }
                session(['username' => $user->name]);
                $certificates=Certificate::select(DB::raw('CONCAT( classification_code," - ",name) as code_name'),'id')->where('type_id',1);
                if($request->session()->has('profession_id')){
                    $certificates=$certificates->where('profession_id',session('profession_id'));
                }
                $certificates=$certificates->pluck('code_name','id');
                $study_programs=Study_Program::pluck('name','id');
                $professions=Profession::pluck('name','id');
                $provinces=Province::pluck('name','name');
                $cities=City::pluck('name','name');
                $education_levels=Education_Level::pluck('name','id');
                return view('frontend.rents.requests.index',compact('study_programs','professions','cities','provinces','education_levels','certificates'));
            } 
        }else{
            return redirect('/login');
        } 
    }
}
