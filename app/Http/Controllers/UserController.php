<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use App\User;
use App\Role;
use App\User_Device;
use App\User_General_Info;
use App\Model\Setting\Log;
use App\Model\General_Info\Company;
use App\Model\General_Info\Study_Program;
use App\Model\General_Info\Profession;
use App\Model\General_Info\Education_Level;
use App\Model\Rent\Rent;
use App\Http\Controllers\Api\RentAPIController;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;
use Hash;
use Image;
use Storage;
use Excel;

class UserController extends Controller
{
    public function __construct(Guard $auth) {
        $this->auth = $auth;
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = Carbon::now();
        $yearorders=substr($now->year, 2, 4);
        $yearorders=(string)$yearorders;
        $monthorders=(string)$now->month;
        $yearordersnoformat=(string)$now->year;
        $userMonthTotal=User::where(DB::raw('month(created_at)'),'>=',$monthorders)->count();
        $userTotal=User::count();

        if(User::find(Auth::id())->hasRole('superadmin')){
            
            $roles = Role::where('name','!=','user')->pluck('display_name','id');;
        }else if(User::find(Auth::id())->hasRole('admin')){
            
            $roles = Role::where('name','!=','user')->where('name','!=','superadmin')->pluck('display_name','id');
        }else{
            
            $roles = Role::where('name','!=','user')->where('name','!=','admin')->where('name','!=','superadmin')->pluck('display_name','id');
        }

        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin'])){
                return view('backend.users.admins.index',compact('userMonthTotal','userTotal','roles'));
            }else{
                return view('505');
            } 
        }else{
            return redirect('/login');
        } 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexCustomer()
    {
        $now = Carbon::now();
        $yearorders=substr($now->year, 2, 4);
        $yearorders=(string)$yearorders;
        $monthorders=(string)$now->month;
        $yearordersnoformat=(string)$now->year;
        $userMonthTotal=User::where(DB::raw('month(created_at)'),'>=',$monthorders)->count();
        $userTotal=User::count();
        if(Auth::check()){
            $user=User::find(Auth::id());
            if($user->hasRole(['admin', 'superadmin'])){
                return view('backend.users.customers.index',compact('userMonthTotal','userTotal'));
            }else{
                return view('505');
            } 
        }else{
            return redirect('/login');
        } 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        if(Auth::check()){
            $user=User::find(Auth::id());
            $general_info=User_General_Info::where('user_id',Auth::id())->get();
            $general_info_all=$general_info;
            if(count($general_info)>0){
                $general_info= $general_info->first();
                if($general_info->profession_id==null||$general_info->profession_id=="")
                {
                    $general_info->profession_name="-";
                }else{
                    $general_info->profession_name=Profession::find($general_info->profession_id)->name;
                } 
                if($general_info->education_level_id==null||$general_info->education_level_id=="")
                {
                    $general_info->education_level_name="-";
                }else{
                    $general_info->education_level_name=Education_level::find($general_info->education_level_id)->name;
                }
                if($general_info->study_program_id==null||$general_info->study_program_id=="")
                {
                    $general_info->study_program_name="-";
                }else{
                    $general_info->study_program_name=Study_Program::find($general_info->study_program_id)->name;
                }
                if($general_info->company_id==null||$general_info->company_id=="")
                {
                    $general_info->company_name="-";
                }else{
                    $general_info->company_name=Company::find($general_info->company_id)->name;
                }
            }
            $logs=Log::where('user_id',Auth::id())->orderBy('created_at','DESC')->limit(10)->get();
            $apiRent= new RentAPIController();
            $rent= $apiRent->allRentForDetailCertificatebyUser(Auth::id(),6);
            if ($request->ajax()) {
            $view = view('backend.rents.data',compact('rent'))->render();
                    return response()->json(['html'=>$view]);
            }
            $study_programs=Study_Program::pluck('name','id');
            $professions=Profession::pluck('name','id');
            $education_levels=Education_Level::pluck('name','id');
            $companies=Company::pluck('name','id');
            return view('backend.users.profile',compact('user','general_info','general_info_all','logs','rent','study_programs','professions','education_levels','companies')); 
        }else{
            return redirect('/login');
        } 
    }
}
