<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use DB;
use Hash;
use Image;
use Storage;
use Carbon\Carbon;
use LogActivity;
use App\Model\Certificate\Certificate;
use App\Model\Rent\Rent;
use App\Model\Payment\Payment;
use App\User;
use App\User_Device;
use App\User_General_Info;
use App\Model\Rent\Cart;
use App\Model\Rent\Order;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\Model\Setting\Notification;
use FCM;use DateTime;

use App\Http\Controllers\Api\GeneralInfoAPIController;
class RentAPIController extends Controller
{

    public function sendNotif($var){

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);

        $title=""; $Body="";
        if($var['notif_type']=="soft_sended"){
            $title="Soft copy Sertifikat Dikirim";
            $Body="Soft copy sertifikat ".$var['name']." telah dikirim";
        }else if($var['notif_type']=="hard_sended"){
            $title="Hard copy Sertifikat Dikirim";
            $Body="Hard copy sertifikat ".$var['name']." telah dikirim";
        }else if($var['notif_type']=="expired"){
            $title="Sertifikat Kadaluarsa ";
            $Body="Sertifikat ".$var['name']." telah habis masa sewanya ";
        }else{
            $title="Sewa Sertifikat Dibatalkan";
            $Body="Sertifikat ".$var['name']." telah dibatalkan";
        }

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody( $Body)
                            ->setSound('default');
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' =>  $title,'body' =>$Body,'notif_type'=>'rent_detail','notification_data'=>$var['rent_id'],'notification_title'=>$var['serial_number']]);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();

        $data = $dataBuilder->build();
            $token=User_Device::where('user_id',$var['user_id'])->pluck('device_id')->toArray();
            if(User_Device::where('user_id',$var['user_id'])->count()>0){
            $username=User::find($request->input('user_id'))->name;
            $input['user_type']=$username;
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            //return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete(); 

            //return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify(); 

            //return Array - you should try to resend the message to the tokens in the array
            $downstreamResponse->tokensToRetry();

            // return Array (key:token, value:errror) - in production you should remove from your database the tokens
            $downstreamResponse->tokensWithError();

            }
        $var['body']=$Body;
        $var['title']=$title;
        $var['data_type']="rent_detail";
        $var['data_id']=$var['rent_id'];
        $var['user_type']=User::find($var['user_id'])->name;    
        $notification = Notification::create($var);

    }

   public function clean($string) {
       $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
       $string = str_replace('-', '', $string); // Replaces all spaces with hyphens.

       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
    /*** User ***/
    public function save(Request $request)
    {

        $rentAPI= new RentAPIController();
    	$input=$request->all();
        $now = Carbon::now('Asia/Jakarta');
        $str=$rentAPI->clean($now);
        $input['serial_number']="REQ".$str;
        $input['user_id']=Auth::id();
    	$rent=Rent::create($input);
        LogActivity::addToLog(User::find(Auth::id())->name.' Menyewa Sertifikat');
		return response()->json(['success' => true, 'message' => 'Data sewa berhasil disimpan', 'data'=> $rent]);
    }
    public function update(Request $request,$id)
    {
        $input=$request->all();
        $rent=Rent::find($id);
        LogActivity::addToLog(User::find(Auth::id())->name.' mengubah data penyewaan sertifikat '. $rent->serial_number);
        $rent->update($input);
        return response()->json(['success' => true, 'message' => 'Update data penyewaan berhasil', 'type' => 'update']);
    }
    public function delete($id)
    {
        $rent= Rent::find($id);
        LogActivity::addToLog(User::find(Auth::id())->name.' menghapus penyewaan sertifikat '. $rent->serial_number);
        $rent->delete();
        return response()->json(['success' => true, 'message' => 'Update data penyewaan berhasil', 'type' => 'delete']);
    }
    public function allRentForDetailCertificate($id)
    {
        $order='rents.created_at';
        $sort='DESC';
        $rent=Rent::allRentsByUser()->where('certificate_id',$id)->orderby($order,$sort)->limit(7)->get();
        return $rent;   
    }
    public function allRentForDetailCertificatebyCode($code)
    {
        $order='rents.created_at';
        $sort='DESC';
        $certificate=Certificate::where('classification_code',$code)->first();
        $rent=Rent::allRentsByUser()->where('certificate_id',$certificate->id)->orderby($order,$sort)->limit(7)->get();
        return $rent;   
    }
    public function allRentForDetailCertificatebyUser($id,$page)
    {
        $order='rents.created_at';
        $sort='DESC';
        $rent=Rent::allRentsByUser()->where('user_id',$id)->orderby($order,$sort)->paginate($page);
        return $rent;   
    }
    public function allRent()
    {
        $order='rents.created_at';
        $sort='DESC';
        $rent=Rent::allRents()->orderby($order,$sort)->get();
        return response()->json(['success' => true, 'message' => 'Semua data sewa berhasil didapatkan', 'data' => $rent]);   
    }
    public function allRentStatus($status)
    {
        $rent=Rent::allRents();
                $user=User::find(Auth::id());
        if($user->hasRole('user')){
            $id=Auth::id();
            $rent=$rent->where('orders.user_id',$id);
        }
        $order='rents.created_at';
        $sort='DESC';
        switch ($status) {
            case 'request':
                $rent=$rent->where('rents.status',0)->orderby($order,$sort)->get();
                break;
            case 'waiting_for_payment':
                $rent=$rent->where('rents.status',1)->orderby($order,$sort)->get();
                break;
            case 'process_soft_copy':
                $rent=$rent->where('rents.status',2)->orderby($order,$sort)->get();
                break;
            case 'soft_copy_finish':
                $rent=$rent->where('rents.status',3)->orderby($order,$sort)->get();
                break;
            case 'request_hard_copy':
                $rent=$rent->where('rents.status',4)->orderby($order,$sort)->get();
                break;
            case 'waiting_for_payment_hard_copy':
                $rent=$rent->where('rents.status',5)->orderby($order,$sort)->get();
                break;
            case 'process_hard_copy':
                $rent=$rent->where('rents.status',6)->orderby($order,$sort)->get();
                break;
            case 'hard_copy_finish':
                $rent=$rent->where('rents.status',7)->orderby($order,$sort)->get();
                break;
            case 'cancel':
                $rent=$rent->where('rents.status',8)->orderby($order,$sort)->get();
                break;
            case 'expired':
                $rent=$rent->where('rents.status',9)->orderby($order,$sort)->get();
                break;
            default:
                $rent=$rent->orderby($order,$sort)->get();
                break;
        }
        return response()->json(['success' => true, 'message' => 'Semua data sewa berhasil didapatkan', 'data' => $rent]);   
    }
    public function allRentOrder($order,$sort)
    {
        switch ($order) {
            case "valid_date":
                $order="rents.valid_until";
                break;
            case "certificate_name":
                $order="certificate.name";
                break;
            case "issued_date":
                $order="rents.date_of_issued";
                break;
            case "classification":
                $order="certificate.classification_code";
                break;
            case "serial_number":
                $order="rents.serial_number";
                break;
            default:
                $order="rents.created_at";
                break;
        }
        $rent=Rent::allRents()->orderby($order,$sort)->get();
        return response()->json(['success' => true, 'message' => 'Semua data sewa berhasil didapatkan', 'data' => $rent]);   
    }
    public function allRentUser($type,$page)
    {
        $now = Carbon::now('Asia/Jakarta');
        $rent=Rent::allRents()->where('orders.user_id',Auth::id());
        switch ($type) {
            case "request":
                $rent=$rent->where('rents.status',0);
                break;
            case "in_process":
                $rent=$rent->whereIn('rents.status',[1,2,4,5]);
                break;
            case "available":
                $rent=$rent->whereIn('rents.status',[3,4,5,6,7]);
                break;
            case "expired":
                $rent=$rent->where('rents.status',9);
                break;
            default:
                break;
        }
        $order='rents.created_at';
        $sort='DESC';
        $rent=$rent->orderby($order,$sort)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Semua data sewa berhasil didapatkan', 'data' => $rent]);   
    }
    public function allRentUserOrderBy($type,$page,$order,$sort)
    {
        $now = Carbon::now('Asia/Jakarta');
        $rent=Rent::allRents()->where('orders.user_id',Auth::id());
        switch ($type) {
            case "request":
                $rent=$rent->where('rents.status',0);
                break;
            case "in_process":
                $rent=$rent->whereIn('rents.status',[1,2,4,5]);
                break;
            case "available":
                $rent=$rent->whereIn('rents.status',[3,4,5,6,7]);
                break;
            case "expired":
                $rent=$rent->where('rents.status',9);
                break;
            default:
                break;
        }
        switch ($order) {
            case "valid_date":
                $order="rents.valid_until";
                break;
            case "certificate_name":
                $order="certificate.name";
                break;
            case "issued_date":
                $order="rents.date_of_issued";
                break;
            case "classification":
                $order="certificate.classification_code";
                break;
            case "serial_number":
                $order="rents.serial_number";
                break;
            default:
                $order="rents.created_at";
                break;
        }
        if($sort==""||$sort==null){
            $sort="DESC";
        }
        $rent=$rent->orderby($order,$sort)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Semua data sewa order  berhasil didapatkan', 'data' => $rent]);   

    }
    public function updateStatusRentHardCopy(Request $request, $id)
    {
        $input=$request->all();
        $order=Order::find($id)->update(array('status' => 4));
        foreach ($input['rent_ids'] as $key => $value) {
            $rent=Rent::find($value['id']);
            $rent->update(array('status' => 4));
            LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. Rent::find($value['id'])->serial_number. ' menjadi hardcopy menunggu konfirmasi admin');
        }
        return response()->json(['success' => true, 'message' => 'Update Status penyewaan berhasil']);
    }
    public function updateSendRentHardCopy(Request $request, $id)
    {
        $input=$request->all();
        $order=Order::find($id)->update(array('status' => 7));
        $now = Carbon::now('Asia/Jakarta');
        $rents=Rent::where('order_id',$id)->where('status',6)->get();
           $input=$request->all();
        foreach ($rents as $key => $rent) {
                if($request->has('pick_up_by')==false){
                    $input['pick_up_by']=User::find(Order::find($rent->order_id)->user_id)->name;
                }
                if($request->has('pick_up_date')==false){
                    $input['pick_up_date']=$now;
                }else{
                    $input['pick_up_date'] = Carbon::createFromFormat('m/d/Y', $input['pick_up_date'])->toDateTimeString();
                }
                if($request->has('exact_return_date')==false){
                    $input['exact_return_date']=$now->addDays(Certificate::find($rent->certificate_id)->period_of_validity);
                }else{
                    $input['exact_return_date'] =  Carbon::createFromFormat('m/d/Y', $input['exact_return_date'])->toDateTimeString();
                }
                $rent->update($input);
                $rent->update(array('status' => 7,'hard_copy' => 1));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi hardcopy diterima penyewa');
        }
                

        return response()->json(['success' => true, 'message' => 'Update Status penyewaan berhasil']);
    }
    public function updateStatusRent(Request $request, $type, $id)
    {
        $now = Carbon::now('Asia/Jakarta');
        $rent=Rent::find($id);
        switch ($type) {
            case "hard_sended":
                $notif_type="hard_sended";
                $input=$request->all();
                if($request->has('pick_up_by')==false){
                    $input['pick_up_by']=User::find(Order::find($rent->order_id)->user_id)->name;
                }
                if($request->has('pick_up_date')==false){
                    $input['pick_up_date']=$now;
                }
                if($request->has('exact_return_date')==false){
                    $input['exact_return_date']=$now->addDays(Certificate::find($rent->certificate_id)->period_of_validity);
                }
                $rent->update($input);
                $rent->update(array('status' => 7,'hard_copy' => 1));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi hardcopy diterima penyewa');
                $rentAPI= new RentAPIController();
                $notif;
                $notif['notif_type']=$notif_type;
                $notif['user_id']=Order::find($rent->order_id)->user_id;
                $notif['rent_id']=$rent->id;
                $notif['name']=$rent->name;
                $notif['serial_number']=$rent->serial_number;
                $rentAPI->sendNotif($notif);
                break;
            case "cancel":
                $notif_type="cancel";
                $rent->update(array('status' => 8,'hard_copy' => 0));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi dibatalkan');

                $rentAPI= new RentAPIController();
                $notif;
                $notif['notif_type']=$notif_type;
                $notif['user_id']=Order::find($rent->order_id)->user_id;
                $notif['rent_id']=$rent->id;
                $notif['name']=$rent->name;
                $notif['serial_number']=$rent->serial_number;
                $rentAPI->sendNotif($notif);
                break;
            case "hard_payed":
                $rent->update(array('status' => 6));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi hard copy dibayar');
                break;
            case "accept":
                $rent->update(array('status' => 1));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi hard copy dibayar');
                break;
            case "request":
                $rent->update(array('status' => 0));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi hard copy dibayar');
                break;
            case "soft_payed":
                $rent->update(array('status' => 2));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi soft copy dibayar');
                break;
            case "soft_process":
                $input=$request->all();
                if($request->has('date_of_issued')==false){
                    $input['date_of_issued']=$now;
                    $input['valid_until']=$now->addDays(Certificate::find($rent->certificate_id)->period_of_validity); 
                }else{
                    $now=Carbon::createFromDateString($input['date_of_issued'], 'Asia/Jakarta');
                    $input['valid_until']=$now->addDays(Certificate::find($rent->certificate_id)->period_of_validity); 
                }
                $rent->update($input);
                LogActivity::addToLog(User::find(Auth::id())->name.' update informasi penyewaan '. $rent->serial_number);
                break;
            case "soft_sended":
                $notif_type="soft_sended";
                $input=$request->all();
                if($request->has('date_of_issued')==false){
                    $input['date_of_issued']=$now;
                    $input['valid_until']=$now->addDays(Certificate::find($rent->certificate_id)->period_of_validity); 
                }else{
                    $now=Carbon::createFromDateString($input['date_of_issued'], 'Asia/Jakarta');
                    $input['valid_until']=$now->addDays(Certificate::find($rent->certificate_id)->period_of_validity); 
                }
                $rent->update($input);
                $rent->update(array('status' => 3));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi soft copy dikirim');

                $rentAPI= new RentAPIController();
                $notif;
                $notif['notif_type']=$notif_type;
                $notif['user_id']=Order::find($rent->order_id)->user_id;
                $notif['rent_id']=$rent->id;
                $notif['name']=$rent->name;
                $notif['serial_number']=$rent->serial_number;
                $rentAPI->sendNotif($notif);
                break;
            case "cancel_payment_soft":
                $rent->update(array('status' => 1));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi soft copy belum dibayar');
                break;
            case "cancel_payment_hard":
                $rent->update(array('status' => 5));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi hard copy belum dibayar');
                break;
            default:
                return response()->json(['success' => false, 'message' => 'Update Status penyewaan gagal, Pilihan tidak ditemukan']);
                break;
        }

        return response()->json(['success' => true, 'message' => 'Update Status penyewaan berhasil']);
    }
    public function updateStatusRentviaOrder($type, $id)
    {
        $now = Carbon::now('Asia/Jakarta');
        $rent=Rent::find($id);
        switch ($type) {
            case "hard_sended":
                $notif_type="soft_sended";
                $input=$request->all();
                if($request->has('pick_up_by')==false){
                    $input['pick_up_by']=User::find(Order::find($rent->order_id)->user_id)->name;
                }
                if($request->has('pick_up_date')==false){
                    $input['pick_up_date']=$now;
                }
                if($request->has('return_date')==false){
                    $input['return_date']=$now->addDays(Certificate::find($rent->certificate_id)->period_of_validity);
                }
                $rent->update($input);
                $rent->update(array('status' => 7,'hard_copy' => 1));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi hard copy dikirim');
                $rentAPI= new RentAPIController();
                $notif;
                $notif['notif_type']=$notif_type;
                $notif['user_id']=Order::find($rent->order_id)->user_id;
                $notif['rent_id']=$rent->id;
                $notif['name']=$rent->name;
                $notif['serial_number']=$rent->serial_number;
                $rentAPI->sendNotif($notif);
                break;
            case "cancel":
                $notif_type="cancel";
                $rent->update(array('status' => 8,'hard_copy' => 0));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi dibatalkan');

                $rentAPI= new RentAPIController();
                $notif;
                $notif['notif_type']=$notif_type;
                $notif['user_id']=Order::find($rent->order_id)->user_id;
                $notif['rent_id']=$rent->id;
                $notif['name']=$rent->name;
                $notif['serial_number']=$rent->serial_number;
                $rentAPI->sendNotif($notif);
                break;
            case "hard_payed":
                $rent->update(array('status' => 6));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi hard copy dibayar');
                break;
            case "accept":
                $rent->update(array('status' => 1));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi hard copy dibayar');
                break;
            case "request":
                $rent->update(array('status' => 0));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi hard copy dibayar');
                break;
            case "soft_payed":
                $rent->update(array('status' => 2));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi soft copy dibayar');
                break;
            case "soft_sended":
                $notif_type="soft_sended";
                $input=$request->all();
                if($request->has('date_of_issued')==false){
                    $input['date_of_issued']=$now;
                    $input['valid_until']=$now->addDays(Certificate::find($rent->certificate_id)->period_of_validity); 
                }else{
                    $now=Carbon::createFromDateString($input['date_of_issued'], 'Asia/Jakarta');
                    $input['valid_until']=$now->addDays(Certificate::find($rent->certificate_id)->period_of_validity); 
                }
                $rent->update($input);
                $rent->update(array('status' => 3));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi soft copy dikirim');

                $rentAPI= new RentAPIController();
                $notif;
                $notif['notif_type']=$notif_type;
                $notif['user_id']=Order::find($rent->order_id)->user_id;
                $notif['rent_id']=$rent->id;
                $notif['name']=$rent->name;
                $notif['serial_number']=$rent->serial_number;
                $rentAPI->sendNotif($notif);
                break;
            case "cancel_payment_soft":
                $rent->update(array('status' => 1));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi soft copy belum dibayar');
                break;
            case "cancel_payment_hard":
                $rent->update(array('status' => 5));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi hard copy belum dibayar');
                break;
            default:
                return response()->json(['success' => false, 'message' => 'Update Status penyewaan gagal, Pilihan tidak ditemukan']);
                break;
        }

        return response()->json(['success' => true, 'message' => 'Update Status penyewaan berhasil']);
    }
    public function autoUpdateExpired()
    {
        Rent::where('valid_until','<=', DB::raw('subdate(curdate(), 1)'))->update(array('status' => 7));
        $user_id=Rent::where('valid_until','<=', DB::raw('subdate(curdate(), 1)'))->select('user_id')->get();
       

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);

        $title=""; $Body="";
            $title="Sertifikat Kadaluarsa";
            $Body="Sertifikat yang anda sewa sudah habis masa berlakunya";

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody( $Body)
                            ->setSound('default');
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' =>  $title,'body' =>$Body,'notif_type'=>'rent_general','notification_data'=>'','notification_title'=>'']);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();

        $data = $dataBuilder->build();
            $token=User_Device::whereIn('user_id',$user_id)->pluck('device_id')->toArray();
            if(User_Device::whereIn('user_id',$user_id)->count()>0){
            $username=User::find($request->input('user_id'))->name;
            $input['user_type']=$username;
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            //return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete(); 

            //return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify(); 

            //return Array - you should try to resend the message to the tokens in the array
            $downstreamResponse->tokensToRetry();

            // return Array (key:token, value:errror) - in production you should remove from your database the tokens
            $downstreamResponse->tokensWithError();
            $var['body']=$Body;
            $var['title']=$title;
            $var['data_type']="rent_general";
            $var['data_id']='';
            $var['user_type']='custom user';    
            $notification = Notification::create($var);

            }
        return response()->json(['success' => true, 'message' => 'Otomatis Expired Berhasil dijalankan', 'data'=>$rent]);
    }
    public function autoNotifAlmostExpired()
    {
        $user_id=Rent::where('valid_until','>',DB::raw('curdate()'))->where('valid_until','<=', DB::raw('DATE_ADD(curdate(), 3)'))->select('user_id')->get();
        $device=User_Device::whereIn('user_id',$user_id)->get();

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);

        $title=""; $Body="";
            $title="Sertifikat Akan Kadaluarsa";
            $Body="Mohon cek masa berlaku sertifikat yang anda miliki";

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody( $Body)
                            ->setSound('default');
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' =>  $title,'body' =>$Body,'notif_type'=>'rent_general','notification_data'=>'','notification_title'=>'']);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();

        $data = $dataBuilder->build();
            $token=User_Device::whereIn('user_id',$user_id)->pluck('device_id')->toArray();
            if(User_Device::whereIn('user_id',$user_id)->count()>0){
            $username=User::find($request->input('user_id'))->name;
            $input['user_type']=$username;
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            //return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete(); 

            //return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify(); 

            //return Array - you should try to resend the message to the tokens in the array
            $downstreamResponse->tokensToRetry();

            // return Array (key:token, value:errror) - in production you should remove from your database the tokens
            $downstreamResponse->tokensWithError();
            $var['body']=$Body;
            $var['title']=$title;
            $var['data_type']="rent_general";
            $var['data_id']='';
            $var['user_type']='custom user';    
            $notification = Notification::create($var);

            }
        return response()->json(['success' => true, 'message' => 'Otomatis Expired Berhasil dijalankan', 'data'=>$rent]);
    }
    public function select($id)
    {
        $rent=Rent::find($id);
        $rent->certificate_detail=Certificate::find($rent->certificate_id);
        // $rent->user_detail=User::find($rent->certificate_id);
        $rent->payment_detail=Payment::where('order_id',$rent->order_id)->first();
        $rent->order_detail=Order::find($rent->order_id);
        return response()->json(['success' => true, 'message' => 'Data penyewaan berhasil ditampilkan', 'data'=>$rent]);
    }
    public function show($id)
    {
        $rent=Rent::find($id);
        $rent->certificate_detail=Certificate::find($rent->certificate_id);
        // $rent->user_detail=User::find($rent->certificate_id);
        $rent->payment_detail=Payment::where('order_id',$rent->order_id)->first();
        $rent->order_detail=Order::find($rent->order_id);
        return $rent;
    }
}
