<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use DB;
use Hash;
use Image;
use Storage;
use Carbon\Carbon;
use LogActivity;
use App\Model\Certificate\Certificate;
use App\Model\Rent\Rent;
use App\Model\Rent\Order;
use App\Model\Rent\RequestCertificate;
use App\Model\Rent\RequestRelation;
use App\Model\Payment\Payment;
use App\User;
use App\User_Device;
use App\User_General_Info;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\Model\Setting\Notification;
use FCM;
use Illuminate\Support\Facades\Mail;

class PaymentAPIController extends Controller
{
    public function sendNotif($var){

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);

        $title=""; $Body="";
        if($var['notif_type']=="generated"){
            $title="Order Diterima";
            $Body="Order ".$var['invoice']." telah diterima oleh admin";
        }else if($var['notif_type']=="paysoft"){
            $title="Pembayaran Softcopy";
            $Body="Pembayaran Softcopy Order ".$var['invoice']." telah diverifikasi oleh admin ";
        }else if($var['notif_type']=="payhard"){
            $title="Pembayaran Hardcopy";
            $Body="Pembayaran Hardcopy Order ".$var['invoice']." telah diverifikasi oleh admin ";
        }else{
            $title="Order Dibatalkan";
            $Body="Order ".$var['invoice']." telah dibatalkan oleh admin";
        }

        $notificationBuilder = new PayloadNotificationBuilder( $title);
        $notificationBuilder->setBody( $Body)
                            ->setSound('default');
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' =>  $title,'body' =>$Body,'notif_type'=>'order_detail','notification_data'=>$var['order_id'],'notification_title'=>$var['invoice']]);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();

        $data = $dataBuilder->build();
            $token=User_Device::where('user_id',$var['user_id'])->pluck('device_id')->toArray();
            if(User_Device::where('user_id',$var['user_id'])->count()>0){
            $username=User::find($request->input('user_id'))->name;
            $input['user_type']=$username;
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            //return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete(); 

            //return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify(); 

            //return Array - you should try to resend the message to the tokens in the array
            $downstreamResponse->tokensToRetry();

            // return Array (key:token, value:errror) - in production you should remove from your database the tokens
            $downstreamResponse->tokensWithError();

            }
        $var['body']=$Body;
        $var['title']=$title;
        $var['data_type']="order_detail";
        $var['data_id']=$var['order_id'];
        $var['user_type']=User::find($var['user_id'])->name;    
        $notification = Notification::create($var);

        
        
    }

    /*** User ***/
    public function save(Request $request)
    {
        $input=$request->all();
        if(Payment::where('order_id',$input['order_id'])->count()>0){
            return response()->json(['success' => false, 'message' => 'Data pembayaran sewa telah dibuat']);
        }
        $checkanother=true;
        $order=Order::find($input['order_id']);
        $notif_type="generated";
            if ($request->hasFile('prove_phase_soft_copy')) {
                $checkanother=false;
                $notif_type="paysoft";
                $thumb = public_path('images/thumb/prove_soft_copy');
                $full = public_path('images/full/prove_soft_copy');
                $scalled = public_path('images/scalled/prove_soft_copy');
                $image = $request->file('prove_phase_soft_copy');
                $name = str_random(5) . '.' . $image->getClientOriginalExtension();
                Image::make($image)->save($full . '/' . $name);
                Image::make($image)->resize('300', '300')->save($thumb . '/' . $name);
                Image::make($image)->resize('480', '640')->save($scalled . '/' . $name);
                $order->update(array('status' => 2 ));
                RequestRelation::whereIn('request_id',RequestCertificate::where('order_id',$input['order_id'])->select('id as request_id')->get())->whereIn('status',[1])->update(array('status' => 2 ));
                $input['prove_phase_soft_copy']= 'images/full/prove_soft_copy/'. $name;
            }
            if ($request->hasFile('prove_phase_hard_copy')) {
                $checkanother=false;
                $notif_type="payhard";
                $thumb = public_path('images/thumb/prove_hard_copy');
                $full = public_path('images/full/prove_hard_copy');
                $scalled = public_path('images/scalled/prove_hard_copy');
                $image = $request->file('prove_phase_hard_copy');
                $name = str_random(5) . '.' . $image->getClientOriginalExtension();
                Image::make($image)->save($full . '/' . $name);
                Image::make($image)->resize('300', '300')->save($thumb . '/' . $name);
                Image::make($image)->resize('480', '640')->save($scalled . '/' . $name);
                $order->update(array('status' => 6 ));
                RequestRelation::whereIn('request_id',RequestCertificate::where('order_id',$input['order_id'])->select('id as request_id')->get())->whereIn('status',[5])->update(array('status' => 6 ));
                $input['prove_phase_hard_copy']= 'images/full/prove_hard_copy/'. $name;
            }
        $payment=Payment::create($input);
        if($checkanother){
            if($order->status==0){
                $order->update(array('status' => 1 ));
                $rent= RequestRelation::whereIn('request_id',RequestCertificate::where('order_id',$input['order_id'])->select('id as request_id')->get())->where('request_certificates.status', 0)->get();
                $rentx=RequestCertificate::allRequestsByCertificate()->whereIn('request_id',RequestCertificate::where('order_id',$input['order_id'])->select('id as request_id')->get())->where('request_certificates.status', 0)->get();
                $invoicesoft = array('order' => $order,'rents'=>$rentx,'payment'=>$payment);
                Mail::send('backend.mail.mailinvoicesoft', $invoicesoft, function($message) use ($order,$rent)
                    {
                        $message->from('admin@skaskt.id')->to(User::find($order->id)->email)->subject('Mohon lakukan pembayaran penyewaan sertifikat soft copy untuk order '.$order->invoice);
                    });
                foreach ($rent as $key => $value) {
                    $value->update(array('status' => 1));
                } 
            }

        }
        $paymentAPI= new PaymentAPIController();
        $notif;
        $notif['notif_type']=$notif_type;
        $notif['user_id']=$order->user_id;
        $notif['order_id']=$order->id;
        $notif['invoice']=$order->invoice;
        $paymentAPI->sendNotif($notif);

        LogActivity::addToLog(User::find(Auth::id())->name.' membuat pembayaran untuk Order '.Order::find($payment->order_id)->invoice);
        return response()->json(['success' => true, 'message' => 'Data pembayaran sewa berhasil disimpan', 'data'=> $payment]);
    }
    public function update(Request $request,$id)
    {
        $input=$request->all();
        $checkanother=true;
        $order=Order::find($input['order_id']);
        $payment=Payment::where('order_id',$id)->first();
        LogActivity::addToLog(User::find(Auth::id())->name.' mengubah pembayaran penyewaan '.Order::find($payment->order_id)->invoice);
        $notif_type="generated";


        if(Payment::where('order_id',$id)->count()>0){
            if ($request->hasFile('prove_phase_soft_copy')) {
                $checkanother=false;
                $user_image=$payment;
                $notif_type="paysoft";
                if($user_image->prove_phase_soft_copy!=null){
                    if(file_exists(public_path('images/thumb/prove_soft_copy/' . $user_image->prove_phase_soft_copy))){
                        unlink(public_path('images/thumb/prove_soft_copy/' . $user_image->prove_phase_soft_copy));
                    }
                    if(file_exists(public_path('images/full/prove_soft_copy/' . $user_image->prove_phase_soft_copy))){
                        unlink(public_path('images/full/prove_soft_copy/' . $user_image->prove_phase_soft_copy));
                    }
                    if(file_exists(public_path('images/scalled/prove_soft_copy/' . $user_image->prove_phase_soft_copy))){
                        unlink(public_path('images/scalled/prove_soft_copy/' . $user_image->prove_phase_soft_copy));
                    }
                }
                $thumb = public_path('images/thumb/prove_soft_copy');
                $full = public_path('images/full/prove_soft_copy');
                $scalled = public_path('images/scalled/prove_soft_copy');
                $image = $request->file('prove_phase_soft_copy');
                $name = str_random(5) . '.' . $image->getClientOriginalExtension();
                Image::make($image)->save($full . '/' . $name);
                Image::make($image)->resize('300', '300')->save($thumb . '/' . $name);
                Image::make($image)->resize('480', '640')->save($scalled . '/' . $name);
                Order::find($payment->order_id)->update(array('status' => 2 ));
                RequestRelation::whereIn('request_id',RequestCertificate::where('order_id',$payment->order_id)->select('id as request_id')->get())->where('status',1)->update(array('status' => 2 ));
                $input['prove_phase_soft_copy']= 'images/full/prove_soft_copy/'. $name;
                $input['phase_soft_copy']=1;
            }
            if ($request->hasFile('prove_phase_hard_copy')) {
                $checkanother=false;
                $user_image=$payment;
                if($user_image->prove_phase_hard_copy!=null){
                    if(file_exists(public_path('images/thumb/prove_hard_copy/' . $user_image->prove_phase_hard_copy))){
                        unlink(public_path('images/thumb/prove_hard_copy/' . $user_image->prove_phase_hard_copy));
                    }
                    if(file_exists(public_path('images/full/prove_hard_copy/' . $user_image->prove_phase_hard_copy))){
                        unlink(public_path('images/full/prove_hard_copy/' . $user_image->prove_phase_hard_copy));
                    }
                    if(file_exists(public_path('images/scalled/prove_hard_copy/' . $user_image->prove_phase_hard_copy))){
                        unlink(public_path('images/scalled/prove_hard_copy/' . $user_image->prove_phase_hard_copy));
                    }
                }
                $thumb = public_path('images/thumb/prove_hard_copy');
                $full = public_path('images/full/prove_hard_copy');
                $scalled = public_path('images/scalled/prove_hard_copy');
                $image = $request->file('prove_phase_hard_copy');
                $name = str_random(5) . '.' . $image->getClientOriginalExtension();
                Image::make($image)->save($full . '/' . $name);
                Image::make($image)->resize('300', '300')->save($thumb . '/' . $name);
                Image::make($image)->resize('480', '640')->save($scalled . '/' . $name);
                Order::find($payment->order_id)->update(array('status' => 6 ));
                RequestRelation::whereIn('request_id',RequestCertificate::where('order_id',$payment->order_id)->select('id as request_id')->get())->where('status',5)->update(array('status' => 6 ));
                $input['prove_phase_hard_copy']= 'images/full/prove_hard_copy/'. $name;
                $input['phase_hard_copy']=1;
                $notif_type="payhard";
            }

            if($checkanother){
                if(Order::find($payment->order_id)->status==0){
                    Order::find($payment->order_id)->update(array('status' => 1 ));
                    $rent=RequestRelation::whereIn('request_id',RequestCertificate::where('order_id',$payment->order_id)->select('id as request_id')->get())->where('status', 0)->get();
                    $rentx=RequestCertificate::allRequestsByCertificate()->whereIn('request_id',RequestCertificate::where('order_id',$input['order_id'])->select('id as request_id')->get())->where('request_certificates.status', 0)->get();
                    $payment->update($input);
                    $invoicesoft = array('order' => $order,'rents'=>$rentx,'payment'=>$payment);
                    Mail::send('backend.mail.mailinvoicesoft', $invoicesoft, function($message) use ($order,$rent)
                    {
                        $message->from('admin@skaskt.id')->to(User::find($order->id)->email)->subject('Mohon lakukan pembayaran penyewaan sertifikat soft copy untuk order '.$order->invoice);
                    });
                    foreach ($rent as $key => $value) {
                        $value->update(array('status' => 1));
                    } 
                }
                if(Order::find($payment->order_id)->status==4){
                    Order::find($payment->order_id)->update(array('status' => 5 ));
                    $rent=RequestRelation::whereIn('request_id',RequestCertificate::where('order_id',$payment->order_id)->select('id as request_id')->get())->where('status', 4)->get();
                    $rentx=RequestCertificate::allRequestsByCertificate()->whereIn('request_id',RequestCertificate::where('order_id',$input['order_id'])->select('id as request_id')->get())->where('rents.status', 4)->get();
                    $invoicehard = array('order' => $order,'rents'=>$rentx,'payment'=>$payment);
                    Mail::send('backend.mail.mailinvoicehard', $invoicehard, function($message) use ($order,$rent)
                    {
                        $message->from('admin@skaskt.id')->to(User::find($order->id)->email)->subject('Mohon lakukan pembayaran penyewaan sertifikat hard copy untuk order '.$order->invoice);
                    });
                    foreach ($rent as $key => $value) {
                        $value->update(array('status' => 5));
                    } 
                }
            }
        $paymentAPI= new PaymentAPIController();
        $notif;
        $notif['notif_type']=$notif_type;
        $notif['user_id']=$order->user_id;
        $notif['order_id']=$order->id;
        $notif['invoice']=$order->invoice;
        $paymentAPI->sendNotif($notif);

        }else{
            return response()->json(['success' => false, 'message' => 'Data pembayaran belum diproses admin']);
        }
        $payment->update($input);
        return response()->json(['success' => true, 'message' => 'Update data pembayaran penyewaan berhasil', 'type' => 'update']);
    }
    public function delete($id)
    {
        $payment= Payment::find($id);
        LogActivity::addToLog(User::find(Auth::id())->name.' menghapus pembayaran Order '. Order::find($payment->order_id)->invoice);
        $payment->delete();
        return response()->json(['success' => true, 'message' => 'Update data pembayaran berhasil', 'type' => 'delete']);
    }
    public function updateStatusPayment($id,$type)
    {
        $payment=Payment::find($id);
        switch ($type) {
            case "soft-copy-finish":
                $payment->update(array('phase_soft_copy' => 1));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status pembayaran soft copy Order '. Order::find($payment->order_id)->invoice. ' menjadi selesai');
                break;
            case "hard-copy-finish":
                $payment->update(array('phase_hard_copy' => 1));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status pembayaran soft copy Order '. Order::find($payment->order_id)->invoice. ' menjadi belum selesai');
                break;
            case "soft-copy-cancel":
                $payment->update(array('phase_soft_copy' => 0));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status pembayaran hard copy Order '. Order::find($payment->order_id)->invoice. ' menjadi selesai');
                break;
            case "hard-copy-cancel":
                $payment->update(array('phase_hard_copy' => 0));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status pembayaran hard copy Order '. Order::find($payment->order_id)->invoice. ' menjadi belum selesai');
                break;
            default:
                return false;
                break;
        }
        return true;
    }
    public function select($id)
    {
        $payment=Payment::find($id);
        $payment->order_detail=Order::find($payment->order_id);
        return response()->json(['success' => true, 'message' => 'Data penyewaan berhasil ditampilkan', 'data'=>$payment]);
    }
    public function selectByOrderID($id)
    {
        $payment=Payment::where('order_id',$id);
        if($payment->count()>0){
            $payment=$payment->first();
            $payment->order_detail=Order::find($payment->order_id);
            return response()->json(['success' => true, 'message' => 'Data penyewaan berhasil ditampilkan', 'data'=>$payment]);
        }else{
            return response()->json(['success' => false, 'message' => 'Data penyewaan gagal ditampilkan']);
        }
    }
    public function selectByRentID($id)
    {
        $payment=Payment::where('order_id',RequestCertificate::find($id)->order_id);
        if($payment->count()>0){
            $payment=$payment->first();
            $payment->order_detail=Order::find($payment->order_id);
            return response()->json(['success' => true, 'message' => 'Data penyewaan berhasil ditampilkan', 'data'=>$payment]);
        }else{
            return response()->json(['success' => false, 'message' => 'Data penyewaan gagal ditampilkan']);
        }
    }
}
