<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use DB;
use Image;
use Storage;
use Carbon\Carbon;
use LogActivity;
use App\User;
use App\User_Device;
use App\User_General_Info;
use App\Model\Rent\Cart;
use App\Model\Rent\Order;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\Model\Setting\Notification;
use App\Model\Certificate\Certificate;
use App\Model\Certificate\User_Certificate;
use App\Model\Rent\RequestCertificate;
use App\Model\Rent\RequestRelation;
use App\Model\Payment\Payment;
use FCM;use DateTime;

class RequestCertificateAPIController extends Controller
{

    public function sendNotif($var){

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);

        $title=""; $Body="";
        if($var['notif_type']=="soft_sended"){
            $title="Soft copy Sertifikat Dikirim";
            $Body="Soft copy sertifikat ".$var['name']." telah dikirim";
        }else if($var['notif_type']=="hard_sended"){
            $title="Hard copy Sertifikat Dikirim";
            $Body="Hard copy sertifikat ".$var['name']." telah dikirim";
        }else if($var['notif_type']=="expired"){
            $title="Sertifikat Kadaluarsa ";
            $Body="Sertifikat ".$var['name']." telah habis masa sewanya ";
        }else{
            $title="Sewa Sertifikat Dibatalkan";
            $Body="Sertifikat ".$var['name']." telah dibatalkan";
        }

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody( $Body)
                            ->setSound('default');
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' =>  $title,'body' =>$Body,'notif_type'=>'rent_detail','notification_data'=>$var['rent_id'],'notification_title'=>$var['serial_number']]);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();

        $data = $dataBuilder->build();
            $token=User_Device::where('user_id',$var['user_id'])->pluck('device_id')->toArray();
            if(User_Device::where('user_id',$var['user_id'])->count()>0){
            $username=User::find($request->input('user_id'))->name;
            $input['user_type']=$username;
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            //return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete(); 

            //return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify(); 

            //return Array - you should try to resend the message to the tokens in the array
            $downstreamResponse->tokensToRetry();

            // return Array (key:token, value:errror) - in production you should remove from your database the tokens
            $downstreamResponse->tokensWithError();

            }
        $var['body']=$Body;
        $var['title']=$title;
        $var['data_type']="rent_detail";
        $var['data_id']=$var['rent_id'];
        $var['user_type']=User::find($var['user_id'])->name;    
        $notification = Notification::create($var);

    }

   	public function allRequest()
    {
        $order='requests.created_at';
        $sort='DESC';
        $requestCertificate=RequestCertificate::allRequests()->orderby($order,$sort)->get();
        return response()->json(['success' => true, 'message' => 'Semua data sewa berhasil didapatkan', 'data' => $requestCertificate]);
    }
    public function allRequestUser($type,$page)
    {
        $now = Carbon::now('Asia/Jakarta');
        $rent=RequestCertificate::allRequests()->where('orders.user_id',Auth::id());
        switch ($type) {
            case "request":
                $rent=$rent->where('request_certificates.status',0);
                break;
            case "in_process":
                $rent=$rent->whereIn('request_certificates.status',[1,2,4,5]);
                break;
            case "available":
                $rent=$rent->whereIn('request_certificates.status',[3,4,5,6,7]);
                break;
            case "expired":
                $rent=$rent->where('request_certificates.status',9);
                break;
            default:
                break;
        }
        $order='requests.created_at';
        $sort='DESC';
        $rent=$rent->orderby($order,$sort)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Semua data sewa berhasil didapatkan', 'data' => $rent]);   
    }
    public function allRequestByStatus($status)
    {
        $order='requests.created_at';
        $sort='DESC';
        $requestCertificate=RequestCertificate::allRequestsByCertificate();
        $user=User::find(Auth::id());
        if($user->hasRole('user')){
            $id=Auth::id();
            $orders=$requestCertificate->where('orders.user_id',$id);
        }

        switch ($status) {
            case 'request':
                $requestCertificate=$requestCertificate->where('request_certificates.status',0)->orderby($order,$sort)->get();
                break;
            case 'waiting_for_payment':
                $requestCertificate=$requestCertificate->where('request_certificates.status',1)->orderby($order,$sort)->get();
                break;
            case 'process_soft_copy':
                $requestCertificate=$requestCertificate->where('request_certificates.status',2)->orderby($order,$sort)->get();
                break;
            case 'soft_copy_finish':
                $requestCertificate=$requestCertificate->where('request_certificates.status',3)->orderby($order,$sort)->get();
                break;
            case 'request_hard_copy':
                $requestCertificate=$requestCertificate->where('request_certificates.status',4)->orderby($order,$sort)->get();
                break;
            case 'waiting_for_payment_hard_copy':
                $requestCertificate=$requestCertificate->where('request_certificates.status',5)->orderby($order,$sort)->get();
                break;
            case 'process_hard_copy':
                $requestCertificate=$requestCertificate->where('request_certificates.status',6)->orderby($order,$sort)->get();
                break;
            case 'hard_copy_finish':
                $requestCertificate=$requestCertificate->where('request_certificates.status',7)->orderby($order,$sort)->get();
                break;
            case 'cancel':
                $requestCertificate=$requestCertificate->where('request_certificates.status',8)->orderby($order,$sort)->get();
                break;
            case 'expired':
                $requestCertificate=$requestCertificate->where('request_certificates.status',9)->orderby($order,$sort)->get();
                break;
            default:
                $requestCertificate=$requestCertificate->orderby($order,$sort)->get();
                break;
        }
        return response()->json(['success' => true, 'message' => 'Semua data sewa berhasil didapatkan', 'data' => $requestCertificate]);
    }
   	public function allRequestCertificates()
    {
        $order='requests.created_at';
        $sort='DESC';
        $requestCertificate=RequestCertificate::allRequestsCertificate()->orderby($order,$sort)->get();
        return response()->json(['success' => true, 'message' => 'Semua data sewa berhasil didapatkan', 'data' => $requestCertificate]);
    }


    public function updateStatusRent(Request $request, $type, $id)
    {
        $now = Carbon::now('Asia/Jakarta');
        $rent=RequestRelation::find($id);
        $order_id=RequestCertificate::find($rent->request_id)->order_id;
        $certificate=User_Certificate::where('order_id',$order_id)->where('certificate_id',$rent->certificate_id)->where('qualification',$rent->qualification)->first();
        switch ($type) {
            case "hard_sended":
                $notif_type="hard_sended";
                $rent->update(array('status' => 7));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi hardcopy diterima penyewa');
                $rentAPI= new RentAPIController();
                $notif;
                $notif['notif_type']=$notif_type;
                $notif['user_id']=Order::find(RequestCertificate::find($rent->request_id)->order_id)->user_id;
                $notif['rent_id']=$rent->id;
                $notif['name']=$certificate->name;
                $notif['serial_number']=$certificate->registration_number;
                $rentAPI->sendNotif($notif);
                $rent->update(array('status' => 8));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $certificate->registration_number. ' menjadi hard copy diterima user');
                break;
            case "cancel":
                $notif_type="cancel";
                $rent->update(array('status' => 8));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. Order::find($order_id)->invoice. " sertifikat ".Certificate::find($rent->certificate)->name.' menjadi dibatalkan');
                break;
            case "hard_payed":
                $rent->update(array('status' => 6));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $certificate->registration_number. ' menjadi hard copy dibayar');
                break;
            case "accept":
                $rent->update(array('status' => 1));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. Order::find($order_id)->invoice. " sertifikat ".Certificate::find($rent->certificate)->name. ' menjadi dikonfirm oleh admin');
                break;
            case "request":
                $rent->update(array('status' => 0));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. Order::find($order_id)->invoice. " sertifikat ".Certificate::find($rent->certificate)->name. ' menjadi pengajuan permohonan awal');
                break;
            case "soft_payed":
                $rent->update(array('status' => 2));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. Order::find($order_id)->invoice. " sertifikat ".Certificate::find($rent->certificate)->name. ' menjadi soft copy dibayar');
                break;
            case "soft_process":
                $input=$request->all();
                $rent->update(array('status' => 2));
                LogActivity::addToLog(User::find(Auth::id())->name.' update informasi penyewaan '. Order::find($order_id)->invoice. " sertifikat ".Certificate::find($rent->certificate)->name. " menjadi sedang diproses");
                break;
            case "soft_sended":
                $notif_type="soft_sended";
                $rent->update(array('status' => 3));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi soft copy dikirim');

                $rentAPI= new RentAPIController();
                $notif;
                $notif['notif_type']=$notif_type;
                $notif['user_id']=Order::find(RequestCertificate::find($rent->request_id)->order_id)->user_id;
                $notif['rent_id']=$rent->id;
                $notif['name']=$certificate->name;
                $notif['serial_number']=$certificate->registration_number;
                $rentAPI->sendNotif($notif);
                break;
            case "cancel_payment_soft":
                $rent->update(array('status' => 1));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. Order::find($order_id)->invoice. " sertifikat ".Certificate::find($rent->certificate)->name. ' menjadi soft copy belum dibayar');
                break;
            case "cancel_payment_hard":
                $rent->update(array('status' => 5));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. Order::find($order_id)->invoice. " sertifikat ".Certificate::find($rent->certificate)->name.' menjadi hard copy belum dibayar');
                break;
            default:
                return response()->json(['success' => false, 'message' => 'Update Status penyewaan gagal, Pilihan tidak ditemukan']);
                break;
        }

        return response()->json(['success' => true, 'message' => 'Update Status penyewaan berhasil']);
    }

    public function updateSendRequestHardCopy(Request $request, $id)
    {
        $input=$request->all();
        $order=Order::find($id)->update(array('status' => 7));
        $now = Carbon::now('Asia/Jakarta');
        $rents=RequestRelation::whereIn('request_id',RequestCertificate::where('order_id',$id)->select('id as request_id')->get())->whereIn('status',[5])->where('status',6)->update(array('status' => 7));
        $certificates=User_Certificate::where('order_id',$id)->get();
           $input=$request->all();
        foreach ($certificates as $key => $certificate) {
                if($request->has('pick_up_by')==false){
                    $input['pick_up_by']=User::find(Order::find($id)->user_id)->name;
                }
                if($request->has('pick_up_date')==false){
                    $input['pick_up_date']=$now;
                }else{
                    $input['pick_up_date'] = Carbon::createFromFormat('m/d/Y', $input['pick_up_date'])->toDateTimeString();
                }
                if($request->has('exact_return_date')==false){
                    $input['exact_return_date']=$now->addDays(Certificate::find($certificate->certificate_id)->period_of_validity);
                }else{
                    $input['exact_return_date'] =  Carbon::createFromFormat('m/d/Y', $input['exact_return_date'])->toDateTimeString();
                }
                $certificate->update($input);
                $certificate->update(array('status' => 3));
                LogActivity::addToLog(User::find(Auth::id())->name.' update status penyewaan '. $rent->serial_number. ' menjadi hardcopy diterima penyewa');
        }
        return response()->json(['success' => true, 'message' => 'Update Status penyewaan berhasil']);
    }

    public function allCertificateByRequest()
    {
    	# code...
    }
    public function allUserByRequest()
    {
    	# code...
    }
}
