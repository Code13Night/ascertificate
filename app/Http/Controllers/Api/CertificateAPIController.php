<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use DB;
use Image;
use Storage;
use Hash;
use App\User;
use Carbon\Carbon;
use App\Model\Certificate\Certificate;
use App\Model\Certificate\User_Certificate;
use App\Model\Certificate\Certificate_Type;
use App\Model\Certificate\Certificate_Education_Level;
use App\Model\Certificate\Certificate_Study_Program;
use App\Model\Rent\Rent;
use App\Model\Rent\Order;
use App\Model\General_Info\Company;
use App\Model\General_Info\Study_Program;
use App\Model\General_Info\Profession;
use App\Model\General_Info\Education_Level;
use LogActivity;

class CertificateAPIController extends Controller
{
    public function save(Request $request)
    {

        $this->validate($request, [
            'classification_code' => 'required',
            'name' => 'required',
            'type_id' => 'required'
        ]);

        $input = $request->all();

        $existing=Certificate::where('classification_code',$input['classification_code'])->count();
        if($existing>0){
            return response()->json(['success' => false, 'message' => 'Simpan sertifikat gagal']);    
        }
        if($input['time_spesification']=="year"){
            $input['period_of_validity']=$input['valid_time']*360;
        }else if($input['time_spesification']=="month"){
            $input['period_of_validity']=$input['valid_time']*30;
        }else{
            $input['period_of_validity']=$input['valid_time'];
        }
        if ($request->hasFile('image')) {
            $thumb = public_path('images/thumb/certificate');
            $full = public_path('images/full/certificate');
            $scalled = public_path('images/scalled/certificate');
            $image = $request->file('image');
            if($input['type_id']==1){
                $type_name="SKA";
            }else{
                $type_name="SKT";
            }
            $name =  $type_name. '_' .$input['classification_code'].'_' .$input['name']. '.' . $image->getClientOriginalExtension();
            $extension=strtolower($image->getClientOriginalExtension());
            $input['image_name'] = $name;
            $input['image_path'] = 'images/full/certificate/' . $name;
         
            if($extension=="jpg"||$extension=="png"||$extension=="jpeg"||$extension=="bmp"){
                $input['image_thumb'] = 'images/thumb/certificate/' . $name;
                Image::make($image)->save($full . '/' . $name);
                Image::make($image)->resize('300', '300')->save($thumb . '/' . $name);
                Image::make($image)->resize('480', '640')->save($scalled . '/' . $name);       
            }else{
                $request->file('image')->move($full,$name);
            }
         
        }
        $certificate = Certificate::create($input);

        if($request->has('education_levels')){
            if($request->input('education_levels')!=null){
                foreach ($request->input('education_levels') as $key => $value) {
                    $relation= new Certificate_Education_Level;
                    $relation->certificate_id=$certificate->id;
                    $relation->education_level_id=$value;
                    $relation->save();
                }
            }
        }
        if($request->has('study_programs')){
            if($request->input('study_programs')!=null){
                foreach ($request->input('study_programs') as $key => $value) {
                    $relation= new Certificate_Study_Program;
                    $relation->certificate_id=$certificate->id;
                    $relation->study_program_id=$value;
                    $relation->save();
                }
            }
        }

        LogActivity::addToLog(User::find(Auth::id())->name.' membuat klasifikasi sertifikat '. $certificate->classification_code." - ".$certificate->name);
        return response()->json(['success' => true, 'message' => 'Sertifikat ditambahkan', 'type' => 'create']);
    }
    public function update(Request $request,$id)
    {
        $input=$request->all();
        $certificate=Certificate::find($id);
        if($certificate->classification_code!=$input['classification_code']){ 
            $existing=Certificate::where('classification_code',$input['classification_code'])->count();
            if($existing>0){
                return response()->json(['success' => false, 'message' => 'Simpan sertifikat gagal']);    
            }    
        }
        LogActivity::addToLog(User::find(Auth::id())->name.' mengubah informasi klasifikasi sertifikat '. $certificate->classification_code." - ".$certificate->name);

        if($input['time_spesification']=="year"){
            $input['period_of_validity']=$input['valid_time']*360;
        }else if($input['time_spesification']=="month"){
            $input['period_of_validity']=$input['valid_time']*30;
        }else{
            $input['period_of_validity']=$input['valid_time'];
        }
        if ($request->hasFile('image')) {
                $user_image=$certificate;
                if($certificate->image_name!=null){
                    if(file_exists(public_path('images/thumb/certificate/' . $user_image->image_name))){
                        unlink(public_path('images/thumb/certificate/' . $user_image->image_name));
                    }
                    if(file_exists(public_path('images/full/certificate/' . $user_image->image_name))){
                        unlink(public_path('images/full/certificate/' . $user_image->image_name));
                    }
                    if(file_exists(public_path('images/scalled/certificate/' . $user_image->image_name))){
                        unlink(public_path('images/scalled/certificate/' . $user_image->image_name));
                    }
                }
            $thumb = public_path('images/thumb/certificate');
            $full = public_path('images/full/certificate');
            $scalled = public_path('images/scalled/certificate');
            $image = $request->file('image');
            if($request->has('type_id')){
                if($input['type_id']==1){
                    $type_name="SKA";
                }else{
                    $type_name="SKT";
                }  
            }else{
                if($certificate->type_id==1){
                    $type_name="SKA";
                }else{
                    $type_name="SKT";
                }  
            }
            $name =  $type_name. '_' .$input['classification_code'].'_' .$input['name']. '.' . $image->getClientOriginalExtension();
            $extension=strtolower($image->getClientOriginalExtension());
            $input['image_name'] = $name;
            $input['image_path'] = 'images/full/certificate/' . $name;
         
            if($extension=="jpg"||$extension=="png"||$extension=="jpeg"||$extension=="bmp"){
                $input['image_thumb'] = 'images/thumb/certificate/' . $name;
                Image::make($image)->save($full . '/' . $name);
                Image::make($image)->resize('300', '300')->save($thumb . '/' . $name);
                Image::make($image)->resize('480', '640')->save($scalled . '/' . $name);       
            }else{
                $request->file('image')->move($full,$name);
            }
        }
        $certificate->update($input);

        Certificate_Education_Level::where('certificate_id',$id)->delete();
        if($request->has('education_levels')){
            if($request->input('education_levels')!=null){
                foreach ($request->input('education_levels') as $key => $value) {
                    $relation= new Certificate_Education_Level;
                    $relation->certificate_id=$certificate->id;
                    $relation->education_level_id=$value;
                    $relation->save();
                }
            }
        }
        Certificate_Study_Program::where('certificate_id',$id)->delete();
        if($request->has('study_programs')){
            if($request->input('study_programs')!=null){
                foreach ($request->input('study_programs') as $key => $value) {
                    $relation= new Certificate_Study_Program;
                    $relation->certificate_id=$certificate->id;
                    $relation->study_program_id=$value;
                    $relation->save();
                }
            }
        }
       
        return response()->json(['success' => true, 'message' => 'Update Klasifikasi Sertifikat berhasil', 'type' => 'update']);
    }
    public function delete($id)
    {
        $certificate=Certificate::find($id);
        LogActivity::addToLog(User::find(Auth::id())->name.' menghapus sertifikat '. $certificate->classification_code." - ".$certificate->name);
        $value=$certificate->delete();
        return response()->json(['success' => $value, 'message' => 'Sertifikat dihapus ']);
    }
    public function allCertificatesWebFilterselect($type_id,$profession_id){
        $certificate=Certificate::select(DB::raw('CONCAT( classification_code," - ",name) as text'),'id');
        if($type_id!="all"){
            $certificate=$certificate->where('type_id',$type_id);
        }
        if($profession_id!="all"){
            $certificate=$certificate->where('profession_id',$profession_id);
        }
        $certificate=$certificate->get();
        return response()->json(['success' => true, 'message' => 'sertifikat berhasil didapatkan', 'data' => $certificate]);
        
    }
    public function allCertificatesWeb(){
            $order="certificates.created_at";
            $sort="DESC";
        $certificate=Certificate::allCertificates()->orderby($order,$sort)->get();
        return response()->json(['success' => true, 'message' => 'Semua sertifikat berhasil didapatkan', 'data' => $certificate]);
        
    }
    public function allCertificates($order="",$sort="",$page){
        if($order==""||$order==null){
            $order="certificates.created_at";
        }
        if($sort==""||$sort==null){
            $sort="DESC";
        }

        $certificate=Certificate::allCertificates()->orderby($order,$sort)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Semua sertifikat berhasil didapatkan', 'data' => $certificate]);
    }
    public function allAvailableCertificates($page){
        $order="certificates.created_at";
        $sort="DESC";
        $certificate=Certificate::allCertificates()->where('status',1)->orderby($order,$sort)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Semua sertifikat berhasil didapatkan', 'data' => $certificate]);
    }
    public function searchCertificate($query,$page){
        $order="certificates.created_at";
        $sort="DESC";
        $certificate=Certificate::searchCertificate($query)->where('status',1)->orderby($order,$sort)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Pencarian sertifikat berhasil didapatkan', 'data' => $certificate]);
    }
    public function allAvailableCertificatesOrder($page,$order=null,$sort=null){

        if($order==""||$order==null){
            $order="certificates.created_at";
        }
        if($sort==""||$sort==null){
            $sort="DESC";
        }
        if($order=="time"){
            $order="certificates.created_at";
        }
        if($order=="name"){
            $order="certificates.name";
        }
        if($order=="classification_code"){
            $order="certificates.classification_code";
        }
        if($order=="price"){
            $order="certificates.price_soft";
        }
        $certificate=Certificate::allCertificates()->where('status',1)->orderby($order,$sort)->paginate($page);
        
        return response()->json(['success' => true, 'message' => 'Semua sertifikat berhasil didapatkan', 'data' => $certificate]);
    }
    public function allCertificatesFilterBy($page,$filter="",$filtervalue="")
    {
        
        $order="certificates.created_at";
        $sort="DESC";
        if($filter=="profession"){
            $filter="profession_id";
        }else if($filter=="education_level"){
            $filter="education_level_id";
        }else if("study_program"){
            $filter="study_program_id";
        }else{
            return response()->json(['success' => false, 'message' => 'Semua sertifikat gagal didapatkan', 'detail' => "filter tidak terdaftar"]);
        }
        if($filter!="none"){
            $certificate=Certificate::allCertificates()->where('status',1)->where($filter,$filtervalue)->orderby($order,$sort)->paginate($page);
        }else{
            $certificate=Certificate::allCertificates()->where('status',1)->orderby($order,$sort)->paginate($page);
        }
        return response()->json(['success' => true, 'message' => 'Semua sertifikat berhasil didapatkan', 'data' => $certificate]);
    }
    public function allCertificatesFilterByOrder($page,$filter="",$filtervalue="",$orderby="",$sort="")
    {
        
        if($order==""||$order==null){
            $order="certificates.created_at";
        }
        if($sort==""||$sort==null){
            $order="DESC";
        }
        if($filter=="profession"){
            $filter="profession_id";
        }else if($filter=="education_level"){
            $filter="education_level_id";
        }else if("study_program"){
            $filter="study_program_id";
        }else{
            $filter="none";
        }
        if($filter!="none"){
            $certificate=Certificate::allCertificates()->where('status',1)->where($filter,$filtervalue)->orderby($order,$sort)->paginate($page);
        }else{
            $certificate=Certificate::allCertificates()->where('status',1)->orderby($order,$sort)->paginate($page);
        }
        return response()->json(['success' => true, 'message' => 'Semua sertifikat berhasil didapatkan', 'data' => $certificate]);
    }
    public function select($id)
    {
        $certificate=Certificate::find($id);
        if($certificate->profession_id!=null){
            $certificate->profession_name=Profession::find($certificate->profession_id)->name;
        }else{
            $certificate->profession_name="-";
        }
        $certificate_education_level_id=Certificate_Education_Level::where('certificate_id',$id)->select('education_level_id as id')->get();
        $certificate_education_levels=Education_Level::whereIn('id',$certificate_education_level_id)->select('id','name')->get();
        $certificate_study_program_id=Certificate_Study_Program::where('certificate_id',$id)->select('study_program_id as id')->get();
        $certificate_study_programs=Study_Program::whereIn('id',$certificate_study_program_id)->select('id','name')->get();
        $certificate->education_level=$certificate_education_levels;
        $certificate->study_program=$certificate_study_programs;
        if($certificate->type_id!=null){
            $certificate->type_name=Certificate_Type::find($certificate->type_id)->name;
        }else{
            $certificate->type_name="-";
        }
        return response()->json(['success' => true, 'message' => 'Sertifikat berhasil ditampilkan', 'data'=>$certificate]);
    }
    public function show($id)
    {
        $certificate=Certificate::find($id);
        if($certificate->profession_id!=null){
            $certificate->profession_name=Profession::find($certificate->profession_id)->name;
        }else{
            $certificate->profession_name="-";
        }
        $certificate_education_level_id=Certificate_Education_Level::where('certificate_id',$id)->select('education_level_id as id')->get();
        $certificate_education_levels=Education_Level::whereIn('id',$certificate_education_level_id)->select('id','name')->get();
        $certificate_study_program_id=Certificate_Study_Program::where('certificate_id',$id)->select('study_program_id as id')->get();
        $certificate_study_programs=Study_Program::whereIn('id',$certificate_study_program_id)->select('id','name')->get();
        $certificate->education_level=$certificate_education_levels;
        $certificate->study_program=$certificate_study_programs;
        if($certificate->type_id!=null){
            $certificate->type_name=Certificate_Type::find($certificate->type_id)->name;
        }else{
            $certificate->type_name="-";
        }
        return $certificate;
    }
    public function showbyCode($code)
    {
        $certificate=Certificate::where('classification_code',$code)->first();
        if($certificate->profession_id!=null){
            $certificate->profession_name=Profession::find($certificate->profession_id)->name;
        }else{
            $certificate->profession_name="-";
        }
        $certificate_education_level_id=Certificate_Education_Level::where('certificate_id',$certificate->id)->select('education_level_id as id')->get();
        $certificate_education_levels=Education_Level::whereIn('id',$certificate_education_level_id)->select('id','name')->get();
        $certificate_study_program_id=Certificate_Study_Program::where('certificate_id',$certificate->id)->select('study_program_id as id')->get();
        $certificate_study_programs=Study_Program::whereIn('id',$certificate_study_program_id)->select('id','name')->get();
        $certificate->education_level=$certificate_education_levels;
        $certificate->study_program=$certificate_study_programs;
        if($certificate->type_id!=null){
            $certificate->type_name=Certificate_Type::find($certificate->type_id)->name;
        }else{
            $certificate->type_name="-";
        }
        return $certificate;
    }
    public function showbyRegistrationNumber($reqistration_number)
    {
        $certificate=Certificate::where('classification_code',$code)->first();
        if($certificate->profession_id!=null){
            $certificate->profession_name=Profession::find($certificate->profession_id)->name;
        }else{
            $certificate->profession_name="-";
        }
        $certificate_education_level_id=Certificate_Education_Level::where('certificate_id',$certificate->id)->select('education_level_id as id')->get();
        $certificate_education_levels=Education_Level::whereIn('id',$certificate_education_level_id)->select('id','name')->get();
        $certificate_study_program_id=Certificate_Study_Program::where('certificate_id',$certificate->id)->select('study_program_id as id')->get();
        $certificate_study_programs=Study_Program::whereIn('id',$certificate_study_program_id)->select('id','name')->get();
        $certificate->education_level=$certificate_education_levels;
        $certificate->study_program=$certificate_study_programs;
        if($certificate->type_id!=null){
            $certificate->type_name=Certificate_Type::find($certificate->type_id)->name;
        }else{
            $certificate->type_name="-";
        }
        return $certificate;
    }
    public function updateCertificateStatus($id)
    {
        $certificate=Certificate::find($id);
        if($certificate->status==0){
            $certificate->update(array('status' => 1));
            LogActivity::addToLog(User::find(Auth::id())->name.' mengubah status sertifikat '. $certificate->classification_code." - ".$certificate->name.' menjadi tampil');
        }else{
            $certificate->update(array('status' => 0));
            LogActivity::addToLog(User::find(Auth::id())->name.' mengubah status sertifikat '. $certificate->classification_code." - ".$certificate->name.' menjadi sembunyi');
        }
        return response()->json(['success' => true, 'message' => 'Update Status Sertifikat berhasil']);
    }

    // user certificate
    public function saveUserCertificate(Request $request)
    {

        $this->validate($request, [
            'certificate_id' => 'required',
            'registration_number' => 'required|unique:user_certificates',
            'name' => 'required'
        ]);

        $now = Carbon::now('Asia/Jakarta');
        if($request->input('user_id')=='not_selected'){
            $input = $request->except(['user_id']);
        }else{
            $input = $request->all();
            $existing=User_Certificate::where('certificate_id',$input['certificate_id'])->where('user_id',$input['user_id'])->count();
            if($existing>0){
                return response()->json(['success' => false, 'message' => 'Simpan sertifikat gagal']);    
            }
        }
        $certificate=Certificate::find($input['certificate_id']);
        

        if($request->has('date_of_registration')==false){
            $input['date_of_registration']=$now;
        }else{
            $now=Carbon::createFromFormat('m/d/Y', $input['date_of_registration']);
            $input['date_of_registration']=$now;
        }
        if(!$request->has('period_of_validity')){
            $input['period_of_validity']=$certificate->period_of_validity;
        }
        if ($request->hasFile('file')) {
            $thumb = public_path('images/thumb/certificate');
            $full = public_path('images/full/certificate');
            $scalled = public_path('images/scalled/certificate');
            $image = $request->file('file');
            if($certificate->type_id==1){
                $type_name="SKA";
            }else{
                $type_name="SKT";
            }

            if($request->input('user_id')=='not_selected'){
            $name =  'not_assign_' .$input['registration_number'].'_' .$type_name. '_' .$input['certificate_id'].'_' .$input['name'].'_' .$input['qualification']. '.' . $image->getClientOriginalExtension();
            }else{
            $name =  $input['user_id'].'_' .$input['registration_number']. '_' .$type_name. '_' .$input['certificate_id'].'_' .$input['name'].'_' .$input['qualification']. '.' . $image->getClientOriginalExtension();
            }
            $extension=strtolower($image->getClientOriginalExtension());
            $input['file_name'] = $name;
            $input['file_path'] = 'images/full/certificate/' . $name;
         
            if($extension=="jpg"||$extension=="png"||$extension=="jpeg"||$extension=="bmp"){
                $input['file_thumb'] = 'images/thumb/certificate/' . $name;
                Image::make($image)->save($full . '/' . $name);
                Image::make($image)->resize('300', '300')->save($thumb . '/' . $name);
                Image::make($image)->resize('480', '640')->save($scalled . '/' . $name);       
            }else{
                $request->file('file')->move($full,$name);
            }

        }
        $user_certificate = User_Certificate::create($input);

        if(!($request->input('user_id')=='not_selected')){
            $order_id=Order::where('user_id',$input['user_id'])->where('status',2)->select('id as order_id')->get();
            $rent=Rent::whereIn('order_id',$order_id)->where('certificate_id',$input['certificate_id']);
            if($rent->count()>0){
                $rent=$rent->first();
                $rent->update(array('user_certificate_id' => $user_certificate->id ,'serial_number' => $user_certificate->registration_number,'date_of_issued'=>$user_certificate->date_of_registration,'qualification'=>$user_certificate->qualification,'valid_until'=>$now->addDays($user_certificate->period_of_validity) ));
            }
        }
        

        LogActivity::addToLog(User::find(Auth::id())->name.' membuat sertifikat '. $user_certificate->registration_number." - ".$user_certificate->name);
        return response()->json(['success' => true, 'message' => 'Sertifikat ditambahkan', 'type' => 'create']);
    }
    public function updateUserCertificate(Request $request,$id)
    {
        $input=$request->all();
        $now = Carbon::now('Asia/Jakarta');
        $user_certificate=User_Certificate::find($id);
        if($user_certificate->registration_number!=$input['registration_number']){ 
            $existing=User_Certificate::where('registration_number',$input['registration_number'])->count();
            if($existing>0){
                return response()->json(['success' => false, 'message' => 'Simpan sertifikat gagal']);    
            }    
        }
        LogActivity::addToLog(User::find(Auth::id())->name.' mengubah informasi sertifikat '.  $user_certificate->registration_number." - ".$user_certificate->name);

        $certificate=Certificate::find($input['certificate_id']);
        if($request->has('date_of_registration')==false){
            $input['date_of_registration']=$now;
        }else{
            $now=Carbon::createFromFormat('m/d/Y', $input['date_of_registration']);
            $input['date_of_registration']=$now;
        }
        if(!$request->has('period_of_validity')){
            $input['period_of_validity']=$certificate->period_of_validity;
        }
        if ($request->hasFile('file')) {
                $user_image=$user_certificate;
                if($user_image->file_name!=null){
                    if(file_exists(public_path('images/thumb/certificate/' . $user_image->file_name))){
                        unlink(public_path('images/thumb/certificate/' . $user_image->file_name));
                    }
                    if(file_exists(public_path('images/full/certificate/' . $user_image->file_name))){
                        unlink(public_path('images/full/certificate/' . $user_image->file_name));
                    }
                    if(file_exists(public_path('images/scalled/certificate/' . $user_image->file_name))){
                        unlink(public_path('images/scalled/certificate/' . $user_image->file_name));
                    }
                }
            $thumb = public_path('images/thumb/certificate');
            $full = public_path('images/full/certificate');
            $scalled = public_path('images/scalled/certificate');
            $image = $request->file('file');
            if($request->has('type_id')){
                if($input['type_id']==1){
                    $type_name="SKA";
                }else{
                    $type_name="SKT";
                }  
            }else{
                if($certificate->type_id==1){
                    $type_name="SKA";
                }else{
                    $type_name="SKT";
                }  
            }
            $name =  $input['user_id']. '_' .$type_name. '_' .$input['certificate_id'].'_' .$input['name'].'_' .$input['qualification']. '.' . $image->getClientOriginalExtension();
            $extension=strtolower($image->getClientOriginalExtension());
            $input['file_name'] = $name;
            $input['file_path'] = 'images/full/certificate/' . $name;
         
            if($extension=="jpg"||$extension=="png"||$extension=="jpeg"||$extension=="bmp"){
                $input['file_thumb'] = 'images/thumb/certificate/' . $name;
                Image::make($image)->save($full . '/' . $name);
                Image::make($image)->resize('300', '300')->save($thumb . '/' . $name);
                Image::make($image)->resize('480', '640')->save($scalled . '/' . $name);       
            }else{
                $request->file('file')->move($full,$name);
            }
        }
        $user_certificate->update($input);
       
        return response()->json(['success' => true, 'message' => 'Update Klasifikasi Sertifikat berhasil', 'type' => 'update']);
    }
    public function deleteUserCertificate($id)
    {
        $user_certificate=User_Certificate::find($id);
        LogActivity::addToLog(User::find(Auth::id())->name.' menghapus sertifikat '. $user_certificate->registration_number." - ".$user_certificate->name);
        $value=$user_certificate->delete();
        return response()->json(['success' => $value, 'message' => 'Sertifikat dihapus ']);
    }
    public function allUserCertificatesWeb(){
            $order="user_certificates.created_at";
            $sort="DESC";
        $certificate=User_Certificate::allCertificates()->orderby($order,$sort)->get();
        return response()->json(['success' => true, 'message' => 'Semua sertifikat berhasil didapatkan', 'data' => $certificate]);
        
    }
    public function allUserCertificatesWebStatus($status){
        $order="user_certificates.created_at";
        $sort="DESC";
        $certificate=User_Certificate::allCertificates();
        $user=User::find(Auth::id());
        if($user->hasRole('user')){
            $id=Auth::id();
            $certificate=$certificate->where('user_certificates.user_id',$id);
        }
        switch ($status) {
            case 'request':
                $certificate=$certificate->where('user_certificates.status',0)->orderby($order,$sort)->get();
                break;
            case 'waiting_for_payment':
                $certificate=$certificate->where('user_certificates.status',1)->orderby($order,$sort)->get();
                break;
            case 'process_soft_copy':
                $certificate=$certificate->where('user_certificates.status',2)->orderby($order,$sort)->get();
                break;
            case 'soft_copy_finish':
                $certificate=$certificate->where('user_certificates.status',3)->orderby($order,$sort)->get();
                break;
            case 'request_hard_copy':
                $certificate=$certificate->where('user_certificates.status',4)->orderby($order,$sort)->get();
                break;
            case 'waiting_for_payment_hard_copy':
                $certificate=$certificate->where('user_certificates.status',5)->orderby($order,$sort)->get();
                break;
            case 'process_hard_copy':
                $certificate=$certificate->where('user_certificates.status',6)->orderby($order,$sort)->get();
                break;
            case 'hard_copy_finish':
                $certificate=$certificate->where('user_certificates.status',7)->orderby($order,$sort)->get();
                break;
            case 'cancel':
                $certificate=$certificate->where('user_certificates.status',8)->orderby($order,$sort)->get();
                break;
            case 'expired':
                $certificate=$certificate->where('user_certificates.status',9)->orderby($order,$sort)->get();
                break;
            default:
                $certificate=$certificate->orderby($order,$sort)->get();
                break;
        }
        return response()->json(['success' => true, 'message' => 'Semua sertifikat berhasil didapatkan', 'data' => $certificate]);
        
    }
    public function selectUserCertificate($id)
    {
        $user_certificate=User_Certificate::allCertificates()->where('user_certificates.id',$id)->first();
        return response()->json(['success' => true, 'message' => 'Sertifikat berhasil ditampilkan', 'data'=>$user_certificate]);
    }

    //new mechanism
    public function searchUserCertificate($query)
    {
        $user_certificate=User_Certificate::allCertificates()
                            ->where('user_certificates.order_id',null)
                            ->orwhere('registration_number','like','%'.$query.'%')
                            ->orwhere('user_certificates.name','like','%'.$query.'%')
                            ->orwhere('serial_number','like','%'.$query.'%')
                            ->orwhere('certificates.classification_code','like','%'.$query.'%')
                            ->orwhere('certificates.name','like','%'.$query.'%')
                            ->orwhere('user_certificates.qualification','like','%'.$query.'%')
                            ->orwhere('certificate_types.name','like','%'.$query.'%')
                            ->orwhere('professions.name','like','%'.$query.'%')
                            ->get();
        return response()->json(['success' => true, 'message' => 'Sertifikat berhasil ditampilkan', 'data'=>$user_certificate]);
    }

    public function assignCertificate($order_id,$user_certificate_id)
    {

    }

}
