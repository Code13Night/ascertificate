<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use DB;
use Hash;
use App\User;
use Carbon\Carbon;
use App\Model\Certificate\Certificate;
use App\Model\Certificate\User_Certificate;
use App\Model\Certificate\Certificate_Type;
use App\Model\Certificate\Certificate_Education_Level;
use App\Model\Certificate\Certificate_Study_Program;
use App\Model\General_Info\Company;
use App\Model\General_Info\City;
use App\Model\General_Info\Study_Program;
use App\Model\General_Info\Profession;
use App\Model\General_Info\Education_Level;
// use App\Model\Rent\Rent;
use App\Model\Rent\Cart;
use App\Model\Rent\CartRelation;
use App\Model\Rent\RequestCertificate;
use App\Model\Rent\RequestRelation;
use App\Model\Rent\Order;
use LogActivity;

class CartAPIController extends Controller
{

   public function clean($string) {
       $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
       $string = str_replace('-', '', $string); // Replaces all spaces with hyphens.

       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
    public function addtoCart(Request $request)
    {
        $input=$request->all();
        $input['user_id']=Auth::id();
        if($input['budget_type']=='APBD II'){
            if(is_int($input['city'])){
                $input['city']=City::find($input['city'])->name;
            }
        }else if($input['budget_type']=='APBD I'){
            $input['city']="";
        }else{
            $input['city']="";
            $input['province']="";
        }
        $cartchecked=Cart::where('user_id',$input['user_id'])->select('id as cart_id')->get();
        $checked=CartRelation::whereIn('cart_id',$cartchecked)->where('certificate_id',$input['certificate_id'])->where('qualification',$input['qualification'])->count();
        if($checked>0){
            return response()->json(['success' => false, 'message' => 'Sertifikat sudah ada dicart ']);
        }
        $checked2=User_Certificate::where('user_id',$input['user_id'])->where('certificate_id',$input['certificate_id'])->where('qualification',$input['qualification'])->count();

        if($checked2>0){
            return response()->json(['success' => false, 'message' => 'Sertifikat sudah pernah dibuat']);
        }
        $order_check=Order::where('user_id',$input['user_id'])->select('id as order_id')->get();
        $request_check=RequestCertificate::whereIn('order_id',$order_check)->select('id as request_id')->get();
        $checked3=RequestRelation::whereIn('request_id',$request_check)->where('certificate_id',$input['certificate_id'])->where('qualification',$input['qualification'])->whereIn('status',[0,1])->count();
        if($checked3>0){
            return response()->json(['success' => false, 'message' => 'Sertifikat sedang diproses oleh admin']);
        }
        if($input['cart_id']==null||$input['cart_id']==""){
            $cart=Cart::create($input);
            $input['cart_id']=$cart->id;
        }else{
            $cart=Cart::find($input['cart_id']);
            $input['cart_id']=$input['cart_id'];
        }
        $cart_rel=CartRelation::create($input);
        LogActivity::addToLog(User::find(Auth::id())->name.' Memasukkan sertifikat '.Certificate::find($input['certificate_id'])->name.' kedalam cart');
        session(['availablecart' =>  true]);
        return response()->json(['success' => true, 'message' => 'Data Cart berhasil disimpan', 'data'=> $cart, 'data_id'=> $cart->id, 'request_id'=> $cart_rel->id]);
    }
    public function checkout(Request $request)
    {
        $cartAPI= new CartAPIController();
        $now = Carbon::now('Asia/Jakarta');
        $str=$cartAPI->clean($now);
        $input=json_decode($request->getContent(), true);
        $id=Auth::id();
        //check JSON input;
        if (is_array($input) && array_key_exists('cart', $input)) {   
            $cartval=$input['cart'];
        }else{
            $cartval=Cart::where('user_id',$id)->get();
            if(count($cartval)==0){
                return response()->json(['success' => false, 'message' => 'API order gagal dieksekusi cart user kosong']);
            }
        }
        $input['invoice']="INV".$str;
        $input['user_id']=Auth::id();
        $input['note']=$input['note'];
        $order=Order::create($input);
        foreach ($cartval as $key => $value) {
            $requestcertificate= new RequestCertificate();
            $cartnow=Cart::find($value['id']);
            $requestcertificate->order_id=$order->id;
            if(count($cartval)==$key+1){
                $order->update(array('budget_type' => $cartnow->budget_type, 'city' => $cartnow->city,'company'=>$cartnow->company,'province'=>$cartnow->province,'position'=>$cartnow->position));
            }
            
            $requestcertificate->profession_id=$cartnow->profession_id;
            $requestcertificate->study_program_id=$cartnow->study_program_id;
            $requestcertificate->education_level_id=$cartnow->education_level_id;
            $requestcertificate->graduate_year=$cartnow->graduate_year;
            $requestcertificate->save();
            $cartcertificate=CartRelation::where('cart_id',$value['id'])->get();
            foreach ($cartcertificate as $keyx => $valuex) {
                $requestrelation= new RequestRelation();
                $requestrelation->certificate_id=$valuex['certificate_id'];
                $requestrelation->qualification=$valuex['qualification'];
                $requestrelation->request_id=$requestcertificate->id;
                $requestrelation->save();
            }
            $cartnow->delete();
                    session(['budget_type'=>""]);
                    session(['province'=>""]);
                    session(['city'=>""]);
                    session(['company'=>""]);
                    session(['position'=>""]);
        }
        LogActivity::addToLog(User::find(Auth::id())->name.' checkout sertifikat, Order '.$input['invoice'].' Berhasil dibuat');
        return response()->json(['success' => true, 'message' => 'Data Cart berhasil checkout', 'data'=> $order]);
    }
    public function delete($id)
    {
        $cart=Cart::find($id);
        LogActivity::addToLog(User::find(Auth::id())->name.' menghapus cart ');
        $value=$cart->delete();

            if(Cart::where('user_id',Auth::id())->count()>0){
                session(['availablecart' =>  true]);
            }else{
                session(['availablecart' =>  false]);
            }
        return response()->json(['success' => $value, 'message' => 'Cart dihapus ']);
    }
    public function deleteRequest($id)
    {
        $cart=CartRelation::find($id);
        $cart_checked=CartRelation::where('cart_id',$cart->cart_id)->count();
        LogActivity::addToLog(User::find(Auth::id())->name.' menghapus request ');
        if($cart_checked==1){
            $value=Cart::find($cart->cart_id)->delete();
        }else{
            $value=$cart->delete();
        }
        
        return response()->json(['success' => $value, 'message' => 'Request dihapus ','count_request'=>$cart_checked]);
    }
    public function deletebycertificate($id)
    {
        $cart=Cart::where('certificate_id',$id)->first();
        LogActivity::addToLog(User::find(Auth::id())->name.' menghapus cart ');
        $value=$cart->delete();
        return response()->json(['success' => $value, 'message' => 'Cart dihapus ']);
    }
    public function deleteAllCartUser()
    {
        $cart=Cart::where('user_id',Auth::id());
        LogActivity::addToLog(User::find(Auth::id())->name.' Mengosongkan cart ');
        $value=$cart->delete();
        return response()->json(['success' => $value, 'message' => 'Cart dikosongkan ']);
    }
    public function allCartWeb(){
        
            if(Cart::where('user_id',Auth::id())->count()>0){
                session(['availablecart' =>  true]);
            }else{
                session(['availablecart' =>  false]);
            }
        $user_id=Auth::id();
        $cart=Cart::allCarts()->where('carts.user_id',$user_id)->get();
        return response()->json(['success' => true, 'message' => 'Semua Cart berhasil didapatkan', 'data' => $cart]);
        
    }
    public function allCartUser($page){
        $user_id=Auth::id();
        $cart=Cart::allCarts()->where('carts.user_id',$user_id)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Semua Cart berhasil didapatkan', 'data' => $cart]);
        
    }
}

