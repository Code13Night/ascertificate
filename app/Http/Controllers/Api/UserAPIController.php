<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Model\General_Info\Study_Program;
use App\Model\General_Info\Profession;
use App\Model\General_Info\Education_Level;
use App\Model\General_Info\Company;
use Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use LogActivity;
use App\User_General_Info;
use DB;
use Hash;
use Image;
use Storage;
use App\Role;
use App\User_Device;
use Carbon\Carbon;

class UserAPIController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }
    /*** User ***/
    public function save(Request $request)
    {
    	 $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password'
        ]);

        $input = $request->all();
        $existing=User::where('email',$input['email'])->count();
        if($existing>0){
            return response()->json(['success' => false, 'message' => 'Simpan User gagal']);    
        }
        $input['password'] = Hash::make($input['password']);
        $input['api_token'] = str_random(60);

        $user = User::create($input);
        if($request->input('roles')!=null){
            foreach ($request->input('roles') as $key => $value) {
                $user->attachRole($value);
            }
        }else{
            $user->attachRole(4);
        }
        LogActivity::addToLog(User::find(Auth::id())->name.' menambahkan User: '.$user->name);
        return response()->json(['success' => true, 'message' => 'User ditambahkan', 'type' => 'create', 'data' => $user]);
    }
    public function update(Request $request,$id)
    {
    	$this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));    
        }

        $user = User::find($id);
        if($user->email!=$input['email']){ 
            $existing=User::where('email',$input['email'])->count();
            if($existing>0){
                return response()->json(['success' => false, 'message' => 'Simpan User gagal']);    
            } 
        }
        $user->update($input);

        if($request->has('roles')){
            DB::table('role_user')->where('user_id',$id)->delete();
            foreach ($request->input('roles') as $key => $value) {
                $user->attachRole($value);
            } 
        }else{
            $user->attachRole(1);
        }

        LogActivity::addToLog(User::find(Auth::id())->name.' mengubah informasi  user: '.$user->name);
        return response()->json(['success' => true, 'message' => 'User diperbaruhi', 'type' => 'update']);
    }
    public function updateStatus($id)
    {
        $user=User::find($id);
        if($user->active==1){
            $user->update(array('active' => 0));
            LogActivity::addToLog(User::find(Auth::id())->name.' mengubah Status  user: '.$user->name. ' menjadi tidak aktif');
        }else{
            $user->update(array('active' => 1));
            LogActivity::addToLog(User::find(Auth::id())->name.' mengubah Status  user: '.$user->name. ' menjadi aktif');
        }
        return response()->json(['success' => true, 'message' => 'Data user  berhasil diupdate']);
    }
    public function checkUser(Request $request)
    {
        $input = $request->all();
        $checkuser=User::where('email',$input['email'])->get()->count();
        if($checkuser>0){
            return response()->json(['success' => false, 'message' => 'User telah terdaftar']);
        }else{
            return response()->json(['success' => true, 'message' => 'User tersedia']);
        }
    }
    public function updateProfile(Request $request)
    {
        $id=Auth::id();
        $input = $request->all();
        $checkuser=User::where('email',$input['email'])->get()->count();
        if(User::find($id)->email!=$input['email']){
            if($checkuser>0){
                return response()->json(['success' => false, 'message' => 'Email telah terdaftar']);
            }
        }
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));    
        }

        $user = User::find($id);
        $user->update($input);
        $input['user_id']=Auth::id();
        $general_info_user= User_General_Info::where('user_id',$id);

        if ($request->hasFile('image')) {
            if($general_info_user->count()>0){
                $user_image=$general_info_user->first();
                if($user_image->image_name!=null){
                    if(file_exists(public_path('images/thumb/profile/' . $user_image->image_name))){
                        unlink(public_path('images/thumb/profile/' . $user_image->image_name));
                    }
                    if(file_exists(public_path('images/full/profile/' . $user_image->image_name))){
                        unlink(public_path('images/full/profile/' . $user_image->image_name));
                    }
                    if(file_exists(public_path('images/scalled/profile/' . $user_image->image_name))){
                        unlink(public_path('images/scalled/profile/' . $user_image->image_name));
                    }
                }
            }
            $thumb = public_path('images/thumb/profile');
            $full = public_path('images/full/profile');
            $scalled = public_path('images/scalled/profile');
            $image = $request->file('image');
            $name = str_random(5) . '.' . $image->getClientOriginalExtension();
            $input['image_name'] = $name;
            $input['image_thumb'] = 'images/thumb/profile/' . $name;
            $input['image_path'] = 'images/full/profile/' . $name;
            Image::make($image)->save($full . '/' . $name);
            Image::make($image)->resize('300', '300')->save($thumb . '/' . $name);
            Image::make($image)->resize('480', '640')->save($scalled . '/' . $name);
        }
        if($general_info_user->count()>0){

            
            $general_info_user=$general_info_user->first();
            $checkuser=User_General_Info::where('id_card_number',$input['id_card_number'])->get()->count();
            if($general_info_user->id_card_number!=$input['id_card_number']){
                if($checkuser>0){
                    return response()->json(['success' => false, 'message' => 'No. KTP telah terdaftar oleh user lain']);
                }
            }    
            $general_info_user->update($input);
        }else{
            $general_info_user_new=User_General_Info::create($input);
        }
        LogActivity::addToLog($user->name."update profil");
        return response()->json(['success' => true, 'message' => 'Info User diperbaruhi', 'type' => 'update', 'data_image'=>$input['image_thumb']]);
    }
    public function delete($id)
    {
        $user=User::find($id);
        $username=$user->username;
        $user->delete();
        LogActivity::addToLog(User::find(Auth::id())->name.' menghapus hser: '.$user->name);
        return response()->json(['success' => true, 'message' => 'User dihapus', 'username' => $username]);
    }

    public function register(Request $request) {
        $input = $request->all();
        $checkifexist=User::where('email',$request->get('email'))->get();
        if(count($checkifexist)>0){
            return response()->json(['success' => false, 'message' => 'User sudah ada']);
        }
        $user=User::firstOrCreate(
                            ['email'=>$request->get('email')],
                            ['name'=> $request->get('name'),'password'=>Hash::make($request->get('password')),'api_token'=>str_random(60)]
                            );
        $user->attachRole(4);

        if($request->has('device_id')){
            User_Device::create(['user_id'=>$user->id,'device_id'=>$request->get('device_id')]); 
        }
        auth()->login($user);
        $id=$this->auth->user()->id;
        $data_token = DB::table('users')->where('id',$id)->first();
        LogActivity::addToLog($user->name.' bergabung menjadi user projectas');
        return response()->json(['success' => true, 'data'=>$user, 'token'=>$user->api_token ]);

    }
    public function login(Request $request) {
        $input = $request->all();
        $available_user=User::where('email',$request->input('email'));
            if($available_user->count()>0){
                $available_user=$available_user->first();
                if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
                    $id=$this->auth->user()->id;
                    $data_token = DB::table('users')->where('id',$id)->first();
                    if($request->has('device_id')){
                        User_Device::create(['user_id'=>$id,'device_id'=>$request->get('device_id')]); 
                    }
                    LogActivity::addToLog($available_user->name.' Berhasil login');
                        return response()->json(['success' => true, 'data'=>$data_token]);
                }else{
                    return response()->json(['success' => false, 'msg' => 'Password Anda salah']);
                }
            }else{
                return response()->json(['success' => false, 'msg' => 'Email Anda belum Terdaftar di sistem']);
            }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Checkconnection(Request $request)
    {
        $id=Auth::id();
        $checkdevice=User_Device::where('user_id',$id)->where('device_id',$request->input('device_id'))->get();
        if(count($checkdevice)>0){
            return response()->json(['success' => true, 'msg' => 'Koneksi Ke Device ini tersedia']);
        }else{
            return response()->json(['success' => false, 'msg' => 'Koneksi Ke device_id ini telah terputus']);
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyConnectiontoAllDevice()
    {
        $id=Auth::id();
        User_Device::where('user_id',$id)->delete();
        LogActivity::addToLog(User::find(Auth::id())->name.' Berhasil Logout');
        return response()->json(['success' => true, 'msg' => 'Koneksi Ke Device Lain Telah Diputus']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyConnectiontoAllDeviceExpectCurrent(Request $request)
    {
        $id=Auth::id();
        if($request->has('device_id')){
            LogActivity::addToLog(User::find(Auth::id())->name.' Berhasil Logout');
            User_Device::where('user_id',$id)->where('device_id','!=',$request->input('device_id'))->delete();
            return response()->json(['success' => true, 'msg' => 'Koneksi Ke Device Lain Telah Diputus']);    
        }else{
            return response()->json(['success' => false, 'msg' => 'Koneksi Ke Device Lain Gagal Diputus']);  
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyConnectiontoDevice(Request $request)
    {
        $id=Auth::id();
        if($request->has('device_id')){
            LogActivity::addToLog(User::find(Auth::id())->name.' Berhasil Logout');
            User_Device::where('user_id',$id)->where('device_id',$request->input('device_id'))->delete();
            return response()->json(['success' => true, 'msg' => 'Berhasil Logout']);
        }else{
            return response()->json(['success' => false, 'msg' => 'Gagal logout']);  
        }
    }
    public function allUsers()
    {
        $data = User::AllUsers()->get();
        return response()->json(['data' => $data]);
    }
    public function allUserAdmin()
    {
        if(User::find(Auth::id())->hasRole('superadmin')){
            $data = User::AllUsersByRoles([1,2,3])->get();
        }else if(User::find(Auth::id())->hasRole('admin')){
            $data = User::AllUsersByRoles([2,3])->get();
        }else{
            $data = User::AllUsersByRoles([3])->get();
        }
        return response()->json(['data' => $data]);
    }
    public function allUserCostumer()
    {
        $data = User::AllUsersByRoles([4])->get();
        return response()->json(['data' => $data]);
    }
    public function select($id)
    {
        $user=User::find($id);
        $user->general_info=User_General_Info::where('user_id',$id)->first();
        return response()->json(['success' => true, 'message' => 'Data user  berhasil didapatkan', 'data' => $user]);
    }
    public function selectWeb($id)
    {
        $user=User::find($id);
        $user->role=DB::table('role_user')->where('user_id',$id)->get();
        $user->general_info=User_General_Info::where('user_id',$id)->first();
        return response()->json(['success' => true, 'message' => 'Data user  berhasil didapatkan', 'data' => $user]);
    }
    public function getProfile()
    {
        $id=Auth::id();
        $user=User::find($id);
        $general_info=User_General_Info::where('user_id',Auth::id())->get();
        if(count($general_info)>0){
            $general_info= $general_info->first();
            if($general_info->profession_id==null||$general_info->profession_id=="")
            {
                $general_info->profession_name="-";
            }else{
                $general_info->profession_name=Profession::find($general_info->profession_id)->name;
            } 
            if($general_info->education_level_id==null||$general_info->education_level_id=="")
            {
                $general_info->education_level_name="-";
            }else{
                $general_info->education_level_name=Education_level::find($general_info->education_level_id)->name;
            }
            if($general_info->study_program_id==null||$general_info->study_program_id=="")
            {
                $general_info->study_program_name="-";
            }else{
                $general_info->study_program_name=Study_Program::find($general_info->study_program_id)->name;
            }
            if($general_info->company_id==null||$general_info->company_id=="")
            {
                $general_info->company_name="-";
            }else{
                $general_info->company_name=Company::find($general_info->company_id)->name;
            }
        }else{
            $general_info=null;
        }
        $user->general_info=$general_info;
        return response()->json(['success' => true, 'message' => 'Profile berhasil didapatkan', 'data' => $user]);
    }
}
