<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\General_Info\Company;
use App\Model\General_Info\Study_Program;
use App\Model\General_Info\City;
use App\Model\General_Info\Province;
use App\Model\General_Info\Profession;
use App\Model\General_Info\Education_Level;
use LogActivity;
use Auth;
use App\User;
use DB;

class GeneralInfoAPIController extends Controller
{

	
	/*** Profession ***/
    public function saveProfession($input)
    {
    	$profession= new Profession();
    	$profession->name=$input['profession_name'];
    	$profession->save();
        LogActivity::addToLog(User::find(Auth::id())->name.' menambahkan profesi ' .$profession->name );
        return $profession->id;
    }
    public function saveProfessionAPI(Request $request)
    {
        $input=$request->all();
        $existing=Profession::where('name',$input['name'])->count();
        if($existing>0){
            return response()->json(['success' => false, 'message' => 'Simpan profesi gagal']);    
        }
        $profession=Profession::create($input);
        LogActivity::addToLog(User::find(Auth::id())->name.' menambahkan profesi ' .$profession->name );
        return response()->json(['success' => true, 'message' => 'Simpan profesi berhasil']);
    }
    public function updateProfession(Request $request,$id)
    {
    	
        $input=$request->all();
        $profession=Profession::find($id);
        if($profession->name!=$input['name']){ 
            $existing=Profession::where('name',$input['name'])->count();
            if($existing>0){
                return response()->json(['success' => false, 'message' => 'Simpan profesi gagal']);    
            }   
        }
        LogActivity::addToLog(User::find(Auth::id())->name.' mengubah profesi ' .$profession->name );
        $profession->update($input);
        return response()->json(['success' => true, 'message' => 'Update profesi berhasil']);
    }
    public function deleteProfession($id)
    {
        $profession=Profession::find($id);
        LogActivity::addToLog(User::find(Auth::id())->name.' menghapus profesi ' .$profession->name );
        $value=$profession->delete();
        return response()->json(['success' => $value, 'message' => 'Profesi dihapus ']);
    }
    public function updateTotalProfession($id)
    {
    	$profession=Profession::find($value);
		$profession->update(array('total' => $profession->total+1));
		return $profession->total;			
    }
    public function allProfessions()
    {
    	$profession=Profession::orderBy("name","ASC")->get();
    	return response()->json(['success' => true, 'message' => 'Tampilkan semua profesi', 'data'=> $profession]);
    }

    public function allProfessionsPaginate($page)
    {
        $order="created_at";
        $sort="DESC";
        $profession=Profession::orderBy($order,$sort)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Tampilkan semua profesi', 'data'=> $profession]);
    }
    public function allProfessionsPaginateOrder($page,$order="",$sort="")
    {
        if($order=="time"){
            $order="created_at";
        }else if($order=="total"){
            $order="total";
        }else if($order=="name"){
            $order="total";
        }else{
            $order="created_at";
        }
        if($sort=""){
            $sort="ASC";
        }else{
            $sort=$sort;
        }
        $profession=Profession::orderBy($order,$sort)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Tampilkan semua profesi', 'data'=> $profession]);
    }

    public function selectedProfession($id)
    {
    	$profession=Profession::find($id);
		return response()->json(['success' => true, 'message' => 'Tampilkan profesi yang dipilih', 'data'=> $profession]);
    }

	/*** EducationLevel ***/
    public function saveEducationLevel($input)
    {
    	$educationlevel= new Education_Level();
    	$educationlevel->name=$input['education_level_name'];
    	$educationlevel->save();
        LogActivity::addToLog(User::find(Auth::id())->name.' menambahkan tingkat pendidikan ' .$educationlevel->name );
        return $educationlevel->id;
    }
    public function updateEducationLevel(Request $request,$id)
    {
        $input=$request->all();
        $educationlevel=Education_Level::find($id);
        LogActivity::addToLog(User::find(Auth::id())->name.' mengubah tingkat pendidikan ' .$educationlevel->name );
        $educationlevel->update($input);
        return response()->json(['success' => true, 'message' => 'Update jenjang pendidikan  berhasil']);
    }
    public function deleteEducationLevel($id)
    {
        $educationlevel=Education_Level::find($id);
        LogActivity::addToLog(User::find(Auth::id())->name.' menghapus tingkat pendidikan ' .$educationlevel->name );
        $value=$educationlevel->delete();
        return response()->json(['success' => $value, 'message' => 'Jenjang Pendidikan dihapus ']);
    }
    public function updateTotalEducationLevel($id)
    {
		$educationlevel=Education_Level::find($value);
		$educationlevel->update(array('total' => $educationlevel->total+1));
		return $educationlevel->total;			
    }
    public function allEducationLevels()
    {
    	$educationlevel=Education_Level::orderBy("id","ASC")->get();
    	return response()->json(['success' => true, 'message' => 'Tampilkan semua jenjang pendidikan', 'data'=> $educationlevel]);
    }
    public function allEducationLevelsPaginate($page)
    {
        $order="id";
        $sort="ASC";
        $educationlevel=Education_Level::orderBy($order,$sort)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Tampilkan semua jenjang pendidikan', 'data'=> $educationlevel]);
    }
    public function allEducationLevelsPaginateOrder($page,$order="",$sort="")
    {
        if($order=="time"){
            $order="created_at";
        }else if($order=="total"){
            $order="total";
        }else if($order=="name"){
            $order="total";
        }else{
            $order="created_at";
        }
        if($sort=""){
            $sort="ASC";
        }else{
            $sort=$sort;
        }
        $educationlevel=Education_Level::orderBy($order,$sort)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Tampilkan semua jenjang pendidikan', 'data'=> $educationlevel]);
    }
    public function selectedEducationLevel($id)
    {
    	$educationlevel=Education_Level::find($id);
    	return response()->json(['success'=>true, 'message'=>'Tampilkan Jenjang Pendidikan', 'data'=>$educationlevel]);
    }

    /*** EducationLevel ***/
    public function saveStudyProgram($input)
    {    
    	$studyprogram= new Study_Program();
    	$studyprogram->name=$input['study_program_name'];
    	$studyprogram->save();
        LogActivity::addToLog(User::find(Auth::id())->name.' menambahkan program studi ' .$studyprogram->name );
        return $studyprogram->id;
    }
    public function saveStudyProgramAPI(Request $request)
    {
        $input=$request->all();
        $existing=Study_Program::where('name',$input['name'])->count();
        if($existing>0){
            return response()->json(['success' => false, 'message' => 'Simpan program studi gagal']);    
        }
        $studyprogram=Study_Program::create($input);
        LogActivity::addToLog(User::find(Auth::id())->name.' menambahkan program studi ' .$studyprogram->name );
        return response()->json(['success' => true, 'message' => 'Simpan program studi berhasil']);
    }
    public function updateStudyProgram(Request $request,$id)
    {
        $input=$request->all();
        $studyprogram=Study_Program::find($id);
        if($studyprogram->name!=$input['name']){ 
            $existing=Study_Program::where('name',$input['name'])->count();
            if($existing>0){
                return response()->json(['success' => false, 'message' => 'Simpan program studi gagal']);    
            }
        }
        LogActivity::addToLog(User::find(Auth::id())->name.' mengubah program studi ' .$studyprogram->name );
        $studyprogram->update($input);
        return response()->json(['success' => true, 'message' => 'Update Program Studi  berhasil']);
    }
    public function deleteStudyProgram($id)
    {
        $studyprogram=Study_Program::find($id);
        LogActivity::addToLog(User::find(Auth::id())->name.' menghapus program studi ' .$studyprogram->name );
        $value=$studyprogram->delete();
        return response()->json(['success' => $value, 'message' => 'Program Studi dihapus ']);
    }
    public function updateTotalStudyProgram($id)
    {
    	$studyprogram=Study_Program::find($value);
		$studyprogram->update(array('total' => $studyprogram->total+1));
		return $studyprogram->total;			
    }
    public function allStudyPrograms()
    {
    	$order="name";
    	$sort="ASC";
    	$studyprogram=Study_Program::orderBy($order,$sort)->get();
    	return response()->json(['success' => true, 'message' => 'Tampilkan semua Program Studi', 'data'=> $studyprogram]);
    }
    public function allStudyProgramsPaginate($page)
    {
            $order="name";
            $sort="ASC";
        $studyprogram=Study_Program::orderBy($order,$sort)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Tampilkan semua Program Studi', 'data'=> $studyprogram]);
    }
    public function allStudyProgramsPaginateOrder($page,$order="",$sort="")
    {
        if($order=="time"){
            $order="created_at";
        }else if($order=="total"){
            $order="total";
        }else if($order=="name"){
            $order="total";
        }else{
            $order="name";
        }
        if($sort=""){
            $sort="ASC";
        }else{
            $sort=$sort;
        }
        $studyprogram=Study_Program::orderBy($order,$sort)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Tampilkan semua Program Studi', 'data'=> $studyprogram]);
    }
    public function selectedStudyProgram($id)
    {
    	$studyprogram=Study_Program::find($id);
    	return response()->json(['success'=>true, 'message'=>'Tampilkan program studi', 'data'=>$studyprogram]);
    }


    /*** EducationLevel ***/
    public function saveCompany($input)
    {
    	$company= new Company();
    	$company->name=$input['company_name'];
    	$company->save();
        LogActivity::addToLog(User::find(Auth::id())->name.' menambahkan perusahaan ' .$company->name );
        return $company->id;
    }
    public function saveCompanyAPI(Request $request)
    {
        $input=$request->all();
        $existing=Company::where('name',$input['name'])->count();
        if($existing>0){
            return response()->json(['success' => false, 'message' => 'Simpan Perusahaan gagal']);    
        }
        $company=Company::create($input);
        LogActivity::addToLog(User::find(Auth::id())->name.' menambahkan perusahaan ' .$company->name );
        return response()->json(['success' => true, 'message' => 'Simpan Perusahaan berhasil']);
    }
    public function updateCompany(Request $request,$id)
    {
        $input=$request->all();
		$company=Company::find($id);
        if($company->name!=$input['name']){ 
            $existing=Company::where('name',$input['name'])->count();
            if($existing>0){
                return response()->json(['success' => false, 'message' => 'Simpan Perusahaan gagal']);    
            } 
        }
        LogActivity::addToLog(User::find(Auth::id())->name.' mengubah perusahaan ' .$company->name );
        $company->update($input);
        return response()->json(['success' => true, 'message' => 'Update Perusahaan berhasil']);
    }
    public function deleteCompany($id)
    {
        $company=Company::find($id);
        LogActivity::addToLog(User::find(Auth::id())->name.' menghapus perusahaan ' .$company->name );
        $value=$company->delete();
        return response()->json(['success' => $value, 'message' => 'Perusahaan dihapus ']);
    }
    public function updateTotalCompany($id)
    {
    	$company=Company::find($value);
		$company->update(array('total' => $studyprogram->total+1));
		return $company->total;			
    }
    public function allCompanies()
    {
        $order="created_at";
    	$sort="DESC";
    	
    	$company=Company::orderBy($order,$sort)->get();
    	return response()->json(['success' => true, 'message' => 'Tampilkan semua Perusahaan', 'data'=> $company]);
    }
    public function allCompaniesPaginate($page)
    {
        $order="created_at";
        $sort="DESC";
        $company=Company::orderBy($order,$sort)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Tampilkan semua Perusahaan', 'data'=> $company]);
    }
    public function allCompaniesPaginateOrder($page,$order="",$sort="")
    {
        if($order=="time"){
            $order="created_at";
        }else if($order=="total"){
            $order="total";
        }else if($order=="name"){
            $order="total";
        }else{
            $order="name";
        }
        if($sort=""){
            $sort="ASC";
        }else{
            $sort=$sort;
        }
        $company=Company::orderBy($order,$sort)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Tampilkan semua Perusahaan', 'data'=> $company]);
    }
    public function selectedCompany($id)
    {
		$company=Company::find($id);
    	return response()->json(['success'=>true, 'message'=>'Tampilkan Perusahaan', 'data'=>$company]);
    }
    public function allCityWebFilter($province_id){
        $province_id=Province::where('name',$province_id);
        if($province_id->count()>0){
            $province_id=$province_id->first()->id;
            $certificate=City::select(DB::raw('name as text'),'id');
            $certificate=$certificate->where('province_id',$province_id);
            $certificate=$certificate->get();
            return response()->json(['success' => true, 'message' => 'sertifikat berhasil didapatkan', 'data' => $certificate]);
        }else{
            return response()->json(['success' => true, 'message' => 'sertifikat berhasil didapatkan', 'data' => null]);
        }
        
    }

    public function getAllProvinces()
    {
        $province=Province::get();
        return response()->json(['success'=> true, 'data'=>$province]);
    }

    public function getSelectedProvince($id)
    {
        $province=City::where('province_id',$id)->get();
        return response()->json(['success'=> true, 'data'=>$province]);
    }

    public function getAllCities()
    {
        $city=City::get();
        return response()->json(['success'=> true, 'data'=>$city]);
    }

}
