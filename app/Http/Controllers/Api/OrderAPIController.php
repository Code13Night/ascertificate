<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\RentAPIController;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use DB;
use Hash;
use App\User;
use App\User_Device;
use App\User_General_Info;
use Carbon\Carbon;
use App\Model\Certificate\Certificate;
use App\Model\Certificate\User_Certificate;
use App\Model\Certificate\Certificate_Type;
use App\Model\Certificate\Certificate_Education_Level;
use App\Model\Certificate\Certificate_Study_Program;
use App\Model\General_Info\Company;
use App\Model\General_Info\Study_Program;
use App\Model\General_Info\Profession;
use App\Model\General_Info\Education_Level;
use App\Model\Payment\Payment;
use App\Model\Rent\Rent;
use App\Model\Rent\Cart;
use App\Model\Rent\Order;
use LogActivity;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\Model\Rent\RequestCertificate;
use App\Model\Rent\RequestRelation;
use FCM;
use Illuminate\Support\Facades\Mail;
use App\Model\Setting\Notification;

class OrderAPIController extends Controller
{
    public function testsend()
    {
        $data = array('name'=>"Virat Gandhi");
        Mail::send('backend.mail.mailinvoicehard', $data, function($message)
                    {
                        $message->from('admin@skaskt.id')->to('vianendra@gmail.com')->subject('test email sertifikat')->attach('http://projectascom-under.cloud.revoluz.io/images/full/not-found.jpg', [
                                'as' => 'notfound.jpg',
                                'mime' => 'image/jpeg',
                            ]);
                    });
    }
    public function sendNotif($var){

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);

        $title=""; $Body="";
        if($var['notif_type']=="generated"){
            $title="Order Diterima";
            $Body="Order ".$var['invoice']." telah diterima oleh admin";
        }else if($var['notif_type']=="paysoft"){
            $title="Pembayaran Softcopy";
            $Body="Pembayaran Softcopy Order ".$var['invoice']." telah diverifikasi oleh admin ";
        }else if($var['notif_type']=="payhard"){
            $title="Pembayaran Hardcopy";
            $Body="Pembayaran Hardcopy Order ".$var['invoice']." telah diverifikasi oleh admin ";
        }else if($var['notif_type']=="softsend"){
            $title="Softcopy dikirim";
            $Body="Sertifikat Soft copy untuk order".$var['invoice']." telah telah dikirim ";
        }else{
            $title="Order Dibatalkan";
            $Body="Order ".$var['invoice']." telah dibatalkan oleh admin";
        }

        $notificationBuilder = new PayloadNotificationBuilder( $title);
        $notificationBuilder->setBody( $Body)
                            ->setSound('default');
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' =>  $title,'body' =>$Body,'notif_type'=>'order_detail','notification_data'=>$var['order_id'],'notification_title'=>$var['invoice']]);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();

        $data = $dataBuilder->build();
            $token=User_Device::where('user_id',$var['user_id'])->pluck('device_id')->toArray();
            if(User_Device::where('user_id',$var['user_id'])->count()>0){
            $username=User::find($request->input('user_id'))->name;
            $input['user_type']=$username;
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            //return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete(); 

            //return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify(); 

            //return Array - you should try to resend the message to the tokens in the array
            $downstreamResponse->tokensToRetry();

            // return Array (key:token, value:errror) - in production you should remove from your database the tokens
            $downstreamResponse->tokensWithError();

            }
        $var['body']=$Body;
        $var['title']=$title;
        $var['data_type']="order_detail";
        $var['data_id']=$var['order_id'];
        $var['user_type']=User::find($var['user_id'])->name;    
        $notification = Notification::create($var);

        
        
    }
    public function sendSoftCopyByOrder($id)
    {
        $order=Order::find($id);
        $order->update(array('status' => 3));
        $order->user_name=User::find($order->user_id)->name;
        $rents=RequestCertificate::allRequestsByCertificate()->where('order_id',$id)->where('request_certificates.status',2);
        $certificates=User_Certificate::where('order_id',$id)->get();
        if($rents->count()>0){
            RequestCertificate::allRequestsByCertificate()->where('order_id',$id)->update(array('request_certificates.status' => 3));
            $rents=$rents->get();
            $sendsoft = array('order' => $order,'rents'=>$rents,'certificates'=>$certificates);
            Mail::send('backend.mail.mailsendsoft', $sendsoft, function($message) use ( $rents,$order,$certificates)
                    {
                        $message->from(User::find(Auth::id())->email);
                        $message->to(User::find($order->user_id)->email);
                        $message->subject('Soft copy sertifikat untuk invoice #'.$order->invoice);
                        if(count($certificates)>0){
                            foreach ($certificates as $key => $certificate) {
                                if(!($certificate->file_name==""||$certificate->file_name==null)){
                                    $message->attach($certificate->file_path);
                                }
                            }
                        }
                    });
            $notif_type ="softsend";
            $orderAPI= new OrderAPIController();
            $notif;
            $notif['notif_type']=$notif_type;
            $notif['user_id']=$order->user_id;
            $notif['order_id']=$order->id;
            $notif['invoice']=$order->invoice;
            $orderAPI->sendNotif($notif);
            return response()->json(['success' => true, 'message' => 'Soft copy berhasil dikirim']);  
        }else{
            return response()->json(['success' => false, 'message' => 'Soft copy gagal dikirim, tidak ada data sewa']);  
        }
        
    }
    public function allOrderWeb(){
        $order=Order::get();
        return response()->json(['success' => true, 'message' => 'Semua Order berhasil didapatkan', 'data' => $order]);
        
    }

    public function allOrderWebByStatus($status){

        $order='orders.updated_at';
        $sort='DESC';
        $orders=Order::allOrders();
                $user=User::find(Auth::id());
        if($user->hasRole('user')){
            $id=Auth::id();
            $orders=$orders->where('user_id',$id);
        }

        switch ($status) {
            case 'request':
                $order=$orders->where('orders.status',0)->orderby($order,$sort)->get();
                break;
            case 'waiting_for_payment':
                $order=$orders->where('orders.status',1)->orderby($order,$sort)->get();
                break;
            case 'process_soft_copy':
                $order=$orders->where('orders.status',2)->orderby($order,$sort)->get();
                break;
            case 'soft_copy_finish':
                $order=$orders->where('orders.status',3)->orderby($order,$sort)->get();
                break;
            case 'request_hard_copy':
                $order=$orders->where('orders.status',4)->orderby($order,$sort)->get();
                break;
            case 'waiting_for_payment_hard_copy':
                $order=$orders->where('orders.status',5)->orderby($order,$sort)->get();
                break;
            case 'process_hard_copy':
                $order=$orders->where('orders.status',6)->orderby($order,$sort)->get();
                break;
            case 'hard_copy_finish':
                $order=$orders->where('orders.status',7)->orderby($order,$sort)->get();
                break;
            case 'cancel':
                $order=$orders->where('orders.status',8)->orderby($order,$sort)->get();
                break;
            case 'expired':
                $order=$orders->where('orders.status',9)->orderby($order,$sort)->get();
                break;
            default:
                $order=$orders->orderby($order,$sort)->get();
                break;
        }
        return response()->json(['success' => true, 'message' => 'Semua Order berhasil didapatkan', 'data' => $order]);
        
    }
    public function allOrderUser($page){
        $user_id=Auth::id();
        $order=Order::where('orders.user_id',$user_id)->paginate($page);
        return response()->json(['success' => true, 'message' => 'Semua Order berhasil didapatkan', 'data' => $order]);
        
    }
    public function show($id){
        $user_id=Auth::id();
        $order=Order::find($id);
        $order->user_name=User::find($order->user_id)->name;
        $order->rent_detail=Rent::allRents()->where('rents.order_id',$id)->get();
        $order->payment_detail=Payment::where('order_id',$id)->get();
        $order->sumRent=23;
        foreach ($order->rent_detail as $key => $value) {
            if($value->status==0||$value->status==1){
                $order->sumRent=$order->sumRent+$value->certificate_price_soft;
            }else if($order->status==4||$order->status==5){
                $order->sumRent=$order->sumRent+$value->certificate_price_hard;
            }
        }
        if($order->status==8||$order->status==9){
            $order->sumRent=0;
        }
       
        $order->sumRent=23;
        $order->certificates=User_Certificate::allCertificates()->where('order_id',$order->id)->get();
        return $order;
    }
    public function showCode($code){
        $user_id=Auth::id();
        $order=Order::where('invoice',$code)->first();
        $order->request_detail=RequestCertificate::allRequestsByCertificate()->where('requests.order_id',$order->id)->get();
        $order->payment_detail=Payment::where('order_id',$order->id)->get(); 
        $order->certificates=User_Certificate::allCertificates()->where('order_id',$order->id)->get();
        return $order;
        
    }
    public function selectedOrder($id){
        $user_id=Auth::id();
        $order=Order::find($id);
        $order->rent_detail=RequestCertificate::allRequestsByCertificate()->where('order_id',$id)->get();
        $order->payment_detail=Payment::where('order_id',$id)->get();
        return response()->json(['success' => true, 'message' => 'Semua Order berhasil didapatkan', 'data' => $order]);
    }
    public function cancelOrder($id)
    {
        $order=Order::find($id);
        if($order->status==0||$order->status==1||$order->status==2){
            LogActivity::addToLog(User::find(Auth::id())->name.' membatalkan order '.$order->invoice);
            
        $order->update(array('status' => 8));
        $notif_type ="cancelorder";
        $orderAPI= new OrderAPIController();
        $notif;
        $notif['notif_type']=$notif_type;
        $notif['user_id']=$order->user_id;
        $notif['order_id']=$order->id;
        $notif['invoice']=$order->invoice;
        $orderAPI->sendNotif($notif);
        
            return response()->json(['success' => true, 'message' => 'Order dibatalkan ']);
        }else{
            if($order->status==3||$order->status==4||$order->status==5||$order->status==6||$order->status==7){
                return response()->json(['success' => false, 'message' => 'Order gagal dibatalkan, Sertifikat masih berlaku ']);
            }else if($order->status==7){
                return response()->json(['success' => false, 'message' => 'Order gagal dibatalkan, Sertifikat sudah tidak berlaku ']);
            }
            else{
                return response()->json(['success' => false, 'message' => 'Order gagal dibatalkan, Order sudah dibatalkan ']);
            }
        }
    }
    public function delete($id)
    {
        $order=Order::find($id);
        if($order->status==0||$order->status==1||$order->status==8||$order->status==9){
            LogActivity::addToLog(User::find(Auth::id())->name.' menghapus order '.$order->invoice);
            $value=$order->delete();
            return response()->json(['success' => $value, 'message' => 'Order dihapus ']);
        }else{
            if($order->status==3||$order->status==4||$order->status==5||$order->status==6||$order->status==7){
                return response()->json(['success' => false, 'message' => 'Order gagal dihapus, Sertifikat masih berlaku ']);
            }
        }
    }

    public function updateStatusOrder(Request $request, $type, $id)
    {
        $order=Order::find($id);
        if($request->has('rent_ids')){
            $rents=Rent::whereIn('order_id',$request->input('rent_ids'))->get();
        }else{
            $rents=Rent::where('order_id',$id)->get();
        }
        $rentAPI= new RentAPIController();
        foreach ($rents as $key => $rent) {
            $rentAPI->updateStatusRentviaOrder($type,$id);
        }
    }

}

