<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Hash;
use Image;
use Storage;
use Carbon\Carbon;
use App\Model\Message\Message;
use App\Model\Message\Message_Detail;
use LogActivity;
use App\User;
use App\User_Device;


class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Auth::check()){
            return view('backend.messages.index');
        }else{
            return redirect('/login');
        } 
    }

	public function update(Request $request, $id)
    {
        $input=$request->all();
        $input['is_read']=1;
        Message_Detail::find($id)->update($input);
        $message_id=Message_Detail::find($id)->message_id;
        Message::find($message_id)->update($input);
        return response()->json(['success' => true, 'message' => 'Pesan Telah Dibaca']);
    }
    public function destroyConversation($id){
		Message::find($id)->delete();
        return response()->json(['success' => true, 'message' => 'Percakapan Anda Telah terhapus']);
    }
    public function destroy($id){
		Message_Detail::find($id)->delete();
        return response()->json(['success' => true, 'message' => 'Pesan Anda Telah terhapus']);
    }
}
