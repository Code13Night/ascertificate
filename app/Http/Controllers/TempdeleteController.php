<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use DB;
use Hash;
use Image;
use Storage;
use App\Temp_Delete;

class TempdeleteController extends Controller
{
    public function savetempdelete(Request $request)
    {
        $input=$request->all();
        Temp_Delete::create($input);
        return response()->json(['success' => true]);
    }
    public function alltempdelete()
    {
        $temp_delete=Temp_Delete::get();
        return response()->json(['success' => true,'data'=>$temp_delete]);
    }
    public function filename(Request $request)
    {

        $input=$request->all();
        $temp_delete=Temp_Delete::where('file_path',$input['file_path'])->count();
        if($temp_delete>0){
            $datax=Temp_Delete::where('file_path',$input['file_path'])->first();
            return "payment success ".$datax->user;
        }else{
            $datax="";
            return "payment fail, ID not match  ";
        }
    }
}
