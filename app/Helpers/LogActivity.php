<?php


namespace App\Helpers;
use Request;
use Auth;
use App\Model\Setting\Log;


class LogActivity
{


    public static function addToLog($subject)
    {
        $log = [];
        $log['subject'] = $subject;
        $log['url'] = Request::fullUrl();
        $log['method'] = Request::method();
        $log['ip'] = Request::ip();
        $log['agent'] = Request::header('user-agent');
        $log['user_id'] = Auth::check() ? Auth::id() : null;
        Log::create($log);
    }


    public static function logActivityLists()
    {
    return Log::latest()->get();
    }


}