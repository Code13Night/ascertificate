<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temp_Delete extends Model
{
    protected $table = 'temp_delete';
    public $fillable = ['file_path','user'];
    public $timestamps = false;
}
